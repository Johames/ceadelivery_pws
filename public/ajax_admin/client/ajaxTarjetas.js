
function tarjetas() {

    listar_tarjetas();

}


function listar_tarjetas() {

    $('#tabla_tarjetas').html('<tr><td class="text-center" colspan="4"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></td></tr>');

    $.get("/cliente/tarjetas/listar", function (data, status, jqXHR) {

        $('#tabla_tarjetas').html(data);

    });
}

function eliminar_tarjetas(id) {
    crud_eliminar('/cliente/tarjetas/eliminar/' + id , function(){ listar_tarjetas(); }, function(){ console.log('Eror') });
}



tarjetas();
