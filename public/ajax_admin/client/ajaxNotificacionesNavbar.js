function init(){

    notificaciones_navbar();

}

function notificaciones_navbar(){
    $.get("/cliente/listar/notificaciones/navbar", function (data, textStatus, jqXHR) {

        $("#NotifNavbar").html(data);

    });
}


function modal_notificacion(id){

    $.get("/cliente/mostrar/notificacion/"+ id, function (data, status) {
        $('#DetallePedidoNotificacion').modal('show');

        $("#DetallePedidoNotif").html(data);
    });

}



init();
