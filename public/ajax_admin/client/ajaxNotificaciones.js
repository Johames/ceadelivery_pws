function listas_notificaciones() {

    listar_notificaciones();

    $("input[name=filtro]").on("click",function (e) {
        listar_notificaciones();
    });

}


function listar_notificaciones(){

    notificaciones_navbar();

    var filtro_on = $('input:radio[name=filtro]:checked').val();

    $.get("/cliente/listar/notificaciones?filtro=" + filtro_on, function (data, status, jqXHR) {

        $('#lstsNotificaciones').html(data);

    });

}

function mostrar_notificacion(id){

    $.get("/cliente/mostrar/notificacion/"+ id, function (data, status) {
        $('#DetallePedidoNotificacion').modal('show');

        $("#DetallePedidoNotif").html(data);
    });

}


listas_notificaciones();
