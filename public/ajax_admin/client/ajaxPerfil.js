function perfil_cliente() {

    mostar_perfil();

    $('#btn_modificar_perfil').on('click', function () {

        $("#modal_cliente_perfil").modal('show');
        mostar_perfil_modal();

    });

    $('#formulario_cliente_perfil').submit(function (e){
        editar_perfil(e);
    });

    $("#formulario_avatar").on("submit", function(e) {
        guardar_icono(e);
    });

}

function editar_perfil(e) {
    crud_guardar_editar(
        e,
        '/cliente/editar/perfil',
        'cliente_perfil',
        function(){ limpiar_cliente(); },
        function(){ mostar_perfil(); },
        function(){ console.log('Console Error'); }

    );
}

function limpiar_cliente(){
    $('#ruc_dni').val('');
    $('#telefono_perfil').val('');
    $('#nombres').val('');
    $('#apellidos').val('');
    $('#representante_legal').val('');
}

function mostar_perfil() {

    $.get("/cliente/mostrar/perfil", function (data, status, jqXHR) {
        data = JSON.parse(data);

        $('#view_ruc_dni').text(data.ruc_dni);

        if(data.telefono){
            $('#view_telefono').text(data.telefono);
        } else {
            $('#view_telefono').text('Aun sin definir...');
        }

        if(data.razon_social) {
            $('#view_nombres_razon_social').text(data.razon_social);
        }else if(data.nombres){
            $('#view_nombres_razon_social').text(data.nombres);
        } else {
            $('#view_nombres_razon_social').text('Aun sin definir...');
        }

        if(data.nombre_comercial) {
            $('#view_apellidos_nombre_comercial').text(data.nombre_comercial);
        } else if(data.apellidos){
            $('#view_apellidos_nombre_comercial').text(data.apellidos);
        } else {
            $('#view_apellidos_nombre_comercial').text('Aun sin definir...');
        }

        if(data.representante_legal) {
            $('#view_representante_legal').text(data.representante_legal);
        }else{
            $('#view_representante_legal').text('Aun sin definir...');
        }

        if(data.avatar) {
            $('#icono_info').attr('src', '/img/' + data.avatar);
        } else {
            $('#icono_info').attr('src', '/img/default_img.png');
        }

    });

}


function mostar_perfil_modal() {
    limpiar_cliente();
    $.get("/cliente/mostrar/perfil", function (data, status, jqXHR) {
        data = JSON.parse(data);

        $('#ruc_dni').val(data.ruc_dni);
        $('#telefono_perfil').val(data.telefono);
        $('#nombres').val(data.nombres);
        $('#apellidos').val(data.apellidos);
        $('#representante_legal').val(data.representante_legal);

    });

}


function guardar_icono(e) {
    crud_guardar_editar(
        e,
        '/cliente/editar/perfil/avatar',
        'avatar',
        function(){ console.log('Limpiar avatar'); },
        function(){ mostar_perfil(); },
        function(){ console.log('Console Error avatar'); }

    );
}


perfil_cliente();
