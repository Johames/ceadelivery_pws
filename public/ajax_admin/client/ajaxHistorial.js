function listas_pedidos() {

    listar_pedidos(1);

    $(document).on("click", '.pagination a', function(e) {

        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        listar_pedidos(page);
    });

    $("input[name=filtro]").on("click",function (e) {
        listar_pedidos(1);
    });

}


function listar_pedidos(page){

    var filtro_on = $('input:radio[name=filtro]:checked').val();

    $.get("/cliente/historial/compras?page=" + page + "&filtro=" + filtro_on, function (data, status, jqXHR) {

        $('#listapedidos').html(data);

    });

}

function mostrar_detalle_pedido(id){

    $("#DetallePedido").modal('show');

    $("#detailsPedido").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $.get("/cliente/historial/compra/detalle/"+ id, function (data, status) {

        $('#detailsPedido').html(data);

    });

}







function mostrar_discusion_pedido(id){

    sessionStorage.setItem('pedId', id);

    $("#DiscusionPedido").modal('show');

    $("#discussPedido").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $.get("/cliente/discusion/pedido/"+ id, function (data) {

        $('#discussPedido').html(data);


        $("#formulario_discuss").on("submit", function(e) {
            guardar_editar_discuss(e);
        });

        mostrar_respuestas_discusion(sessionStorage.getItem('pedId'));

    });

}

function mostrar_respuestas_discusion(id){

    $("#contentMessages").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $.get('/cliente/discusion/pedido/detalle/' + id, function(data){

        $('#contentMessages').html(data);

    });

}

function guardar_editar_discuss(e) {

    $("#pedido_id").val(sessionStorage.getItem('pedId'));

    crud_guardar_editar_sm(
        e,
        '/cliente/discusion/pedido/guardar',
        'discuss',
        function(){ limpiar_discuss(); },
        function(){ mostrar_respuestas_discusion(sessionStorage.getItem('pedId')); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_discuss(){

    $("#mensaje").val('');

}







listas_pedidos();
