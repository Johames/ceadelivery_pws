function listas_deseos() {

    $("#formulario_deseos").on("submit", function(e) {
        guardar_editar_deseos(e);
    });

    listar_deseos();

}

function guardar_editar_deseos(e) {

    crud_guardar_editar(
        e,
        '/cliente/deseos/guardar',
        'deseos',
        function(){ limpiar_deseos(); },
        function(){ listar_deseos(); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_deseos(){

    $("#nombre").val('');
    $("#descripcion").val('');

}

function listar_deseos(){

    $.get("/cliente/listar/deseos", function (data, status, jqXHR) {

        $('#accordion').html(data);

    });

}

function mostrar_detalle_deseo(id){

    $.get("/cliente/listar/deseos/detalle/"+ id, function (data, status){

        $('#modal_detalle_deseos').modal('show');

        $("#detailDeseos").html(data);

    });

}

function mostrar_deseo(id){

    limpiar_deseos();
    $.get("/cliente/mostrar/deseos/"+ id, function (data, status) {
        data = JSON.parse(data);

        $('#modal_deseos').modal('show');

        $.each(data.deseos, function (index, deseo) {

            $("#deseos_id").val(id);
            $("#nombre").val(deseo.nombre);
            $("#descripcion").val(deseo.descripcion);

        });

    });

}

function eliminar_lista_deseo(id){
    crud_eliminar('/cliente/deseos/eliminar/' + id , function(){ listar_deseos(); }, function(){ console.log('Eror') });
}

function eliminar_detalle_deseos(id, idDes){
    crud_eliminar('/cliente/deseos/producto/eliminar/' + id + '/' + idDes, function(){ mostrar_detalle_deseo(idDes); }, function(){ console.log('Eror') });
}


listas_deseos();
