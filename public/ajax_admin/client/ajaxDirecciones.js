
function direcciones() {

    listar_direcciones();

    $("#formulario_direcciones").on("submit", function(e) {
        guardar_editar_direcciones(e);
    });

    $('#btn_buscar_direccion').on('click', function () {

        var direccion = $("#direccion").val();

        if (direccion !== "") {
            cargar_mapa_direccion("map", direccion, null);
        }

    });

    $("input[name=filtro]").on("click",function (e) {
        listar_direcciones();
    });

}

function guardar_editar_direcciones(e) {

    crud_guardar_editar(
        e,
        '/cliente/direcciones/guardar',
        'direcciones',
        function(){ limpiar_direcciones(); },
        function(){ listar_direcciones(); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_direcciones() {
    $("#direciones_id").val('');
    $("#direccion").val('');
    $("#referencia").val('');
    $("#coordenadas").val('');
}

function listar_direcciones() {

    var filtro_on = $('input:radio[name=filtro]:checked').val();

    $.get("/cliente/direcciones/listar?filtro=" + filtro_on, function (data, status, jqXHR) {

        $('#tabla_direcciones').html(data);

    });
}

function mostar_direcciones(id) {
    limpiar_direcciones();
    $.get("/cliente/direcciones/mostar/"+ id, function (data, status) {
        data = JSON.parse(data);

        $("#modal_direcciones").modal('show');

        $("#direciones_id").val(data.direciones_id);
        $("#direccion").val(data.direccion);
        $("#referencia").val(data.referencia);
        $("#coordenadas").val(data.coordenadas);

        cargar_mapa_direccion("map", data.direccion, data.coordenadas);

    });
}

function eliminar_direcciones(id) {
    crud_eliminar('/cliente/direcciones/eliminar/' + id , function(){ listar_direcciones(); }, function(){ console.log('Eror') });
}

function desactivar_direcciones(id) {
    crud_desactivar('/cliente/direcciones/desactivar/' + id , function(){ listar_direcciones(); }, function(){ console.log('Eror') });
}

function activar_direcciones(id) {
    crud_activar('/cliente/direcciones/activar/' + id , function(){ listar_direcciones(); }, function(){ console.log('Eror') });
}

function principal_direcciones(id) {
    crud_eliminar('/cliente/direcciones/principal/' + id , function(){ listar_direcciones(); }, function(){ console.log('Eror') });
}


direcciones();
