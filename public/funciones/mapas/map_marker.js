
function cargar_mapa_direccion(elemento, direccion, coordenadas) {

    var geocoder = new google.maps.Geocoder();

    var map = new google.maps.Map(document.getElementById(elemento), {
        zoom: 14,
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker = new google.maps.Marker({
        map: map,
    });

    var infowindow = new google.maps.InfoWindow({
        content: '<h6>' + direccion + '</h6>',
    });

    map.setCenter(new google.maps.LatLng('-6.487733', '-76.359886'));

    // marker.setPosition(new google.maps.LatLng('-6.487733', '-76.359886'));


    if (direccion) {
        geocoder.geocode({
            'address': direccion
        }, function (results, status) {
            if (status === 'OK') {
    
                var resultados = results[0].geometry.location,
    
                    resultados_lat = resultados.lat(),
                    resultados_long = resultados.lng();
    
                var coordenadas_map;
    
                // console.log(results[0].geometry.location);
    
                // NO MANDO COORDENADAS INICIALES PARA MOSTRAR
                if (coordenadas == null) {
    
                    coordenadas_map = results[0].geometry.location;
    
                }else{
                // SI MANDO COORDENADAS
    
                    var array_coordenadas = coordenadas.split(',');
    
                    coordenadas_map = new google.maps.LatLng(array_coordenadas[0], array_coordenadas[1]);
                }
    
                map.setCenter(coordenadas_map);
    
                marker.setPosition(coordenadas_map);
    
                $("#coordenadas").val(resultados_lat + ',' + resultados_long);
    
    
            } else {
                var mensajeError = "";
                if (status === "ZERO_RESULTS") {
                    mensajeError = "No hubo resultados para la dirección ingresada.";
                } else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
                    mensajeError = "Error general del mapa.";
                } else if (status === "INVALID_REQUEST") {
                    mensajeError = "Error de la web. Contacte con Name Agency.";
                }
                alert(mensajeError);
            }
        });
    }


    

    google.maps.event.addListener(marker, 'mouseover', function() {
        infowindow.open(map,marker);
    });


    google.maps.event.addListener(map, 'click', function (event) {
        // setMapOnAll(null);

        var latlng_fake = new google.maps.LatLng(event.latLng.lat(), event.latLng.lng());

        marker.setPosition(latlng_fake);

        $("#coordenadas").val(event.latLng.lat() + ',' + event.latLng.lng());

    });


    google.maps.event.addDomListener(window, "resize", function () {

        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);

    });
}


function cargar_mapa_mostar(elemento, direccion, coordenadas) {

    $("#" + elemento).css('position',  'static');


    var geocoder = new google.maps.Geocoder();

    var map = new google.maps.Map(document.getElementById(elemento), {
        zoom: 14,
        scrollwheel: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var array_coordenadas = coordenadas.split(',');
    var coordenadas_map = new google.maps.LatLng(array_coordenadas[0], array_coordenadas[1]);

    var marker = marker = new google.maps.Marker({
        map: map,
    });

    map.setCenter(coordenadas_map);
    marker.setPosition(coordenadas_map);


    //Callout Content
    var contentString = '<h6>' + direccion + '</h6>';
    //Set window width + content

    var infowindow = new google.maps.InfoWindow({
        content: contentString,
    });

    infowindow.open(map,marker);

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
    });

    //Resize Function
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });

}

