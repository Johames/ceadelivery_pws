
function tr(idtable, param) {
    var tr = '<tr id="tr_' + idtable + '" class="tr_table">'
                + param
           + '</tr>';
    return tr;
}

function td(param) {
    return '<td>' + param + ' </td>';
}

function input(nombre, value) {
    return '<input name="'+ nombre +'[]" type="hidden" value="' + value + '">';
}

function btn_eliminar(id, btn_funcion = 'eliminar_tr') {
    return '<a href="#" class="btn btn-danger btn-xs" onclick="' + btn_funcion + '(' + id + ')"><i class="fa fa-trash"></i></a>';
}

function eliminar_tr(id) {
    swal({
        title: "¿Deseas eliminar permanetemente?",
        text: "¡No se podra recuperar!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, eliminar",
        cancelButtonText: "No, cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {

            $("#tr_" + id).remove();
            
            swal({
                title: "Se eliminó",
                timer: 2000,
                type: "success"
            });

        } else {
            swal({
                title: "Se canceló",
                timer: 2000,
                type: "error"
            });
        }
    });
}

function eliminar_tr_all() {
    $(".tr_table").remove();
}

function validar_tr(id, input) {
    flat = false;
    cont = 0;

    $('input[name="' + input + '[]"').each(function() {
        cont++;
        
        if (id == $(this).val()) {
            flat = true;
            
        }
    
    });
    return flat;
}

function unique_id() {
    return parseInt(Math.round(new Date().getTime() + (Math.random() * 100)));
}