(function ($) {

    // Declaración del plugin.
    $.fn.axfile = function (options) {

        // Obtenemos los parámetros.
        options = $.extend({}, $.fn.axfile.defaultOptions, options);

        this.each(function () {
            var input    = $(this);
            var input_id = $(this).attr('id');

            var src = 'https://i.ibb.co/LdxgGhV/default-home.png';
            var width = 200;

            if (options.src != null) {
                src = options.src;
            }

            if(options.width != null){
                width = options.width;
            }

            var img = $("<img id='ax_" + input_id + "' src='" + src +"' style='cursor:pointer' width='" + width + "' class='img-fluid' alt='Responsive image'>");

            img.insertAfter($(this));

            img.click(function () {
                input.trigger('click');
            });

            console.log(img)

            input.change(function (e) {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        img.attr('src', e.target.result);

                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });



            input.css('display', 'none');
        });

        return this;
    }

    function create_img(){
        return "<img src='https://i.ibb.co/LdxgGhV/default-home.png' style='cursor:pointer' width='200' class='img-fluid' alt='Responsive image'>"
    }
})(jQuery);
