function deseos(){

    $("#formulario_deseos").on("submit", function(e) {
        guardar_editar_deseos_modal(e);
    });

    $("#formulario_registar_valoracion").on("submit", function(e) {
        guardar_valoracion(e);
    });

}

function guardar_editar_deseos_modal(e) {

    crud_guardar_editar(
        e,
        '/cliente/deseos/guardar',
        'deseos',
        function(){ limpiar_deseos(); },
        function(){ listar_deseos_modal(); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_deseos(){

    $("#nombre").val('');
    $("#descripcion").val('');

    console.log('Limpiando');

}

function mostrar_modal_elegir_deseos(id) {

    $('#modal_elegir_deseos').modal('show');

    sessionStorage.setItem('detalleProdId', id);

    listar_deseos_modal();

}

function listar_deseos_modal(){

    $("#ListasDeseos").html('<div class="row lsts_deseos justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $(".lsts_deseos").remove();
    $.get("/cliente/listar/deseos/select", function (data, status, jqXHR) {
        data = JSON.parse(data);
        $.each(data.deseos, function (index, value) {

            var li =    '<li class="list-group-item lsts_deseos d-flex justify-content-between align-items-center" onclick="agregar_prod_deseo(' + sessionStorage.getItem('detalleProdId') + ', ' + value.deseos_id + ')" style="cursor: pointer;">'
                            + '<span>'
                                + value.nombre
                            + '</span>'
                            + '<span class="badge badge-pill badge-info text-center">'
                                + '<i class="fas fa-chevron-right text-center m-1"></i>'
                            + '</span>'
                        + '</li>';

            $('#ListasDeseos').append(li);

        });

    });

}



function agregar_prod_deseo(idProd, idDeseo){

    var _token = $('#_token').val()

    $.ajax({
        url: '/cliente/deseos/producto/guardar',
        type: "POST",
        data: {
            _token: _token,
            idproducto: idProd,
            iddeseo: idDeseo
        },
        success: function (datos) {
            datos = JSON.parse(datos);

            if (datos.status) {
                sw_success(datos.message);
                $('#modal_elegir_deseos').modal('hide');
                sessionStorage.setItem('detalleProdId', '');
            } else {
                sw_error(datos.message);
            }
        },
        error: function (jqXhr) {
            if(jqXhr.status == 419){
                sw_error('Recargue la pagina');
            }
        }
    });
}









$('input:radio[name=rating]').change(function () {
    var me = $(this).val();
    $("#valoracion").val(me);
});


function mostrar_modal_valoracion(tipo, id) {

    if(tipo == 'emp'){

        $.get("/valoracion/empresa/mostar/"+ id, function (data, status) {
            data = JSON.parse(data);

            $("#modal_registar_valoracion").modal('show');

            $("#img_valor").attr('src','https://tiendas.ceamarket.com/img/' + data.icono);

            if(data.razon_social != null){
                $("#nombre_valor").html(data.razon_social);
            } else{
                $("#nombre_valor").html(data.nombre_comercial);
            }

            $("#tipo").val(tipo);

            $("#id_valoracion").val(data.empresa_id);

        });

    } else if(tipo == 'prod'){

        $.get("/valoracion/producto/mostar/"+ id, function (data, status) {
            data = JSON.parse(data);

            $("#modal_registar_valoracion").modal('show');

            $.each(data, function (index, value) {

                if(value.primera_imagen != null){
                    $("#img_valor").attr('src','https://tiendas.ceamarket.com/img/' + value.primera_imagen.ruta);
                } else {
                    $("#img_valor").attr('src','/img/default-product.png');
                }

                $("#id_valoracion").val(value.producto_detalle_id);

                $("#nombre_valor").text(value.modelo_producto.nombre);

            });

            $("#tipo").val(tipo);

        });

    } else if(tipo == 'deliv'){

        $.get("/valoracion/deliverista/mostar/"+ id, function (data, status) {
            data = JSON.parse(data);

            $("#modal_registar_valoracion").modal('show');



            if(data.foto != null){
                $("#img_valor").attr('src','https://api.ceamarket.com/img/' + data.foto);
            } else {
                $("#img_valor").attr('src','/img/default_img.png');
            }

            $("#id_valoracion").val(data.deliverista_id);

            $("#nombre_valor").text(data.nombres + ' ' + data.apellidos);

            $("#tipo").val(tipo);

        });

    }



}

function guardar_valoracion(e) {

    var tipo = $("#tipo").val();

    if(tipo == 'emp'){

        crud_guardar_editar(
            e,
            '/valoracion/empresa/agregar',
            'registar_valoracion',
            function(){ limpiar_valoracion(); },
            function(){ console.log('Console listar'); },
            function(){ console.log('Console Error'); }
        );

    } else if(tipo == 'prod'){

        crud_guardar_editar(
            e,
            '/valoracion/producto/agregar',
            'registar_valoracion',
            function(){ limpiar_valoracion(); },
            function(){ console.log('Console listar'); },
            function(){ console.log('Console Error'); }
        );

    } else if(tipo == 'deliv'){

        crud_guardar_editar(
            e,
            '/valoracion/deliverista/agregar',
            'registar_valoracion',
            function(){ limpiar_valoracion(); },
            function(){ console.log('Console listar'); },
            function(){ console.log('Console Error'); }
        );

    }

}

function limpiar_valoracion(){
    $("#img_valor").attr('src', '');
    $("#tipo").val('');
    $('input:radio[name=rating]').removeProp('checked')
    $("#nombre_valor").html('');
    $("#id_valoracion").val('');
    $("#valoracion").val('1');
    $("#resena").val('');
}




deseos();
