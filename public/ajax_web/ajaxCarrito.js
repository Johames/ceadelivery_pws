
var ToastCarrito = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

function carrito_compra() {

    listar_carrito_modal();
    // mostrar_carrito_nav();

}

function addCarrito(id) {
    Swal.fire({
        title: "¿Deseas agregar este producto?",
        text: "Podra editar su cantidad en el carrito, si añades doble vez un producto este se sumará automaticamente",
        input: 'number',
        inputValue: 1,
        inputAttributes: {
            min: 1,
        },
        showCancelButton: true,
        confirmButtonColor: "#42d697",
        confirmButtonText: "Si, agregar",
        cancelButtonText: "No, cancelar",
        inputValidator: (value) => {
            if (value < 0) {
              return 'Solo valores mayores a 0'
            } else {
                if (value - Math.floor(value) != 0) {
                    return 'Solo números enteros'
                }
            }
        },
        closeOnConfirm: false,
        closeOnCancel: false
    }).then((result) => {
        if (result.value) {
            $.get('/add/carrito/producto/' + id + '/' + result.value, function (data, status) {
                data = JSON.parse(data);
                if (data.status) {
                    ToastCarrito.fire({
                        icon: 'success',
                        title: data.message,
                    })

                    listar_carrito_modal();

                    $("#DetalleProducto").modal('hide');

                } else {
                    ToastCarrito.fire({
                        icon: 'error',
                        title: data.message,
                    })
                }

            });

        } else if ( result.dismiss ) {
            ToastCarrito.fire({
                icon: 'error',
                title: 'Se canceló',
            })
        }
    });

}

function addCarritoDirecto(id) {
    $.get('/add/carrito/producto/' + id, function (data, status) {
        data = JSON.parse(data);
        if (data.status) {
            ToastCarrito.fire({
                icon: 'success',
                title: data.message,
            })

            listar_carrito_modal();

        } else {
            ToastCarrito.fire({
                icon: 'error',
                title: data.message,
            })
        }
    });
}

function mostrar_carrito_nav() {
    //
    $.get("/pedido/listar/carrito/nav", function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        if (data.count_pedidos > 1) {

            //Más de un pedido
            $("#nav_car_pedido_num").text(data.count_pedidos);

            $("#nav_car_pedido_num").removeClass('bg-primary');
            $("#nav_car_pedido_num").addClass('bg-info');

            $("#nav_car_pedido_detalle").text('Ver');

        }else if(data.count_pedidos == 1){

            // Solo un  pedido
            $("#nav_car_pedido_num").text(data.count_productos);

            $("#nav_car_pedido_num").removeClass('bg-info');
            $("#nav_car_pedido_num").addClass('bg-primary');

            $("#nav_car_pedido_detalle").text( 'S/  ' + data.subtotal);

        }else{
            $("#nav_car_pedido_num").hide();
            $("#lista_carrito_modal").html('<div class="row justify-content-center my-5"><strong> No hay productos en el carrito</strong> </div>');
        }
        // console.log(data);
    });

    // $.ajax({
    //     url: "/pedido/listar/carrito/nav",
    //     type: "GET",

    //     contentType: false,
    //     processData: false,
    //     success: function (datos) {
    //         datos = JSON.parse(datos);

    //         console.log(datos)

    //     },
    //     xhr: function () {
    //         var xhr = new window.XMLHttpRequest();

    //         xhr.addEventListener("progress", function (evt) {
    //             if (evt.lengthComputable) {

    //                 var prct = (evt.loaded / evt.total) * 100;

    //                 console.log('prct: ' + prct)
    //                 // $("#barra_progress_" + nombre_modulo).css({
    //                 //     "width": prct + '%'
    //                 // });

    //                 // $("#barra_progress_" + nombre_modulo).text(prct + "%");

    //                 // if (prct === 100) {
    //                 //     setTimeout(function(){ reniciar_barra(nombre_modulo) }, 600);
    //                 // }
    //             }
    //         }, false);
    //         return xhr;
    //     },
    //     beforeSend: function () {
    //         // $("#div_barra_progress_" + nombre_modulo).show();
    //         // $("#barra_progress_" + nombre_modulo).css({
    //         //     "width": '0%'
    //         // });
    //         // $("#barra_progress_" + nombre_modulo).text("0%");
    //     },
    //     complete: function () {
    //         // $("#div_barra_progress_" + nombre_modulo).hide();
    //         // $("#barra_progress_" + nombre_modulo).css({
    //         //     "width": '0%'
    //         // });
    //         // $("#barra_progress_" + nombre_modulo).text("0%");
    //     },
    //     error: function (jqXhr) {

    //         // comprobar_errores(jqXhr, nombre_modulo);
    //     }
    // });

}

function listar_carrito_modal() {
    // $("#modal_carrito").modal('show')
    $("#lista_carrito_modal").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $.get("/pedido/listar/carrito/modal", function (data, textStatus, jqXHR) {

        $("#lista_carrito_modal").html(data);
        mostrar_carrito_nav();
    });
}

function deleteProductDirecto(id) {

    crud_eliminar(
        "/pedido/eliminar/producto/" + id,
        function(){ listar_carrito_modal(); },
        function(){  }
    );

}



carrito_compra();
