function init(){

    $("#formulario_sugerencia").on("submit", function(e) {
        guardar_editar_sugerencia(e);
    });

}


function guardar_editar_sugerencia(e){

    crud_guardar_editar(
        e,
        'agregar/sugerencia',
        'sugerencia',
        function(){ limpiar_sugerencias(); },
        function(){ limpiar_sugerencias(); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_sugerencias(){

    $("#area_coment").val('');
    $("#tipo_coment").val();
    $("#comentario_coment").val('');

}



function verPreguntas(slug){

    $.get("/preguntas_frecuentes/" + slug, function (data) {

        $('#PreguntasFrecuentes').html(data);

    });

}



init();
