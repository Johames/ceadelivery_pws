Culqi.publicKey = 'pk_test_d97c4004ba28f3e1';
Culqi.init();

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


function paso_tres() {

    listar_productos();
    calcular_totales();
    listar_metodos_pago();
    mostar_pedido();

    sessionStorage.setItem('formToken', $('#_token').val());
    sessionStorage.setItem('guard', 'NO');

    $("#formulario_paso_tres").on("submit", function(e) {
        guardar_paso_tres(e);
    });

    $("#formulario_pago_app").on("submit", function(e) {
        subirConstanciaPago(e);
    });

}

function btn_pagar_click(e){

    if($("#terminos").prop('checked')){
        if($('input[name=cardEmail]').val() !== ""){

            if($('input:text[name=cardNumero]').val() !== ""){

                if($('input:text[name=cardMes]').val() !== ""){

                    if($('input:text[name=cardYear]').val() !== ""){

                        if($('input:text[name=cardCVV]').val() !== ""){

                            $("#contentLoader1").css('display', 'block');

                            e.preventDefault();
                            Culqi.createToken();

                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'Ingrese el CVV de su tarjeta...',
                            });
                        }

                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'Seleccione el Año de Vencimiento...',
                        });
                    }

                } else {
                    Toast.fire({
                        icon: 'error',
                        title: 'Seleccione el Mes de Vencimiento...',
                    });
                }

            } else {
                Toast.fire({
                    icon: 'error',
                    title: 'Ingrese un Número de Tarjeta...',
                });
            }

        } else {
            Toast.fire({
                icon: 'error',
                title: 'Ingrese un Correo Electrónico...',
            });
        }

    }else {
        Toast.fire({
            icon: 'error',
            title: 'Debe Aceptar los Términos y Condiciones...',
        });
    }

}

function culqi(){
    if (Culqi.token.id) {

        $.ajax({
            url: '/pedido/pago/tarjeta',
            type: "POST",
            data: {
                _token: sessionStorage.getItem('formToken'),
                card: Culqi.token.id,
                cardno: '',
                guardar: sessionStorage.getItem('guard')
            },
            success: function (datos) {
                datos = JSON.parse(datos);

                if(datos.outcome){

                    sw_success(datos.outcome.user_message);

                    setTimeout(
                        function(){

                            $("#contentLoader1").css('display', 'none');

                            agregar();

                        }
                    , 900);

                } else {

                    $("#contentLoader1").css('display', 'none');

                    var errors = JSON.parse(datos);

                    sw_error(errors.user_message, 7000);

                }

            },
            error: function (jqXhr) {

                $("#contentLoader1").css('display', 'none');

                var errors = jqXhr.responseJSON;

                var user_message = JSON.parse(errors.message);

                if(user_message.user_message){
                    sw_error(user_message.user_message, 7000);
                } else {
                    sw_error(errors.message);
                }

            }

        });

    } else {

        $("#contentLoader1").css('display', 'none');

        sw_error(Culqi.token.user_message, 7000);

    }
}

function cardExistst(e){
    e.preventDefault();

    if($("#tarjeta_" + sessionStorage.getItem('card')).prop('checked')){

        $("#contentLoader1").css('display', 'block');

        $.ajax({
            url: '/pedido/pago/tarjeta',
            type: "POST",
            data: {
                _token: sessionStorage.getItem('formToken'),
                card: '',
                cardno: sessionStorage.getItem('card'),
                guardar: 'NO'
            },
            success: function (datos) {
                datos = JSON.parse(datos);

                if(datos.outcome){

                    sw_success(datos.outcome.user_message);

                    setTimeout(
                        function(){

                            $("#contentLoader1").css('display', 'none');

                            agregar();

                        }
                    , 900);

                } else {

                    $("#contentLoader1").css('display', 'none');

                    var errors = JSON.parse(datos);

                    sw_error(errors.user_message, 7000);

                }

            },
            error: function (jqXhr) {

                $("#contentLoader1").css('display', 'none');

                var errors = jqXhr.responseJSON;

                var user_message = JSON.parse(errors.message);

                if(user_message.user_message){
                    sw_error(user_message.user_message, 7000);
                } else {
                    sw_error(errors.message);
                }

            }

        });

    } else {

        sw_error('Los datos han sido manipulados, el evento sera registrado y se tomarán las acciones necesarias.')

    }

}

function guardarCard(){

    if($("#guardar").prop('checked') == true){
        sessionStorage['guard'] = 'SI';
    } else {
        sessionStorage['guard'] = 'NO';
    }

}


function guardar_paso_tres(e) {
    e.preventDefault();

    var pag = $("#pago_id").val();

    if(pag != ""){

        if(sessionStorage['tipo'] == 0){

            if($("#metodos_pago_" + sessionStorage.getItem('tipo')).prop('checked')){

                $("#contentLoader1").css('display', 'block');

                agregar();

            } else {

                $("#contentLoader1").css('display', 'none');

                sw_error('Los datos han sido manipulados, el evento sera registrado y se tomarán las acciones necesarias.')

            }

        } else if(sessionStorage['tipo'] == 1){

            if($("#metodos_pago_" + sessionStorage.getItem('tipo')).prop('checked')){

                $("#modal_pago_tarjeta").modal('show');

                $("#ContentModalTarjetas").html('<div class="modal-body lateral bg-faded-success"> <div class="col-12 px-2 mb-4"> <div class="row justify-content-center my-5"> <i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i> </div> </div> </div>');

                $.get("/listar/tarjetas/guardadas", function(data){

                    $("#ContentModalTarjetas").html(data);

                    $("#btn_pago").on("click", function(e) {
                        btn_pagar_click(e);
                    });

                    $("#btn_card_pago").on("click", function(e) {
                        cardExistst(e);
                    });

                });

            } else {

                sw_error('Los datos han sido manipulados, el evento sera registrado y se tomarán las acciones necesarias.')

            }

        } else if(sessionStorage['tipo'] == 2){


            if($("#metodos_pago_" + sessionStorage.getItem('tipo')).prop('checked')){

                $("#modal_pago_app").modal('show');

                $('#MetodoPagoQR').attr('src','https://tiendas.ceamarket.com/img/' + sessionStorage.getItem('ref'));

            } else {

                sw_error('Los datos han sido manipulados, el evento sera registrado y se tomarán las acciones necesarias.')

            }

        } else if(sessionStorage['tipo'] == 3){

            if($("#metodos_pago_" + sessionStorage.getItem('tipo')).prop('checked')){

                $("#modal_pago_paypal").modal('show');

            } else {

                sw_error('Los datos han sido manipulados, el evento sera registrado y se tomarán las acciones necesarias.')

            }

        } else {

            sw_error('Los datos han sido manipulados, el evento sera registrado y se tomarán las acciones necesarias.')

        }

    } else {
        Toast.fire({
            icon: 'error',
            title: 'Seleccione un método de pago...',
        });
    }

}

function agregar(){

    $("#pago_id").val(sessionStorage.getItem('met'));

    var formData = new FormData($("#formulario_paso_tres")[0]);

    $("#contentLoader1").css('display', 'block');

    $.ajax({
        url: '/pedido/guardar/paso/3',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            datos = JSON.parse(datos);

            if (datos.status) {
                Toast.fire({
                    icon: 'success',
                    title: 'Guardando, redireccionando...',
                });

                location.replace('/mi/pedido/paso/4/')
            }

        },
        xhr: function () {
            var xhr = new window.XMLHttpRequest();

            return xhr;
        },
        beforeSend: function () {},
        complete: function () {},
        error: function (jqXhr) {

            $("#contentLoader1").css('display', 'none');

            var errors = jqXhr.responseJSON;

            sw_error(errors.message);

        }
    });

}



function subirConstanciaPago(e) {

    $("#contentLoader1").css('display', 'block');

    crud_guardar_editar(
        e,
        '/pedido/constancia/guardar',
        'pago_app',
        function(){ agregar(); },
        function(){ limpiar_pago_pedido(); },
        function(){ $("#contentLoader1").css('display', 'none'); }

    );

}

function limpiar_pago_pedido(){
    $("#constancia").val(null);
    $("#constancia").next('label').text('Subir constancia de pago (.png, .jpg)...');
}



function listar_productos() {

    $('#lista_productos').html('');

    $.get("/pedido/listar/productos/list/", function (data) {
        $('#lista_productos_list').html(data);
    });

}

function mostar_pedido() {

    $.get("/pedido/mostrar/pedido/carrito", function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        //Empresa
        if(data.modelo_empresa.icono){
            $('#imgEmpresaPedido3').attr('src','https://tiendas.ceamarket.com/img/' + data.modelo_empresa.icono);
        } else {
            $('#imgEmpresaPedido3').attr('src','/img/default-store.png');
        }

        if(data.modelo_empresa.razon_social){
            $("#urlEmpresaPedido3").text(data.modelo_empresa.razon_social);
        } else {
            $("#urlEmpresaPedido3").text(data.modelo_empresa.nombre_comercial);
        }

        $("#urlEmpresaPedido3").attr('href',"/store/" + data.modelo_empresa.slug);

        $("#rubroEmpresaPedido3").text(data.modelo_empresa.modelo_rubro.nombre);

        if(!data.modelo_persona.telefono){

            $("#modal_telefono").modal('show');

            $("#formulario_telefono").on("submit", function(e) {
                guardar_telefono(e);
            });

        }

    });
}

function calcular_totales() {

    $.get("/pedido/totales/", function(data) {

        data = JSON.parse(data);

        var sub_total_array     = data.productos_total_desc.toString().split(".");
        var envio_array         = data.metodo_envio_precio.toString().split(".");
        var pago_array          = data.metodo_pago_precio.toString().split(".");
        var total_array         = data.total.toString().split(".");

        $('#sub_total').html('S/ ' + sub_total_array[0] + '.<small>' + (sub_total_array[1] ? sub_total_array[1] : '00') + '</small>');
        $('#envio').html('S/ ' + envio_array[0] + '.<small>' + (envio_array[1] ? envio_array[1] : '00') + '</small>');
        $('#pago').html('S/ ' + pago_array[0] + '.<small>' + (pago_array[1] ? pago_array[1] : '00') + '</small>');
        $('#total').html('S/ ' + total_array[0] + '.<small>' + (total_array[1] ? total_array[1] : '00') + '</small>');
    });
}

function listar_metodos_pago() {

    // $("#tabla_metodos_pago").html('<div class="row justify-content-center my-5"><div class="col-12"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div></div>');
    $("#tabla_metodos_pago").html('<tr><td class="text-center" colspan="3"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></td></tr>');

    $.get("/pedido/listar/metodos/pago/", function (data, textStatus, jqXHR) {

        $("#tabla_metodos_pago").html(data);

    });
}



function selectMetodo(idMet, tip, ref){

    $("#metodos_pago_" + tip).prop('checked', true);

    $("#pago_id").val(idMet);

    sessionStorage.setItem('met', idMet);
    sessionStorage.setItem('tipo', tip);
    sessionStorage.setItem('ref', ref);

}

function selectCard(card){

    $("#tarjeta_" + card).prop('checked', true);

    sessionStorage.setItem('card', card);

}

paso_tres();
