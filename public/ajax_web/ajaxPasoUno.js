

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


function paso_uno() {

    listar_productos(1, 1);
    calcular_subtotal();
    mostar_pedido();

    $("#formulario_paso_uno").on("submit", function(e) {
        guardar_paso_uno(e);
    });

}

function mostar_pedido() {

    $.get("/pedido/mostrar/pedido/carrito", function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        //Empresa
        if(data.modelo_empresa.icono){
            $('#imgEmpresaPedido1').attr('src','https://tiendas.ceamarket.com/img/' + data.modelo_empresa.icono);
        } else {
            $('#imgEmpresaPedido1').attr('src','/img/default-store.png');
        }

        if(data.modelo_empresa.razon_social){
            $("#urlEmpresaPedido1").text(data.modelo_empresa.razon_social);
        } else {
            $("#urlEmpresaPedido1").text(data.modelo_empresa.nombre_comercial);
        }

        $("#urlEmpresaPedido1").attr('href',"/store/" + data.modelo_empresa.slug);

        $("#rubroEmpresaPedido1").text(data.modelo_empresa.modelo_rubro.nombre);

        //COMENTARIOS
        $("#comentarios").val(data.comentarios);

        if (data.codigo_descuento) {
            //CODIGO DE PROMOCION
            $("#codigo_promocion").val(data.codigo_descuento);
            $("#codigo_promocion").prop('disabled', true);
            $("#btn_aplicar_codigo_promocion").hide()
        }else{

            $("#btn_eliminar_codigo_promocion").hide()
        }

    });
}

function guardar_paso_uno(e) {
    e.preventDefault();

    $("#contentLoader1").css('display', 'block');

    var formData = new FormData($("#formulario_paso_uno")[0]);
    var nombre_modulo = 'paso_uno';

    $.ajax({
        url: '/pedido/guardar/paso/1',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            datos = JSON.parse(datos);

            if(datos.status == 'Confirmar'){

                $("#contentLoader1").css('display', 'none');
                sw_confirmar_email(datos.message);

            } else if (datos.status) {

                Toast.fire({
                    icon: 'success',
                    title: 'Guardando, redireccionando...',
                });

                location.replace('/mi/pedido/paso/2/')

            } else {

                $("#contentLoader1").css('display', 'none');
                sw_error(datos.message);

            }

        },
        xhr: function () {
            var xhr = new window.XMLHttpRequest();

            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var prct = (evt.loaded / evt.total) * 100;
                        prct = Math.round(prct);

                    $("#barra_progress_" + nombre_modulo).css({
                        "width": prct + '%'
                    });

                    $("#barra_progress_" + nombre_modulo).text(prct + "%");
                }
            }, false);
            return xhr;

        },
        beforeSend: function () {
            $("#div_barra_progress_" + nombre_modulo).show();
            $("#barra_progress_" + nombre_modulo).css({
                "width": '0%'
            });
            $("#barra_progress_" + nombre_modulo).text("0%");
        },
        complete: function () {
            $("#div_barra_progress_" + nombre_modulo).hide();
            $("#barra_progress_" + nombre_modulo).css({
                "width": '0%'
            });
            $("#barra_progress_" + nombre_modulo).text("0%");
        },
        error: function (jqXhr) {

            $("#contentLoader1").css('display', 'none');

            comprobar_errores(jqXhr, nombre_modulo);

        }
    });

}

function listar_productos(page, filtro) {

    $('#lista_productos').html('');

    if(filtro == 1){

        $.get("/pedido/listar/productos?page=" + page + "&filtro=" + filtro, function (data) {
            $('#lista_productos').html(data);

            mostar_pedido();
        });

    } else if(filtro == 2){

        $.get("/pedido/listar/productos?filtro=" + filtro, function (data) {
            $('#lista_productos').html(data);

            mostar_pedido();
        });

    } else {

        Toast.fire({
            icon: 'error',
            title: 'Elija un tipo de lista',
        });

    }



}

function sumar(id) {

    var cantidad = parseInt($("#cantidad_"+id).val()) + 1;

    $.get("/pedido/editar/producto/" + id + "/" + cantidad, function (data) {

        data = JSON.parse(data);

        if (data.status) {

            $("#cantidad_"+id).val(cantidad);
            calcular_subtotal();

            listar_carrito_modal();

            Toast.fire({
                icon: 'success',
                title: 'Ok 1 más',
            })
        }else{
            Toast.fire({
                icon: 'error',
                title: data.message,
            })
        }



    });
}

function restar(id) {
    var cantidad = parseInt($("#cantidad_"+id).val()) - 1;

    $.get("/pedido/editar/producto/" + id + "/" + cantidad, function (data) {

        data = JSON.parse(data);

        if (data.status) {
            $("#cantidad_"+id).val(cantidad);
            calcular_subtotal();

            listar_carrito_modal();

            Toast.fire({
                icon: 'success',
                title: 'Ok 1 menos',
            })

        }else{
            Toast.fire({
                icon: 'error',
                title: data.message,
            })
        }



    });
}

function eliminar(id) {

    Swal.fire({
        title: "¿Deseas eliminar del carrito?",
        // text: "¡No se podra recuperar!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#f34770",
        confirmButtonText: "Si, eliminar",
        cancelButtonText: "No, cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
    }).then((result) => {
        if (result.value) {
            $.get("/pedido/eliminar/producto/" + id , function (data, status) {
                data = JSON.parse(data);
                if (data.status) {

                    listar_productos();
                    calcular_subtotal();

                    Toast.fire({
                        icon: 'success',
                        title: data.message,
                    });

                    if (data.lasted) {
                        document.location.href = "/";
                    }

                } else {

                    Toast.fire({
                        icon: 'error',
                        title: data.message,
                    });

                }
            });

        } else if ( result.dismiss ) {
            sw_cancelar();
        }
    });


}

function aplicar_codigo_promocion() {

    var codigo_promocion = $("#codigo_promocion").val();

    if (codigo_promocion) {
        $.get("/pedido/aplicar/codigo/promocion/" + codigo_promocion , function(data) {
            data = JSON.parse(data);
            console.log(data);

            if (data.staus) {
                mostar_pedido();
            }else{
                Toast.fire({
                    icon: 'error',
                    title: data.message,
                })
            }
        });
    }else{
        Toast.fire({
            icon: 'error',
            title: 'Ingrese un código de promoción',
        })
    }

}

function calcular_subtotal() {

    $.get("/pedido/totales/", function(data) {

        data = JSON.parse(data);

        var sub_total_array = data.productos_total_desc.toString().split(".")

        $('#sub_total').html('S/  ' + sub_total_array[0] + '.<small>' + (sub_total_array[1] ? sub_total_array[1] : '00') + '</small>');
    });
}


paso_uno();
