const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })


function paso_dos() {

    mostar_pedido();
    listar_productos();
    calcular_totales();

    listar_metodos_envio();

    verificarDirecciones();

    $('#select_modal_direcciones').select2({
        theme: 'bootstrap4',
        width: 'style',
        placeholder: 'Seleccione una dirección',
        allowClear: true
    });

    lista_select2('/tienda/direcciones/listar/select', 'direcciones', null);

    $("#select_modal_direcciones").on('change', function () {

        if ($(this).val()) {

            sessionStorage.setItem('dirId', $(this).val());

            Toast.fire({
                icon: 'success',
                title: 'Dirección seleccionada',
            })
        }
    });

    $("#formulario_direcciones").on("submit", function(e) {
        guardar_editar_direcciones(e);
    });

    $('#btn_buscar_direccion').on('click', function () {

        var direccion = $("#direccion").val();

        if (direccion !== "") {
            cargar_mapa_direccion("map", direccion, null);
        }else{
            // alert('mapa vacio');
        }

    });

    $("#formulario_paso_dos").on("submit", function(e) {
        guardar_paso_dos(e);
    });

}

function mostar_pedido() {

    $.get("/pedido/mostrar/pedido/carrito", function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        //Empresa
        if(data.modelo_empresa.icono){
            $('#imgEmpresaPedido2').attr('src','https://tiendas.ceamarket.com/img/' + data.modelo_empresa.icono);
        } else {
            $('#imgEmpresaPedido2').attr('src','/img/default-store.png');
        }

        if(data.modelo_empresa.razon_social){
            $("#urlEmpresaPedido2").text(data.modelo_empresa.razon_social);
        } else {
            $("#urlEmpresaPedido2").text(data.modelo_empresa.nombre_comercial);
        }

        $("#urlEmpresaPedido2").attr('href',"/store/" + data.modelo_empresa.slug);

        $("#rubroEmpresaPedido2").text(data.modelo_empresa.modelo_rubro.nombre);

        if(!data.modelo_persona.telefono){

            $("#modal_telefono").modal('show');

            $("#formulario_telefono").on("submit", function(e) {
                guardar_telefono(e);
            });

        }

    });
}

function guardar_paso_dos(e) {
    e.preventDefault();

    $("#contentLoader1").css('display', 'block');

    var dir = sessionStorage.getItem('dirId');
    var met = sessionStorage.getItem('env');

    $("#direcion_id").val(sessionStorage.getItem('dirId'));
    $("#envio_id").val(sessionStorage.getItem('env'));

    var nombre_modulo = 'paso_dos';

    if(dir !== "" && met !== ""){

        var formData = new FormData($("#formulario_paso_dos")[0]);

        $.ajax({
            url: '/pedido/guardar/paso/2',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos) {
                datos = JSON.parse(datos);

                if (datos.status) {
                    Toast.fire({
                        icon: 'success',
                        title: 'Guardando, redireccionando...',
                    });

                    location.replace('/mi/pedido/paso/3/')
                } else {

                    $("#contentLoader1").css('display', 'none');
                    sw_error(datos.message);

                }

            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var prct = (evt.loaded / evt.total) * 100;
                        prct = Math.round(prct);

                        $("#barra_progress_" + nombre_modulo).css({
                            "width": prct + '%'
                        });

                        $("#barra_progress_" + nombre_modulo).text(prct + "%");
                    }
                }, false);
                return xhr;
            },
            beforeSend: function () {
                $("#div_barra_progress_" + nombre_modulo).show();
                $("#barra_progress_" + nombre_modulo).css({
                    "width": '0%'
                });
                $("#barra_progress_" + nombre_modulo).text("0%");
            },
            complete: function () {
                $("#div_barra_progress_" + nombre_modulo).hide();
                $("#barra_progress_" + nombre_modulo).css({
                    "width": '0%'
                });
                $("#barra_progress_" + nombre_modulo).text("0%");
            },
            error: function (jqXhr) {

                $("#contentLoader1").css('display', 'none');

                comprobar_errores(jqXhr, nombre_modulo);

            }
        });

    } else {
        Toast.fire({
            icon: 'error',
            title: 'Seleccione su dirección y método de envío...',
        });
    }

}

function listar_metodos_envio() {

    $("#tabla_metodos_envio").html('<tr><td class="text-center" colspan="4"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></td></tr>');

    $.get("/pedido/listar/metodos/envio/", function (data, textStatus, jqXHR) {

        $("#tabla_metodos_envio").html(data);

    });
}

function listar_productos() {

    $('#lista_productos').html('');

    $.get("/pedido/listar/productos/list/", function (data) {
        $('#lista_productos_list').html(data);
    });

}

function calcular_totales() {

    $.get("/pedido/totales/", function(data) {

        data = JSON.parse(data);

        var sub_total_array         = data.productos_total_desc.toString().split(".");
        var envio_array             = data.metodo_envio_precio.toString().split(".");
        var pago_array              = data.metodo_pago_precio.toString().split(".");
        var total_array             = data.total.toString().split(".");

        $('#sub_total').html('S/ ' + sub_total_array[0] + '.<small>' + (sub_total_array[1] ? sub_total_array[1] : '00') + '</small>');
        $('#envio').html('S/ ' + envio_array[0] + '.<small>' + (envio_array[1] ? envio_array[1] : '00') + '</small>');
        $('#pago').html('S/ ' + pago_array[0] + '.<small>' + (pago_array[1] ? pago_array[1] : '00') + '</small>');
        $('#total').html('S/ ' + total_array[0] + '.<small>' + (total_array[1] ? total_array[1] : '00') + '</small>');
    });
}

function guardar_editar_direcciones(e) {

    crud_guardar_modal(
        e,
        '/cliente/direcciones/guardar',
        'direcciones',
        function(){ limpiar_direcciones(); },
        function(){ verificarDirecciones(); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_direcciones() {
    $("#direciones_id").val('');
    $("#direccion").val('');
    $("#referencia").val('');
    $("#coordenadas").val('');
}


function verificarDirecciones(){

    $.get("/tienda/direcciones/listar/select", function(data, textStatus, jqXHR){
        data = JSON.parse(data);

        if(data.length == 0){
            $("#modal_direcciones").modal('show');
            $("#select_modal_direcciones").prop('disabled', 'disabled');
        } else {
            $("#select_modal_direcciones").removeAttr("disabled");
        }

    });

}

function selectMetodo(idMet){

    $("#metodos_envio_" + idMet).prop('checked', true);

    sessionStorage.setItem('env', idMet);

    Toast.fire({
        icon: 'success',
        title: 'Metodo de envio seleccionado',
    })

}

paso_dos();

