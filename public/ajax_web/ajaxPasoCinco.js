


function paso_cinco() {

    listar_productos();
    calcular_totales();
}


function listar_productos() {

    $('#lista_productos').html('');

    $.get("/pedido/listar/productos/final/", function (data) {
        $('#lista_productos').html(data);
    });

}

function calcular_totales() {

    $.get("/pedido/totales/", function(data) {

        data = JSON.parse(data);

        var sub_total_array = roundTwo(data.productos_total_desc).split(".")
        var envio_array = roundTwo(data.metodo_envio_precio).split(".")
        var pago_array = roundTwo(data.metodo_pago_precio).split(".")
        var total_array = roundTwo(data.total).split(".")

        $('#sub_total').html('S/  ' + sub_total_array[0] + '.<small>' + sub_total_array[1] + '</small>');
        $('#envio').html('S/  ' + envio_array[0] + '.<small>' + envio_array[1] + '</small>');
        $('#pago').html('S/  ' + pago_array[0] + '.<small>' + pago_array[1] + '</small>');
        $('#total').html('S/  ' + total_array[0] + '.<small>' + total_array[1] + '</small>');
    });
}



paso_cinco();
