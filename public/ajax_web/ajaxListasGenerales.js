function init(){

    listar_rubros_nav();
    lista_rubros_select();

}


function listar_rubros_nav(){
    $.get("filtros/lista/usuarios", function(data, status){
        // data = JSON.parse(data);
        $(".filaUser").remove();
        $.each(data, function(i, item){
             var filaUser = '<option class="filaUser" value="' + item.codigo + '">' + item.usuario + '</option>';
             $('#SelectUsuarios').append(filaUser);
        });
   });
}

function lista_rubros_select(url, nombre_modulo, id) {
    $.get(url, function(data, status){
        data = JSON.parse(data);

        $("#select_modal_" + nombre_modulo).html('');

        $.each(data, function(i, item){

            if (id == item.id) {
                var option = '<option  value="' + item.id + '" selected>' + item.nombre + '</option>';
            }else{
                var option = '<option  value="' + item.id + '">' + item.nombre + '</option>';
            }

            $('#select_modal_' + nombre_modulo).append(option);

        });

        $("#select_modal_" + nombre_modulo).val(null).trigger('change')
   });
}

function marcas(params) {

}

init();
