const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

function paso_cuatro() {
    listar_productos();
    calcular_totales();
    mostrar_facturacion();
    mostar_pedido();

    // $("#nro_documento").inputmask("99999999[999]");

    $("input[name='comprobante']").on('change', function () {
        cambiar_comprobante();
    });

    $("#cliente").keyup(function () {

        if ($(this).val().length > 0) {

            $("#view_cliente").text($(this).val());

        }else{

            $("#view_cliente").text('Sin definir...');
            $("#formulario_paso_4").addClass('was-validated');

        }

    });

    $("#nro_documento").keyup(function () {

        if ($(this).val().length > 0) {

            $("#view_nro_documento").text($(this).val());
            $("#nro_documento").attr('minlength', 11)

            if ($("#boleta").is(":checked")) {

                $("#nro_documento").attr('minlength', 8);
                $("#nro_documento").attr('maxlength', 11);
                // $("#nro_documento").inputmask("99999999[999]");
                // $("#nro_documento").inputmask("99999999[999]");

            }else if($("#factura").is(":checked")){
                $("#nro_documento").attr('minlength', 11);
                $("#nro_documento").attr('maxlength', 11);
                // $("#nro_documento").inputmask("99999999999");
            }

        }else{

            $("#view_nro_documento").text('Sin definir...');
            $("#formulario_paso_4").addClass('was-validated');

        }
    });

    $("#telefono").keyup(function () {

        if ($(this).val().length > 0) {
            $("#view_telefono").text($(this).val());
        }else{
            $("#view_telefono").text('Sin definir...');
            $("#formulario_paso_4").addClass('was-validated')
        }

    });

    $("#direccion").keyup(function () {

        if ($(this).val().length > 0) {
            $("#view_direccion").text($(this).val());
        }else{
            $("#view_direccion").text('Sin definir...');
            $("#formulario_paso_4").addClass('was-validated')
        }
    });

    $("#formulario_paso_cuatro").on("submit", function(e) {
        guardar_paso_cuatro(e);
    });

}

function guardar_paso_cuatro(e) {

    e.preventDefault();

    var formData = new FormData($("#formulario_paso_cuatro")[0]);

    var nombre_modulo = 'paso_cuatro';

    $("#contentLoader1").css('display', 'block');

    $.ajax({
        url: '/pedido/guardar/paso/4',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            datos = JSON.parse(datos);

            if(datos.status == 'error'){

                $("#contentLoader1").css('display', 'none');

                Toast.fire({
                    icon: 'error',
                    title: datos.message,
                });

            } else if (datos.status) {
                Toast.fire({
                    icon: 'success',
                    title: 'Guardando, redireccionando...',
                });

                // location.replace('/cliente/historial')
                location.replace('/pedido/guardar/paso/fin')

            } else {
                $("#contentLoader1").css('display', 'none');

                var errors = JSON.parse(datos);

                sw_error(errors.message, 5000);
            }

        },
        xhr: function () {
            var xhr = new window.XMLHttpRequest();

            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var prct = (evt.loaded / evt.total) * 100;
                }
            }, false);

            return xhr;
        },
        beforeSend: function () {},
        complete: function () {},
        error: function (jqXhr) {

            $("#contentLoader1").css('display', 'none');

            comprobar_errores(jqXhr, nombre_modulo);

        }
    });
}

function listar_productos() {

    $('#lista_productos').html('');

    $.get("/pedido/listar/productos/final/", function (data) {
        $('#lista_productos').html(data);
    });

}

function mostar_pedido() {

    $.get("/pedido/mostrar/pedido/carrito", function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        //Empresa
        if(data.modelo_empresa.icono){
            $('#imgEmpresaPedido4').attr('src','https://tiendas.ceamarket.com/img/' + data.modelo_empresa.icono);
        } else {
            $('#imgEmpresaPedido4').attr('src','/img/default-store.png');
        }

        if(data.modelo_empresa.razon_social){
            $("#urlEmpresaPedido4").text(data.modelo_empresa.razon_social);
        } else {
            $("#urlEmpresaPedido4").text(data.modelo_empresa.nombre_comercial);
        }

        $("#urlEmpresaPedido4").attr('href',"/store/" + data.modelo_empresa.slug);

        $("#rubroEmpresaPedido4").text(data.modelo_empresa.modelo_rubro.nombre);

    });
}

function mostrar_facturacion() {

    $.get("/pedido/mostrar/facturacion", function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        // var comprobante = '';

        // if (data.comprobante == 1) {
        //     comprobante = 'Boleta';

        //     $("#boleta").prop('checked', true);
        //     $("#factura").prop('checked', false);

        // }else if (data.comprobante == 2) {
        //     comprobante = 'Factura';

        //     $("#boleta").prop('checked', false);
        //     $("#factura").prop('checked', true);

        // }

        $("#view_comprobante").text('Boleta');
        $("#view_cliente").text(data.cliente);
        $("#view_nro_documento").text(data.nro_documento);
        $("#view_telefono").text(data.telefono);
        $("#view_direccion").text(data.direccion);

        $("#cliente").val(data.cliente);
        $("#nro_documento").val(data.nro_documento);
        $("#telefono").val(data.telefono);
        $("#direccion").val(data.direccion);

    });

}

function cambiar_comprobante() {

    if ($("#boleta").is(":checked") &&  $("#cliente").val().length == 0) {
        mostrar_facturacion();
    } else if($("#factura").is(":checked")){
        $("#view_comprobante").text('Factura');
    }


    var nro_documento = $("#nro_documento").val();

    if (nro_documento.length == 8) {

        //
        $("#view_cliente").text('Sin Definir');
        $("#view_nro_documento").text('Sin Definir');
        $("#view_telefono").text('Sin Definir');
        $("#view_direccion").text('Sin Definir');

        $("#cliente").val('');
        $("#nro_documento").val('');
        $("#telefono").val('');
        $("#direccion").val('');


    }

}

function calcular_totales() {

    $.get("/pedido/totales/", function(data) {

        data = JSON.parse(data);

        var sub_total_array         = data.productos_total_desc.toString().split(".");
        var envio_array             = data.metodo_envio_precio.toString().split(".");
        var pago_array              = data.metodo_pago_precio.toString().split(".");
        var total_array             = data.total.toString().split(".");

        $('#sub_total').html('S/ ' + sub_total_array[0] ? sub_total_array[0] : '0' + '.<small>' + sub_total_array[1] ? sub_total_array[1] : '00' + '</small>');
        $('#envio').html('S/ ' + envio_array[0] + '.<small>' + envio_array[1] ? envio_array[1] : '00' + '</small>');
        $('#pago').html('S/ ' + pago_array[0] + '.<small>' + pago_array[1] ? pago_array[1] : '00' + '</small>');
        $('#total').html('S/ ' + total_array[0] + '.<small>' + total_array[1] ? total_array[1] : '00' + '</small>');
    });
}


paso_cuatro();
