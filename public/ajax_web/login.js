var errores_list = [];

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer);
      toast.addEventListener('mouseleave', Swal.resumeTimer);
    }
  });


function login() {

    $("#formulario_registar_cliente").on("submit", function(e) {
        registar_cliente(e);
    });

    $("#formulario_login").on("submit", function(e) {
        loguearse(e);
    });

    $("#ruc_dni").on('keyup', function () {

        sessionStorage.setItem('dni', '');
        sessionStorage.setItem('name', '');
        sessionStorage.setItem('lastname', '');

        $("#nombres").val('');
        $("#apellidos").val('');

    });

    $("#nombres").on('keyup', function(){

        $(this).val(sessionStorage.getItem('name'));

        // $(this).attr("disabled", true);

    });

    $("#apellidos").on('keyup', function(){

        $(this).val(sessionStorage.getItem('lastname'));

        // $(this).attr("disabled", true);

    });

}

function consultaRuc(){

    var dni = $("#ruc_dni").val();

    if(dni.length == 8){

        $.get("cliente/buscar/dni/" + dni, function(data){

            data = JSON.parse(data);

            if(data.error){

                Toast.fire({
                    icon: 'error',
                    title: 'No de ha encontrado el DNI ingresado.',
                });

            } else {

                sessionStorage.setItem('dni', data.dni);
                sessionStorage.setItem('name', data.nombres);
                sessionStorage.setItem('lastname', data.apellido_paterno + ' ' + data.apellido_materno);

                // $("#nombres").attr("disabled", true);
                // $("#apellidos").attr("disabled", true);

                $("#nombres").val(data.nombres);
                $("#apellidos").val(data.apellido_paterno + ' ' + data.apellido_materno);

            }

        });

    } else {

        Toast.fire({
            icon: 'error',
            title: 'DNI inválido',
        });

    }

}

function loguearse(e) {

    e.preventDefault();

    var formData = new FormData($("#formulario_login")[0]);

    $.ajax({
        url: '/loguearse',
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function (datos) {
            datos = JSON.parse(datos);
            if (datos.status) {

                Swal.fire({ title: "Redireccionando...", timer: 2000, icon: "success" });

                if (datos.redirect === '') {
                    location.reload();
                }else{
                    location.replace(datos.redirect)
                }

                // location.reload();
            }else{
                Swal.fire({ title: "Error", text: datos.message, timer: 2000, icon: "error" });
            }
        },
        xhr: function () {
            var xhr = new window.XMLHttpRequest();

            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {

                    var prct = (evt.loaded / evt.total) * 100;

                    $("#barra_progress_login").css({ "width": prct + '%' });

                    $("#barra_progress_login").text(prct + "%");

                    if (prct === 100) {
                        setTimeout(function(){ reniciar_barra('login') }, 600);
                    }
                }
            }, false);
            return xhr;
        },
        error: function (jqXhr) {
            comprobar_errores(jqXhr, 'login');
        }
    });
}

function registar_cliente(e) {
    e.preventDefault();

    if($("#aceptarTerminosCondiciones").prop('checked')){

        var formData = new FormData($("#formulario_registar_cliente")[0]);

        $.ajax({
            url: '/registar/cliente',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function (datos) {
                datos = JSON.parse(datos);
                if (datos.status) {

                    Swal.fire({ title: "Exito", timer: 2000, icon: "success" });

                    location.reload();

                }else{
                    Swal.fire({ title: "Error", text: datos.message, timer: 2000, icon: "error" });
                }
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {

                        var prct = (evt.loaded / evt.total) * 100;

                        $("#barra_progress_registar_cliente").css({ "width": prct + '%' });

                        $("#barra_progress_registar_cliente").text(prct + "%");

                        if (prct === 100) {
                            setTimeout(function(){ reniciar_barra("registar_cliente") }, 600);
                        }
                    }
                }, false);
                return xhr;
            },
            error: function (jqXhr) {

                comprobar_errores(jqXhr, 'registar_cliente');

            }
        });

    } else {
        Toast.fire({
            icon: 'error',
            title: 'Debe aceptar los Términos y Condiciones para continuar.',
        })
    }

}

login()
