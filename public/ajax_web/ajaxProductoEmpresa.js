var categoriaActiva = '';

// const Toast = Swal.mixin({
//     toast: true,
//     position: 'bottom-end',
//     showConfirmButton: false,
//     timer: 4000,
//     timerProgressBar: true,
//     onOpen: (toast) => {
//       toast.addEventListener('mouseenter', Swal.stopTimer)
//       toast.addEventListener('mouseleave', Swal.resumeTimer)
//     }
// });

$(window).scroll(function () {
    //bind scroll event
    var bottom_margin = 300;
    var height = $(document).height() - $(window).height() - bottom_margin;
    var max = height + 147 ;

    if ($(window).scrollTop() >= height && $(window).scrollTop() < max ) {
        console.log('LISTAR PRODUCTOS');
    }
});

function productos_empresa(){

    $('#select_modal_rubro').select2({
        theme: 'bootstrap4',
        width: 'style',
        placeholder: 'Seleccione un rubro',
        allowClear: true
    });

    $("#buscarEnTienda").keypress(function(e) {
        var slug = $("#empresa_slug").val();
        if(e.which == 13) {
            buscarProd(slug);
        }
    });

    mostrarProductosSinCategoria(0);

    var direccion = $("#direccion").val();
    var coordenadas = $("#coordenadas").val();

    // cargar_mapa_direccion("map", direccion, coordenadas);

}


function limpiar_rubro() {
    $('#nombre_rubro').val('');
}


function mostrar_tienda_detalle(slug){

    $("#InformacionTienda").modal('show');

    $("#detalleTienda").html('<div class="modal-body lateral bg-faded-success"> <div class="col-12 px-2 mb-4"> <div class="row justify-content-center my-5"> <i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i> </div> </div> </div>');

    $.get("/store/informacion/"+ slug, function (data, status) {

        $('#detalleTienda').html(data);

    });

}

function mostrar_tienda_valor(slug){

    $("#ResenasTienda").modal('show');

    $("#detalle_resena").html('<div class="modal-body lateral bg-faded-success"> <div class="col-12 px-2 mb-4"> <div class="row justify-content-center my-5"> <i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i> </div> </div> </div>');

    $.get("/store/resenas/"+ slug, function (data, status) {

        $('#detalle_resena').html(data);

    });

}


function mostrarProductosSinCategoria(origen){

    $("#mostrarTodos").addClass('seleccionAsside');

    $("#NombreSeccionCat").css("display", "block");
    $("#botonVerMas").css("display", "block");

    if (categoriaActiva) {
        $("#" + categoriaActiva).removeClass('seleccionAsside');
    }

    var idEmp = $("#empresa_id").val();

    $("#prodsCat").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $('#buscarEnTienda').val('');

    $.get("/tienda/productos/categoria/" + idEmp + "/0?page=1", function (data, status, jqXHR) {

        if (data.length == 0) {
            $("#botonVerMas").css('display', 'none');
            ("#borrarCarga").remove();
        } else {

            $('#prodsCat').html(data);

            $("#NombreSeccionCat").html('<h3 class="h5 mb-0 pt-3 mr-3">Todos los Productos</h3>');
            $("#NombreCat").val('Todos los Productos');
            $("#CatProdId").val('0');
            $("#Page").val('1');

            if(origen != 0){
                $('html, body').animate({
                    scrollTop: $('#prodsCat').offset().top - 90
                }, 1000);
            }

        }

    });

}

function mostrarProductosCategoria(nombre, idCat){

    $("#mostrarTodos").removeClass('seleccionAsside');

    $("#NombreSeccionCat").css("display", "block");
    $("#botonVerMas").css('display', 'none');

    if (categoriaActiva) {
        $("#" + categoriaActiva).removeClass('seleccionAsside');
    }

    $("#" + nombre + '_'+ idCat).addClass('seleccionAsside');

    categoriaActiva = nombre + '_'+ idCat;

    console.log($("#" + nombre + '_'+ idCat));

    var idEmp = $("#empresa_id").val();

    $("#prodsCat").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $('#buscarEnTienda').val('');

    $.get("/tienda/productos/categoria/" + idEmp + "/" + idCat + "?page=1", function (data, status, jqXHR) {

        if (data.length == 0) {
            $("#botonVerMas").css('display', 'none');
            ("#borrarCarga").remove();
        } else {

            $('#prodsCat').html(data);

            $("#NombreSeccionCat").html('<h3 class="h5 mb-0 pt-3 mr-3">' + nombre + '</h3>');
            $("#NombreCat").val(nombre);
            $("#CatProdId").val(idCat);
            $("#Page").val("1");

            $('html, body').animate({
                scrollTop: $('#prodsCat').offset().top - 90
            }, 1000);

        }

    });

}

function mostrarProductosCategoriaPage(){

    $("#NombreSeccionCat").css("display", "block");
    $("#botonVerMas").css("display", "block");

    var idEmp = $("#empresa_id").val();
    var idCat = $("#CatProdId").val();
    var nombre = $("#NombreCat").val();

    var p = $("#Page").val();
    var page = parseInt(p) + 1;

    $("#prodsCat").append('<div class="row justify-content-center m-5 p-4" id="borrarCarga"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $('#buscarEnTienda').val('');

    if(idCat != 0){
        $.get("/tienda/productos/categoria/" + idEmp + "/" + idCat + "?page=" + page, function (data, status, jqXHR) {

            if (data.length == 0) {
                $("#botonVerMas").css('display', 'none');
                $("#borrarCarga").remove();
            } else {
                $("#borrarCarga").remove();
                $('#prodsCat').append(data);

                $("#NombreSeccionCat").html('<h3 class="h5 mb-0 pt-3 mr-3">' + nombre + '</h3>');
                $("#NombreCat").val(nombre);
                $("#CatProdId").val(idCat);
                $("#Page").val(page);

                $("html, body").animate({ scrollTop: $(window).scrollTop() - 600 }, "slow");

            }

        });
    } else {
        $.get("/tienda/productos/categoria/" + idEmp + "/0" + "?page=" + page, function (data, status, jqXHR) {


            if (data.length == 0) {
                $("#botonVerMas").css('display', 'none');
                $("#borrarCarga").remove();
            } else {
                $("#borrarCarga").remove();
                $('#prodsCat').append(data);

                $("#NombreSeccionCat").html('<h3 class="h5 mb-0 pt-3 mr-3">' + nombre + '</h3>');
                $("#NombreCat").val(nombre);
                $("#CatProdId").val(idCat);
                $("#Page").val(page);

                $("html, body").animate({ scrollTop: $(window).scrollTop() - 600 }, "slow");

            }

        });
    }

}

function buscarProd(slug){

    var buscar = $('#buscarEnTienda').val();

    if($.trim(buscar) != ''){

        $("#NombreSeccionCat").css("cssText", "display: none !important");
        $("#botonVerMas").css('display', 'none');

        $("#prodsCat").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

        $.get("/buscar/productos/empresa/" + slug + "/" + buscar, function (data, status, jqXHR) {
            $('#prodsCat').html(data);
            $('html, body').animate({
                scrollTop: $('#prodsCat').offset().top - 80
            }, 1000);
        });
    } else {
        Toasts.fire({
            icon: 'error',
            title: 'Ingrese un texto a buscar',
        });
    }

}

function limpiarBuscado(){
    $('#buscarEnTienda').val('');
    $("#contenidoEmpresa").show();
    $('#productosBuscados').html('');

    $('body,html').animate({
        scrollTop : 0
    }, 500);
}



productos_empresa();
