
function recover() {
    
    $("#formulario_recover").on('submit', function (e) {
        
        recuperar_cuenta(e);
    });
}

function recuperar_cuenta(e) {
    
    e.preventDefault();
    var formData = new FormData();

    formData.append('_token', $("input[name='_token']").val());
    formData.append('email',  $("input[name='email']").val());
    
    $.ajax({
        url: "/recover",
        type: "POST",
        data: formData,
        dataType: "json",
        contentType: false,
        processData: false,
        success: function (datos) {
            console.log(datos)
        }
    });
}


recover();