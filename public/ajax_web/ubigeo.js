function index(){

    $("#formulario_ciudad").on("submit", function(e) {
        guardar_editar_ciudades(e);
    });

}

function guardar_editar_ciudades(e) {

    crud_guardar_editar(
        e,
        '/cliente/ciudad/agregar',
        'ciudad',
        function(){ console.log('Console limpiar'); },
        function(){ listar(); },
        function(){ console.log('Console Error'); }

    );

}

function listar(){
    setTimeout(function() {
        location.reload();
    }, 1000);
}

function listarSelect() {

    $('#select_modal_ciudad').select2({
        theme: 'bootstrap4',
        width: 'style',
        placeholder: 'Seleccione una ciudad',
        allowClear: true,
        ajax: {
            url: function(params) {
                return '/lista/ciudades/select/?filter=3';
            },
            dataType: 'json',
            delay: 250,
            data: function (params) {

                return {
                    term: params.term || '',
                    page: params.page || 1
                }
            },
            processResults: function (data, params) {
                console.log(data)
                console.log('a: ' +params.page)
                console.log(params)
                params.page = params.page || 1;

                return {
                    results: data.results,
                    pagination: {
                        more: (params.page * 10) < data.total
                    }
                };
            },
            cache: true
        }
    });

}

function mostrarCiudades(){

    listarSelect();

    $.get("/mostrar/ubigeo/modal", function (data, status) {
        data = JSON.parse(data);

        $("#modal_ciudad").modal('show');

        if (data.status) {

            $("#select_modal_ciudad").select2("trigger", "select", {
                data: { id: data.ciudad, text: data.nombre  }
            });

            $("#textoRefrencia").text(data.texto);

        } else {

            $("#textoRefrencia").text('');

        }

    });

}

function cambioCiudad(){

    var id = $('#select_modal_ciudad').val();

    if (id) {

        $.get("/mostrar/texto/ciudad/" + id, function (data, status) {

            $("#textoRefrencia").html(data);

        });

    }

}



index();
