var modal_lv = 0;

$('.modal').on('shown.bs.modal', function (e) {
    $(document).off('focusin.modal');
    $('.modal-backdrop:last').css('zIndex',1051+modal_lv);
    $(e.currentTarget).css('zIndex',1052+modal_lv);
    modal_lv++
});

$('.modal').on('hidden.bs.modal', function (e) {
    modal_lv--
});

const Toasts = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 4000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});

function ir_a(name) {
    $('html, body').animate({
        scrollTop: $(name).offset().top - 60
    }, 1000);
}

(function ($) {
    "use strict";

    // Add active state to sidbar nav links
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path

    $("#aside_admin .list-unstyled a.nav-link-style").each(function () {

        if(path.substr(path.length-1) == "#"){
            path = path.substr(0,path.length-1)
        }

        //console.log(path)

        if (this.href === path) {
            $(this).addClass("active");
        }
    });

    $("#aside_client .list-unstyled a.nav-link-style").each(function () {

        if(path.substr(path.length-1) == "#"){
            path = path.substr(0,path.length-1)
        }

        //console.log(path)

        if (this.href === path) {
            $(this).addClass("active");
        }
    });

})(jQuery);

function init(){

    $("#Buscador").keypress(function(e) {
        if(e.which == 13) {
            buscartext();
        }
    });

    $("#Buscadormovil").keypress(function(e) {
        if(e.which == 13) {
            buscartextmovil();
        }
    });

    cargar_preguntas_footer();

    cargar_redes_pagina();

}



function mostrar_producto_detalle(id){

    $("#DetalleProducto").modal('show');

    $("#detailProd").html('<div class="row justify-content-center my-5"><i class="fas fa-spinner fa-pulse fa-6x" aria-hidden="true"></i></div>');

    $.get("/store/detalle/producto/"+ id, function (data, status) {

        $('#detailProd').html(data);

    });

}

function cambiar_img_det(ruta){
    $('#img_producto_detalle').attr('src','https://tiendas.ceamarket.com/img/' + ruta);
    $('#img_producto_detalle').attr('data-zoom','https://tiendas.ceamarket.com/img/' + ruta);
}

// $('#Buscador').autocomplete({
//     source: function(request, response){
//         $.ajax({
//             url: '/buscar/productos',
//             dataType: 'json',
//             data: {
//                 term: request.term
//             },
//             success: function(data){
//                 response(data);
//             }
//         })
//     },
//     minlenght: 1,
//     select: function(event, ui){
//         location.replace('/buscador/'+ui.item.label);
//     }
// });

function buscartext(){

    var label = $('#Buscador').val();

    if($.trim(label) != ''){
        location.replace('/buscador/'+label);
    } else {
        Toasts.fire({
            icon: 'error',
            title: 'Ingrese un texto a buscar',
        });
    }

}

function buscartextmovil(){

    var label = $('#Buscadormovil').val();

    if($.trim(label) != ''){
        location.replace('/buscador/'+label);
    } else {
        Toasts.fire({
            icon: 'error',
            title: 'Ingrese un texto a buscar',
        });
    }

}

function modalAyudanos(){

    $("#modal_sugerencia").modal('show');

}

function guardar_telefono(e) {

    crud_guardar_editar(
        e,
        '/cliente/telefono/guardar',
        'telefono',
        function(){ limpiar_telefonos(); },
        function(){ console.log('Console Limpiar'); },
        function(){ console.log('Console Error'); }

    );

}

function limpiar_telefonos(){
    $("#telefono").val('');
}

function mostrar_qr_pedido(codigo){

    $("#modal_qr").modal('show');

    $("#QRCodeImg").html('');

    $("#QRCodeImg").qrcode({
        width: 200,
        height: 200,
        text: codigo
    });

}

function cargar_preguntas_footer(){

    $.get('/mostrar/preguntas', function(data){
        data = JSON.parse(data);

        $.each(data, function(i, item){
            var list =  '<li class="widget-list-item">'
                            + '<a class="widget-list-link" href="/preguntas_frecuentes/' + item.slug + '" target="_blank">'
                                + item.pregunta
                            + '</a>'
                        + '</li>';

            $('#mostrar_preg_frec').append(list);
       });

    });

}





function cargar_redes_pagina(){

    $.get('/inicio/redes/sociales', function(data){

        $("#redes_pagina").html(data);

    });

}

init();
