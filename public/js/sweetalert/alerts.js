function msjSwa(head, body, type, tiempo){
     swal({
          title: head,
          text: body,
          type: type,
          timer: tiempo,
          showConfirmButton: false
     });
};

function msjConfirm(head, body, type, tiempo){
     swal({
          title: head,
          text: body,
          type: type,
          timer: tiempo,
          showConfirmButton: true
     });
};

function msjTextConfirm(head, body, type, placeholder, textError, headComplete, bodyComplete, typeComplete){
     swal({
          title: head,
          text: body,
          type: type,
          showCancelButton: true,
          closeOnConfirm: false,
          inputPlaceholder: placeholder
     }, function (inputValue) {
          if (inputValue === false) return false;
          if (inputValue === "") {
               swal.showInputError(textError);
               return false
          }
          swal(headComplete, bodyComplete + inputValue, typeComplete);
     });
}

function msjConfirmAction(head, body, type, btnConfirmtxt, btnCanceltxt, action, title, desc, tipo){
     funcion = action;
     swal({
          title: head,
          text: body,
          type: type,
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: btnConfirmtxt,
          cancelButtonText: btnCanceltxt,
          closeOnConfirm: true,
          closeOnCancel: false
     }, function(isConfirm){
          if (isConfirm) {
               funcion();
          } else {
               msjConfirm(title, desc, tipo, danger_time);
          }
     });
}

function msjConfirmSesion(head, body, type, btnConfirmtxt, btnCanceltxt, title, desc, tipo){
     swal({
          title: head,
          text: body,
          type: type,
          showCancelButton: true,
          confirmButtonClass: "btn-danger",
          confirmButtonText: btnConfirmtxt,
          cancelButtonText: btnCanceltxt,
          closeOnConfirm: true,
          closeOnCancel: true
     }, function(isConfirm){
          if (isConfirm) {
               $("#modal_sesion").modal('show');
          }
     });
}

function msjDesactivar(title, type, confirm, cancel, ruta, id, tabla, titlesuccess, titleerror, titlecancel){
     swal({
               title: title,
               type: type,
               showCancelButton: true,
               confirmButtonColor: "#DD6B55",
               confirmButtonText: confirm,
               cancelButtonText: cancel,
               closeOnConfirm: false,
               closeOnCancel: false
          },
          function(isConfirm) {
               if (isConfirm) {
               $.get( ruta + id,
               function(data, status) {
                    data = JSON.parse(data);

                    if (data == 1) {
                         swal({
                              title: titlesuccess,
                              timer: 2000,
                              type: "success"
                         });
                    } else {
                         swal({
                              title: titleerror,
                              timer: 2000,
                              type: "error"
                         });
                    }
                    tabla.ajax.reload();
               });
               } else {
                    swal({
                         title: titlecancel,
                         timer: 2000,
                         type: "error"
                    });
               }
          });
}

function msjLoading(){
     //
}
