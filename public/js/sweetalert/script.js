function onlyNumberTxt(e) {
     key = e.keyCode || e.which;
     teclado = String.fromCharCode(key);
     numeros = "0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
     especiales = "8-37-38-39-46-16";   //array
     tecladoEspecial = false;
     for (var i in especiales) {
          if (key == especiales[i]) {
               tecladoEspecial = true;
          }
     }
     if (numeros.indexOf(teclado) == -1 && !tecladoEspecial) {
          return false;
     }
}

function onlyNumber(e) {
     key = e.keyCode || e.which;
     teclado = String.fromCharCode(key);
     numeros = "0123456789.";
     especiales = "8-37-38-39-46-16";   //array
     tecladoEspecial = false;
     for (var i in especiales) {
          if (key == especiales[i]) {
               tecladoEspecial = true;
          }
     }
     if (numeros.indexOf(teclado) == -1 && !tecladoEspecial) {
          return false;
     }
}

//SweetAlert
//Botones
var btnCanceltxt = "Cancelar";
var btnConfirmtxt = "Aceptar";
var btnConfirmSesion = "Iniciar Sesion";
//Tiempo alerts estandar
var success_time = 2000;
var success_time_pos = 4000;
var danger_time = 7000;
var error_time = 10000;
//Success --------
//sh1 = succes + head + correlativo
var sh1 = "Felicidades!";


var sb1 = "La operación se realizo correctamente." ;
var sb2 = "El credito fue despachado correctamente.";

//ih1 = info + head + correlativo
//Info
var ih1 = "Verificar!";

var ib1 = "El Cliente no esta registrado.";
var ib2 = "El Cliente no tiene crédito o su crédito fue anulado.";

//Warning
var wh1 = "¿Esta seguro de realizar esta operación?";

var wb1 = "Si usted desactiva, no podra hacer ninguna operación con este registro";
var wb2 = "Usted esta apunto de activar este registro.";
var wb3 = "Si usted acepta, el crédito sera despachado bajo su responsabilidad.";
var wb4 = "Si usted elimina este registro, no podrá recuperarlo.";

//Danger
var dh1 = "Operación cancelada";
var dh2 = "Ocurrio un error!";
var dh3 = "Acceso denegado!";

var dh4 = "¿Aun no tiene cuenta?";
var dh5 = "¿Olvidó su contraseña?";


var db1 = "Su operación fue cancelada, no se hará ningun cambio en este registro.";
var db2 = "No se pudo realizar su operación, intentelo nuevamente o comuniquese con Soporte.";
var db3 = "Usuario y/o contraseña incorrecto, verifique sus datos e intentelo nuevamente.";

var db4 = "comuniquese con soporte para habilitar su usuario y contraseña.";
var db5 = "comuniquese con soporte para recuperar su contraseña o para obtener una nueva.";
var db6 = "ingrese los datos requeridos para buscar su comprobante.";
var db7 = "verifique que los datos ingresados sean los correctos.";
var db8 = "Debe tener una cuenta para poder realizar esta acción.";
