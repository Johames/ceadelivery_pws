<?php

// Route::get('/', 'empresa\EmpresaController@getIndexMantenimiento')->name('Inicio');

Route::get('/', 'empresa\EmpresaController@getIndex')->name('Inicio');

Route::get('/test-empresas', 'empresa\EmpresaController@getTest');


Route::get('tienda/rubro/listar/prueba', 'empresa\EmpresaController@getTest');


Route::post('/loguearse', 'Auth\LoginController@LogIn');
Route::post('/cerrar_sesion', 'Auth\LoginController@LogOut')->name('LogOut');

Route::get('cliente/buscar/dni/{dni}', 'usuario\ConsultasController@consultarDNI');
Route::get('store/cliente/buscar/dni/{dni}', 'usuario\ConsultasController@consultarDNI');

Route::post('/registar/empresa', 'usuario\UsuarioController@postRegistarEmpresa');
Route::post('/registar/cliente', 'usuario\UsuarioController@postRegistarCliente');

//OK
Route::get('/recover', 'cuenta\CuentaController@getViewRecuperarCuenta');
Route::post('/recover', 'cuenta\CuentaController@postRecuperarCuenta');

//OK
Route::get('/recover/codigo', 'cuenta\CuentaController@getRecuperarCuentaCodigo');
//OK
Route::post('/recover/codigo/validate', 'cuenta\CuentaController@postValidarCodigo');


Route::get('/recover/reset', 'cuenta\CuentaController@getResetCuenta');
Route::post('/recover/reset/validate', 'cuenta\CuentaController@postResetCuentaSave');



Route::get('/ver/blogs', 'blogs\BlogsController@getIndex')->name('Blogs');
Route::get('/ver/detalle/blog/{idBlog}', 'blogs\BlogsController@getBlog');

Route::get('inicio/redes/sociales', 'empresa\EmpresaController@getRedesPagina');


// php artisan cache:clear, php artisan config:clear, php artisan config:cache



// Route::post('/recover/reset', 'cuenta\CuentaController@postRecuperarCuenta');

// Route::get('/recover/test', 'cuenta\CuentaController@getTest');


Route::get('/usuarios_prueba', 'usuario\UsuarioController@getCrearUsuariosPrueba');
Route::get('/rubro_test/{nombre}', 'usario\UsuarioController@getRubroTest');
Route::get('/rubro_tests/nombre', 'empresa\EmpresaController@prueba');


Route::get('buscar/productos', 'empresa\EmpresaController@buscador');
Route::get('tienda/productos/categoria/{idEmp}/{idCat?}', 'empresa\EmpresaController@getProductosCategoria');
Route::get('/buscador/{text?}', 'empresa\EmpresaController@getIndexBuscador');


// Route::get('/crear_mi_rol/{rol}','usuario\UsuarioController@getCrearRol')->middleware('permission.eliminar');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/crear_rol/{rol}', 'usuario\UsuarioController@getCrearRol');
Route::get('/asignar_rol/{rol}', 'usuario\UsuarioController@getAsignarRol');



// Registro de Empresa, Persona y Usuario
Route::post('registro/persona', 'cliente\ClienteController@AgregarCliente');
Route::post('registro/tienda', 'empresa\EmpresaController@AgregarEmpresa');
Route::post('register/usuario', 'usuario\UsuarioController@registrar');

/* ------------------------------------- Rutas Generales ------------------------------------- */
// Rutas generales de Tiendas
Route::get('/rubro/{slug}', 'empresa\EmpresaController@getIndexRubro');

Route::get('/store/{slug}', 'empresa\EmpresaController@getIndexProductosEmpresa');
Route::get('/buscar/productos/empresa/{slug}/{texto}', 'empresa\EmpresaController@buscarProductosEmpresa');
// Route::get('/store/{slug}/categoria/{cat?}', 'empresa\EmpresaController@getIndexProductosCategoriaEmpresa');


Route::get('store/informacion/{slug}', 'empresa\EmpresaController@getTiendaDetalle');
Route::get('store/resenas/{slug}', 'empresa\EmpresaController@getTiendaResena');

Route::get('store/detalle/producto/{id}', 'producto\ProductoController@getProductoDetalle');




Route::get('preguntas_frecuentes', 'preguntas\PreguntasController@getIndexPreguntas');
Route::get('preguntas_frecuentes/{slug}', 'preguntas\PreguntasController@getPreguntas');
Route::get('mostrar/preguntas', 'preguntas\PreguntasController@mostrarPreguntas');



Route::post('agregar/sugerencia', 'preguntas\SugerenciasController@agregarSugerencia');




Route::middleware(['auth'])->group(function () {

    Route::group(['middleware' => ['role:cliente']], function () {

        // Route::get('/inicio', 'empresa\EmpresaController@getIndexClientes')->name('Index');

        Route::get('/verificar', 'cuenta\CuentaController@getVerificarCuentaIndex');

        Route::post('/verificar/comprobar', 'cuenta\CuentaController@postVerificarCuenta');

        Route::get('/verificar/link/{codigo}', 'cuenta\CuentaController@getVerificarCuenta');

        Route::get('/verificar/reenviar', 'cuenta\CuentaController@getReenviarVerificarCuenta');


        Route::post('/cliente/ciudad/agregar', 'cliente\ClienteController@AgregarCiudad');

        Route::get('lista/ciudades/select', 'cliente\ClienteController@getSelectUbigeo');
        Route::get('mostrar/ubigeo/modal', 'cliente\ClienteController@mostrarModalUbigeo');
        Route::get('mostrar/texto/ciudad/{id}', 'cliente\ClienteController@getUbigeoForHumans');

        /* ------------------------------------- Rutas para los clientes ------------------------------------- */
        // Rutas para las Notificaciones
        Route::get('cliente/notificaciones', 'cliente\ClienteController@getIndexNotificaciones')->name('AdmNotificacionesClient');

        Route::get('cliente/listar/notificaciones', 'cliente\NotificacionesController@ListaNotificacionesCliente');
        Route::get('cliente/listar/notificaciones/navbar', 'cliente\NotificacionesController@NotificacionesClienteNavbar');
        Route::get('cliente/mostrar/notificacion/{id}', 'cliente\NotificacionesController@mostrarNotificaciones');

        // Rutas para el Historial de Compras
        Route::get('cliente/historial', 'cliente\ClienteController@getIndexHistorial')->name('AdmHistorialClient');

        Route::get('cliente/historial/compras', 'cliente\HistorialComprasController@ListaHistorial');
        Route::get('cliente/historial/compra/detalle/{id}', 'cliente\HistorialComprasController@DetalleHistorial');

        Route::get('cliente/discusion/pedido/{idHist}', 'cliente\HistorialComprasController@DiscussPedido');
        Route::get('cliente/discusion/pedido/detalle/{idHist}', 'cliente\HistorialComprasController@DiscussPedidoRespuestas');
        Route::post('cliente/discusion/pedido/guardar', 'cliente\HistorialComprasController@guardarReclamo');

        // Rutas para las tarjetas
        Route::get('cliente/tarjetas', 'cliente\ClienteController@getIndexTarjetas')->name('AdmMedioPagoClient');

        Route::get('cliente/tarjetas/listar', 'cliente\TarjetasController@listarTarjetas');
        Route::get('cliente/tarjetas/eliminar/{id}', 'cliente\TarjetasController@eliminarTarjetas');


        // Rutas de los Deseos
        Route::get('cliente/deseos', 'cliente\ClienteController@getIndexDeseos')->name('AdmDeseosClient');

        Route::get('cliente/listar/deseos', 'cliente\DeseosController@ListaDeseos');
        Route::get('cliente/listar/deseos/detalle/{id}', 'cliente\DeseosController@ListaDeseosDetalle');
        Route::get('cliente/mostrar/deseos/{id}', 'cliente\DeseosController@MostrarDeseos');
        Route::get('cliente/listar/deseos/select', 'cliente\DeseosController@ListaDeseosSelect');
        Route::post('cliente/deseos/guardar', 'cliente\DeseosController@postGuardarEditar');
        Route::post('cliente/deseos/producto/guardar', 'cliente\DeseosController@postGuardarProdDeseo');
        Route::get('cliente/deseos/eliminar/{id} ', 'cliente\DeseosController@eliminarListaDeseo');
        Route::get('cliente/deseos/producto/eliminar/{id}/{idDes} ', 'cliente\DeseosController@eliminarProdDeseo');


        // Rutas para las Calificaciones
        Route::get('cliente/calificaciones', 'cliente\ClienteController@getIndexCalificaciones')->name('AdmCalificacionesClient');

        Route::get('cliente/listar/calificaciones', 'cliente\CalificacionesController@getListarCalificaciones');

        Route::get('valoracion/empresa/mostar/{id}', 'empresa\ValoracionEmpresaController@datosEmpresa');
        Route::post('valoracion/empresa/agregar', 'empresa\ValoracionEmpresaController@AgregarModificarResenaEmp');

        Route::get('valoracion/producto/mostar/{id}', 'producto\ValoracionProductoController@datosProducto');
        Route::post('valoracion/producto/agregar', 'producto\ValoracionProductoController@AgregarModificarResenaProd');

        Route::get('valoracion/deliverista/mostar/{id}', 'deliverista\ValoracionDeliveristaController@datosDeliverista');
        Route::post('valoracion/deliverista/agregar', 'deliverista\ValoracionDeliveristaController@AgregarModificarResenaDelivery');


        // Rutas para los CEA Puntos
        Route::get('cliente/ceapuntos', 'cliente\ClienteController@getIndexCeaPuntos')->name('AdmCeaPuntosClient');


        // Rutas del Perfil
        Route::get('cliente/perfil', 'cliente\ClienteController@getIndexPefil')->name('AdmPerfilClient');

        Route::get('cliente/mostrar/perfil', 'cliente\ClienteController@getMostarPerfilCliente');
        Route::post('cliente/editar/perfil', 'cliente\ClienteController@postEditarClientePerfil');
        Route::post('cliente/editar/perfil/avatar', 'cliente\ClienteController@postEditarClienteIcono');

        Route::post('cliente/telefono/guardar', 'cliente\ClienteController@postAgregarTelefono');


        // Rutas de las Direcciones
        Route::get('cliente/direcciones', 'cliente\ClienteController@getIndexDirecciones')->name('AdmDireccionesClient');

        Route::get('cliente/direcciones/listar', 'cliente\DireccionController@getListar');
        Route::get('cliente/direcciones/mostar/{id}', 'cliente\DireccionController@getMostar');
        Route::post('cliente/direcciones/guardar', 'cliente\DireccionController@postGuardarEditar');
        Route::get('cliente/direcciones/eliminar/{id}', 'cliente\DireccionController@getEliminar');
        Route::get('cliente/direcciones/desactivar/{id}', 'cliente\DireccionController@getDesactivar');
        Route::get('cliente/direcciones/activar/{id}', 'cliente\DireccionController@getActivar');
        Route::get('cliente/direcciones/principal//{id}', 'cliente\DireccionController@getPrincipal');

        Route::get('tienda/direcciones/listar/select', 'cliente\DireccionController@getListarSelect');


        /* ------------------------------------- Rutas para las Empresas ------------------------------------- */
        // Rutas para el Carrito y el Pedido
        Route::get('pedido/listar/carrito/modal', 'venta\CarritoController@getListarProductosModal');

        Route::get('pedido/listar/carrito/nav', 'venta\CarritoController@getListarProductosNav');

        Route::get('redireccionar/pedido/{codigo}', 'venta\PedidoController@getRedirectPedido');
        Route::get('mi/pedido/paso/{nro_paso}', 'venta\PedidoController@getIndexPaso');

        Route::post('pedido/constancia/guardar', 'venta\CarritoController@agregarConstancia');

        // Agregar producto al carrito
        Route::get('add/carrito/producto/{id}/{cant}', 'venta\CarritoController@postAddCarrito');

        Route::get('pedido/totales', 'venta\CarritoController@getPedidoTotales');

        //PASO 1
        // Guardar
        Route::post('pedido/guardar/paso/1', 'venta\CarritoController@postGuardarPaso1');
        //Listar productos en el inicio del carrito - pedido
        Route::get('pedido/listar/productos', 'venta\CarritoController@getListarProductosPedido');

        //Editar la contidad del producto del carrtio
        Route::get('pedido/editar/producto/{detalle_id}/{cantidad}', 'venta\CarritoController@getEditarCantidad');

        //Eliminar producto del carrito
        Route::get('pedido/eliminar/producto/{detalle_id}', 'venta\CarritoController@getEliminar');

        //Aplicar Codigo de promocion
        Route::get('pedido/aplicar/codigo/promocion/{codigo_promocion}', 'venta\CarritoController@getAplicarCodigoPromocion');

        //Mostrar datos del paso uno
        Route::get('pedido/mostrar/pedido/carrito', 'venta\CarritoController@getMostarPedidoCarrito');

        //PASO 2
        // Guardar
        Route::post('pedido/guardar/paso/2', 'venta\CarritoController@postGuardarPaso2');

        // Route::get('pedido/listar/direcciones', 'venta\CarritoController@getListarDirecciones');
        Route::get('pedido/listar/productos/list', 'venta\CarritoController@getListarProductosPedidoList');
        Route::get('pedido/listar/metodos/envio', 'venta\CarritoController@getListarMetodosEnvio');

        //Rutas de las tarjetas gusrdadas
        Route::get('listar/tarjetas/guardadas', 'cliente\TarjetasController@ListarTarjetasPago');

        //PASO 3
        Route::post('pedido/pago/tarjeta', 'venta\PagosController@registrarPagoTarjeta');

        // Guardar
        Route::post('pedido/guardar/paso/3', 'venta\CarritoController@postGuardarPaso3');
        Route::get('pedido/listar/metodos/pago', 'venta\CarritoController@getListarMetodosPago');

        //PASO 4
        Route::post('pedido/guardar/paso/4', 'venta\CarritoController@postGuardarPaso4');
        Route::get('pedido/listar/productos/final', 'venta\CarritoController@getListarProductosPedidoFinal');
        Route::get('pedido/mostrar/facturacion', 'venta\CarritoController@getMostarFacturacion');

        Route::get('pedido/guardar/paso/fin', 'venta\CarritoController@getCodigoVentaFin');
        // Route::post('pedido/guardar/paso/4', 'venta\CarritoController@SendMailConfirmacion');

    });

});
