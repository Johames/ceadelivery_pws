@guest
<!-- Bottom Navigation para Dispositivos Pequeños -->
<div class="cea-handheld-toolbar">
    <div class="d-table table-fixed w-100">
        <a class="d-table-cell cea-handheld-toolbar-item" data-toggle="modal" href="#InicioSesion">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-heart"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Deseos</span>
        </a>
        <a class="d-table-cell cea-handheld-toolbar-item" href="#navbarCollapse" data-toggle="collapse" onclick="window.scrollTo(0, 0)">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-chevron-up"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Subir</span>
        </a>
        <a class="d-table-cell cea-handheld-toolbar-item" data-toggle="modal" href="#InicioSesion">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-shopping-cart"></i>
            </span>
            <span class="cea-handheld-toolbar-label">S/  0.00</span>
        </a>
    </div>
</div>
@endguest

@hasrole('cliente')
<!-- Bottom Navigation para Dispositivos Pequeños -->
<div class="cea-handheld-toolbar">
    <div class="d-table table-fixed w-100">
        <a class="d-table-cell cea-handheld-toolbar-item" href="{{ route('AdmDeseosClient') }}">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-heart"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Deseos</span>
        </a>
        <a class="d-table-cell cea-handheld-toolbar-item" href="{{ route('AdmHistorialClient') }}">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-user-tie"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Perfil</span>
        </a>
        <a class="d-table-cell cea-handheld-toolbar-item" href="{{ route('LogOut') }}" onclick="event.preventDefault(); document.getElementById('logout-forms').submit();">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-sign-out-alt"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Salir</span>

            <form id="logout-forms" action="{{ route('LogOut') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </a>
    </div>
</div>
@endhasrole

@hasrole('administrador_empresa|editor_empresa|deliverista_empresa')
<!-- Bottom Navigation para Dispositivos Pequeños -->
<div class="cea-handheld-toolbar">
    <div class="d-table table-fixed w-100">
        <a class="d-table-cell cea-handheld-toolbar-item" href="{{ route('AdmPerfilEmp') }}">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-user-tie"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Perfil</span>
        </a>
        <a class="d-table-cell cea-handheld-toolbar-item" href="#navbarCollapse" data-toggle="collapse" onclick="window.scrollTo(0, 0)">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-chevron-up"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Subir</span>
        </a>
        <a class="d-table-cell cea-handheld-toolbar-item" href="{{ route('LogOut') }}" onclick="event.preventDefault(); document.getElementById('logout-forms').submit();">
            <span class="cea-handheld-toolbar-icon">
                <i class="fas fa-sign-out-alt"></i>
            </span>
            <span class="cea-handheld-toolbar-label">Salir</span>

            <form id="logout-forms" action="{{ route('LogOut') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </a>
    </div>
</div>
@endhasrole

