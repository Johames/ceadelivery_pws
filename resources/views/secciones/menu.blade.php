<div id="mainMenu" class="mainMenuOverlay floating2">
     {{-- <div class="navire floating3"></div> --}}
     <div class="itemMenuBox bills">
          <a href="{{ route('AdmPedidosEmp') }}" class="itemMenu"rel="tooltip" title="Ir a Inicio" data-placement="top" onclick="abrircerrar()">
               <i class="now-ui-icons shopping_shop"></i>
          </a>
     </div>
     <div class="itemMenuBox tarsheed">
          <a href="{{ route('Productos') }}" class="itemMenu" rel="tooltip" title="Todos los Productos" data-placement="right" onclick="abrircerrar()">
               <i class="now-ui-icons files_box"></i>
          </a>
     </div>
     <div class="itemMenuBox contact">
          <a href="#comprar_precios" data-toggle="modal" class="itemMenu" rel="tooltip" title="Comparar Precios" data-placement="left" onclick="abrircerrar()">
               <i class="now-ui-icons arrows-1_refresh-69"></i>
          </a>
     </div>

     {{-- Solo cuando se inicie sesión --}}
     {{-- <div class="itemMenuBox employees">
          <a href="javascript:void(0)" class="itemMenu" rel="tooltip" title="Historial de Pedidos" data-placement="right" onclick="abrircerrar()">
               <i class="now-ui-icons education_paper"></i>
          </a>
     </div>
     <div class="itemMenuBox eservices">
          <a href="javascript:void(0)" class="itemMenu" rel="tooltip" title="Lista de Deseos" data-placement="left" onclick="abrircerrar()">
               <i class="now-ui-icons ui-2_favourite-28"></i>
          </a>
     </div>
     <div class="itemMenuBox location">
          <a href="#perfil_usuario" data-toggle="modal" class="itemMenu" rel="tooltip" title="Perfil de Usuario" data-placement="bottom" onclick="abrircerrar()">
               <i class="now-ui-icons users_single-02"></i>
          </a>
     </div> --}}

     <a href="javascript:void(0)" class="toggleMenu floating" onclick="abrircerrar()">
          <i id="MenuBar" class="now-ui-icons design_bullet-list-67" rel="tooltip" title="Abrir Menú" data-placement="Top"></i>
          <i id="MenuClose" class="fa fa-close" rel="tooltip" title="Cerrar Menú" data-placement="bottom"></i>
     </a>
</div>

{{-- #a3f7bf --}}
