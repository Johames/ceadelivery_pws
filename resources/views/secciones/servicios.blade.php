{{-- <section class="bg-primary text-dark bg-size-cover bg-position-center pt-4 pb-4">
    <div class="container">
        <h2 class="h3 mb-3 pb-4 text-light text-center">¿Por qué elegirnos?</h2>
        <div class="row pt-lg-2 text-center">
            <div class="col-4 mb-grid-gutter">
                <div class="d-inline-block">
                    <div class="media media-ie-fix align-items-center text-left">
                        <i class="fas fa-clock fa-2x text-light"></i>
                        <div class="media-body pl-3">
                            <h6 class="text-light font-size-base mb-1">Ahorra tiempo</h6>
                            <p class="text-light font-size-ms opacity-70 mb-0">Recibe tus productos en menos de 1 hora.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4 mb-grid-gutter">
                <div class="d-inline-block">
                    <div class="media media-ie-fix align-items-center text-left">
                        <i class="fas fa-hand-holding-heart fa-2x text-light"></i>
                        <div class="media-body pl-3">
                            <h6 class="text-light font-size-base mb-1">En manos expertas</h6>
                            <p class="text-light font-size-ms opacity-70 mb-0">Un shopper selecciona tus productos con amor.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4 mb-grid-gutter">
                <div class="d-inline-block">
                    <div class="media media-ie-fix align-items-center text-left">
                        <i class="fas fa-hand-holding-heart fa-2x text-light"></i>
                        <div class="media-body pl-3">
                            <h6 class="text-light font-size-base mb-1 ">Calidad 100%</h6>
                            <p class="text-light font-size-ms opacity-70 mb-0">Si no te gusta el estado de un producto, ¡No lo pagas!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

{{-- <section class="container-fluid bg-faded-primary px-0">
    <div class="row no-gutters">
        <div class="col-4">
            <div class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-primary">
                <div class="card-body text-center">
                    <i class="fas fa-clock fa-2x mt-2 mb-4 text-primary"></i>
                    <h3 class="h5 mb-1">Ahorra tiempo</h3>
                    <p class="text-muted font-size-sm">Recibe tus productos en menos de 1 hora.</p>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-accent">
                <div class="card-body text-center">
                    <i class="fas fa-hand-holding-heart fa-2x mt-2 mb-4 text-primary"></i>
                    <h3 class="h5 mb-1">En manos expertas</h3>
                    <p class="text-muted font-size-sm">Un shopper selecciona tus productos con amor.</p>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card border-0 rounded-0 text-decoration-none py-md-4 bg-faded-primary">
                <div class="card-body text-center">
                    <i class="fas fa-hand-holding-heart fa-2x mt-2 mb-4 text-primary"></i>
                    <h3 class="h5 mb-1">Calidad 100%</h3>
                    <p class="text-muted font-size-sm">Si no te gusta el estado de un producto, ¡No lo pagas!</p>
                </div>
            </div>
        </div>
    </div>
</section> --}}

<section class="container-fluid px-0">
    <div class="row no-gutters">
        <div class="col-4 h-100 pb-0 mb-0">
            <div class="media p-4 bg-faded-primary tamaño_beneficios">
                <i class="d-none d-sm-block d-md-block d-lg-block fas fa-clock fa-2x mt-2 mb-2 text-primary"></i>
                <div class="media-body pl-3">
                    <h6 class="pt-1 mb-1">Ahorra tiempo</h6>
                    <p class="font-size-sm text-muted mb-0">Recibe tus productos en menos de 1 hora.</p>
                </div>
            </div>
        </div>
        <div class="col-4 h-100 pb-0 mb-0">
            <div class="media p-4 bg-faded-accent tamaño_beneficios">
                <i class="d-none d-sm-block d-md-block d-lg-block fas fa-hand-holding-heart fa-2x mt-2 mb-2 text-primary"></i>
                <div class="media-body pl-3">
                    <h6 class="pt-1 mb-1">En manos expertas</h6>
                    <p class="font-size-sm text-muted mb-0">Un shopper selecciona tus productos con amor.</p>
                </div>
            </div>
        </div>
        <div class="col-4 h-100 pb-0 mb-0">
            <div class="media p-4 bg-faded-primary tamaño_beneficios">
                <i class="d-none d-sm-block d-md-block d-lg-block fas fa-medal fa-2x mt-2 mb-2 text-primary"></i>
                <div class="media-body pl-3">
                    <h6 class="pt-1 mb-1">Calidad 100%</h6>
                    <p class="font-size-sm text-muted mb-0">Si no te gusta el estado de un producto, ¡No lo pagas!.</p>
                </div>
            </div>
        </div>
    </div>
</section>

@yield('pagos')
