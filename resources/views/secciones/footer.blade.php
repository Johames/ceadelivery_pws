<br>
<div class="container-fluid">
    <div class="row pb-2">
        <div class="col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <div class="widget widget-links widget-light pb-0 mb-0">
                <h3 class="widget-title text-light">Perfil de Usuario</h3>
                @hasrole('cliente')
                    <ul class="widget-list">
                        <li class="widget-list-item"><a class="widget-list-link" href="{{ route('AdmPerfilClient') }}">Tu Perfil</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="{{ route('AdmHistorialClient') }}">Historial de Pedidos</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="{{ route('AdmDireccionesClient') }}">Direcciones</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="{{ route('AdmDeseosClient') }}">Listas de Deseos</a></li>
                    </ul>
                @endhasrole

                @guest
                    <ul class="widget-list">
                        <li class="widget-list-item"><a class="widget-list-link" data-toggle="modal" href="#InicioSesion">Tu Perfil</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" data-toggle="modal" href="#InicioSesion">Historial de Pedidos</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" data-toggle="modal" href="#InicioSesion">Direcciones</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" data-toggle="modal" href="#InicioSesion">Listas de Deseos</a></li>
                    </ul>
                @endguest
            </div>
        </div>
        <div class="col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <div class="widget widget-links widget-light pb-0 mb-0">
                <div class="widget widget-links widget-light pb-2 mb-4">
                    <h3 class="widget-title text-light">Acerca de Ceatec Soft</h3>
                    <ul class="widget-list">
                        <li class="widget-list-item"><a class="widget-list-link" href="https://ceatec.com.pe/quienes_somos" target="_blank">Sobre Nosotros</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="https://ceatec.com.pe" target="_blank">Nuestro Equipo</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="https://ceatec.com.pe/modulos" target="_blank">Nuestros Productos</a></li>
                        <li class="widget-list-item"><a class="widget-list-link" href="https://ceatec.com.pe/sucursales" target="_blank">Ubicanos</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <div class="widget widget-links widget-light pb-0 mb-0">
                <div class="widget widget-links widget-light pb-2 mb-4">
                    <h3 class="widget-title text-light">Preguntas frecuentes</h3>
                    <ul class="widget-list" id="mostrar_preg_frec">
                    </ul>
                    <ul class="widget-list mt-2">
                        <li class="widget-list-item"><a class="widget-list-link" href="/preguntas_frecuentes" target="_blank">Mas preguntas...</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-12 col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
            <div class="widget widget-links widget-light pb-0 mb-0">
                <div class="widget widget-links widget-light pb-2 mb-4">
                    <h3 class="widget-title text-light">Visita nuestras Redes Sociales</h3>
                    <div class="d-flex justify-content-between">
                        <div class="tab-pane fade active show mt-2" id="redes_pagina" role="tabpanel">
                             {{-- <a class="social-btn sb-round sb-outline sb-instagram mb-2" data-toggle="tooltip" data-placement="bottom" tittle="Instagram" href="https://www.instagram.com/ceamarket/" target="_blank">
                                  <i class="fab fa-instagram"></i>
                             </a> --}}
                        </div>
                   </div>
                </div>
            </div>
        </div>


        {{-- <div class="col-md-4 col-sm-6">
            <div class="widget pb-2 mb-4">
                <h3 class="widget-title text-light pb-1">Mantente Informado</h3>
                @guest
                    <form class="cea-subscribe-form validate" novalidate>
                        <div class="input-group input-group-overlay flex-nowrap">
                            <div class="input-group-prepend-overlay">
                                <span class="input-group-text text-muted font-size-base">
                                    <i class="fas fa-envelope"></i>
                                </span>
                            </div>
                            <input class="form-control prepended-form-control" type="email" name="corre_electronico" placeholder="Ingresa tu Correo Electrónico" disabled>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" disabled>Suscribirse</button>
                            </div>
                        </div>
                        <small class="form-text text-light opacity-50">
                            *Suscríbase a nuestro boletín para recibir ofertas de descuentos, actualizaciones e información sobre nuevos productos.
                        </small>
                        <div class="subscribe-status"></div>
                    </form>
                @endguest

                @hasanyrole('cliente')
                <form class="validate" action="" method="post" novalidate>
                    <div class="input-group input-group-overlay flex-nowrap">
                        <div class="input-group-prepend-overlay">
                            <span class="input-group-text text-muted font-size-base">
                                <i class="fas fa-envelope"></i>
                            </span>
                        </div>
                        <input class="form-control prepended-form-control" type="email" name="corre_electronico" placeholder="Ingresa tu Correo Electrónico" required>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit" name="suscribirse">Suscribirse</button>
                        </div>
                    </div>
                    <small class="form-text text-light opacity-50">
                        *Suscríbase a nuestro boletín para recibir ofertas de descuentos, actualizaciones e información sobre nuevos productos.
                    </small>
                    <div class="subscribe-status"></div>
                </form>
                @endhasanyrole
            </div>
            <!-- <div class="widget pb-2 mb-4">
                <h3 class="widget-title text-light pb-1">Descarga Nuestra App</h3>
                <div class="d-flex flex-wrap">
                    <div class="mr-2 mb-2">
                        <a class="btn-market btn-apple" href="#" role="button">
                            <span class="btn-market-subtitle">
                                Download on the
                            </span>
                            <span class="btn-market-title">
                                App Store
                            </span>
                        </a>
                    </div>
                    <div class="mb-2">
                        <a class="btn-market btn-google" href="#" role="button">
                            <span class="btn-market-subtitle">
                                Descargar en
                            </span>
                            <span class="btn-market-title">
                                Google Play
                            </span>
                        </a>
                    </div>
                </div>
            </div> -->
        </div> --}}


    </div>
    <div class="row pb-2">
        {{--  --}}
    </div>
</div>
<div class="pt-3 bg-darker">
    <div class="container-fluid mb-xs-5">
        {{-- <div class="widget w-100 mb-4 pb-3 text-center mx-auto">
            <h3 class="widget-title text-light pb-1">Descarga Nuestra App</h3>
            <div class="d-flex flex-wrap justify-content-center">
                <div class="mr-2 mb-2">
                    <a class="btn-market btn-apple" href="#" role="button">
                        <span class="btn-market-subtitle">
                            Descargar en
                        </span>
                        <span class="btn-market-title">
                            App Store
                        </span>
                    </a>
                </div>
                <div class="mb-2">
                    <a class="btn-market btn-google" href="#" role="button">
                        <span class="btn-market-subtitle">
                            Descargar en
                        </span>
                        <span class="btn-market-title">
                            Google Play
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <hr class="hr-light pb-2 mb-3"> --}}
        <div class="pb-3 font-size-xs text-light opacity-50 text-center text-md-left" id="copyright">
            &copy;
            <script>
                document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Desarrollado por
            <a class="text-light" href="https://ceatec.com.pe" target="_blank" rel="noopener">
                Ceatec Soft
            </a>
        </div>
    </div>
</div>
