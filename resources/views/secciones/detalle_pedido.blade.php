@foreach ($detalle as $detail)
<div class="modal-body font-size-sm">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                {{-- <div class="media align-items-center">
                    <div class="col-4"></div>
                    <div class="col-4">
                        <div class="img-thumbnail mt-2 rounded-circle align-center" style="width: 10rem;">
                            <img class="rounded-circle" src="/img/{{ $detail->ModeloPersona->avatar }}" alt="Cliente">
                       </div>
                    </div>
                    <div class="col-4"></div>
               </div> --}}
                <div class="card-body">
                    <!--<h6>Datos del Cliente</h6>-->
                    <ul class="list-unstyled mb-0">
                        @if ($detail->ModeloPersona->nombres)
                            <li class="media pb-2 border-bottom">
                                <i class="fas fa-user font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted">Nombres del Cliente</span>
                                    <p class="d-block text-heading font-size-sm">{{ $detail->ModeloPersona->nombres }} {{ $detail->ModeloPersona->apellidos }}</p>
                                </div>
                            </li>
                        @else
                            @if ($detail->ModeloPersona->razon_social)
                                <li class="media pb-2 border-top border-bottom">
                                    <i class="fas fa-user font-size-lg mt-2 mb-0 text-primary"></i>
                                    <div class="media-body pl-3">
                                        <span class="font-size-ms text-muted">Nombres del Cliente</span>
                                        <p class="d-block text-heading font-size-sm">{{ $detail->ModeloPersona->razon_social }}</p>
                                    </div>
                                </li>
                            @else
                                <li class="media pb-2 border-top border-bottom">
                                    <i class="fas fa-user font-size-lg mt-2 mb-0 text-primary"></i>
                                    <div class="media-body pl-3">
                                        <span class="font-size-ms text-muted">Nombres del Cliente</span>
                                        <p class="d-block text-heading font-size-sm">{{ $detail->ModeloPersona->nombre_comercial }}</p>
                                    </div>
                                </li>
                            @endif
                        @endif
                        <li class="media pb-2 border-bottom">
                            <i class="fas fa-map-marked-alt font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Dirección del Cliente</span>
                                <p class="d-block text-heading font-size-sm">{{ $detail->ModeloDireccion->direccion }}</p>
                            </div>
                        </li>
                        <li class="media pt-2 pb-2 border-bottom">
                            <i class="fas fa-phone font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Número de Teléfono</span>
                                @if ($detail->ModeloPersona->telefono)
                                    <a href="tel:+{{ $detail->ModeloPersona->telefono }}" class="d-block text-heading font-size-sm">{{ $detail->ModeloPersona->telefono }}</a>
                                @else
                                    <p class="d-block text-heading font-size-sm">No definido.</p>
                                @endif
                            </div>
                        </li>
                        <li class="media pt-1 border-bottom">
                        </li>
                        <li class="media pt-2 pb-2 border-bottom">
                            <i class="fas fa-shipping-fast font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Método de Envío</span>
                                <p class="d-block mb-1 text-heading font-size-sm">{{ $detail->ModeloDetalleMetodoEnvio->ModeloMetodoEnvio->nombre }}</p>
                                <p class="d-block m-0 p-0 text-heading font-size-sm">
                                    <small>T. Espera: {{ $detail->ModeloDetalleMetodoEnvio->llegada }}</small>
                                     -
                                    <small> Costo: S/ {{ $detail->ModeloDetalleMetodoEnvio->precio }}</small>
                                </p>
                            </div>
                        </li>
                        <li class="media pt-2 pb-2 border-bottom">
                            <i class="fas fa-credit-card font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Método de Pago</span>
                                <p class="d-block mb-1 text-heading font-size-sm">{{ $detail->ModeloDetalleMetodoPago->ModeloMetodoPago->nombre }}</p>
                                <p class="d-block m-0 p-0 text-heading font-size-sm">
                                    @if ($detail->ModeloDetalleMetodoPago->precio_comision)
                                        <small>Comision: S/ {{ $detail->ModeloDetalleMetodoPago->precio_comision }}</small>
                                    @else
                                        <small>Comision: S/ 0.00</small>
                                    @endif
                                </p>
                                <p></p>
                            </div>
                        </li>
                        @if ($detail->ModeloDeliverista)
                            <li class="media pt-2 mb-0">
                                <i class="fas fa-credit-card font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted">Deliverista</span>
                                    <p class="d-block mb-1 text-heading font-size-sm">{{ $detail->ModeloDeliverista->nombres }}</p>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="widget widget-cart px-3 pt-2 pb-3">
                <h3 class="widget-title">Detalle del Pedido</h3>
                <div style="height: 17.5rem;" data-simplebar data-simplebar-auto-hide="false">
                    @foreach ($detail->ModeloDetallePedidos as $prod)
                        @php
                            $img = 1;
                        @endphp
                        <div class="widget-cart-item pb-1 pt-1 border-bottom">
                            <button class="close text-danger mr-2 pr-2" type="button" aria-label="Remove">
                                <i class="fas fa-check fa-xs" style="color: #26a69a;" aria-hidden="true"></i>
                            </button>
                            <div class="media align-items-center">
                                @if (count($prod->ModeloDetalleProducto->ModeloProductoImagenes) > 0)
                                    @foreach ($prod->ModeloDetalleProducto->ModeloProductoImagenes as $images)
                                        @if ($img == 1)
                                            @php
                                                $ruta_img = $images->ruta;
                                            @endphp
                                            <span class="d-block mr-2">
                                                <img style="height: 64px; width: 64px;" src="https://tiendas.ceamarket.com/img/{{ $ruta_img }}" alt="Product" />
                                            </span>
                                        @endif
                                        @php
                                            $img++;
                                        @endphp
                                    @endforeach
                                @else
                                    <span class="d-block mr-2">
                                        <img width="64" src="{{ asset("/img/default-product.png") }}" alt="Product" />
                                    </span>
                                @endif
                                <div class="media-body">
                                    <h6 class="widget-product-title">
                                        <span>{{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}</span>
                                    </h6>
                                    <div class="widget-product-meta">
                                        @php
                                            $precio_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                        @endphp
                                        <span class="text-accent mr-2">
                                            S/ {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small>
                                        <span class="text-muted">x {{ $prod->cantidad }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <ul class="list-unstyled font-size-sm pb-2 pt-2 mt-2 border-top border-bottom">
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Subtotal:</span>
                    @php
                        $subtotal_array = explode(".", number_format((float)$detail->total_pagar, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $subtotal_array[0] }}.<small>{{ $subtotal_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Delivery:</span>
                    @php
                        $delivery_array = explode(".", number_format((float)$detail->precio_envio, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $delivery_array[0] }}.<small>{{ $delivery_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Comisión:</span>
                    @php
                        $comision_array = explode(".", number_format((float)$detail->precio_comision, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $comision_array[0] }}.<small>{{ $comision_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">IGV:</span>
                    @php
                        $igv_array = explode(".", number_format((float)$detail->igv, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $igv_array[0] }}.<small>{{ $igv_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Descuento:</span>
                    @php
                        $descuento_array = explode(".", number_format((float)$detail->descuento, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $descuento_array[0] }}.<small>{{ $descuento_array[1] }}</small>
                    </span>
                </li>
            </ul>
            @php
                $total_array = explode(".", number_format((float)$detail->total_pagar - $detail->descuento + $detail->precio_envio, 2, '.', ''));
            @endphp
            <h3 class="font-weight-normal text-center">
                S/ {{ $total_array[0] }}.<small>{{ $total_array[1] }}</small>
            </h3>
        </div>
    </div>
</div>
<div class="modal-footer">
    @if ($detail->estado == 0)
        <span class="badge badge-warning px-5 p-2">Pedido Pendiente</span>
    @elseif ($detail->estado == 1)
        <span class="badge badge-success px-5 p-2">Pedido en Proceso</span>
    @elseif ($detail->estado == 2)
        <span class="badge badge-success px-5 p-2">Pedido Confirmado</span>
    @elseif ($detail->estado == 3)
        <span class="badge badge-success px-5 p-2">Pedido en Camino</span>
    @elseif ($detail->estado == 4)
        <span class="badge badge-primary px-5 p-2">Pedido Entregado</span>
    @elseif ($detail->estado == 5)
        <span class="badge badge-danger px-5 p-2">Pedido Rechazado</span>
    @endif
    @hasrole('administrador_empresa')
        <button class="btn btn-primary btn-sm" type="button" onclick="modal_estado_cambiar('{{ $detail->pedido_id }}', '{{ $detail->estado }}')">Cambiar Estado del Pedido</button>
        <button class="btn btn-outline-primary btn-sm" type="button" onclick="agregar_delivery('{{ $detail->pedido_id }}', '{{ $detail->codigo }}')">Asignar Deliverista</button>
    @endhasrole
    @hasrole('deliverista_empresa')
        <button class="btn btn-outline-primary btn-sm" type="button">Marcar como entregado</button>
    @endhasrole
    <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>
@endforeach
