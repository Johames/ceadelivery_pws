<head>
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/cea_market_icon.svg') }}">
    <link rel="icon" type="image/png" href="{{ asset('img/cea_market_icon.svg') }}">

    {{-- Titulo y Descripcion --}}
    <title>Cea Market - @yield('title_header', 'Titulo')</title>
    <meta name="description" content="Sistema de Pedidos Online">
    <meta name="keywords" content="Cea Market">
    <meta name="author" content="Ceatec Soft: Edwin Alexander Bautista Villegas; Johann James Valles Paz">

    <!-- Viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="{{ asset('css/site.webmanifest') }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts and icons -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    {{-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> --}}
    <script src="{{ asset('resources/font-awesome/kit-fontawesome.js') }}" crossorigin="anonymous"></script>

    <!-- Estilos -->
    <link rel="stylesheet" media="screen" href="{{ asset('css/estilos/simplebar.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/estilos/tiny-slider.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/estilos/drift-basic.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/estilos/lightgallery.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/estilos/nouislider.css') }}"/>

    <link rel="stylesheet" media="screen" href="{{ asset('/resources/jquery-ui-1.12.1/jquery-ui.min.css') }}"/>

    <!-- Estilos Principales -->
    <link rel="stylesheet" media="screen" href="{{ asset('css/estilos/theme.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('css/scrollbar/scrollbar.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/menu/menu_floating.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/emp/socialbuttons.css') }}"/>
    <link rel="stylesheet" media="screen" href="{{ asset('css/modal/modal.css') }}"/>

    <!-- sweetalert -->
    <link rel="stylesheet" href="{{ asset('js/sweetalert2/sweetalert2.min.css') }}">
    <script src="{{ asset('js/sweetalert2/sweetalert2.all.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('/resources/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/resources/select2/dist/css/select2-bootstrap4.css') }}">

    <link rel="stylesheet" href="{{ asset('/resources/dropzone/dist/dropzone.css') }}">

    @yield('css')

    @yield('open_graph')


</head>
