@hasrole('cliente')
    <a class="d-none d-xs-none d-sm-none d-md-none d-lg-block d-xl-block navbar-tool-icon-box bg-secondary dropdown-toggle" href="{{ route('AdmNotificacionesClient') }}">
        @if (count($notificaciones) > 0)
            <span class="navbar-tool-label">{{ count($notificaciones) }}</span>
        @endif
        <i class="navbar-tool-icon fas fa-bell"></i>
    </a>
    <a class="d-block d-xs-block d-sm-block d-md-block d-lg-none d-xl-none navbar-tool-icon-box bg-secondary dropdown-toggle" href="{{ route('AdmNotificacionesClient') }}">
        @if (count($notificaciones) > 0)
            <span class="navbar-tool-label">{{ count($notificaciones) }}</span>
        @endif
        <i class="navbar-tool-icon fas fa-bell"></i>
    </a>
@endhasrole
@if (count($notificaciones) > 0)
    <div class="dropdown-menu dropdown-menu-right" style="width: 20rem;">
        <div class="widget widget-cart px-3 pt-2 pb-3">
            <div style="height: 15rem;" data-simplebar data-simplebar-auto-hide="false">
                @foreach ($notificaciones as $notif)
                    <div class="widget-cart pb-1 pt-1 border-bottom" style="cursor: pointer;" onclick="modal_notificacion({{ $notif->notificacion_id }})">
                        <div class="media align-items-center">
                            <p class="d-block mr-2">
                                <i class="img-thumbnail fas fa-bell fa-3x text-success mb-2 mr-2"></i>
                                <!-- <img width="64" src="{{ asset('img/shop/cart/widget/05.jpg') }}" alt="Product" /> -->
                            </p>
                            <div class="media-body">
                                <h6 class="widget-product-title">
                                    {{ $notif->titulo }}
                                </h6>
                                <div class="widget-product-meta">
                                    <small>
                                        <strong>{{ $notif->descripcion }}</strong>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex flex-wrap justify-content-between align-items-center py-3">
                @hasrole('cliente')
                    <a class="btn btn-outline-primary btn-sm" href="{{ route('AdmNotificacionesClient') }}">
                        Ver Todas las Notificaciones
                        <i class="fas fa-chevron-right ml-1 mr-n1"></i>
                    </a>
                @endhasrole
                @hasanyrole('administrador_empresa|deliverista_empresa')
                    <a class="btn btn-outline-primary btn-sm" href="{{ route('AdmNotificacionesEmp')}}">
                        Ver Todas las Notificaciones
                        <i class="fas fa-chevron-right ml-1 mr-n1"></i>
                    </a>
                @endhasanyrole
            </div>
        </div>
    </div>
@endif
