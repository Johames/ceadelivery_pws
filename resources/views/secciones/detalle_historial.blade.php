@foreach ($detalle as $detail)
<div class="modal-body font-size-sm">
    <div class="row pb-1 mb-1">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">

            @if ($detail->estado > 3)
                <span class="badge badge-pill badge-primary p-2 px-4 font-size-sm mb-2" style="cursor: pointer;" onclick="mostrar_modal_valoracion('emp', {{ $detail->ModeloEmpresa->empresa_id }})">
                    Calificar Empresa
                </span>

                @if ($detail->deliverista_id)
                    <span class="badge badge-pill badge-info p-2 px-4 font-size-sm mb-2" style="cursor: pointer;" onclick="mostrar_modal_valoracion('deliv', {{ $detail->deliverista_id }})">
                        Calificar Deliverista
                    </span>
                @endif
            @endif

            <div class="card">
                <div class="card-img-top overflow-hidden"  style="background: url('https://tiendas.ceamarket.com/img/{{ $detail->ModeloEmpresa->baner }}') no-repeat center center; background-size: cover; height: 130px;">

                </div>
                <div class="card-body">
                    <ul class="list-unstyled mb-0">
                        <li class="media pb-2 border-bottom">
                            <i class="fas fa-user font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Nombre de la Tienda</span>
                                @if ($detail->ModeloEmpresa->razon_social)
                                    <span class="d-block text-heading font-size-sm">
                                        {{ $detail->ModeloEmpresa->razon_social }}
                                    </span>
                                @else
                                    <span class="d-block text-heading font-size-sm">
                                        {{ $detail->ModeloEmpresa->nombre_comercial }}
                                    </span>
                                @endif

                            </div>
                        </li>
                        <li class="media pb-2 border-bottom">
                            <i class="fas fa-map-marked-alt font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Dirección de la Tienda</span>
                                @if ($detail->ModeloEmpresa->direccion)
                                    <span class="d-block text-heading font-size-sm">
                                        {{ $detail->ModeloEmpresa->direccion }}
                                    </span>
                                @else
                                    <span class="d-block text-heading font-size-sm">
                                        No definido.
                                    </span>
                                @endif
                            </div>
                        </li>
                        <li class="media pt-2 pb-2 border-bottom">
                            <i class="fas fa-phone font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Números de Teléfono</span>
                                @foreach ($detail->ModeloEmpresa->ModeloContactos as $telefono)
                                    @if ($telefono->tipo == 0)
                                        <a href="tel:+{{ $telefono->nombre }}" class="d-block text-heading font-size-sm">
                                            {{ $telefono->nombre }}
                                        </a>
                                    @else
                                        <p>No definido</p>
                                    @endif
                                @endforeach
                            </div>
                        </li>
                        <li class="media pt-1 border-bottom">
                        </li>
                        <li class="media pt-2 pb-2 border-bottom">
                            <i class="fas fa-shipping-fast font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Método de Envío</span>
                                <p class="d-block mb-1 text-heading font-size-sm">{{ $detail->ModeloDetalleMetodoEnvio->ModeloMetodoEnvio->nombre }}</p>
                                <p class="d-block m-0 p-0 text-heading font-size-sm">
                                    <small>T. Espera: {{ $detail->ModeloDetalleMetodoEnvio->llegada }}</small>
                                     -
                                    <small> Costo: S/ {{ $detail->ModeloDetalleMetodoEnvio->precio }}</small>
                                </p>
                            </div>
                        </li>
                        <li class="media pt-2 mb-0">
                            <i class="fas fa-credit-card font-size-lg mt-2 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="font-size-ms text-muted">Método de Pago</span>
                                <p class="d-block mb-1 text-heading font-size-sm">{{ $detail->ModeloDetalleMetodoPago->ModeloMetodoPago->nombre }}</p>
                                <p class="d-block m-0 p-0 text-heading font-size-sm">
                                    @if ($detail->ModeloDetalleMetodoPago->precio_comision > 0)
                                        <small>Comision: S/ {{ $detail->ModeloDetalleMetodoPago->precio_comision }}</small>
                                    @endif
                                </p>
                                @if($detail->voucher)
                                    <p class="mb-0">
                                        <span class="badge badge-info px-2 p-1"><small>Pedido Pagado</small></span>
                                         -
                                        <small>
                                            @if (file_exists(public_path($detail->voucher)))
                                                <a href="https://ceamarket.com/img/{{ $detail->voucher }}" target="_blank">Ver constancia de pago.</a>
                                            @else
                                                <a href="https://tiendas.ceamarket.com/img/{{ $detail->voucher }}" target="_blank">Ver constancia de pago.</a>
                                            @endif
                                        </small>
                                    </p>
                                @endif
                            </div>
                        </li>
                        @if ($detail->ModeloDeliverista)
                            <li class="media pt-2 pb-2 border-top">
                                <i class="fas fa-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted">Delivery</span>
                                    <span class="d-block text-heading font-size-sm">
                                        {{ $detail->ModeloDeliverista->nombres }}
                                    </span>
                                </div>
                            </li>
                        @elseif($detail->ModeloDeliveristaExterno)
                            <li class="media pt-2 pb-2 border-top">
                                <i class="fas fa-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3">
                                    <span class="font-size-ms text-muted">Delivery</span>
                                    <span class="d-block text-heading font-size-sm">
                                        {{ $detail->ModeloDeliveristaExterno->nombres . " " . $detail->ModeloDeliveristaExterno->apellidos }}
                                    </span>
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <div class="widget widget-cart px-3 pt-2 pb-3">
                <h4 class="widget-title">
                    Pedido Nro:
                    <strong>
                        <a href="#" onclick="mostrar_qr_pedido('{{ $detail->codigo }}')">{{ $detail->codigo }}</a>
                    </strong>
                </h4>
                <div style="height: 17.5rem;" data-simplebar data-simplebar-auto-hide="false">
                    @foreach ($detail->ModeloDetallePedidos as $prod)
                        <div class="widget-cart-item pb-1 pt-1 border-bottom">

                            @if ($detail->estado > 3)
                                <button class="close text-danger mr-2 pr-2" type="button" aria-label="Remove" onclick="mostrar_modal_valoracion('prod', {{ $prod->ModeloDetalleProducto->producto_detalle_id }})" data-toggle="tooltip" data-placement="bottom" title="Calificar Producto">
                                    <i class="fas fa-star fa-xs" style="color: #26a69a;" aria-hidden="true"></i>
                                </button>
                            @endif

                            <div class="media align-items-center">
                                @if($prod->ModeloDetalleProducto->PrimeraImagen)
                                    <span class="d-block mr-2">
                                        <img style="height: 64px; width: 64px;" src="https://tiendas.ceamarket.com/img/{{ $prod->ModeloDetalleProducto->PrimeraImagen->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" />
                                    </span>
                                @else
                                    <span class="d-block mr-2">
                                        <img width="64" src="{{ asset("/img/default-product.png") }}" alt="Product" />
                                    </span>
                                @endif
                                <div class="media-body">
                                    <h6 class="widget-product-title">
                                        <span>{{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}</span>
                                    </h6>
                                    <div class="widget-product-meta">
                                        @php
                                            $precio_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                        @endphp
                                        <span class="text-accent mr-2">
                                            S/ {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small>
                                        <span class="text-muted">x {{ $prod->cantidad }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <ul class="list-unstyled font-size-sm pb-2 pt-2 mt-2 border-top border-bottom">
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Subtotal:</span>
                    @php
                        $subtotal_array = explode(".", number_format((float)$detail->total_pagar, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $subtotal_array[0] }}.<small>{{ $subtotal_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Delivery:</span>
                    @php
                        $delivery_array = explode(".", number_format((float)$detail->precio_envio, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $delivery_array[0] }}.<small>{{ $delivery_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">IGV:</span>
                    @php
                        $igv_array = explode(".", number_format((float)$detail->igv, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $igv_array[0] }}.<small>{{ $igv_array[1] }}</small>
                    </span>
                </li>
                <li class="d-flex justify-content-between align-items-center">
                    <span class="mr-2">Descuento:</span>
                    @php
                        $descuento_array = explode(".", number_format((float)$detail->descuento, 2, '.', ''));
                    @endphp
                    <span class="text-right">
                        S/ {{ $descuento_array[0] }}.<small>{{ $descuento_array[1] }}</small>
                    </span>
                </li>
            </ul>
            @php
                $total_array = explode(".", number_format((float)$detail->total_pagar - $detail->descuento + $detail->precio_envio, 2, '.', ''));
            @endphp
            <h3 class="font-weight-normal text-center">
                S/ {{ $total_array[0] }}.<small>{{ $total_array[1] }}</small>
            </h3>
        </div>
    </div>
</div>
<div class="modal-footer">
    @if ($detail->estado == 0)
        <span class="badge badge-warning px-5 p-2">Pedido Pendiente</span>
    @elseif ($detail->estado == 1)
        <span class="badge badge-success px-5 p-2">Pedido en Proceso</span>
    @elseif ($detail->estado == 2)
        <span class="badge badge-success px-5 p-2">Pedido Confirmado</span>
    @elseif ($detail->estado == 3)
        <span class="badge badge-success px-5 p-2">Su Pedido esta en Camino</span>
    @elseif ($detail->estado == 4)
        <span class="badge badge-primary px-5 p-2">Pedido Entregado</span>
    @elseif ($detail->estado == 5)
        <span class="badge badge-danger px-5 p-2">Pedido Rechazado</span>
    @endif
    <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
</div>
@endforeach
