<aside class="col-lg-4 pt-4 pt-lg-0">
    <div class="cea-sidebar-static rounded-lg box-shadow-lg px-0 pb-0 mb-5 mb-lg-0" id="aside_client">
         <div class="px-4 mb-4">
              <div class="media align-items-center">
                   <div class="img-thumbnail rounded-circle position-relative" style="width: 6.375rem;">
                        {{-- <span class="badge badge-warning" data-toggle="tooltip" title="Reward points">384</span> --}}
                        @if (Auth::user()->ModeloPersona->avatar)
                            <img class="rounded-circle" src="{{ asset('img/'.Auth::user()->ModeloPersona->avatar) }}" alt="Cliente">
                        @else
                            <img class="rounded-circle" src="{{ asset('img/default_img.png') }}" alt="Cliente">
                        @endif
                   </div>
                   <div class="media-body pl-3">
                        <h3 class="font-size-base mb-0">
                            @if (Auth::user()->ModeloPersona->nombres)
                                {{ Auth::user()->ModeloPersona->nombres }} {{ Auth::user()->ModeloPersona->apellidos }}
                            @else

                                @if (Auth::user()->ModeloPersona->razon_social)
                                    {{ Auth::user()->ModeloPersona->razon_social }}
                                @else
                                    {{ Auth::user()->ModeloPersona->nombre_comercial }}
                                @endif

                            @endif
                        </h3>
                        <span class="text-accent font-size-sm">{{ Auth::user()->email}}</span>
                   </div>
              </div>
         </div>
         <div class="bg-secondary border-top px-4 py-3">
              <h3 class="font-size-sm mb-0 text-muted">Dashboard</h3>
         </div>
         <ul class="list-unstyled mb-0">
              <li class="border-top border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmNotificacionesClient') }}">
                        <i class="fas fa-bell opacity-60 mr-2"></i>
                        Notificaciones
                        <span class="font-size-sm text-muted ml-auto">
                            {{-- 100 --}}
                        </span>
                   </a>
              </li>
              <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmHistorialClient') }}">
                        <i class="fas fa-history opacity-60 mr-2"></i>
                        Historial de Pedidos
                        <span class="font-size-sm text-muted ml-auto">
                            {{-- 100 --}}
                        </span>
                   </a>
              </li>
              <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmDeseosClient') }}">
                        <i class="fas fa-heart opacity-60 mr-2"></i>
                        Lista de Deseos
                        <span class="font-size-sm text-muted ml-auto">
                            {{-- 300 --}}
                        </span>
                   </a>
              </li>
              <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmCalificacionesClient') }}">
                        <i class="fas fa-star opacity-60 mr-2"></i>
                        Reseñas y Calificaciones
                        <span class="font-size-sm text-muted ml-auto">
                            {{-- 100 --}}
                        </span>
                   </a>
              </li>
              {{-- <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmCeaPuntosClient') }}">
                        <i class="fas fa-coins opacity-60 mr-2"></i>
                        CEA Puntos
                        <span class="font-size-sm text-muted ml-auto">
                            <!-- 100 -->
                        </span>
                   </a>
              </li> --}}
         </ul>
         <div class="border-top border-bottom bg-secondary px-4 py-3">
              <h3 class="font-size-sm mb-0 text-muted">Configuracion del Perfil</h3>
         </div>
         <ul class="list-unstyled mb-0">
              <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmPerfilClient') }}">
                        <i class="fas fa-user-tie opacity-60 mr-2"></i>
                        Información de Perfil
                   </a>
              </li>
              <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmDireccionesClient') }}">
                        <i class="fas fa-map-marked-alt opacity-60 mr-2"></i>
                        Administrar Direcciones
                   </a>
              </li>
              <li class="border-bottom mb-0">
                   <a class="nav-link-style d-flex align-items-center px-4 py-3" href="{{ route('AdmMedioPagoClient') }}">
                        <i class="fas fa-credit-card opacity-60 mr-2"></i>
                        Administrar Medios de Pago
                   </a>
              </li>
         </ul>
    </div>
</aside>
