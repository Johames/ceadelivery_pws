<div class="modal-header-lateral bg-dark">
    <h5 class="modal-title" style="color: white;">Reseña de la Tienda</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" style="color: white;">&times;</span>
    </button>
</div>
<div class="modal-body lateral bg-faded-success">
    <div class="col-12 mb-2 p-2">
        <div class="card">
            <div class="card-header" id="RedesHeading">
                @php
                    $total_star = 0;
                    $count_star = 0;
                    $promedio_star = 0;
                @endphp
                @foreach ($empresa->ModeloValoracionEmpresas as $star)
                    @php
                        $total_star += $star->estrellas;
                        $count_star ++;
                        $promedio_star = ($total_star/$count_star);
                    @endphp
                @endforeach
                <div class="star-rating mr-2 py-2">



                    @for ($e = 0; $e < 5; $e++)
                        @if ($e < floor($promedio_star))
                            <i class="sr-star fas fa-star active" style="font-size: 1.8rem;"></i>

                        @elseif($e == floor($promedio_star))

                            @if (is_float($promedio_star))
                                @php
                                    $star_array = explode(".", number_format((float)$promedio_star, 2, '.', ''));
                                @endphp

                                @if (intval($star_array[1]) >= 50 )
                                    <i class="sr-star fas fa-star-half-alt active" style="font-size: 1.8rem;"></i>
                                @else
                                    <i class="sr-star fas fa-star" style="font-size: 1.8rem;"></i>
                                @endif
                            @else
                                <i class="sr-star fas fa-star" style="font-size: 1.8rem;"></i>
                            @endif
                        @else
                            <i class="sr-star fas fa-star" style="font-size: 1.8rem;"></i>
                        @endif
                    @endfor

                </div>
                <br>
                <span class="d-inline-block align-middle">{{ round($promedio_star, 2) }} Calificación general</span>
            </div>
            <div class="card-body" style="overflow-y: auto;" >
                <div class="widget widget-cart px-3 pt-2 pb-3">
                    <div style="height: 30rem;" data-simplebar data-simplebar-auto-hide="false">
                        @foreach ($empresa->ModeloValoracionEmpresas as $valoracion)
                        <div class="widget pt-2 pb-2 border-top border-bottom">
                            <div class="media align-items-center">

                                <div class="img-thumbnail d-block mr-2 rounded-circle position-relative" style="width: 3.3rem;">
                                    @if ($valoracion->ModeloPersona->avatar)
                                        <img class="rounded-circle" src="/img/{{ $valoracion->ModeloPersona->avatar }}" alt="Cliente">
                                    @else
                                        <img class="rounded-circle" src="{{ asset('img/default_img.png') }}" alt="Cliente">
                                    @endif
                                </div>
                                <div class="media-body">
                                    <h6 class="widget-product-title">
                                        <span>{{ $valoracion->ModeloPersona->nombres . ' ' . $valoracion->ModeloPersona->apellidos }}</span>
                                    </h6>
                                    <div class="star-rating pt-2 mb-2">

                                        @for ($i = 0; $i < 5; $i++)
                                            @if ($i < $valoracion->estrellas)
                                                <i class="sr-star fas fa-star active"></i>
                                            @else
                                                <i class="sr-star fas fa-star"></i>

                                            @endif
                                        @endfor

                                    </div>
                                    <br>
                                    <small>
                                        {{ $valoracion->resena }}
                                    </small>
                                </div>

                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
