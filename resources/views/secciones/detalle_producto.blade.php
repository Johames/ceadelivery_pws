@foreach ($productos as $prod)
<div class="modal-header">
    <h4 class="modal-title product-title">
        <span>
            {{ $prod->ModeloProducto->nombre }} /
            <a href="/store/{{ $prod->ModeloEmpresa->slug }}">
                @if ($prod->ModeloEmpresa->razon_social)
                    {{ $prod->ModeloEmpresa->razon_social }}
                @else
                    {{ $prod->ModeloEmpresa->nombre_comercial }}
                @endif
            </a>
        </span>
    </h4>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-lg-7 pr-lg-0">
            <div class="cea-product-gallery">
                <div class="cea-preview order-sm-2">
                    <div class="cea-preview-item active">
                        <img class="cea-image-zoom" id="img_producto_detalle" src="https://tiendas.ceamarket.com/img/{{ $prod->PrimeraImagen->ruta }}" onerror="this.src='/img/default-product.png'" data-zoom="/img/default-product.png" alt="Producto imagen">
                        <div class="cea-image-zoom-pane"></div>
                    </div>
                </div>
                <div class="cea-thumblist order-sm-1">
                    @foreach ($prod->ModeloProductoImagenes as $images)
                        <a class="cea-thumblist-item active" onclick="cambiar_img_det('{{ $images->ruta }}')" style="cursor: pointer;">
                            <img src="https://tiendas.ceamarket.com/img/{{ $images->ruta }}" onerror="this.src='/img/default-product.png'" alt="{{ $prod->ModeloProducto->nombre }}" style="width: 100%; height: 100%; object-fit: cover;">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>

        <!-- Detalle del Producto -->
        <div class="col-lg-5 pt-4 pt-lg-0 cea-image-zoom-pane">
            <div class="product-details ml-auto pb-3">
                @if ($prod->estado_desc == '0')
                <span class="badge badge-danger badge-shadow">
                    Descuento:
                    @if ($prod->tipo_desc == '0')
                        S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                    @else()
                        -{{ $prod->descuento }}%
                    @endif

                </span>
                @endif
                <div class="h3 font-weight-normal text-accent mt-2 mb-3 mr-1">
                    {{-- $124.<small>99</small> --}}
                    @php
                        $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                    @endphp

                    @if ($prod->estado_desc == '0')

                        @if ($prod->tipo_desc == '0')
                            @php
                                $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                            @endphp

                            <span class="text-accent">
                                Precio: S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                            </span>
                            <del class="font-size-sm text-muted">
                                S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                            </del>

                            @if ($prod->ModeloUnidad)
                                <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                            @endif
                        @else

                            @php
                                $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                            @endphp

                            <span class="text-accent">
                                Precio: S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                            </span>
                            <del class="font-size-sm text-muted">
                                S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                            </del>

                            @if ($prod->ModeloUnidad)
                                <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                            @endif
                        @endif

                    @else
                        <span class="text-accent">Precio: S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                        @if ($prod->ModeloUnidad)
                            <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                        @endif
                    @endif


                </div>
                @guest
                    <div class="w-100 p-1">
                        <button class="btn btn-primary btn-block" type="button" data-toggle="modal" data-target="#InicioSesion">
                            <i class="fas fa-shopping-cart font-size-lg mr-2"></i>
                            Comprar
                        </button>
                    </div>
                @endguest
                @hasrole('cliente')
                    <div class="d-flex mb-4">
                        <div class="w-100 mr-3">
                            <button class="btn btn-outline-primary btn-block" type="button" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                                <i class="fas fa-heart font-size-lg mr-2"></i>
                                <span class='d-sm-inline'>Agregar a Deseos</span>
                            </button>
                        </div>
                        <div class="w-100">
                            <button class="btn btn-primary btn-block" type="button" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                <i class="fas fa-shopping-cart font-size-lg mr-2"></i>
                                Comprar
                            </button>
                        </div>
                    </div>
                @endhasrole

                @php
                    $total_star_perfil = 0;
                    $count_star_perfil = 0;
                    $promedio_star_perfil = 0;
                @endphp

                @foreach ($prod->ModeloValoracionProductos as $star_perfil)
                    @php
                        $total_star_perfil += $star_perfil->estrellas;
                        $count_star_perfil ++;
                        $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                    @endphp
                @endforeach
                <div class="star-rating">

                    @for ($e = 0; $e < 5; $e++)
                        @if ($e < floor($promedio_star_perfil))
                            <i class="sr-star fas fa-star active" style="font-size: 18px;"></i>

                        @elseif($e == floor($promedio_star_perfil))

                            @if (is_float($promedio_star_perfil))
                                @php
                                    $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                @endphp

                                @if (intval($star_array_perfil[1]) >= 50 )
                                    <i class="sr-star fas fa-star-half-alt active" style="font-size: 18px;"></i>
                                @else
                                    <i class="sr-star fas fa-star" style="font-size: 18px;"></i>
                                @endif
                            @else
                                <i class="sr-star fas fa-star" style="font-size: 18px;"></i>
                            @endif
                        @else
                            <i class="sr-star fas fa-star" style="font-size: 18px;"></i>
                        @endif
                    @endfor

                </div>

                <h5 class="h6 mb-3 mt-4 py-2 border-bottom">
                    <i class="fas fa-info-circle text-muted font-size-lg align-middle mt-n1 mr-2"></i>
                    Información del Producto
                </h5>
                <h6 class="font-size-sm mb-2">General</h6>
                <ul class="font-size-sm pb-2">
                    @if ($prod->ModeloMarca)
                        <li><span class="text-muted">Marca: </span>{{ $prod->ModeloMarca->nombre }}</li>
                    @else
                        <li><span class="text-muted">Marca: </span>No definido.</li>
                    @endif

                    @if ($prod->ModeloCategoria)
                        <li><span class="text-muted">Categoría: </span>{{ $prod->ModeloCategoria->nombre }}</li>
                    @else
                        <li><span class="text-muted">Categoría: </span>No definido.</li>
                    @endif

                    @if ($prod->ModeloUnidad)
                        <li><span class="text-muted">Unidad: </span>{{ $prod->ModeloUnidad->nombre }} ({{ $prod->ModeloUnidad->abreviatura }})</li>
                    @else
                        <li><span class="text-muted">Unidad: </span>No definido.</li>
                    @endif

                </ul>

                @if ($prod->ModeloTalla && $prod->ModeloColor)

                    <h6 class="font-size-sm mb-2">Otras Características</h6>
                    <ul class="font-size-sm pb-2">
                        @if ($prod->ModeloTalla)
                            <li><span class="text-muted">Talla: </span>{{ $prod->ModeloTalla->nombre }}</li>
                        @else
                            <li><span class="text-muted">Talla: </span>No definido.</li>
                        @endif

                        @if ($prod->ModeloColor)
                        <li><span class="text-muted">Color: </span>{{ $prod->ModeloColor->nombre }}</li>
                        @else
                            <li><span class="text-muted">Color: </span>No definido.</li>
                        @endif
                    </ul>

                @endif

                <h6 class="font-size-sm mb-2">Descripción del Producto</h6>
                <span class="text-muted">
                    {{ $prod->descripcion }}
                </span>
            </div>
        </div>

        @if (count($prod->ModeloValoracionProductos) > 0)

            <div class="col-12 accordion mt-4" id="accordion">
                <div class="card">

                    <div class="card-header" id="ValoracionHeading">
                        <h3 class="accordion-heading">
                            <a class="collapsed" href="#Valoracion" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="Valoracion">
                                <p class="mb-0">Calificaciones para este producto: {{ count($prod->ModeloValoracionProductos) }}</p>
                                <span class="accordion-indicator"></span>
                            </a>
                        </h3>
                    </div>
                    <div class="collapse" id="Valoracion" aria-labelledby="ValoracionHeading" data-parent="#accordion">
                        <div class="card-body">
                            <div class="col-12 px-2 mb-2">

                                <div class="col-12 p-0">
                                    <div class="container" id="reviews">
                                        <div class="row pb-3">
                                            <div class="col-12">
                                                @php
                                                    $total_star_perfil = 0;
                                                    $count_star_perfil = 0;
                                                    $promedio_star_perfil = 0;
                                                @endphp

                                                @foreach ($prod->ModeloValoracionProductos as $star_perfil)
                                                    @php
                                                        $total_star_perfil += $star_perfil->estrellas;
                                                        $count_star_perfil ++;
                                                        $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                    @endphp
                                                @endforeach
                                                <div class="star-rating">

                                                    @for ($e = 0; $e < 5; $e++)
                                                        @if ($e < floor($promedio_star_perfil))
                                                            <i class="sr-star fas fa-star active" style="font-size: 24px;"></i>

                                                        @elseif($e == floor($promedio_star_perfil))

                                                            @if (is_float($promedio_star_perfil))
                                                                @php
                                                                    $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                @endphp

                                                                @if (intval($star_array_perfil[1]) >= 50 )
                                                                    <i class="sr-star fas fa-star-half-alt active" style="font-size: 24px;"></i>
                                                                @else
                                                                    <i class="sr-star fas fa-star" style="font-size: 24px;"></i>
                                                                @endif
                                                            @else
                                                                <i class="sr-star fas fa-star" style="font-size: 24px;"></i>
                                                            @endif
                                                        @else
                                                            <i class="sr-star fas fa-star" style="font-size: 24px;"></i>
                                                        @endif
                                                    @endfor

                                                </div><br>
                                                <span class="d-inline-block align-middle mt-3">
                                                    Promedio general: <strong>{{ round($promedio_star_perfil, 2) }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt-2 mb-0">
                                        <div class="row">
                                            <div class="col-md-12">

                                                @foreach ($prod->ModeloValoracionProductos as $item)

                                                    <div class="product-review border-bottom pb-2 mt-3">
                                                        <div class="d-flex mb-3">
                                                            <div class="media media-ie-fix align-items-center mr-4 pr-2">
                                                                <img class="rounded-circle" width="50" src="/img/{{ $item->ModeloPersona->avatar }}" onerror="this.src='/img/default-product.png'" alt="Cliente">
                                                                <div class="media-body pl-3">
                                                                    <h6 class="font-size-sm mb-0">{{ $item->ModeloPersona->nombres . " " . $item->ModeloPersona->apellidos }}</h6>
                                                                    <span class="font-size-ms text-muted">{{ $item->created_at }}</span>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h6></h6>
                                                                <div class="star-rating mt-1 pt-2">
                                                                    @for ($i = 0; $i < 5; $i++)
                                                                        @if ($i < $item->estrellas)
                                                                            <i class="sr-star fas fa-star active"></i>
                                                                        @else
                                                                            <i class="sr-star fas fa-star"></i>

                                                                        @endif
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <p class="col-12 font-size-md mb-2">
                                                            {{ $item->resena }}
                                                        </p>
                                                    </div>

                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        @endif
    </div>
</div>
@endforeach
