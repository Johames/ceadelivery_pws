@guest

    {{-- Inicio de Sesión --}}
    <div class="modal fade" id="InicioSesion" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0" style="border: 0px !important">
                    <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
                    <center class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 align-center">
                        <img src="{{ asset('img/cea_market_logo.svg') }}" alt="CeaMarket" style="height: 100px;">
                    </center>
                    <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
                </div>
                <div class="modal-body tab-content pb-4">
                    <ul class="nav nav-tabs card-header-tabs justify-content-center border-bottom mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#formulario_login" data-toggle="tab" role="tab" aria-selected="true">
                                <i class="fas fa-user-lock mr-2 mt-n1"></i>
                                Ingresar
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#formulario_registar_cliente" data-toggle="tab" role="tab"
                                aria-selected="false">
                                <i class="fas fa-user-tie mr-2 mt-n1"></i>
                                Registrarse
                            </a>
                        </li>
                    </ul>
                    <form class="tab-pane fade show active" name="formulario_login" autocomplete="off" id="formulario_login">
                        @csrf
                        <div class="form-group">
                            <label for="si-email">Correo</label>
                            <input class="form-control" type="email" name="email" id="si-email" placeholder="ejemplo@gmail.com">
                        </div>
                        <div class="form-group">
                            <label for="si-password">Password</label>
                            <div class="password-toggle">
                                <input class="form-control" type="password" name="password" id="password" placeholder="Ingrese su contraseña...">
                                <label class="password-toggle-btn">
                                    <input class="custom-control-input" type="checkbox"><i
                                        class="fas fa-eye password-toggle-indicator"></i><span class="sr-only">Ver contraseña</span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group d-flex flex-wrap justify-content-between">
                            <div class="custom-control custom-checkbox mb-2">
                              <input class="custom-control-input"  type="checkbox" name="remember" id="remember">
                              <label class="custom-control-label" for="remember">Recuerdame</label>
                            </div>
                            <a class="font-size-sm" href="{{ asset('/recover') }}">¿Olvidaste tu contraseña?</a>
                        </div>
                        <div class="form-group">
                            <div class="progress" id="div_barra_progress_login">
                                <div id="barra_progress_login" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>

                        <div id="contenedor_de_errores_login"></div>

                        <button class="btn btn-primary btn-block btn-shadow" type="submit">Ingresar</button>
                        <center>
                            <p class="mt-2" style="font-size: 10px;">Si quieres trabajar con nosotros <a href="https://tiendas.ceamarket.com" target="_blank">Ingresa Aquí.</a></p>
                        </center>
                    </form>

                    <form class="tab-pane fade" name="formulario_registar_cliente" id="formulario_registar_cliente" autocomplete="off">
                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="ruc_dni">DNI</label>
                                    <div class="input-group">
                                        {{-- <input class="form-control" type="text" name="ruc" id="ruc" minlength="8" maxlength="8" placeholder="Ingrese su ruc..."> --}}
                                        <input class="form-control" type="text" name="ruc_dni" id="ruc_dni" minlength="8" maxlength="8" placeholder="Ingrese su número de dni..." >
                                        <div class="input-group-prepend">
                                            <button class="btn btn-primary" type="button" onclick="consultaRuc()" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px;">
                                                <i class="fas fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nombres">Nombre(s)</label>
                                    <input class="form-control" type="text" name="nombres" id="nombres" placeholder="Ingrese su(s) nombre(s)">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="apellidos">Apellidos</label>
                                    <input class="form-control" type="text" name="apellidos" id="apellidos" placeholder="Ingrese sus apellidos...">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="email">Correo</label>
                                    <input class="form-control" type="email" name="email" id="email" placeholder="ejemplo@gmail.com" >
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <div class="password-toggle">
                                        <input class="form-control" type="password" name="password" id="password"  placeholder="Ingrese su contraseña...">
                                        <label class="password-toggle-btn">
                                            <input class="custom-control-input" type="checkbox"><i class="fas fa-eye"></i><span class="sr-only">Ver contraseña</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password_confirmation">Confirmar contraseña</label>
                                    <div class="password-toggle">
                                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" placeholder="Ingrese nuevamente su contraseña...">
                                        <label class="password-toggle-btn">
                                            <input class="custom-control-input" type="checkbox"><i class="fas fa-eye"></i><span class="sr-only">Ver confirmar contraseña</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <div class="form-group d-flex flex-wrap justify-content-between">
                                    <div class="custom-control custom-checkbox mb-2">
                                      <input class="custom-control-input"  type="checkbox" name="aceptarTerminosCondiciones" id="aceptarTerminosCondiciones">
                                      <label class="custom-control-label" for="aceptarTerminosCondiciones">Acepto los <a href="{{ asset('img/Terminos y Condiciones de CEAMARKET.pdf') }}" target="_blank">Términos y Condiciones</a></label>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="progress" id="div_barra_progress_registar_cliente">
                                    <div id="barra_progress_registar_cliente" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_registar_cliente"></div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-block btn-shadow" type="submit">Registrarse</button>
                        <center>
                            <p class="mt-2" style="font-size: 10px;">Si quieres trabajar con nosotros <a href="https://tiendas.ceamarket.com" target="_blank">Ingresa Aquí.</a></p>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endguest

    <div id="DetalleProducto" class="modal-quick-view modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content" id="detailProd">

            </div>
        </div>
    </div>

    {{-- Modal Rubro --}}
    {{-- <div class="modal fade" id="modal_rubro" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
        <form class="needs-"  id="formulario_rubro" name="formulario_rubro">
            @csrf
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Registrar un rubro</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="account-fn">Nombre del Rubro</label>
                                    <input class="form-control" type="text" id="nombre_rubro" name="nombre" placeholder="Nombre del Rubro" >
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="progress" id="div_barra_progress_rubro">
                                        <div id="barra_progress_rubro" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div id="contenedor_de_errores_rubro"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary btn-shadow btn-sm" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </form>
    </div> --}}

    {{-- Modal para dejar tu sugerencia --}}
    <div class="modal fade" id="modal_sugerencia" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-body tab-content">
                    <b class="h4">Ayúdanos a mejorar</b>
                    <br><br>

                    <div class="alert alert-info alert-with-icon" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon fas fa-info-circle"></i>
                        </div>
                        <p class="pb-0 mb-0 text-center" style="font-size: .77rem !important;">
                            Déjanos un comentario con tus ideas para mejorar nuestro servicio.
                        </p>
                    </div>


                    <form id="formulario_sugerencia" name="formulario_sugerencia" method="post">
                        @csrf
                        <div class="card">
                            <div class="card-body row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="ruc_dni">Área</label>
                                        <input class="form-control" type="text" name="area_coment" id="area_coment" placeholder="Área de la página..." >
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="ruc_dni">Tipo</label>
                                        <select class="form-control custom-select" name="tipo_coment" id="tipo_coment">
                                            <option selected disabled>Seleccionar un Tipo</option>
                                            <option value="1">Sugerencia</option>
                                            <option value="2">Reclamo</option>
                                            <option value="3">Otro</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="ruc_dni">Comentario</label>
                                        <textarea class="form-control" id="comentario_coment" name="comentario_coment" rows="5" placeholder="Escríbenos su Comentario!..."></textarea>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="progress" id="div_barra_progress_sugerencia">
                                            <div id="barra_progress_sugerencia" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="contenedor_de_errores_sugerencia"></div>
                                </div>

                                <div class="col-12 align-content-center justify-content-center text-center">
                                    <button class="btn btn-outline-primary btn-sm" type="submit">
                                        Enviar Comentario
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@hasrole('cliente')

    {{-- Modal Elegir Lista de Deseos --}}
    <div class="modal fade" id="modal_elegir_deseos" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Seleccionar una lista de deseos</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body font-size-sm">
                    <div class="col-12 p-0">
                        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">

                        <ul class="list-group" id="ListasDeseos" style="height: 450px; overflow-y: auto;">
                        </ul>

                        <div class="col-12 mt-2">
                            <div class="form-group">
                                <div class="progress" id="div_barra_progress_elegir_deseos">
                                    <div id="barra_progress_elegir_deseos" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mt-2">
                            <div id="contenedor_de_errores_elegir_deseos"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer m-0 p-0">
                    <button class="btn btn-primary btn-block btn-shadow m-0" style="border-radius: 0px !important;" data-toggle="modal" data-target="#modal_deseos" type="button ">Agregar Lista de deseos</button>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Agregar Lista de Deseos --}}
    <div class="modal fade" id="modal_deseos" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md modal-dialog-scrollable modal-dialog-centered" role="document">
            <form class="needs-"  id="formulario_deseos" name="formulario_deseos">
                @csrf
                <input type="hidden" id="deseos_id" name="deseos_id">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title">AGREGAR O MODIFICAR UNA LISTA DE DESEOS</h6>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="llegada">Nombre de la lista de deseos</label>
                                <input class="form-control" type="text" id="nombre" name="nombre" placeholder="Nombre">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="descripcion">Descripción <b class="text-danger"><small>(opcional)</small></b></label>
                                <textarea class="form-control" rows="5" id="descripcion" name="descripcion" placeholder="Descripcion"></textarea>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="progress" id="div_barra_progress_deseos">
                                    <div id="barra_progress_deseos" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="contenedor_de_errores_deseos"></div>
                        </div>
                    </div>
                    <div class="modal-footer m-0 p-0">
                        <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary btn-shadow btn-sm" style="border-radius: 0px !important;" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Modal del Carrito --}}
    <div id="modal_carrito" class="modal fade modal-right" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content">
                <div class="modal-header-lateral bg-dark">
                    <h5 class="modal-title" style="color: white;">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> MI CARRITO
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color: white;">&times;</span>
                    </button>
                </div>
                <div class="modal-body lateral bg-faded-success" id="lista_carrito_modal">

                </div>
            </div>
        </div>
    </div>

    {{-- Modal Valoracion Empresa --}}
    <div class="modal fade" id="modal_registar_valoracion" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0" style="border: 0px !important">
                    <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
                    <center class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 align-center">
                        <img id="img_valor" src="" alt="logo" style="height: 100px; width: 100px;">
                    </center>
                    <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
                <div class="modal-body tab-content pb-4">
                    <form name="formulario_registar_valoracion" id="formulario_registar_valoracion" autocomplete="off">
                        @csrf
                        <input type="hidden" id="tipo" name="tipo">
                        <input type="hidden" id="id_valoracion" name="id_valoracion">
                        <div class="row">
                            <div class="col-12">
                                <div class="h5 text-center" id="nombre_valor">
                                </div>
                            </div>
                            <div class="p-0 m-0 col-2 col-xs-2 col-sm-3 col-md-3 col-lg-3"></div>
                            <div class="col-10 col-xs-10 col-sm-7 col-md-7 col-lg-7 col-xl-7 align-items-center">
                                <span class="star-cb-group">
                                    <input type="radio" id="rating-5" name="rating" value="5" />
                                    <label for="rating-5" style="color: white !important;">5</label>
                                    <input type="radio" id="rating-4" name="rating" value="4" />
                                    <label for="rating-4" style="color: white !important;">4</label>
                                    <input type="radio" id="rating-3" name="rating" value="3" />
                                    <label for="rating-3" style="color: white !important;">3</label>
                                    <input type="radio" id="rating-2" name="rating" value="2" />
                                    <label for="rating-2" style="color: white !important;">2</label>
                                    <input type="radio" id="rating-1" name="rating" value="1" />
                                    <label for="rating-1" style="color: white !important;">1</label>
                                    <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" />
                                    <label for="rating-0" style="color: white !important;">0</label>
                                </span>
                            </div>
                            <input type="hidden" id="valoracion" name="valoracion">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="account-phone">Reseña</label>
                                    <textarea class="form-control" rows="5" placeholder="Escriba una reseña..." id="resena" name="resena"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="progress" id="div_barra_progress_registar_valoracion">
                                    <div id="barra_progress_registar_valoracion" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_registar_valoracion"></div>
                            </div>
                        </div>

                        <button class="btn btn-primary btn-block btn-shadow" type="submit">Guardar Reseña</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal para seleccionar Ciudad --}}
    <div class="modal fade" id="modal_ciudad" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0" style="border: 0px !important">
                </div>
                <div class="modal-body tab-content pb-4">
                    <p class="mb-1">Hola,
                        <b class="mb-0">
                            @if (Auth::user()->ModeloPersona->nombres)
                                {{ Auth::user()->ModeloPersona->nombres }} {{ Auth::user()->ModeloPersona->apellidos }}
                            @else

                                @if (Auth::user()->ModeloPersona->razon_social)
                                    {{ Auth::user()->ModeloPersona->razon_social }}
                                @else
                                    {{ Auth::user()->ModeloPersona->nombre_comercial }}
                                @endif

                            @endif
                        </b>
                    </p>
                    <p>¿En donde deseas buscar hoy?</p>

                    <div class="alert alert-info alert-with-icon mt-2" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon fas fa-info-circle"></i>
                        </div>
                        <p class="pb-0" style="font-size: .77rem !important;">
                            Si no selecciona una ciudad se mostraran todas las tiendas existentes en la plataforma.
                            <br><br>
                            Para cambiar la ciudad en la que desea buscar presione sobre el boton azul de la parte inferior derecha de la pagina.
                        </p>
                    </div>


                    <form id="formulario_ciudad" name="formulario_ciudad">
                        @csrf
                        <div class="card align-content-center">
                            <div class="card-body text-center">
                                <i class="fas fa-map-marked-alt fa-3x mt-2 mb-3 text-primary"></i>
                                <div class="col-12 mt-0 px-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <select class="form-control custom-select" id="select_modal_ciudad" name="ciudad" onchange="cambioCiudad()">

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <p class="font-size-sm text-muted mb-3" id="textoRefrencia"></p>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="progress" id="div_barra_progress_ciudad">
                                            <div id="barra_progress_ciudad" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="contenedor_de_errores_ciudad"></div>
                                </div>

                                <button class="btn btn-outline-primary btn-sm" type="submit">
                                    Listo
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    {{-- Modal para Agregar un Teléfono --}}
    <div class="modal fade" id="modal_telefono" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0" style="border: 0px !important">
                </div>
                <div class="modal-body tab-content pb-4">
                    <b>Antes de continuar...</b>

                    <div class="alert alert-info alert-with-icon mt-2" role="alert">
                        <div class="alert-icon-box">
                            <i class="alert-icon fas fa-info-circle"></i>
                        </div>
                        <p class="pb-0" style="font-size: .77rem !important;">
                            Un número de teléfono / celular es indispensable para el proceso de pedido.
                            <br><br>
                            Por favor ingrese su número de teléfono / celular para continuar.
                        </p>
                    </div>


                    <form id="formulario_telefono" name="formulario_telefono">
                        @csrf
                        <div class="card align-content-center">
                            <div class="card-body text-center">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="referencia">Nro. Celular / Telefono</label>
                                        <input class="form-control" type="text" id="telefono" name="telefono" placeholder="Nro. de Celular / Teléfono..." >
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="progress" id="div_barra_progress_telefono">
                                            <div id="barra_progress_telefono" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="contenedor_de_errores_telefono"></div>
                                </div>

                                <button class="btn btn-outline-primary btn-sm" type="submit">
                                    Guardar Número
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    {{-- Modal Pagar el pedido --}}
    <div class="modal fade" id="modal_qr" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-2 justify-content-center" style="border: 0px !important">
                    <h4 class="modal-title">Código QR del Pedido</h4>
                </div>
                <hr>
                <div class="modal-body pt-0">

                    <center>
                        <div class="card card-product mb-3 mt-4" id="QRCodeImg">

                        </div>
                    </center>

                    <center class="mt-2">
                        <button class="btn btn-outline-info btn-shadow" type="button" data-dismiss="modal">cerrar</button>
                    </center>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Detalle Notificacion --}}
    <div class="modal" tabindex="-1" id="modal_detalle_notificacion" role="dialog">
        <div class="modal-dialog" role="document" >
            <div class="modal-content" style="border-radius: 15px !important;  ">

                <div class="modal-header">
                    <h5 class="modal-title">NOTIFICACION NUEVA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="notificaciones_navbar()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" id="detallNotificacionNavBar">

                </div>
            </div>
        </div>
    </div>

    {{-- Modal Detalle Pedido Notificacion --}}
    <div id="DetallePedidoNotificacion" class="modal fade modal-right" tabindex="-1" role="dialog" onclick="notificaciones_navbar()">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content" id="DetallePedidoNotif">

            </div>
        </div>
    </div>

@endhasrole
