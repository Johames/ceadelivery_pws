<!-- Estilos -->
<script src="{{ asset('js/estilos/jquery.min.js') }}"></script>
<script src="{{ asset('/resources/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>
{{-- <script src="{{ asset('js/estilos/jquery.slim.min.js') }}"></script> --}}
<script src="{{ asset('js/estilos/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/estilos/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('js/estilos/simplebar.js') }}"></script>
<script src="{{ asset('js/estilos/tiny-slider.js') }}"></script>
<script src="{{ asset('js/estilos/smooth-scroll.js') }}"></script>
<script src="{{ asset('js/estilos/nouislider.js') }}"></script>
<script src="{{ asset('js/estilos/drift-basic.js') }}"></script>
<script src="{{ asset('js/estilos/lightgallery.js') }}"></script>
<script src="{{ asset('js/estilos/lg-video.js') }}"></script>
<script src="{{ asset('js/estilos/metodos_pago.js') }}"></script>

{{-- sweetalert --}}
<script src=" {{ asset('js/sweetalert/script.js') }}"></script>
<script src=" {{ asset('js/sweetalert/alerts.js') }}"></script>

<script src="{{ asset('/resources/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('/resources/jquery-qrcode-master/jquery.qrcode.min.js') }}"></script>

<script src="{{ asset('/resources/dropzone/dist/dropzone.js') }}"></script>

<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $(function () {
        $('[data-toggle="popover"]').popover()
    });
</script>

<!-- Estilos Principales -->
<script src="{{ asset('js/estilos/theme.js') }}"></script>
<script src="{{ asset('js/general/general.js') }}"></script>
<script src="{{ asset('/funciones/crud.js') }}"></script>

@hasrole('cliente')
<script src="{{ asset('ajax_web/ajaxAddDeseos.js') }}"></script>
<script src="{{ asset('ajax_web/ubigeo.js') }}"></script>
<script src="{{ asset('ajax_web/ajaxCarrito.js') }}"></script>
<script src="{{ asset('/ajax_admin/client/ajaxNotificacionesNavbar.js') }}"></script>
@endhasrole

@hasanyrole('administrador_empresa|deliverista_empresa')
<script src="{{ asset('/ajax_admin/ajaxNotificacionesNavbar.js') }}"></script>
@endhasanyrole

@guest
<script src="{{ asset('/ajax_web/login.js') }}"></script>
@endguest

@yield('js')
