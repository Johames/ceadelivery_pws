<header class="bg-light box-shadow-sm fixed-top"><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <div class="navbar-sticky bg-light">

        @yield('header_nv_1')

        <div class="navbar navbar-expand navbar-light">
            <div class="container">

                <a class="navbar-brand d-none d-sm-block mr-3 flex-shrink-0" href="{{ route('Inicio') }}">
                    <img width="110" src="{{ asset('/img/cea_market_logo.svg') }}" alt="Cea Market">
                </a>
                <a class="navbar-brand d-sm-none mr-2" href="{{ route('Inicio') }}">
                    <img src="{{ asset('img/cea_market_icon.svg') }}" style="height: 45px !important;" alt="Cea Market">
                </a>

                {{-- Buscador --}}
                <div class="input-group-overlay mx-4 col-md-6 d-none d-md-block">
                    <input class="form-control prepended-form-control appended-form-control" type="text" id="Buscador" placeholder="Buscar Productos en todas las tiendas">
                    <div class="input-group-append-overlay" style="background-color: white;">
                        <button class="btn btn-outline-primary" type="submit" onclick="buscartext()" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">
                            <i class="fas fa-search white" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>

                <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">

                    @yield('viewsidebar')

                    <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2 d-block d-md-none" href="/buscador/">
                        <div class="navbar-tool-icon-box">
                            <i class="navbar-tool-icon fas fa-search" aria-hidden="true"></i>
                        </div>
                    </a>

                    @guest

                        <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2" href="#InicioSesion" data-toggle="modal">
                            <div class="navbar-tool-icon-box">
                                <i class="navbar-tool-icon fas fa-user" aria-hidden="true"></i>
                            </div>
                            <div class="navbar-tool-text ml-n3">
                                <small>Por favor</small>Ingresar
                            </div>
                        </a>

                        <!-- CARRITO INVITADO -->
                        <div class="navbar-tool dropdown ml-3">
                            <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" data-toggle="modal" href="#InicioSesion">
                                <i class="navbar-tool-icon fas fa-shopping-cart"></i>
                            </a>
                            <a class="navbar-tool-text" href="javascript:void(0);">
                                <small>Mi Carrito</small>S/  0.00
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" style="width: 20rem;">
                                <div class="widget widget-cart px-3 pt-2 pb-3">
                                    <div data-simplebar data-simplebar-auto-hide="false">
                                        <div class="justify-content-between align-items-center">
                                            <div class="card">
                                                <div class="card-body text-center">
                                                    <i class="fas fa-exclamation-triangle fa-5x mt-2 mb-4 text-primary"></i>
                                                    <p class="font-size-sm text-muted">Inicie Sesión para continuar</p>
                                                    <a class="btn btn-outline-primary btn-sm" data-toggle="modal" href="#InicioSesion">
                                                        <b>Iniciar Sesión</b>
                                                        <i class="fas fa-chevron-right align-middle ml-1"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @else

                    @hasrole('cliente')

                        <a class="d-none d-xs-none d-sm-none d-md-none navbar-tool-icon-box bg-secondary navbar-tool d-lg-flex mr-2" href="{{ route('AdmDeseosClient') }}">
                            {{-- <span class="navbar-tool-tooltip">Deseos</span> --}}
                            <div class="navbar-tool-icon-box">
                                <i class="navbar-tool-icon fas fa-heart"></i>
                            </div>
                        </a>

                        <!-- Cart dropdown-->
                        <div class="navbar-tool dropdown ml-3">
                            <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="#modal_carrito" data-toggle="modal" onclick="listar_carrito_modal()">
                                <span class="navbar-tool-label bg-primary" id="nav_car_pedido_num">

                                </span>
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            </a>

                            <a class="navbar-tool-text" href="#modal_carrito" data-toggle="modal" onclick="listar_carrito_modal()">
                                <small>Mi carrito</small>
                                <span id="nav_car_pedido_detalle">Ver</span>

                            </a>
                        </div>
                    @endhasrole

                    @hasanyrole('cliente|administrador_empresa|deliverista_empresa')
                        <div class="navbar-tool dropdown ml-3" id="NotifNavbar">

                        </div>
                    @endhasanyrole



                    <!-- PERFIL USUARIO -->
                    <div class="navbar-tool dropdown ml-3">
                        <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="{{ route('AdmPerfilClient') }}">
                            <i class="navbar-tool-icon fas fa-user" aria-hidden="true"></i>
                        </a>
                        <a class="d-none d-xs-none s-sm-none d-md-none d-lg-block d-xl-block navbar-tool-text" href="#">
                            <small>Mi Perfil</small>
                            @if ( Auth::user()->ModeloPersona->nombres )
                                {{ Auth::user()->ModeloPersona->nombres }}
                            @else
                                @if (strlen(Auth::user()->ModeloPersona->razon_social) > 20)
                                    {{ substr(Auth::user()->ModeloPersona->razon_social, 1, 20) }}...
                                @endif
                            @endif
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" style="width: 20rem;">
                            <div class="widget widget-cart px-3 pt-2 pb-3">
                                <div data-simplebar data-simplebar-auto-hide="false">
                                    <div class="widget-cart-item pb-2 border-bottom">

                                        @hasrole('cliente')
                                        <a class="dropdown-item" href="{{ route('AdmPerfilClient') }}">
                                            <div class="d-flex">
                                                <div class="lead text-muted pt-1">
                                                    <i class="navbar-tool-icon fas fa-user" aria-hidden="true"></i>
                                                </div>
                                                <div class="ml-2">
                                                    <span class="d-block text-heading">
                                                        @if ( Auth::user()->ModeloPersona->nombres )
                                                            {{ Auth::user()->ModeloPersona->nombres }}
                                                        @else
                                                            @if (strlen(Auth::user()->ModeloPersona->razon_social) > 20)
                                                                {{ substr(Auth::user()->ModeloPersona->razon_social, 1, 20) }}...
                                                            @endif
                                                        @endif
                                                    </span>
                                                    <small class="d-block text-muted">Ir a mi perfil</small>
                                                </div>
                                            </div>
                                        </a>
                                        @endhasrole

                                    </div>
                                    <div class="widget-cart-item py-2 border-bottom">
                                        <a class="dropdown-item" href="{{ route('LogOut') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            <div class="d-flex">
                                                <div class="lead text-muted pt-1">
                                                    <i class="fas fa-sign-out-alt" aria-hidden="true"></i>
                                                </div>
                                                <div class="ml-2">
                                                    <span class="d-block text-heading">Salir</span>
                                                    <small class="d-block text-muted">Cerrar Sesión</small>

                                                    <form id="logout-form" action="{{ route('LogOut') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endguest

                </div>
            </div>
        </div>

        @yield('submenus')

</header>
