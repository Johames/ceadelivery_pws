<div class="modal-header-lateral bg-dark">
    <h5 class="modal-title" style="color: white;">
        @if ($empresas->razon_social)
            {{ $empresas->razon_social }}
        @else
            {{ $empresas->nombre_comercial }}
        @endif
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" style="color: white;">&times;</span>
    </button>
</div>
<div class="modal-body lateral bg-faded-success">
    <div class="col-12 px-2 mb-4">
        {{-- @if (count($empresas->ModeloRedesSociales) > 0)
                <div class="card mb-2">
                    <div class="card-header p-1 m-1">
                        <h5 class="p-0 m-0">Redes Sociales</h5>
                    </div>
                    <div class="card-body">
                        <div class="tab-pane fade active show mt-2" role="tabpanel">
                            @foreach ($empresas->ModeloRedesSociales as $redes)
                                <a class="social-btn sb-round sb-outline {{ $redes->color }} mb-2" href="{{ $redes->url }}">
                                    <i class="{{ $redes->icono }}"></i>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif --}}
        <div class="accordion" id="accordion">
            <div class="card">
                <div class="card-header" id="InfoHeading">
                    <h3 class="accordion-heading">
                        <a href="#Informacion" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="Informacion">
                            Información General
                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse show" id="Informacion" aria-labelledby="InfoHeading" data-parent="#accordion">
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <ul class="list-unstyled mb-0">
                                    <li class="media pt-1 pb-2 border-bottom">
                                        <i class="fas fa-id-card font-size-lg mt-2 mb-0 text-primary"></i>
                                        <div class="media-body pl-3">
                                            <span class="font-size-ms text-muted">Ruc</span>
                                            <span class="d-block text-heading font-size-sm">{{ $empresas->ruc }}</span>
                                        </div>
                                    </li>
                                    <li class="media pt-1 pb-2 border-bottom">
                                        <i class="fas fa-signature font-size-lg mt-2 mb-0 text-primary"></i>
                                        <div class="media-body pl-3">
                                            <span class="font-size-ms text-muted">Razón Social</span>
                                            <span class="d-block text-heading font-size-sm">
                                                @if ($empresas->razon_social)
                                                    {{ $empresas->razon_social }}
                                                @else
                                                    No definido.
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="media pt-1 pb-2 border-bottom">
                                        <i class="fas fa-signature font-size-lg mt-2 mb-0 text-primary"></i>
                                        <div class="media-body pl-3">
                                            <span class="font-size-ms text-muted">Nombre Comercial</span>
                                            <span class="d-block text-heading font-size-sm">
                                                @if ($empresas->nombre_comercial)
                                                    {{ $empresas->nombre_comercial }}
                                                @else
                                                    No definido.
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="media pt-1 pb-2 border-bottom">
                                        <i class="fas fa-user-tie font-size-lg mt-2 mb-0 text-primary"></i>
                                        <div class="media-body pl-3">
                                            <span class="font-size-ms text-muted">Representante Legal</span>
                                            <span class="d-block text-heading font-size-sm">
                                                @if ($empresas->representante_legal)
                                                    {{ $empresas->representante_legal }}
                                                @else
                                                    No definido.
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="ContactoHeading">
                    <h3 class="accordion-heading">
                        <a class="collapsed" href="#InformacionContacto" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="InformacionContacto">
                            Información de Contacto
                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse" id="InformacionContacto" aria-labelledby="ContactoHeading" data-parent="#accordion">
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                <ul class="list-unstyled mb-0">
                                    @if (count($empresas->ModeloContactos) > 0)
                                        <li class="media pt-1 pb-2 border-bottom">
                                            <i class="fas fa-headset font-size-lg mt-2 mb-0 text-primary"></i>
                                            <div class="media-body pl-3">
                                                <span class="font-size-ms text-primary">Número de Teléfono</span>
                                                @foreach ($empresas->ModeloContactos as $telefono)
                                                    @if ($telefono->tipo == 0)
                                                        <a href="tel:+{{ $telefono->nombre }}" class="d-block text-heading font-size-sm">{{ $telefono->nombre }}</a>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </li>
                                    @endif
                                    <li class="media pt-2m pb-3 border-bottom">
                                        <i class="fas fa-city font-size-lg mt-2 mb-0 text-primary"></i>
                                        <div class="media-body pl-3">
                                            <span class="font-size-ms text-primary">Ciudad</span>
                                            <span class="d-block text-heading font-size-sm">
                                                @if ($empresas->ModeloUbigeo)
                                                    {{ $empresas->ModeloUbigeo->nombre }}
                                                @else
                                                   Sin Definir
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    <li class="media pt-2m pb-3 border-bottom">
                                        <i class="fas fa-map-marker-alt font-size-lg mt-2 mb-0 text-primary"></i>
                                        <div class="media-body pl-3">
                                            <span class="font-size-ms text-primary">Dirección</span>
                                            <span class="d-block text-heading font-size-sm">
                                                @if ($empresas->direccion)
                                                    {{ $empresas->direccion }}
                                                @else
                                                    No definido.
                                                @endif
                                            </span>
                                        </div>
                                    </li>
                                    @if (count($empresas->ModeloContactos) > 0)
                                        <li class="media pt-1 pb-2 border-bottom">
                                            <i class="fas fa-envelope-open-text font-size-lg mt-2 mb-0 text-primary"></i>
                                            <div class="media-body pl-3">
                                                <span class="font-size-ms text-primary">Correo Electrónico</span>
                                                @foreach ($empresas->ModeloContactos as $correo)
                                                    @if ($correo->tipo == 1)
                                                        <a href="mailto:{{ $correo->nombre }}" data-toggle="tooltip" data-placement="right" title="" data-original-title="{{ $correo->area }}" class="d-block text-heading font-size-sm">
                                                            {{ $correo->nombre }}
                                                        </a>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="HorarioHeading">
                    <h3 class="accordion-heading">
                        <a class="collapsed" href="#Horarios" role="button" data-toggle="collapse"
                            aria-expanded="true" aria-controls="Horarios">
                            Horarios de Atención
                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse" id="Horarios" aria-labelledby="HorarioHeading" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-hover bg-light font-size-sm border-bottom">
                                <thead>
                                    <tr>
                                        <th class="align-middle">Días</th>
                                        <th class="align-middle">Turno</th>
                                        <th class="align-middle">Entrada</th>
                                        <th class="align-middle">Salida</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($empresas->ModeloHorarios as $horario)
                                        <tr>
                                            <td class="align-middle">{{ $horario->dia }}</td>
                                            <td class="align-middle">
                                                @if ($horario->turnos == '1')
                                                    Un solo turno
                                                @else
                                                    Mañana y Tarde
                                                @endif
                                            </td>
                                            <td class="align-middle">
                                                @if ($horario->turnos == '1')
                                                    {{ $horario->hora_abre_am }}
                                                @else
                                                    {{ $horario->hora_abre_am . ' - ' . $horario->hora_cierre_am }}
                                                @endif
                                            </td>
                                            <td class="align-middle">
                                                @if ($horario->turnos == '1')
                                                    {{ $horario->hora_cierre_pm }}
                                                @else
                                                    {{ $horario->hora_abre_pm . ' - ' . $horario->hora_cierre_pm }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="MetodoEnvioHeading">
                    <h3 class="accordion-heading">
                        <a class="collapsed" href="#MetodoEnvio" role="button" data-toggle="collapse"
                            aria-expanded="true" aria-controls="MetodoEnvio">
                            Métodos de Envío
                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse" id="MetodoEnvio" aria-labelledby="MetodoEnvioHeading" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-hover bg-light font-size-sm border-bottom">
                                <thead>
                                    <tr>
                                        <th class="align-middle">Método</th>
                                        <th class="align-middle">Llegada</th>
                                        <th class="align-middle">Costo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($empresas->ModeloDetalleMetodoEnvios as $detalle_envio)
                                        <tr>
                                            <td class="align-middle">
                                                <div class="media align-items-center">
                                                    <div class="media-body pl-3">
                                                        <span
                                                            class="text-dark font-weight-medium">{{ $detalle_envio->ModeloMetodoEnvio->nombre }}</span>
                                                        <br>
                                                        <span
                                                            class="text-muted">{{ $detalle_envio->descripcion }}</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="align-middle">{{ $detalle_envio->llegada }}</td>
                                            <td class="align-middle"> S/
                                                {{ number_format((float)$detalle_envio->precio, 2, '.', '') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="MetodoPagoHeading">
                    <h3 class="accordion-heading">
                        <a class="collapsed" href="#MetodoPago" role="button" data-toggle="collapse"
                            aria-expanded="true" aria-controls="MetodoPago">
                            Métodos de Pago
                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse" id="MetodoPago" aria-labelledby="MetodoPagoHeading" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive mt-2">
                            <table class="table table-hover bg-light font-size-sm border-bottom">
                                <thead>
                                    <tr>
                                        <th class="align-middle">Método</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($empresas->ModeloDetalleMetodoPagos as $detalle_pagos)
                                        <tr>
                                            <td class="align-middle">
                                                <div class="media align-items-center">
                                                    <div class="media-body pl-3">
                                                        <span
                                                            class="text-dark font-weight-medium">{{ $detalle_pagos->ModeloMetodoPago->nombre }}</span>
                                                        <br>
                                                        <span
                                                            class="text-muted">{{ $detalle_pagos->descripcion }}
                                                        </span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

