<!DOCTYPE html>
<html>

    @section('htmlheader')
        @include('secciones.head')
    @show

    <body @yield('faded') style="background-color: #fafaf8">

        @include('secciones.navbar')

        @yield('menulateral')

        <main class="sidebar-fixed-enabled">
            <section class="mt-3">
              <div class="mt-5">

                @yield('content')

              </div>
            </section>

            <!-- Boton para ir Arriba -->
            <a class="btn-scroll-top" href="#top" data-scroll>
                <span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Subir</span>
                <i class="btn-scroll-top-icon fas fa-chevron-up"> </i>
            </a>

            @yield('socialbuttons')

            <footer class="bg-dark">

                @include('secciones.footer')

            </footer>
        </main>

        @yield('modals')

        @include('secciones.modals')

        @include('secciones.js')

    </body>

</html>
