@extends('principal')

@section('title_header')
 Inicio
@endsection

@section('submenus')

    <hr>
    <div class="navbar navbar-expand-lg navbar-light mt-2 pt-0 pb-2">
        <div class="container">
            <div class="navbar-expand"></div>
            <div class="collapse navbar-collapse" id="navbarCollapse">

                <ul class="navbar-nav mega-nav pr-lg-2 mr-lg-2">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle ml-2 pl-2" href="#" data-toggle="dropdown">
                            <i class="fas fa-th-large align-middle mt-n1 mr-2"></i>
                            Rubros de nuestras tiendas
                        </a>
                        <ul class="dropdown-menu">
                            @foreach ($rubros_empresas as $rubro)
                                <li class="dropdown mega-dropdown">
                                    <a class="dropdown-item dropdown-toggle" href="#" data-toggle="dropdown">
                                        <i class="czi-laptop opacity-60 font-size-lg mt-n1 mr-2"></i>
                                        {{ $rubro->nombre }}
                                    </a>
                                    <div class="dropdown-menu p-0">
                                        <div class="d-flex flex-wrap flex-md-nowrap px-2">
                                            <div class="mega-dropdown-column py-4 px-3">
                                                <div class="widget widget-links">
                                                    <h6>Empresas</h6>
                                                    <ul class="widget-list">
                                                        @foreach ($rubro->ModeloEmpresa as $emp)
                                                            <li class="widget-list-item pb-1">
                                                                <a class="widget-list-link" href="/store/{{ $emp->slug }}">
                                                                    @if ($emp->razon_social)
                                                                        @if (strlen($emp->razon_social) > 35)
                                                                            {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}..
                                                                        @else
                                                                            {{ $emp->razon_social }}
                                                                        @endif
                                                                    @else
                                                                        @if (strlen($emp->nombre_comercial) > 35)
                                                                            {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                                        @else
                                                                            {{ $emp->nombre_comercial }}
                                                                        @endif
                                                                    @endif
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </li>
                </ul>

                <!-- <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="fas fa-truck"></i>
                            Envio Gratis
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="fas fa-store"></i>
                            Supermercado
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="fas fa-tags"></i>
                            Ofertas de la Semana
                        </a>
                    </li>
                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">A menos de S/ 99</a></li>
                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Lo Nuevo</a></li>
                </ul> -->

            </div>
        </div>
    </div>

@endsection

@section('carroucel')

    <!-- Rubros -->
    <div style="height: 163px"></div>


    <section class="bg-accent bg-size-cover bg-position-center mt-1">
        <div class="cea-carousel border-right">
            @if (count($publicacion) > 0)
                <div class="cea-carousel-inner" data-carousel-options="{ &quot;nav&quot;: false, &quot;mode&quot;: &quot;gallery&quot;, &quot;controls&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 15000, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;360&quot;:{&quot;items&quot;:1},&quot;600&quot;:{&quot;items&quot;:1},&quot;991&quot;:{&quot;items&quot;:1},&quot;1200&quot;:{&quot;items&quot;:1}} }">
                    @foreach ($publicacion as $item)
                        @if ($item->ubicacion == 1)
                        <div>
                            <div class="d-block bg-white py-o px-o" style="margin-right: -.0625rem;">
                                <img class="d-none d-xs-none d-sm-none d-md-block d-lg-block d-xl-block mx-auto" src="https://admin.ceamarket.com/img/{{ $item->banner }}" onerror="this.src='/img/cards.png'" style="width: 100%; height: 140px;" alt="{{ $item->alt }}">
                                <img class="d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none" src="https://admin.ceamarket.com/img/{{ $item->banner_xs }}" onerror="this.src='/img/cards.png'" style="width: 100%; height: 140px;" alt="{{ $item->alt }}">
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </section>


    <section class="bg-accent border-bottom bg-size-cover bg-position-center px-0 py-2">
        <div class="container-fluid py-0 px-0 px-md-5 px-lg-5 m-0">
            <section class="cea-carousel">
                @if (count($promociones) > 0)
                    <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 1, &quot;nav&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;: {&quot;nav&quot;: true, &quot;controls&quot;: false}, &quot;576&quot;: {&quot;nav&quot;: false, &quot;controls&quot;: true}}}">
                        @foreach ($promociones as $prom)
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-lg">
                                            <div class="py-2 my-2 my-md-0 px-4 text-center text-sm-left">
                                                <span class="badge badge-danger badge-shadow mb-3">Usa el código: {{ $prom->codigo }}</span>
                                                <h5 class="mb-4">{{ $prom->titulo }}</h5>
                                                <h6 class="font-size-lg font-weight-light mb-2" id="view_descripcion">{{ $prom->descripcion }}</h6>
                                                <br><br>
                                                <span class="font-size-ms text-muted">
                                                    @if ($prom->tipo == 0)
                                                        <p><strong>S/  {{ $prom->descuento }}</strong> de Descuento</p>
                                                    @else
                                                        <p><strong>{{ $prom->descuento }}%</strong> de Descuento</p>
                                                    @endif
                                                </span>
                                                <span class="align-center font-size-ms text-muted" style="font-size: 9px !important;">
                                                    Promoción Válida Hasta: <strong id="view_fecha_fin">{{ $prom->fecha_fin }}</strong>
                                                </span>
                                            </div>
                                            <img class="d-block ml-auto" src="https://tiendas.ceamarket.com/img/{{ $prom->img_prom }}" onerror="this.src='img/default-ads.png'" alt="Promocion" style="height: 320px !important;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </section>
        </div>
    </section>


    <section class="container-fluid mt-2 pb-2 mt-2">
        <div class="justify-content-between">
            <h2 class="text-center py-2">¿Qué estas buscando?</h2>
            <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2 pb-4">
                @if (count($rubros) >= 5)
                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:4}, &quot;1500&quot;:{&quot;items&quot;:5}}}">
                @else
                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:{{ count($rubros) }}}, &quot;1500&quot;:{&quot;items&quot;:{{ count($rubros) }}}}}">
                @endif

                    @foreach($rubros as $rub)

                        <div>
                            <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                <a class="card border-0" href="/rubro/{{ $rub->slug }}">
                                    @if ($rub->img_rubro)
                                        <img class="card-img-top" src="https://admin.ceamarket.com/img/{{ $rub->img_rubro }}" alt="{{ $rub->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                    @else
                                        <img class="card-img-top" src="/img/food-delivery/category/01.jpg" alt="rubro" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                    @endif
                                    <div class="card-body py-2 text-center">
                                        @if (strlen($rub->nombre) > 15)
                                            <h6 class="mt-1" style="font-size: 15px" data-toggle="tooltip" data-placement="bottom" title="{{ $rub->nombre }}">{{ substr($rub->nombre, 0, 15) }}...</h6>
                                        @else
                                            <h6 class="mt-1" style="font-size: 15px">{{ $rub->nombre }}</h6>
                                        @endif
                                    </div>
                                </a>
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </section>


    <section class="bg-accent bg-size-cover bg-position-center">
        <div class="cea-carousel border-right">
            @if (count($publicacion) > 0)
                <div class="cea-carousel-inner" data-carousel-options="{ &quot;nav&quot;: false, &quot;mode&quot;: &quot;gallery&quot;, &quot;controls&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 15000, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;360&quot;:{&quot;items&quot;:1},&quot;600&quot;:{&quot;items&quot;:1},&quot;991&quot;:{&quot;items&quot;:1},&quot;1200&quot;:{&quot;items&quot;:1}} }">
                    @foreach ($publicacion as $item)
                        @if ($item->ubicacion == 2)
                        <div>
                            <div class="d-block bg-white py-o px-o" style="margin-right: -.0625rem;">
                                <img class="d-none d-xs-none d-sm-none d-md-block d-lg-block d-xl-block mx-auto" src="https://admin.ceamarket.com/img/{{ $item->banner }}" onerror="this.src='/img/cards.png'" style="width: 100%; height: 140px;" alt="{{ $item->alt }}">
                                <img class="d-block d-xs-block d-sm-block d-md-none d-lg-none d-xl-none" src="https://admin.ceamarket.com/img/{{ $item->banner_xs }}" onerror="this.src='/img/cards.png'" style="width: 100%; height: 140px;" alt="{{ $item->alt }}">
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </section>



@endsection

@section('content')

    <div class="px-sm-2 px-md-5 px-lg-5">
        <!-- Listado de las Tiendas -->
        {{-- @if (count($rubros_empresas) > 0)

            @if (count($rubros_empresas) > 35)

                <section class="container-fluid bg-light mt-4 mb-2 px-sm-2 px-md-4 px-lg-4" style="border-radius: 10px;">
                    @foreach ($rubros_empresas as $rubro)
                        <div class="mt-4" id="{{ $rubro->nombre }}">
                            <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                                <h2 class="h3 mb-0 pt-1 mr-3">{{ $rubro->nombre }}</h2>
                            </div>
                            <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:4}, &quot;1500&quot;:{&quot;items&quot;:5}}}">
                                    @foreach($rubro->ModeloEmpresa as $emp)

                                        <div>
                                            <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                                <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                                    <i class="fas fa-info-circle"></i>
                                                </button>
                                                <span class="card-img-top d-block overflow-hidden">
                                                    @if ($emp->icono)
                                                        <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                                    @else
                                                        <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                                    @endif
                                                </span>
                                                <div class="card-body py-2">
                                                    <center>
                                                        <div class="product-price">
                                                            @php
                                                                $total_star_perfil = 0;
                                                                $count_star_perfil = 0;
                                                                $promedio_star_perfil = 0;
                                                            @endphp

                                                            @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                                @php
                                                                    $total_star_perfil += $star_perfil->estrellas;
                                                                    $count_star_perfil ++;
                                                                    $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                                @endphp
                                                            @endforeach
                                                            <div class="star-rating mr-2 py-2">

                                                                @for ($e = 0; $e < 5; $e++)
                                                                    @if ($e < floor($promedio_star_perfil))
                                                                        <i class="sr-star fas fa-star active"></i>

                                                                    @elseif($e == floor($promedio_star_perfil))

                                                                        @if (is_float($promedio_star_perfil))
                                                                            @php
                                                                                $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                            @endphp

                                                                            @if (intval($star_array_perfil[1]) >= 50 )
                                                                                <i class="sr-star fas fa-star-half-alt active"></i>
                                                                            @else
                                                                                <i class="sr-star fas fa-star"></i>
                                                                            @endif
                                                                        @else
                                                                            <i class="sr-star fas fa-star"></i>
                                                                        @endif
                                                                    @else
                                                                        <i class="sr-star fas fa-star"></i>
                                                                    @endif
                                                                @endfor

                                                            </div>
                                                        </div>
                                                    </center>
                                                    <a class="text-center" href="/store/{{ $emp->slug }}">
                                                        @if ($emp->razon_social)
                                                            @if (strlen($emp->razon_social) > 35)
                                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                                    {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                                </h6>
                                                            @else
                                                                <h6>{{ $emp->razon_social }}</h6>
                                                            @endif
                                                        @else
                                                            @if (strlen($emp->nombre_comercial) > 35)
                                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                                    {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                                </h6>
                                                            @else
                                                                <h6>{{ $emp->nombre_comercial }}</h6>
                                                            @endif
                                                        @endif
                                                    </a>
                                                    <span class="product-meta d-block font-size-xs pb-1">
                                                        @if ($emp->descripcion)
                                                            <p class="text-center">{{ $emp->descripcion }}</p>
                                                        @else
                                                            <p class="text-center">Sin Descripción</p>
                                                        @endif
                                                    </span>
                                                    <br>
                                                    <div class="col-12 pt-3 border-top">
                                                        <center>
                                                            <b>{{ $rubro->nombre }}</b>
                                                        </center>
                                                    </div>
                                                </div>
                                                <div class="product-floating-btn">
                                                    <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                                        <i class="fas fa-store font-size-base ml-1"></i>
                                                        <i class=" font-size-base ml-1"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </section>

            @else

                <section class="container-fuid bg-light mt-4 mb-2 px-sm-2 px-md-4 px-lg-4" style="border-radius: 10px;">
                    <div class="pb-5 mt-1 mb-2 mb-md-4">
                        <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-2 mb-2">
                            <h2 class="h3 mb-0 pt-1 mr-3">TODAS LAS TIENDAS AFILIADAS</h2>
                        </div>
                        <div class="row">
                            {{  json_encode($grupo_rubro) }}
                        </div>
                        <div class="row">
                            @foreach ($rubros_empresas as $rubro)

                                @foreach($rubro->ModeloEmpresa as $emp)

                                    <div class="pt-1 col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2 mb-4">
                                        <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                                <i class="fas fa-info-circle"></i>
                                            </button>
                                            <span class="card-img-top d-block overflow-hidden">
                                                @if ($emp->icono)
                                                    <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                                @else
                                                    <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                                @endif
                                            </span>
                                            <div class="card-body py-2">
                                                <center>
                                                    <div class="product-price">
                                                        @php
                                                            $total_star_perfil = 0;
                                                            $count_star_perfil = 0;
                                                            $promedio_star_perfil = 0;
                                                        @endphp

                                                        @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                            @php
                                                                $total_star_perfil += $star_perfil->estrellas;
                                                                $count_star_perfil ++;
                                                                $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                            @endphp
                                                        @endforeach
                                                        <div class="star-rating mr-2 py-2">

                                                            @for ($e = 0; $e < 5; $e++)
                                                                @if ($e < floor($promedio_star_perfil))
                                                                    <i class="sr-star fas fa-star active"></i>

                                                                @elseif($e == floor($promedio_star_perfil))

                                                                    @if (is_float($promedio_star_perfil))
                                                                        @php
                                                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                        @endphp

                                                                        @if (intval($star_array_perfil[1]) >= 50 )
                                                                            <i class="sr-star fas fa-star-half-alt active"></i>
                                                                        @else
                                                                            <i class="sr-star fas fa-star"></i>
                                                                        @endif
                                                                    @else
                                                                        <i class="sr-star fas fa-star"></i>
                                                                    @endif
                                                                @else
                                                                    <i class="sr-star fas fa-star"></i>
                                                                @endif
                                                            @endfor

                                                        </div>
                                                    </div>
                                                </center>
                                                <a class="text-center" href="/store/{{ $emp->slug }}">
                                                    @if ($emp->razon_social)
                                                        @if (strlen($emp->razon_social) > 35)
                                                            <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                                {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                            </h6>
                                                        @else
                                                            <h6>{{ $emp->razon_social }}</h6>
                                                        @endif
                                                    @else
                                                        @if (strlen($emp->nombre_comercial) > 35)
                                                            <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                                {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                            </h6>
                                                        @else
                                                            <h6>{{ $emp->nombre_comercial }}</h6>
                                                        @endif
                                                    @endif
                                                </a>
                                                <span class="product-meta d-block font-size-xs pb-1">
                                                    @if ($emp->descripcion)
                                                        <p class="text-center">{{ $emp->descripcion }}</p>
                                                    @else
                                                        <p class="text-center">Sin Descripción</p>
                                                    @endif
                                                </span>
                                                <br>
                                                <div class="col-12 pt-3 border-top">
                                                    <center>
                                                        <b>{{ $rubro->nombre }}</b>
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="product-floating-btn">
                                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                                    <i class="fas fa-store font-size-base ml-1"></i>
                                                    <i class=" font-size-base ml-1"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            @endforeach
                        </div>
                    </div>
                </section>

            @endif

        @else

            <section class="container-fuid bg-light mt-4 p-4 mb-4 px-2" style="border-radius: 10px;">
                <div class="pb-5 mt-1 mb-2 mb-md-4">
                    <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-2 mb-2">
                        <h2 class="h3 mb-0 pt-1 mr-3">TODAS LAS TIENDAS AFILIADAS</h2>
                    </div>
                    <div class="row col-12">
                        <div class="col-12 col-sm-2 col-md-2 col-lg-3 col-xl-3"></div>
                        <div class="col-12 col-sm-8 col-md-8 col-lg-3 col-xl-6">
                            <h2 class="h4 mb-0 mr-3">No hay tiendas afiliadas en tu zona</h2>
                        </div>
                        <div class="col-12 col-sm-2 col-md-2 col-lg-3 col-xl-3"></div>
                    </div>
                </div>
            </section>

        @endif --}}

        <section class="container-fluid bg-light mt-4 mb-2 px-sm-2 px-md-4 px-lg-4" style="border-radius: 10px;">
            @foreach ($grupo_rubro as $grupo)
                <div class="mt-4" id="{{ $grupo->nombre }}">
                    <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                        <h2 class="h3 mb-0 pt-1 mr-3">{{ $grupo->nombre }}</h2>
                    </div>
                    <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2 pb-3">
                        <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:4}, &quot;1500&quot;:{&quot;items&quot;:5}}}">
                            @foreach ($grupo->ModeloRubros as $rubro)
                                @foreach($rubro->ModeloEmpresa as $emp)

                                    <div>
                                        <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                                <i class="fas fa-info-circle"></i>
                                            </button>
                                            <span class="card-img-top d-block overflow-hidden">
                                                @if ($emp->icono)
                                                    <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                                @else
                                                    <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                                @endif
                                            </span>
                                            <div class="card-body py-2">
                                                <center>
                                                    <div class="product-price">
                                                        @php
                                                            $total_star_perfil = 0;
                                                            $count_star_perfil = 0;
                                                            $promedio_star_perfil = 0;
                                                        @endphp

                                                        @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                            @php
                                                                $total_star_perfil += $star_perfil->estrellas;
                                                                $count_star_perfil ++;
                                                                $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                            @endphp
                                                        @endforeach
                                                        <div class="star-rating mr-2 py-2">

                                                            @for ($e = 0; $e < 5; $e++)
                                                                @if ($e < floor($promedio_star_perfil))
                                                                    <i class="sr-star fas fa-star active"></i>

                                                                @elseif($e == floor($promedio_star_perfil))

                                                                    @if (is_float($promedio_star_perfil))
                                                                        @php
                                                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                        @endphp

                                                                        @if (intval($star_array_perfil[1]) >= 50 )
                                                                            <i class="sr-star fas fa-star-half-alt active"></i>
                                                                        @else
                                                                            <i class="sr-star fas fa-star"></i>
                                                                        @endif
                                                                    @else
                                                                        <i class="sr-star fas fa-star"></i>
                                                                    @endif
                                                                @else
                                                                    <i class="sr-star fas fa-star"></i>
                                                                @endif
                                                            @endfor

                                                        </div>
                                                    </div>
                                                </center>
                                                <a class="text-center" href="/store/{{ $emp->slug }}">
                                                    @if ($emp->razon_social)
                                                        @if (strlen($emp->razon_social) > 35)
                                                            <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                                {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                            </h6>
                                                        @else
                                                            <h6>{{ $emp->razon_social }}</h6>
                                                        @endif
                                                    @else
                                                        @if (strlen($emp->nombre_comercial) > 35)
                                                            <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                                {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                            </h6>
                                                        @else
                                                            <h6>{{ $emp->nombre_comercial }}</h6>
                                                        @endif
                                                    @endif
                                                </a>
                                                <span class="product-meta d-block font-size-xs pb-1">
                                                    @if ($emp->descripcion)
                                                        <p class="text-center">{{ $emp->descripcion }}</p>
                                                    @else
                                                        <p class="text-center">Sin Descripción</p>
                                                    @endif
                                                </span>
                                                <br>
                                                <div class="col-12 pt-3 border-top">
                                                    <center>
                                                        <b>{{ $rubro->nombre }}</b>
                                                    </center>
                                                </div>
                                            </div>
                                            <div class="product-floating-btn">
                                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                                    <i class="fas fa-store font-size-base ml-1"></i>
                                                    <i class=" font-size-base ml-1"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </section>

    </div>

    @if ($empZona)

        @if (count($empZona) > 0)
            <section class="container-fluid mt-3 p-4">
                <div class="bg-accent mx-4 p-4" style="border-radius: 10px;">
                    <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                        <h2 class="h3 mb-0 pt-1 mr-3">Tiendas que entregan a tu ciudad</h2>
                    </div>
                    <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                        <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:4}, &quot;1140&quot;:{&quot;items&quot;:5}, &quot;1300&quot;:{&quot;items&quot;:6}, &quot;1500&quot;:{&quot;items&quot;:6}}}">
                            @foreach ($empZona as $emp)
                                <div class="">
                                    <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                        <span class="card-img-top d-block overflow-hidden">
                                            @if ($emp->icono)
                                                <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                            @else
                                                <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                            @endif
                                        </span>
                                        <div class="card-body py-2">
                                            <center>
                                                <div class="product-price">
                                                    @php
                                                        $total_star_perfil = 0;
                                                        $count_star_perfil = 0;
                                                        $promedio_star_perfil = 0;
                                                    @endphp

                                                    @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                        @php
                                                            $total_star_perfil += $star_perfil->estrellas;
                                                            $count_star_perfil ++;
                                                            $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                        @endphp
                                                    @endforeach
                                                    <div class="star-rating mr-2 py-2">

                                                        @for ($e = 0; $e < 5; $e++)
                                                            @if ($e < floor($promedio_star_perfil))
                                                                <i class="sr-star fas fa-star active"></i>

                                                            @elseif($e == floor($promedio_star_perfil))

                                                                @if (is_float($promedio_star_perfil))
                                                                    @php
                                                                        $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                    @endphp

                                                                    @if (intval($star_array_perfil[1]) >= 50 )
                                                                        <i class="sr-star fas fa-star-half-alt active"></i>
                                                                    @else
                                                                        <i class="sr-star fas fa-star"></i>
                                                                    @endif
                                                                @else
                                                                    <i class="sr-star fas fa-star"></i>
                                                                @endif
                                                            @else
                                                                <i class="sr-star fas fa-star"></i>
                                                            @endif
                                                        @endfor

                                                    </div>
                                                </div>
                                            </center>
                                            <a class="text-center" href="/store/{{ $emp->slug }}">
                                                @if ($emp->razon_social)
                                                    @if (strlen($emp->razon_social) > 35)
                                                        <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                            {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                        </h6>
                                                    @else
                                                        <h6>{{ $emp->razon_social }}</h6>
                                                    @endif
                                                @else
                                                    @if (strlen($emp->nombre_comercial) > 35)
                                                        <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                            {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                        </h6>
                                                    @else
                                                        <h6>{{ $emp->nombre_comercial }}</h6>
                                                    @endif
                                                @endif
                                            </a>
                                            <span class="product-meta d-block font-size-xs pb-1">
                                                @if ($emp->descripcion)
                                                    <p class="text-center">{{ $emp->descripcion }}</p>
                                                @else
                                                    <p class="text-center">Sin Descripción</p>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="product-floating-btn">
                                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                                <i class="fas fa-store font-size-base ml-1"></i>
                                                <i class=" font-size-base ml-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        @endif

    @endif


@endsection

@section('pagos')

    @if (count($pagos) > 0)

        <section class="container-fluid my-3">
            <div class="cea-carousel border-right">
                <div class="cea-carousel-inner" data-carousel-options="{ &quot;nav&quot;: false, &quot;controls&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2},&quot;360&quot;:{&quot;items&quot;:2},&quot;600&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:5}} }">
                    @foreach ($pagos as $pago)
                        <div>
                            <div class="d-block bg-white border py-2 px-2 tamaño_pago" style="margin-right: -.0625rem;">
                                {{-- <img class="d-block mx-auto" src="https://tiendas.ceamarket.com/img/{{ $pago->icono }}" onerror="this.src='/img/cards.png'" style="width: 150px; height: 80px;" alt="{{ $pago->nombre }}"> --}}
                                <div class="justify-content-center text-center pt-2">
                                    <i class="{{ $pago->icono }} fa-2x"></i>
                                </div>
                                {{-- 253*131 --}}
                                <div class="card-body text-center">
                                    <p class="mb-1">{{ $pago->nombre }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

    @endif

@endsection

@section('modals')

    {{-- Información de la Tienda --}}
    <div id="InformacionTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content" id="detailsTienda">

            </div>
        </div>
    </div>

    @hasrole('cliente')

        <a href="javascript:mostrarCiudades()">
            <div class="support_ubicacion" data-toggle="tooltip" data-placement="left" title="Cambiar ciudad en la cual buscar" style="padding-top: .9rem !important; padding-left: 1.2rem !important;">
                <span class="full_help">
                    <i class="fas fa-map-marked-alt"></i>
                </span>
            </div>
        </a>

    @endhasrole

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_web/ajaxIndex.js') }}"></script>

    @hasrole('cliente')

        @if (!Auth::user()->ModeloPersona->ubigeo_id)
            <script>

                $(document).ready(function() {

                    mostrarCiudades();

                })

            </script>
        @endif

    @endhasrole

    <script>

        @if(session('success'))
            sw_success("{{ session('success') }}", 4000);
        @endif

        @if(session('error'))
            sw_error("{{ session('error') }}", 4000);
        @endif
    </script>

@endsection
