@extends('principal')

@section('title_header')
 Inicio
@endsection

@section('carroucel')

    <!-- Rubros -->
    <section class="container mt-2 pt-5 pb-2 mt-5">
        <h2 class="text-center py-2">¿Que estas Buscando?</h2>
        <div class="row justify-content-center">
            @foreach ($rubros as $rub)
                <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2 mb-grid-gutter">
                    <a class="card border-0 box-shadow" href="/rubro/{{ $rub->slug }}">
                        @if ($rub->img_rubro)
                            <img class="card-img-top" src="https://admin.ceamarket.com/img/food-delivery/category/01.jpg" alt="rubro"/>
                        @else
                            <img class="card-img-top" src="/img/food-delivery/category/01.jpg" alt="rubro"/>
                        @endif
                        <div class="card-body py-2 text-center">
                            @if (strlen($rub->nombre) > 15)
                                <h6 class="mt-1" style="font-size: 15px" data-toggle="tooltip" data-placement="bottom" title="{{ $rub->nombre }}">{{ substr($rub->nombre, 0, 15) }}...</h6>
                            @else
                                <h6 class="mt-1" style="font-size: 15px">{{ $rub->nombre }}</h6>
                            @endif
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </section>

@endsection

@section('content')

    {{-- Tiendas Recomendadas --}}
    <section class="container mt-0 p-4" style="border-radius: 10px;">
        <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
            <h2 class="h3 mb-0 pt-1 mr-3">Tiendas Recomendadas</h2>
        </div>
        <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
            <div class="cea-carousel-inner"
                data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:4}, &quot;1500&quot;:{&quot;items&quot;:5}}}">
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/01.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1" href="#">Fruits and Vegetables</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Coconut, Indonesia (piece)</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $1.<small>99</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $2.<small>99</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/02.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">Dairy and Eggs</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Soft Creme Cheese (200g)</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $2.<small>99</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $3.<small>99</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/03.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">Soft Drinks and Juice</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Pepsi Soda Can (.33ml)</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $1.<small>00</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $1.<small>25</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/04.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">Fruits and Vegetables</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Fresh Orange, Spain (1kg)</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $1.<small>15</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $1.<small>75</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/05.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">Personal hygiene</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Moisture Body Lotion (250ml)</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $4.<small>20</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $5.<small>99</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/06.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">Snacks, Sweets and Chips</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Nut Chocolate Paste (750g)</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $6.<small>50</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $7.<small>99</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="card product-card card-static pb-3">
                        <span class="badge badge-danger badge-shadow">50%</span>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                            <i class="fas fa-heart"></i>
                        </button>
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="img/grocery/catalog/07.jpg" alt="Product">
                        </span>
                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">Dairy and Eggs</span>
                            <h3 class="product-title font-size-sm">
                                <a data-toggle="modal" href="#DetalleProducto">Mozzarella Mini Cheese</a>
                            </h3>
                            <div class="product-price">
                                <span class="text-accent">
                                    $3.<small>50</small>
                                </span>
                                <del class="font-size-sm text-muted">
                                    $4.<small>99</small>
                                </del>
                            </div>
                        </div>
                        <div class="product-floating-btn">
                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                <i class="fas fa-store font-size-base ml-1"></i>
                                <i class=" font-size-base ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="container-fluid mt-3 p-4">
        <div class="bg-accent mx-4 p-4" style="border-radius: 10px;">
            <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                <h2 class="h3 mb-0 pt-1 mr-3">Tiendas que entregan a tu ciudad</h2>
            </div>
            <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:4}, &quot;1140&quot;:{&quot;items&quot;:5}, &quot;1300&quot;:{&quot;items&quot;:7}, &quot;1500&quot;:{&quot;items&quot;:8}}}">
                    {{-- <div class="">
                        <div class="card product-card card-static pb-3 m-1" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                <i class="fas fa-info-circle"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                @if ($emp->icono)
                                    <img class="d-block rounded-lg mx-auto mt-3" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}"/>
                                @else
                                    <img class="d-block rounded-lg mx-auto mt-3" width="150" src="/img/default-store.png" alt="Sin Imagen"/>
                                @endif
                            </span>
                            <div class="card-body py-2">
                                <h3 class="product-title font-size-sm">
                                    <a class="text-center" href="/store/{{ $emp->slug }}">
                                        @if ($emp->razon_social)
                                            @if (strlen($emp->razon_social) > 35)
                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                    {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                </h6>
                                            @else
                                                <h6>{{ $emp->razon_social }}</h6>
                                            @endif
                                        @else
                                            @if (strlen($emp->nombre_comercial) > 35)
                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                    {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                </h6>
                                            @else
                                                <h6>{{ $emp->nombre_comercial }}</h6>
                                            @endif
                                        @endif
                                    </a>
                                </h3>
                                <span class="product-meta d-block font-size-xs pb-1">
                                    @if ($emp->descripcion)
                                        {{ $emp->descripcion }}
                                    @else
                                        <p class="text-center">Sin Descripción</p>
                                    @endif
                                </span>
                                <div class="product-price">
                                    @php
                                        $total_star_perfil = 0;
                                        $count_star_perfil = 0;
                                        $promedio_star_perfil = 0;
                                    @endphp

                                    @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                        @php
                                            $total_star_perfil += $star_perfil->estrellas;
                                            $count_star_perfil ++;
                                            $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                        @endphp
                                    @endforeach
                                    <div class="star-rating mr-2 py-2">

                                        @for ($e = 0; $e < 5; $e++)
                                            @if ($e < floor($promedio_star_perfil))
                                                <i class="sr-star fas fa-star active"></i>

                                            @elseif($e == floor($promedio_star_perfil))

                                                @if (is_float($promedio_star_perfil))
                                                    @php
                                                        $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                    @endphp

                                                    @if (intval($star_array_perfil[1]) >= 50 )
                                                        <i class="sr-star fas fa-star-half-alt active"></i>
                                                    @else
                                                        <i class="sr-star fas fa-star"></i>
                                                    @endif
                                                @else
                                                    <i class="sr-star fas fa-star"></i>
                                                @endif
                                            @else
                                                <i class="sr-star fas fa-star"></i>
                                            @endif
                                        @endfor

                                    </div>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div> --}}
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/01.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1" href="#">Fruits and Vegetables</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Coconut, Indonesia (piece)</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $1.<small>99</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $2.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/02.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Dairy and Eggs</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Soft Creme Cheese (200g)</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $2.<small>99</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $3.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/03.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Soft Drinks and Juice</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Pepsi Soda Can (.33ml)</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $1.<small>00</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $1.<small>25</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/04.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Fruits and Vegetables</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Fresh Orange, Spain (1kg)</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $1.<small>15</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $1.<small>75</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/05.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Personal hygiene</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Moisture Body Lotion (250ml)</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $4.<small>20</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $5.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/06.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Snacks, Sweets and Chips</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Nut Chocolate Paste (750g)</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $6.<small>50</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $7.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/07.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Dairy and Eggs</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Mozzarella Mini Cheese</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $3.<small>50</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $4.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/07.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Dairy and Eggs</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Mozzarella Mini Cheese</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $3.<small>50</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $4.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card product-card card-static pb-3">
                            <span class="badge badge-danger badge-shadow">50%</span>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Deseos">
                                <i class="fas fa-heart"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="img/grocery/catalog/07.jpg" alt="Product">
                            </span>
                            <div class="card-body py-2">
                                <span class="product-meta d-block font-size-xs pb-1">Dairy and Eggs</span>
                                <h3 class="product-title font-size-sm">
                                    <a data-toggle="modal" href="#DetalleProducto">Mozzarella Mini Cheese</a>
                                </h3>
                                <div class="product-price">
                                    <span class="text-accent">
                                        $3.<small>50</small>
                                    </span>
                                    <del class="font-size-sm text-muted">
                                        $4.<small>99</small>
                                    </del>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="#" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Listado de las Tiendas -->
    {{-- <section class="container bg-light mt-4 p-4 mb-4 px-5" style="border-radius: 10px;">
        @foreach ($rubros_empresas as $rubro)
            <div class="mt-4" id="{{ $rubro->nombre }}"">
                <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                    <h2 class="h3 mb-0 pt-1 mr-3">{{ $rubro->nombre }}</h2>
                </div>
                <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                    <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:4}, &quot;1500&quot;:{&quot;items&quot;:5}}}">
                        @foreach($rubro->ModeloEmpresa as $emp)
                            <div class="">
                                <div class="card product-card card-static pb-3 m-1" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                        <i class="fas fa-info-circle"></i>
                                    </button>
                                    <span class="card-img-top d-block overflow-hidden">
                                        @if ($emp->icono)
                                            <img class="d-block rounded-lg mx-auto mt-3" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}"/>
                                        @else
                                            <img class="d-block rounded-lg mx-auto mt-3" width="150" src="/img/default-store.png" alt="Sin Imagen"/>
                                        @endif
                                    </span>
                                    <div class="card-body py-2">
                                        <h3 class="product-title font-size-sm">
                                            <a class="text-center" href="/store/{{ $emp->slug }}">
                                                @if ($emp->razon_social)
                                                    @if (strlen($emp->razon_social) > 35)
                                                        <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                            {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                        </h6>
                                                    @else
                                                        <h6>{{ $emp->razon_social }}</h6>
                                                    @endif
                                                @else
                                                    @if (strlen($emp->nombre_comercial) > 35)
                                                        <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                            {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                        </h6>
                                                    @else
                                                        <h6>{{ $emp->nombre_comercial }}</h6>
                                                    @endif
                                                @endif
                                            </a>
                                        </h3>
                                        <span class="product-meta d-block font-size-xs pb-1">
                                            @if ($emp->descripcion)
                                                {{ $emp->descripcion }}
                                            @else
                                                <p class="text-center">Sin Descripción</p>
                                            @endif
                                        </span>
                                        <div class="product-price">
                                            @php
                                                $total_star_perfil = 0;
                                                $count_star_perfil = 0;
                                                $promedio_star_perfil = 0;
                                            @endphp

                                            @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                @php
                                                    $total_star_perfil += $star_perfil->estrellas;
                                                    $count_star_perfil ++;
                                                    $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                @endphp
                                            @endforeach
                                            <div class="star-rating mr-2 py-2">

                                                @for ($e = 0; $e < 5; $e++)
                                                    @if ($e < floor($promedio_star_perfil))
                                                        <i class="sr-star fas fa-star active"></i>

                                                    @elseif($e == floor($promedio_star_perfil))

                                                        @if (is_float($promedio_star_perfil))
                                                            @php
                                                                $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                            @endphp

                                                            @if (intval($star_array_perfil[1]) >= 50 )
                                                                <i class="sr-star fas fa-star-half-alt active"></i>
                                                            @else
                                                                <i class="sr-star fas fa-star"></i>
                                                            @endif
                                                        @else
                                                            <i class="sr-star fas fa-star"></i>
                                                        @endif
                                                    @else
                                                        <i class="sr-star fas fa-star"></i>
                                                    @endif
                                                @endfor

                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-floating-btn">
                                        <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                            <i class="fas fa-store font-size-base ml-1"></i>
                                            <i class=" font-size-base ml-1"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        @endforeach
    </section> --}}

@endsection

@section('modals')

    {{-- Información de la Tienda --}}
    <div id="InformacionTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content" id="detailsTienda">

            </div>
        </div>
    </div>

    @hasrole('cliente')

        <a href="javascript:mostrarCiudades()">
            <div class="support_ubicacion" data-toggle="tooltip" data-placement="left" title="Cambiar ciudad en la cual buscar" style="padding-top: .9rem !important; padding-left: 1.2rem !important;">
                <span class="full_help">
                    <i class="fas fa-map-marked-alt"></i>
                </span>
            </div>
        </a>

    @endhasrole

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxIndex.js') }}"></script>

    @if (!Auth::user()->ModeloPersona->ubigeo_id)
    <script>

        $(document).ready(function() {

            mostrarCiudades();

        })

    </script>
    @endif

@endsection
