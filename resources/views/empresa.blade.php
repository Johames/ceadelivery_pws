@extends('layouts.web')

@section('tipo_header')
    @include('layouts.sections_web.headers.header_secundario')
@endsection

@section('content_principal')
   <!-- Promociones y Banners de las Tiendas -->
   <section class="cea-carousel">
        <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 1, &quot;mode&quot;: &quot;gallery&quot;, &quot;nav&quot;: false, &quot;responsive&quot;: {&quot;0&quot;: {&quot;nav&quot;: true, &quot;controls&quot;: false}, &quot;576&quot;: {&quot;nav&quot;: false, &quot;controls&quot;: true}}}">
            <div>
                <div class="px-md-5 text-center text-xl-left" style="background-color: #1a6fb0;">
                    <div class="d-xl-flex justify-content-between align-items-center px-4 px-sm-5 mx-auto" style="max-width: 1226px;">
                            <div class="py-5 mr-xl-4 mx-auto mx-xl-0" style="max-width: 490px;">
                                <h2 class="h1 text-light">24/7 Servicio de Delivery</h2>
                                <p class="text-light pb-4">
                                    Ordene cualquier producto de nuestra tienda en línea y se
                                    lo entregaremos en su puerta en un momento conveniente para usted.
                                </p>
                                <h5 class="text-light pb-3">Prueba nuestra aplicación móvil</h5>
                                <div class="d-flex flex-wrap justify-content-center justify-content-xl-start">
                                    <!-- <a class="btn-market btn-apple mr-2 mb-2" href="#" role="button">
                                                <span class="btn-market-subtitle">Download on the</span>
                                                <span class="btn-market-title">App Store</span>
                                        </a> -->
                                    <a class="btn-market btn-google mb-2" href="#" role="button">
                                        <span class="btn-market-subtitle">Descargar en</span>
                                        <span class="btn-market-title">Google Play</span>
                                    </a>
                                </div>
                            </div>
                            <div>
                                <img src="img/grocery/slider/slide02.jpg" alt="Image">
                            </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="px-md-5 text-center text-xl-left" style="background-color: #59c879;">
                    <div class="d-xl-flex justify-content-between align-items-center px-4 px-sm-5 mx-auto" style="max-width: 1226px;">
                            <div class="py-5 mr-xl-4 mx-auto mx-xl-0" style="max-width: 490px;">
                                <h2 class="h1 text-light">Fresh Foods a Click Away</h2>
                                <p class="text-light pb-4">Order any goods from our store online and we deliver them to your door at a time convenient for you.</p>
                                <h5 class="text-light pb-3">On the go? Try our mobile app</h5>
                                <div class="d-flex flex-wrap justify-content-center justify-content-xl-start">
                                    <!-- <a class="btn-market btn-apple mr-2 mb-2" href="#" role="button">
                                                <span class="btn-market-subtitle">Download on the</span>
                                                <span class="btn-market-title">App Store</span>
                                        </a> -->
                                    <a class="btn-market btn-google mb-2" href="#" role="button">
                                        <span class="btn-market-subtitle">Descargar en</span>
                                        <span class="btn-market-title">Google Play</span>
                                    </a>
                                </div>
                            </div>
                            <div>
                                <img src="img/grocery/slider/slide01.jpg" alt="Image">
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


     <!-- Mas Vendidos por Tiendas -->
     <!-- pb-4 pb-md-5 -->
     <section class="container bg-faded-success">
        <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
             <h2 class="h3 mb-0 pt-3 mr-2">Productos Mas Vendidos por Tiendas</h2>
        </div>
        <div class="row pt-2 mx-n2">
             <div class="col-lg-4 col-md-6 mb-2 py-3">
                  <div class="widget">
                       <h3 class="widget-title">Nakamura SAC</h3>
                       <div class="media align-items-center pb-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/cart/widget/05.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Wireless Bluetooth Headphones</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$259.<small>00</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/cart/widget/06.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Cloud Security Camera</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$122.<small>00</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/cart/widget/07.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Android Smartphone S10</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$799.<small>00</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/cart/widget/08.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Android Smart TV Box</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$67.<small>00</small></span>
                                      <del class="text-muted font-size-xs">$90.<small>43</small></del>
                                 </div>
                            </div>
                       </div>
                       <hr><br>
                       <a class="font-size-sm" href="shop-grid-ls.html">
                            Visitar Tienda
                            <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                       </a>
                  </div>
             </div>
             <div class="col-lg-4 col-md-6 mb-2 py-3">
                  <div class="widget">
                       <h3 class="widget-title">Nakamura SAC</h3>
                       <div class="media align-items-center pb-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/06.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Monoblock Desktop PC</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$1,949.<small>00</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/07.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Laserjet Printer All-in-One</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$428.<small>60</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/08.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Console Controller Charger</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$14.<small>97</small></span>
                                      <del class="text-muted font-size-xs">$16.<small>47</small></del>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/09.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Smart Watch Series 5, Aluminium</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$349.<small>99</small></span>
                                 </div>
                            </div>
                       </div>
                       <hr><br>
                       <a class="font-size-sm" href="shop-grid-ls.html">
                            Visitar Tienda
                            <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                       </a>
                  </div>
             </div>
             <div class="col-lg-4 col-md-6 mb-2 py-3">
                  <div class="widget">
                       <h3 class="widget-title">Nakamura SAC</h3>
                       <div class="media align-items-center pb-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/10.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Android Smartphone S9</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$749.<small>99</small></span>
                                      <del class="text-muted font-size-xs">$859.<small>99</small></del>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/11.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Wireless Bluetooth Headphones</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$428.<small>60</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2 border-bottom">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/12.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">360 Degrees Camera</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$98.<small>75</small></span>
                                 </div>
                            </div>
                       </div>
                       <div class="media align-items-center py-2">
                            <a class="d-block mr-2" href="shop-single-v2.html">
                                 <img width="64" src="img/shop/widget/13.jpg" alt="Product" />
                            </a>
                            <div class="media-body">
                                 <h6 class="widget-product-title">
                                      <a href="shop-single-v2.html">Digital Camera 40MP</a>
                                 </h6>
                                 <div class="widget-product-meta">
                                      <span class="text-accent">$210.<small>00</small></span>
                                      <del class="text-muted font-size-xs">$249.<small>00</small></del>
                                 </div>
                            </div>
                       </div>
                       <hr><br>
                       <a class="font-size-sm" href="shop-grid-ls.html">
                            Visitar Tienda
                            <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                       </a>
                  </div>
             </div>
        </div>
   </section>

   <!-- Primer Modelo de Tiendas si Hay Menos de 6 -->
   <section class="container pt-2">
        <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
             <h2 class="h3 mb-0 pt-3 mr-2">Bodegas</h2>
        </div>
        <div class="row pt-2 mx-n2">
             <div class="col-lg-4 col-md-4 col-sm-6 px-2 mb-4">
                  <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                       <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                            <a class="d-inline-block mx-auto mr-sm-2" href="#">
                                 <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                            </a>
                            <div class="media-body pt-2">
                                 <h4 class="product-title font-size-base mb-2">
                                      <a href="#">Women Colorblock Sneakers</a>
                                 </h4>
                                 <div class="font-size-sm">
                                      Rubro de la Tienda
                                 </div>
                                 <hr>
                                 <div class="tab-pane fade active show mt-2" id="result3" role="tabpanel">
                                      <a class="social-btn sb-round sb-outline sb-facebook mb-2" href="#">
                                           <i class="cea-icon-facebook"></i>
                                      </a>
                                      <a class="social-btn sb-round sb-outline sb-twitter mb-2" href="#">
                                           <i class="cea-icon-twitter"></i>
                                      </a>
                                      <a class="social-btn sb-round sb-outline sb-instagram mb-2" href="#">
                                           <i class="cea-icon-instagram"></i>
                                      </a>
                                      <a class="social-btn sb-round sb-outline sb-google mb-2" href="#">
                                           <i class="cea-icon-google"></i>
                                      </a>
                                      <a class="social-btn sb-round sb-outline sb-linkedin mb-2" href="#">
                                           <i class="cea-icon-linkedin"></i>
                                      </a>
                                      <a class="social-btn sb-round sb-outline sb-pinterest mb-2" href="#">
                                           <i class="cea-icon-pinterest"></i>
                                      </a>
                                 </div>
                                 <div class="text-success font-size-sm">
                                      <a href="#">
                                           Visitar Tienda
                                           <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                      </a>
                                 </div>
                            </div>
                       </div>
                  </div>
                  <hr class="d-sm-none">
             </div>
             <div class="col-lg-4 col-md-4 col-sm-6 px-2 mb-4">
                  <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                       <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                            <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                 <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                            </a>
                            <div class="media-body pt-2">
                                 <h4 class="product-title font-size-base mb-2">
                                      <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                 </h4>
                                 <div class="font-size-sm">
                                      Rubro de la Tienda
                                 </div>
                                 <hr>
                                 <div class="row">
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Facebook</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Instagram</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Twitter</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Messenger</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Whatsap</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-vk"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">YouTube</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                </div>
                                           </a>
                                      </div>
                                 </div>
                                 <div class="text-success font-size-sm">
                                      <a href="modelo_productos_empresa.html">
                                           Visitar Tienda
                                           <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                      </a>
                                 </div>
                            </div>
                       </div>
                  </div>
                  <hr class="d-sm-none">
             </div>
             <div class="col-lg-4 col-md-4 col-sm-6 px-2 mb-4">
                  <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                       <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                            <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                 <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                            </a>
                            <div class="media-body pt-2">
                                 <h4 class="product-title font-size-base mb-2">
                                      <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                 </h4>
                                 <div class="font-size-sm">
                                      Rubro de la Tienda
                                 </div>
                                 <hr>
                                 <div class="row">
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Facebook</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Instagram</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Twitter</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Messenger</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">Whatsap</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-vk"></i>
                                                </div>
                                           </a>
                                      </div>
                                      <div class="col-1">
                                           <a class="navbar-tool d-lg-flex" href="#">
                                                <span class="navbar-tool-tooltip">YouTube</span>
                                                <div class="navbar-tool-icon-box">
                                                     <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                </div>
                                           </a>
                                      </div>
                                 </div>
                                 <div class="text-success font-size-sm">
                                      <a href="modelo_productos_empresa.html">
                                           Visitar Tienda
                                           <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                      </a>
                                 </div>
                            </div>
                       </div>
                  </div>
                  <hr class="d-sm-none">
             </div>
        </div>
   </section>

   <!-- Segundo Modelo de Tiendas si Hay Menos de 6 -->
   <!-- <section class="container pt-2">
             <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                  <h2 class="h3 mb-0 pt-3 mr-2">Restaurantes</h2>
             </div>
             <div class="row pt-2 mx-n2">
                  <div class="col-lg-4 col-md-4 col-sm-6 px-2 mb-4">
                       <a class="media w-100 align-items-center bg-faded-warning rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                            <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                            <div class="media-body py-4 px-2">
                                 <h5 class="mb-2">Nakamura SAC</h5>
                                 <div class="text-warning font-size-sm">
                                      <a href="modelo_productos_empresa.html">
                                           Comprar aquí
                                           <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                      </a>
                                 </div>
                            </div>
                       </a>
                       <hr class="d-sm-none">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 px-2 mb-4">
                       <a class="media w-100 align-items-center bg-faded-dark rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                            <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                            <div class="media-body py-4 px-2">
                                 <h5 class="mb-2">Nakamura SAC</h5>
                                 <div class="text-dark font-size-sm">
                                      <a href="modelo_productos_empresa.html">
                                           Comprar aquí
                                           <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                      </a>
                                 </div>
                            </div>
                       </a>
                       <hr class="d-sm-none">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 px-2 mb-4">
                       <a class="media w-100 align-items-center bg-faded-accent rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                            <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                            <div class="media-body py-4 px-2">
                                 <h5 class="mb-2">Nakamura SAC</h5>
                                 <div class="text-accent font-size-sm">
                                      <a href="modelo_productos_empresa.html">
                                           Comprar aquí
                                           <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                      </a>
                                 </div>
                            </div>
                       </a>
                       <hr class="d-sm-none">
                  </div>
             </div>
        </section> -->

   <!-- Primer Modelo de Tiendas si Hay mas de 6-->
   <!-- <section class="container pt-lg-3 mb-4 mb-sm-5">
             <div class="row">
                  <div class="col-md-5">
                       <div class="d-flex flex-column h-100 overflow-hidden rounded-lg" style="background-color: #f6f8fb;">
                            <div class="d-flex justify-content-between px-grid-gutter py-grid-gutter">
                                 <div>
                                      <h3 class="mb-1">Restaurantes</h3>
                                 </div>
                                 <div class="cea-custom-controls" id="for-women">
                                      <button type="button">
                                           <i class="cea-icon-arrow-left"></i>
                                      </button>
                                      <button type="button">
                                           <i class="cea-icon-arrow-right"></i>
                                      </button>
                                 </div>
                            </div>
                            <a class="d-none d-md-block mt-auto" href="shop-grid-ls.html">
                                 <img class="d-block w-100" src="img/restaurante.jpg" alt="For Women">
                            </a>
                       </div>
                  </div>
                  <div class="col-md-7 pt-4 pt-md-0">
                       <div class="cea-carousel">
                            <div class="cea-carousel-inner" data-carousel-options="{&quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#for-women&quot;}">
                                 <div>
                                      <div class="row mx-n2">
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-info rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-info font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-primary rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-primary font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-accent rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-accent font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-warning rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-warning font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-dark rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-dark font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-danger rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-danger font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                      </div>
                                 </div>
                                 <div>
                                      <div class="row mx-n2">
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-info rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-info font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-primary rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-primary font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-accent rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-accent font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-warning rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-warning font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-dark rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-dark font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                           <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                                <a class="media w-100 align-items-center bg-faded-danger rounded-lg pt-2 pl-2 mb-4 mr-4 mr-xl-0" href="#" style="min-width: 16rem;">
                                                     <img src="img/home/banners/banner-sm01.png" width="125" alt="Banner">
                                                     <div class="media-body py-4 px-2">
                                                          <h5 class="mb-2">Nakamura SAC</h5>
                                                          <div class="text-danger font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Comprar aquí
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </a>
                                           </div>
                                      </div>
                                 </div>
                            </div>
                       </div>
                  </div>
             </div>
        </section> -->

   <!-- Segundo Modelo de Tiendas si Hay mas de 6  -->
   <section class="container pt-lg-3 mb-4 mb-sm-5">
        <div class="row">
             <div class="col-md-5">
                  <div class="d-flex flex-column h-100 overflow-hidden rounded-lg" style="background-color: #f6f8fb;">
                       <div class="d-flex justify-content-between px-grid-gutter py-grid-gutter">
                            <div>
                                 <h3 class="mb-1">Restaurantes</h3>
                            </div>
                            <div class="cea-custom-controls" id="for-womens">
                                 <button type="button">
                                      <i class="cea-icon-arrow-left"></i>
                                 </button>
                                 <button type="button">
                                      <i class="cea-icon-arrow-right"></i>
                                 </button>
                            </div>
                       </div>
                       <a class="d-none d-md-block mt-auto" href="shop-grid-ls.html">
                            <img class="d-block w-100" src="img/restaurante.jpg" alt="For Women">
                       </a>
                  </div>
             </div>
             <div class="col-md-7 pt-4 pt-md-0">
                  <div class="cea-carousel">
                       <div class="cea-carousel-inner" data-carousel-options="{&quot;nav&quot;: false, &quot;controlsContainer&quot;: &quot;#for-womens&quot;}">
                            <div>
                                 <div class="row mx-n2">
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                 </div>
                            </div>
                            <div>
                                 <div class="row mx-n2">
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                      <div class="col-lg-6 col-12 px-0 px-sm-2 mb-sm-4">
                                           <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-4 mr-xl-0 pb-3 border-bottom">
                                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                                     <a class="d-inline-block mx-auto mr-sm-4" href="shop-single-v1.html">
                                                          <img src="img/shop/cart/01.jpg" width="130" alt="Product">
                                                     </a>
                                                     <div class="media-body pt-2">
                                                          <h4 class="product-title font-size-base mb-2">
                                                               <a href="shop-single-v1.html">Women Colorblock Sneakers</a>
                                                          </h4>
                                                          <div class="font-size-sm">
                                                               Rubro de la Tienda
                                                          </div>
                                                          <hr>
                                                          <div class="row">
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Facebook</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-facebook"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Instagram</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-instagram"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Twitter</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-twitter"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Messenger</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-messenger"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">Whatsap</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-vk"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                               <div class="col-1">
                                                                    <a class="navbar-tool d-lg-flex" href="#">
                                                                         <span class="navbar-tool-tooltip">YouTube</span>
                                                                         <div class="navbar-tool-icon-box">
                                                                              <i class="navbar-tool-icon cea-icon-youtube"></i>
                                                                         </div>
                                                                    </a>
                                                               </div>
                                                          </div>
                                                          <div class="text-success font-size-sm">
                                                               <a href="modelo_productos_empresa.html">
                                                                    Visitar Tienda
                                                                    <i class="cea-icon-arrow-right font-size-xs ml-1"></i>
                                                               </a>
                                                          </div>
                                                     </div>
                                                </div>
                                           </div>
                                           <hr class="d-sm-none">
                                      </div>
                                 </div>
                            </div>
                       </div>
                  </div>
             </div>
        </div>
   </section>

@endsection


