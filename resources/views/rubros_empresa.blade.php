@extends('principal')

@section('title_header')
    Inicio
@endsection

@section('carroucel')

    @if (count($empDest) > 0)
        <section class="bg-accent container-fluid mt-2 pt-5 mb-3 pb-2 mt-5" id="cuisine">
            <h2 class="text-center py-2">Tiendas Destacadas</h2>
            <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:3}, &quot;1300&quot;:{&quot;items&quot;:4}, &quot;1500&quot;:{&quot;items&quot;:5}}}">
                    @foreach($empDest as $emp)
                        <div class="">
                            <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                    <i class="fas fa-info-circle"></i>
                                </button>
                                <span class="card-img-top d-block overflow-hidden">
                                    @if ($emp->icono)
                                        <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                    @else
                                        <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                    @endif
                                </span>
                                <div class="card-body py-2">
                                    <center>
                                        <div class="product-price">
                                            @php
                                                $total_star_perfil = 0;
                                                $count_star_perfil = 0;
                                                $promedio_star_perfil = 0;
                                            @endphp

                                            @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                @php
                                                    $total_star_perfil += $star_perfil->estrellas;
                                                    $count_star_perfil ++;
                                                    $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                @endphp
                                            @endforeach
                                            <div class="star-rating mr-2 py-2">

                                                @for ($e = 0; $e < 5; $e++)
                                                    @if ($e < floor($promedio_star_perfil))
                                                        <i class="sr-star fas fa-star active"></i>

                                                    @elseif($e == floor($promedio_star_perfil))

                                                        @if (is_float($promedio_star_perfil))
                                                            @php
                                                                $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                            @endphp

                                                            @if (intval($star_array_perfil[1]) >= 50 )
                                                                <i class="sr-star fas fa-star-half-alt active"></i>
                                                            @else
                                                                <i class="sr-star fas fa-star"></i>
                                                            @endif
                                                        @else
                                                            <i class="sr-star fas fa-star"></i>
                                                        @endif
                                                    @else
                                                        <i class="sr-star fas fa-star"></i>
                                                    @endif
                                                @endfor

                                            </div>
                                        </div>
                                    </center>
                                    <a class="text-center" href="/store/{{ $emp->slug }}">
                                        @if ($emp->razon_social)
                                            @if (strlen($emp->razon_social) > 35)
                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                    {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                </h6>
                                            @else
                                                <h6>{{ $emp->razon_social }}</h6>
                                            @endif
                                        @else
                                            @if (strlen($emp->nombre_comercial) > 35)
                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                    {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                </h6>
                                            @else
                                                <h6>{{ $emp->nombre_comercial }}</h6>
                                            @endif
                                        @endif
                                    </a>
                                    <span class="product-meta d-block font-size-xs pb-1">
                                        @if ($emp->descripcion)
                                            <p class="text-center">{{ $emp->descripcion }}</p>
                                        @else
                                            <p class="text-center">Sin Descripción</p>
                                        @endif
                                    </span>
                                </div>
                                <div class="product-floating-btn">
                                    <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                        <i class="fas fa-store font-size-base ml-1"></i>
                                        <i class=" font-size-base ml-1"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

@endsection

@section('content')

    <div class="container pb-5 mt-1 mb-2 mb-md-4">
        @if (count($empDest) > 0)
            <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-2 mb-2">
                <h2 class="h3 mb-0 pt-1 mr-3">TODAS LAS TIENDAS</h2>
            </div>
        @else
            <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-2 mb-2 mt-5 pt-5">
                <h2 class="h3 mb-0 pt-3 mr-3">TODAS LAS TIENDAS</h2>
            </div>
        @endif

        <!-- Products grid-->
        <div class="row pt-3 mx-n2">

            @if (count($rubros_empresas) > 0)
                @foreach ($rubros_empresas as $emp)
                    <div class="col-6 col-sm-4 col-md-3 col-lg-3 col-xl-3">
                        <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                <i class="fas fa-info-circle"></i>
                            </button>
                            <span class="card-img-top d-block overflow-hidden">
                                @if ($emp->icono)
                                    <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                @else
                                    <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                @endif
                            </span>
                            <div class="card-body py-2">
                                <center>
                                    <div class="product-price">
                                        @php
                                            $total_star_perfil = 0;
                                            $count_star_perfil = 0;
                                            $promedio_star_perfil = 0;
                                        @endphp

                                        @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                            @php
                                                $total_star_perfil += $star_perfil->estrellas;
                                                $count_star_perfil ++;
                                                $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                            @endphp
                                        @endforeach
                                        <div class="star-rating mr-2 py-2">

                                            @for ($e = 0; $e < 5; $e++)
                                                @if ($e < floor($promedio_star_perfil))
                                                    <i class="sr-star fas fa-star active"></i>

                                                @elseif($e == floor($promedio_star_perfil))

                                                    @if (is_float($promedio_star_perfil))
                                                        @php
                                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                        @endphp

                                                        @if (intval($star_array_perfil[1]) >= 50 )
                                                            <i class="sr-star fas fa-star-half-alt active"></i>
                                                        @else
                                                            <i class="sr-star fas fa-star"></i>
                                                        @endif
                                                    @else
                                                        <i class="sr-star fas fa-star"></i>
                                                    @endif
                                                @else
                                                    <i class="sr-star fas fa-star"></i>
                                                @endif
                                            @endfor

                                        </div>
                                    </div>
                                </center>
                                <a class="text-center" href="/store/{{ $emp->slug }}">
                                    @if ($emp->razon_social)
                                        @if (strlen($emp->razon_social) > 35)
                                            <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                            </h6>
                                        @else
                                            <h6>{{ $emp->razon_social }}</h6>
                                        @endif
                                    @else
                                        @if (strlen($emp->nombre_comercial) > 35)
                                            <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                            </h6>
                                        @else
                                            <h6>{{ $emp->nombre_comercial }}</h6>
                                        @endif
                                    @endif
                                </a>
                                <span class="product-meta d-block font-size-xs pb-1">
                                    @if ($emp->descripcion)
                                        <p class="text-center">{{ $emp->descripcion }}</p>
                                    @else
                                        <p class="text-center">Sin Descripción</p>
                                    @endif
                                </span>
                            </div>
                            <div class="product-floating-btn">
                                <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                    <i class="fas fa-store font-size-base ml-1"></i>
                                    <i class=" font-size-base ml-1"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-12 justify-content-between pt-1 mb-2">
                    <dic class="row col-12">
                        <div class="col-3"></div>
                        <div class="col-6">
                            <h2 class="h4 mb-0 mr-3">No hay tiendas afiliadas en tu zona</h2>
                        </div>
                        <div class="col-3"></div>
                    </dic>
                </div>
            @endif

        </div>
    </div>

    @if ($empZona)
        @if (count($empZona) > 0)
            <section class="container-fluid mt-3 p-4">
                <div class="bg-accent mx-4 p-4" style="border-radius: 10px;">
                    <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
                        <h2 class="h3 mb-0 pt-1 mr-3">Tiendas que entregan a tu ciudad</h2>
                    </div>
                    <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                        <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:2}, &quot;720&quot;:{&quot;items&quot;:3}, &quot;991&quot;:{&quot;items&quot;:4}, &quot;1140&quot;:{&quot;items&quot;:5}, &quot;1300&quot;:{&quot;items&quot;:6}, &quot;1500&quot;:{&quot;items&quot;:6}}}">
                            @foreach ($empZona as $emp)
                                <div class="">
                                    <div class="card product-card card-static m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_tienda_detalle('{{ $emp->slug }}')">
                                            <i class="fas fa-info-circle"></i>
                                        </button>
                                        <span class="card-img-top d-block overflow-hidden">
                                            @if ($emp->icono)
                                                <img class="d-block rounded-lg mx-auto" width="150" src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $emp->slug }}" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                            @else
                                                <img class="d-block rounded-lg mx-auto" width="150" src="/img/default-store.png" alt="Sin Imagen" style="width: 100%; height: 12vw; object-fit: cover;"/>
                                            @endif
                                        </span>
                                        <div class="card-body py-2">
                                            <center>
                                                <div class="product-price">
                                                    @php
                                                        $total_star_perfil = 0;
                                                        $count_star_perfil = 0;
                                                        $promedio_star_perfil = 0;
                                                    @endphp

                                                    @foreach ($emp->ModeloValoracionEmpresas as $star_perfil)
                                                        @php
                                                            $total_star_perfil += $star_perfil->estrellas;
                                                            $count_star_perfil ++;
                                                            $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                        @endphp
                                                    @endforeach
                                                    <div class="star-rating mr-2 py-2">

                                                        @for ($e = 0; $e < 5; $e++)
                                                            @if ($e < floor($promedio_star_perfil))
                                                                <i class="sr-star fas fa-star active"></i>

                                                            @elseif($e == floor($promedio_star_perfil))

                                                                @if (is_float($promedio_star_perfil))
                                                                    @php
                                                                        $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                    @endphp

                                                                    @if (intval($star_array_perfil[1]) >= 50 )
                                                                        <i class="sr-star fas fa-star-half-alt active"></i>
                                                                    @else
                                                                        <i class="sr-star fas fa-star"></i>
                                                                    @endif
                                                                @else
                                                                    <i class="sr-star fas fa-star"></i>
                                                                @endif
                                                            @else
                                                                <i class="sr-star fas fa-star"></i>
                                                            @endif
                                                        @endfor

                                                    </div>
                                                </div>
                                            </center>
                                            <a class="text-center" href="/store/{{ $emp->slug }}">
                                                @if ($emp->razon_social)
                                                    @if (strlen($emp->razon_social) > 35)
                                                        <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->razon_social }}">
                                                            {{ ucwords(strtolower(substr($emp->razon_social, 0, 35))) }}...
                                                        </h6>
                                                    @else
                                                        <h6>{{ $emp->razon_social }}</h6>
                                                    @endif
                                                @else
                                                    @if (strlen($emp->nombre_comercial) > 35)
                                                        <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $emp->nombre_comercial }}">
                                                            {{ ucwords(strtolower(substr($emp->nombre_comercial, 0, 35))) }}...
                                                        </h6>
                                                    @else
                                                        <h6>{{ $emp->nombre_comercial }}</h6>
                                                    @endif
                                                @endif
                                            </a>
                                            <span class="product-meta d-block font-size-xs pb-1">
                                                @if ($emp->descripcion)
                                                    <p class="text-center">{{ $emp->descripcion }}</p>
                                                @else
                                                    <p class="text-center">Sin Descripción</p>
                                                @endif
                                            </span>
                                        </div>
                                        <div class="product-floating-btn">
                                            <a class="btn btn-primary btn-shadow btn-sm" type="button" href="/store/{{ $emp->slug }}" data-toggle="tooltip" data-placement="bottom" title="Visitar Tienda">
                                                <i class="fas fa-store font-size-base ml-1"></i>
                                                <i class=" font-size-base ml-1"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        @endif
    @endif

@endsection


@section('modals')

    {{-- Información de la Tienda --}}
    <div id="InformacionTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content" id="detailsTienda">

            </div>
        </div>
    </div>

    @hasrole('cliente')

        <a href="javascript:mostrarCiudades()">
            <div class="support_ubicacion" data-toggle="tooltip" data-placement="left" title="Cambiar ciudad en la cual buscar" style="padding-top: .9rem !important; padding-left: 1.2rem !important;">
                <span class="full_help">
                    <i class="fas fa-map-marked-alt"></i>
                </span>
            </div>
        </a>

    @endhasrole

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_web/ajaxIndex.js') }}"></script>

@endsection
