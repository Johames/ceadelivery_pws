@extends('principal')

@section('title_header')
 Inicio
@endsection

@section('carroucel')

@endsection

@section('content')

<!-- Contenido -->
<div class="container-fluid pb-5 mt-5 pt-3 mb-2 mb-md-4">
    <!-- Menu Expandible para los Filtros-->
    {{-- <div class="bg-light box-shadow-lg rounded-lg p-4 mt-5 mb-4" >
        <div class="d-flex justify-content-between align-items-center">
            <div class="dropdown mr-2">
                <a class="btn btn-outline-secondary dropdown-toggle" href="#shop-filters" data-toggle="collapse">
                    <i class="fas fa-filter mr-2"></i>
                    Mostrar Filtros
                </a>
            </div>
            <div class="d-flex">
                <div class="input-group-overlay mx-4">
                    <div class="input-group-prepend-overlay">
                        <span class="input-group-text">
                            <i class="fas fa-search"></i>
                        </span>
                    </div>
                    <input class="form-control prepended-form-control appended-form-control" type="text"
                        placeholder="Buscar por producto">
                </div>
            </div>
        </div>
        <div class="collapse" id="shop-filters">
            <div class="row pt-4">
                <div class="col-lg-4 col-sm-6">
                    <!-- Categories-->
                    <div class="card mb-grid-gutter">
                        <div class="card-body px-4">
                            <div class="widget widget-categories">
                                <h3 class="widget-title">Categories</h3>
                                <div class="accordion mt-n1" id="shop-categories">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading">
                                                <a class="collapsed" href="#shoes" role="button" data-toggle="collapse"
                                                    aria-expanded="false" aria-controls="shoes">
                                                    Shoes
                                                    <span class="accordion-indicator"></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="collapse" id="shoes" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links cea-filter">
                                                    <div class="input-group-overlay input-group-sm mb-2">
                                                        <input
                                                            class="cea-filter-search form-control form-control-sm appended-form-control"
                                                            type="text" placeholder="Search">
                                                        <div class="input-group-append-overlay">
                                                            <span class="input-group-text">
                                                                <i class="cea-icon-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <ul class="widget-list cea-filter-list pt-1" style="height: 12rem;"
                                                        data-simplebar data-simplebar-auto-hide="false">
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">View
                                                                    all</span><span
                                                                    class="font-size-xs text-muted ml-3">1,953</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Pumps
                                                                    &amp; High Heels</span><span
                                                                    class="font-size-xs text-muted ml-3">247</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Ballerinas
                                                                    &amp; Flats</span><span
                                                                    class="font-size-xs text-muted ml-3">156</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Sandals</span><span
                                                                    class="font-size-xs text-muted ml-3">310</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Sneakers</span><span
                                                                    class="font-size-xs text-muted ml-3">402</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Boots</span><span
                                                                    class="font-size-xs text-muted ml-3">393</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Ankle
                                                                    Boots</span><span
                                                                    class="font-size-xs text-muted ml-3">50</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Loafers</span><span
                                                                    class="font-size-xs text-muted ml-3">93</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Slip-on</span><span
                                                                    class="font-size-xs text-muted ml-3">122</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Flip
                                                                    Flops</span><span
                                                                    class="font-size-xs text-muted ml-3">116</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Clogs
                                                                    &amp; Mules</span><span
                                                                    class="font-size-xs text-muted ml-3">24</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Athletic
                                                                    Shoes</span><span
                                                                    class="font-size-xs text-muted ml-3">31</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Oxfords</span><span
                                                                    class="font-size-xs text-muted ml-3">9</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Smart
                                                                    Shoes</span><span
                                                                    class="font-size-xs text-muted ml-3">18</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading">
                                                <a href="#clothing" role="button" data-toggle="collapse"
                                                    aria-expanded="true" aria-controls="clothing">
                                                    Clothing
                                                    <span class="accordion-indicator"></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="collapse show" id="clothing" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links cea-filter">
                                                    <div class="input-group-overlay input-group-sm mb-2">
                                                        <input
                                                            class="cea-filter-search form-control form-control-sm appended-form-control"
                                                            type="text" placeholder="Search">
                                                        <div class="input-group-append-overlay">
                                                            <span class="input-group-text">
                                                                <i class="cea-icon-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <ul class="widget-list cea-filter-list pt-1" style="height: 12rem;"
                                                        data-simplebar data-simplebar-auto-hide="false">
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">View
                                                                    all</span><span
                                                                    class="font-size-xs text-muted ml-3">2,548</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Blazers
                                                                    &amp; Suits</span><span
                                                                    class="font-size-xs text-muted ml-3">235</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Blouses</span><span
                                                                    class="font-size-xs text-muted ml-3">410</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Cardigans
                                                                    &amp; Jumpers</span><span
                                                                    class="font-size-xs text-muted ml-3">107</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Dresses</span><span
                                                                    class="font-size-xs text-muted ml-3">93</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Hoodie
                                                                    &amp; Sweatshirts</span><span
                                                                    class="font-size-xs text-muted ml-3">122</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Jackets
                                                                    &amp; Coats</span><span
                                                                    class="font-size-xs text-muted ml-3">116</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Jeans</span><span
                                                                    class="font-size-xs text-muted ml-3">215</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Lingerie</span><span
                                                                    class="font-size-xs text-muted ml-3">150</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Maternity
                                                                    Wear</span><span
                                                                    class="font-size-xs text-muted ml-3">8</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Nightwear</span><span
                                                                    class="font-size-xs text-muted ml-3">26</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Shirts</span><span
                                                                    class="font-size-xs text-muted ml-3">164</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Shorts</span><span
                                                                    class="font-size-xs text-muted ml-3">147</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Socks
                                                                    &amp; Tights</span><span
                                                                    class="font-size-xs text-muted ml-3">139</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Sportswear</span><span
                                                                    class="font-size-xs text-muted ml-3">65</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Swimwear</span><span
                                                                    class="font-size-xs text-muted ml-3">18</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">T-shirts
                                                                    &amp; Vests</span><span
                                                                    class="font-size-xs text-muted ml-3">209</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Tops</span><span
                                                                    class="font-size-xs text-muted ml-3">132</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Trousers</span><span
                                                                    class="font-size-xs text-muted ml-3">105</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Underwear</span><span
                                                                    class="font-size-xs text-muted ml-3">87</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading">
                                                <a class="collapsed" href="#bags" role="button" data-toggle="collapse"
                                                    aria-expanded="false" aria-controls="bags">
                                                    Bags
                                                    <span class="accordion-indicator"></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="collapse" id="bags" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links cea-filter">
                                                    <div class="input-group-overlay input-group-sm mb-2">
                                                        <input
                                                            class="cea-filter-search form-control form-control-sm appended-form-control"
                                                            type="text" placeholder="Search">
                                                        <div class="input-group-append-overlay">
                                                            <span class="input-group-text">
                                                                <i class="cea-icon-search"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <ul class="widget-list cea-filter-list pt-1" style="height: 12rem;"
                                                        data-simplebar data-simplebar-auto-hide="false">
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">View
                                                                    all</span><span
                                                                    class="font-size-xs text-muted ml-3">801</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Handbags</span><span
                                                                    class="font-size-xs text-muted ml-3">238</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Backpacks</span><span
                                                                    class="font-size-xs text-muted ml-3">116</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Wallets</span><span
                                                                    class="font-size-xs text-muted ml-3">104</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Luggage</span><span
                                                                    class="font-size-xs text-muted ml-3">115</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Lumbar
                                                                    Packs</span><span
                                                                    class="font-size-xs text-muted ml-3">17</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Duffle
                                                                    Bags</span><span
                                                                    class="font-size-xs text-muted ml-3">9</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Bag /
                                                                    Travel Accessories</span><span
                                                                    class="font-size-xs text-muted ml-3">93</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Diaper
                                                                    Bags</span><span
                                                                    class="font-size-xs text-muted ml-3">5</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Lunch
                                                                    Bags</span><span
                                                                    class="font-size-xs text-muted ml-3">8</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Messenger
                                                                    Bags</span><span
                                                                    class="font-size-xs text-muted ml-3">2</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Laptop
                                                                    Bags</span><span
                                                                    class="font-size-xs text-muted ml-3">31</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span
                                                                    class="cea-filter-item-text">Briefcases</span><span
                                                                    class="font-size-xs text-muted ml-3">45</span></a>
                                                        </li>
                                                        <li class="widget-list-item cea-filter-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span class="cea-filter-item-text">Sport
                                                                    Bags</span><span
                                                                    class="font-size-xs text-muted ml-3">18</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading">
                                                <a class="collapsed" href="#sunglasses" role="button"
                                                    data-toggle="collapse" aria-expanded="false"
                                                    aria-controls="sunglasses">
                                                    Sunglasses
                                                    <span class="accordion-indicator"></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="collapse" id="sunglasses" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links">
                                                    <ul class="widget-list">
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>View all</span><span
                                                                    class="font-size-xs text-muted ml-3">1,842</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Fashion Sunglasses</span><span
                                                                    class="font-size-xs text-muted ml-3">953</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Sport Sunglasses</span><span
                                                                    class="font-size-xs text-muted ml-3">589</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Classic Sunglasses</span><span
                                                                    class="font-size-xs text-muted ml-3">300</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading">
                                                <a class="collapsed" href="#watches" role="button"
                                                    data-toggle="collapse" aria-expanded="false"
                                                    aria-controls="watches">
                                                    Watches
                                                    <span class="accordion-indicator"></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="collapse" id="watches" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links">
                                                    <ul class="widget-list">
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>View all</span><span
                                                                    class="font-size-xs text-muted ml-3">734</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Fashion Watches</span><span
                                                                    class="font-size-xs text-muted ml-3">572</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Casual Watches</span><span
                                                                    class="font-size-xs text-muted ml-3">110</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Luxury Watches</span><span
                                                                    class="font-size-xs text-muted ml-3">34</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Sport Watches</span><span
                                                                    class="font-size-xs text-muted ml-3">18</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="accordion-heading">
                                                <a class="collapsed" href="#accessories" role="button"
                                                    data-toggle="collapse" aria-expanded="false"
                                                    aria-controls="accessories">
                                                    Accessories
                                                    <span class="accordion-indicator"></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="collapse" id="accessories" data-parent="#shop-categories">
                                            <div class="card-body">
                                                <div class="widget widget-links">
                                                    <ul class="widget-list">
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>View all</span><span
                                                                    class="font-size-xs text-muted ml-3">920</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Belts</span><span
                                                                    class="font-size-xs text-muted ml-3">364</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Hats</span><span
                                                                    class="font-size-xs text-muted ml-3">405</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Jewelry</span><span
                                                                    class="font-size-xs text-muted ml-3">131</span></a>
                                                        </li>
                                                        <li class="widget-list-item"><a
                                                                class="widget-list-link d-flex justify-content-between align-items-center"
                                                                href="#"><span>Cosmetics</span><span
                                                                    class="font-size-xs text-muted ml-3">20</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <!-- Filtro por Rango de Precios -->
                    <div class="card mb-grid-gutter">
                        <div class="card-body px-4">
                            <div class="widget">
                                <h3 class="widget-title">Price range</h3>
                                <div class="cea-range-slider" data-start-min="250" data-start-max="680" data-min="0"
                                    data-max="1000" data-step="1">
                                    <div class="cea-range-slider-ui"></div>
                                    <div class="d-flex pb-1">
                                        <div class="w-50 pr-2 mr-2">
                                            <div class="input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input class="form-control cea-range-slider-value-min" type="text">
                                            </div>
                                        </div>
                                        <div class="w-50 pl-2">
                                            <div class="input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input class="form-control cea-range-slider-value-max" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Filtro por Marca -->
                    <div class="card mb-grid-gutter">
                        <div class="card-body px-4">
                            <div class="widget cea-filter">
                                <h3 class="widget-title">Brand</h3>
                                <div class="input-group-overlay input-group-sm mb-2">
                                    <input class="cea-filter-search form-control form-control-sm appended-form-control"
                                        type="text" placeholder="Search">
                                    <div class="input-group-append-overlay">
                                        <span class="input-group-text">
                                            <i class="cea-icon-search"></i>
                                        </span>
                                    </div>
                                </div>
                                <ul class="widget-list cea-filter-list list-unstyled pt-1" style="max-height: 12rem;"
                                    data-simplebar data-simplebar-auto-hide="false">
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="adidas">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="adidas">Adidas</label>
                                        </div>
                                        <span class="font-size-xs text-muted">425</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="ataylor">
                                            <label class="custom-control-label cea-filter-item-text" for="ataylor">Ann
                                                Taylor</label>
                                        </div>
                                        <span class="font-size-xs text-muted">15</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="armani">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="armani">Armani</label>
                                        </div>
                                        <span class="font-size-xs text-muted">18</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="banana">
                                            <label class="custom-control-label cea-filter-item-text" for="banana">Banana
                                                Republic</label>
                                        </div>
                                        <span class="font-size-xs text-muted">103</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="bilabong">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="bilabong">Bilabong</label>
                                        </div>
                                        <span class="font-size-xs text-muted">27</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="birkenstock">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="birkenstock">Birkenstock</label>
                                        </div>
                                        <span class="font-size-xs text-muted">10</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="klein">
                                            <label class="custom-control-label cea-filter-item-text" for="klein">Calvin
                                                Klein</label>
                                        </div>
                                        <span class="font-size-xs text-muted">365</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="columbia">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="columbia">Columbia</label>
                                        </div>
                                        <span class="font-size-xs text-muted">508</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="converse">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="converse">Converse</label>
                                        </div>
                                        <span class="font-size-xs text-muted">176</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="dockers">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="dockers">Dockers</label>
                                        </div>
                                        <span class="font-size-xs text-muted">54</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="fruit">
                                            <label class="custom-control-label cea-filter-item-text" for="fruit">Fruit of
                                                the Loom</label>
                                        </div>
                                        <span class="font-size-xs text-muted">739</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="hanes">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="hanes">Hanes</label>
                                        </div>
                                        <span class="font-size-xs text-muted">92</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="choo">
                                            <label class="custom-control-label cea-filter-item-text" for="choo">Jimmy
                                                Choo</label>
                                        </div>
                                        <span class="font-size-xs text-muted">17</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="levis">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="levis">Levi's</label>
                                        </div>
                                        <span class="font-size-xs text-muted">361</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="lee">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="lee">Lee</label>
                                        </div>
                                        <span class="font-size-xs text-muted">264</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="wearhouse">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="wearhouse">Men's Wearhouse</label>
                                        </div>
                                        <span class="font-size-xs text-muted">75</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="newbalance">
                                            <label class="custom-control-label cea-filter-item-text" for="newbalance">New
                                                Balance</label>
                                        </div>
                                        <span class="font-size-xs text-muted">218</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="nike">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="nike">Nike</label>
                                        </div>
                                        <span class="font-size-xs text-muted">810</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="navy">
                                            <label class="custom-control-label cea-filter-item-text" for="navy">Old
                                                Navy</label>
                                        </div>
                                        <span class="font-size-xs text-muted">147</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="polo">
                                            <label class="custom-control-label cea-filter-item-text" for="polo">Polo
                                                Ralph Lauren</label>
                                        </div>
                                        <span class="font-size-xs text-muted">64</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="puma">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="puma">Puma</label>
                                        </div>
                                        <span class="font-size-xs text-muted">370</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="reebok">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="reebok">Reebok</label>
                                        </div>
                                        <span class="font-size-xs text-muted">506</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="skechers">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="skechers">Skechers</label>
                                        </div>
                                        <span class="font-size-xs text-muted">209</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="hilfiger">
                                            <label class="custom-control-label cea-filter-item-text" for="hilfiger">Tommy
                                                Hilfiger</label>
                                        </div>
                                        <span class="font-size-xs text-muted">487</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="armour">
                                            <label class="custom-control-label cea-filter-item-text" for="armour">Under
                                                Armour</label>
                                        </div>
                                        <span class="font-size-xs text-muted">90</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="urban">
                                            <label class="custom-control-label cea-filter-item-text" for="urban">Urban
                                                Outfitters</label>
                                        </div>
                                        <span class="font-size-xs text-muted">152</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="vsecret">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="vsecret">Victoria's Secret</label>
                                        </div>
                                        <span class="font-size-xs text-muted">238</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="wolverine">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="wolverine">Wolverine</label>
                                        </div>
                                        <span class="font-size-xs text-muted">29</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="wrangler">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="wrangler">Wrangler</label>
                                        </div>
                                        <span class="font-size-xs text-muted">115</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <!-- Filtro por Talla -->
                    <div class="card mb-grid-gutter">
                        <div class="card-body px-4">
                            <div class="widget cea-filter">
                                <h3 class="widget-title">Size</h3>
                                <div class="input-group-overlay input-group-sm mb-2">
                                    <input class="cea-filter-search form-control form-control-sm appended-form-control"
                                        type="text" placeholder="Search">
                                    <div class="input-group-append-overlay">
                                        <span class="input-group-text">
                                            <i class="cea-icon-search"></i>
                                        </span>
                                    </div>
                                </div>
                                <ul class="widget-list cea-filter-list list-unstyled pt-1" style="max-height: 12rem;"
                                    data-simplebar data-simplebar-auto-hide="false">
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-xs">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-xs">XS</label>
                                        </div>
                                        <span class="font-size-xs text-muted">34</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-s">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-s">S</label>
                                        </div>
                                        <span class="font-size-xs text-muted">57</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-m">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-m">M</label>
                                        </div>
                                        <span class="font-size-xs text-muted">198</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-l">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-l">L</label>
                                        </div>
                                        <span class="font-size-xs text-muted">72</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-xl">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-xl">XL</label>
                                        </div>
                                        <span class="font-size-xs text-muted">46</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-39">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-39">39</label>
                                        </div>
                                        <span class="font-size-xs text-muted">112</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-40">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-40">40</label>
                                        </div>
                                        <span class="font-size-xs text-muted">85</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-41">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-40">41</label>
                                        </div>
                                        <span class="font-size-xs text-muted">210</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-42">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-42">42</label>
                                        </div>
                                        <span class="font-size-xs text-muted">57</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-43">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-43">43</label>
                                        </div>
                                        <span class="font-size-xs text-muted">30</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-44">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-44">44</label>
                                        </div>
                                        <span class="font-size-xs text-muted">61</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-45">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-45">45</label>
                                        </div>
                                        <span class="font-size-xs text-muted">23</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-46">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-46">46</label>
                                        </div>
                                        <span class="font-size-xs text-muted">19</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-47">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-47">47</label>
                                        </div>
                                        <span class="font-size-xs text-muted">15</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-48">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-48">48</label>
                                        </div>
                                        <span class="font-size-xs text-muted">12</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center mb-1">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-49">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-49">49</label>
                                        </div>
                                        <span class="font-size-xs text-muted">8</span>
                                    </li>
                                    <li class="cea-filter-item d-flex justify-content-between align-items-center">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" type="checkbox" id="size-50">
                                            <label class="custom-control-label cea-filter-item-text"
                                                for="size-50">50</label>
                                        </div>
                                        <span class="font-size-xs text-muted">6</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Filtro por Color -->
                    <div class="card mb-grid-gutter">
                        <div class="card-body px-4">
                            <div class="widget">
                                <h3 class="widget-title">Color</h3>
                                <div class="d-flex flex-wrap">
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-blue-gray">
                                        <label class="custom-option-label rounded-circle" for="color-blue-gray">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #b3c8db;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1"
                                            for="color-blue-gray">Blue-gray</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-burgundy">
                                        <label class="custom-option-label rounded-circle" for="color-burgundy">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #ca7295;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1"
                                            for="color-burgundy">Burgundy</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-teal">
                                        <label class="custom-option-label rounded-circle" for="color-teal">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #91c2c3;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1"
                                            for="color-teal">Teal</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-brown">
                                        <label class="custom-option-label rounded-circle" for="color-brown">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #9a8480;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1"
                                            for="color-brown">Brown</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-coral-red">
                                        <label class="custom-option-label rounded-circle" for="color-coral-red">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #ff7072;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1" for="color-coral-red">Coral
                                            red</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-navy">
                                        <label class="custom-option-label rounded-circle" for="color-navy">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #696dc8;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1"
                                            for="color-navy">Navy</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-charcoal">
                                        <label class="custom-option-label rounded-circle" for="color-charcoal">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #4e4d4d;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1"
                                            for="color-charcoal">Charcoal</label>
                                    </div>
                                    <div class="custom-control custom-option text-center mb-2 mx-1"
                                        style="width: 4rem;">
                                        <input class="custom-control-input" type="checkbox" id="color-sky-blue">
                                        <label class="custom-option-label rounded-circle" for="color-sky-blue">
                                            <span class="custom-option-color rounded-circle"
                                                style="background-color: #8bcdf5;"></span>
                                        </label>
                                        <label class="d-block font-size-xs text-muted mt-n1" for="color-sky-blue">Sky
                                            blue</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="input-group-overlay d-block d-md-none mt-5">
        <input class="form-control prepended-form-control appended-form-control" type="text" id="Buscadormovil" placeholder="Buscar Productos en todas las tiendas">
        <div class="input-group-append-overlay" style="background-color: white;">
            <button class="btn btn-outline-primary" onclick="buscartextmovil()" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">
                <i class="fas fa-search white" aria-hidden="true"></i>
            </button>
        </div>
    </div>

    <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 pb-2 mb-2 mt-5">
        <h2 class="h3 mb-0 pt-1 mr-3">Esta buscando: {{ $text }}  </h2>
        <br>
        <small>Se encontraron {{ count($productos) }} resultados</small>
    </div>

    <div class="row">

        @if(count($productos) > 0)

            @foreach ($productos as $prod)

                <div class="card pt-1 col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2">
                    <blockquote class="cea-testimonial mb-2">

                        {{-- <div class="col-12 pl-0">
                            <div class="card product-card h-100 m-1" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                @if ($prod->estado_desc == '0')
                                    <span class="badge badge-danger badge-shadow">
                                        @if ($prod->tipo_desc == '0')
                                            S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                                        @else()
                                            -{{ $prod->descuento }}%
                                        @endif
                                    </span>
                                @endif

                                @guest
                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="modal" href="#InicioSesion">
                                        <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito"></i>
                                    </button>
                                @endguest
                                @hasrole('cliente')
                                    <button class="btn-wishlist btn-sm d-sm-block d-lg-none d-xs-block" type="button" style="margin-right: 2.5rem" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                        <i class="fas fa-shopping-cart"></i>
                                    </button>
                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Lista de Deseos" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                                        <i class="fas fa-heart"></i>
                                    </button>
                                @endhasrole

                                @if ($prod->PrimeraImagen)
                                    <span class="card-img-top d-block overflow-hidden">
                                        <img src="https://tiendas.ceamarket.com/img/{{ $prod->PrimeraImagen->ruta }}" alt="Product" onerror="this.src='/img/default-product.png'" style="width: 100%; height: 12vw; object-fit: cover;">
                                    </span>
                                @else
                                    <span class="card-img-top d-block overflow-hidden">
                                        <img src="{{ asset("img/default-product.png") }}" alt="Sin imagen" style="width: 100%; height: 12vw; object-fit: cover;">
                                    </span>
                                @endif

                                <div class="card-body py-2">
                                    <span class="product-meta d-block font-size-xs pb-1">
                                        {{ $prod->ModeloCategoria->nombre }}
                                    </span>

                                    <!-- <h3 class="product-title font-size-sm">
                                        <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                            @if (strlen($prod->ModeloProducto->nombre) > 15)
                                                <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloProducto->nombre }}">{{ substr($prod->ModeloProducto->nombre, 0, 15) }}...</h6>
                                            @else
                                                <h6>{{ $prod->ModeloProducto->nombre }}</h6>
                                            @endif
                                        </a>
                                    </h3> -->

                                    <a class="pb-2" data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                        <h6 class="pb-1 mb-1" style="font-size: .79rem !important" >
                                            {{ $prod->ModeloProducto->nombre }}
                                        </h6>
                                    </a>

                                    <span class="product-meta d-block font-size-xs pb-1">
                                        Disponible:
                                        {{ $prod->stock . ' ' . $prod->ModeloUnidad->abreviatura }}
                                    </span>
                                    <div class="product-price">
                                        @php
                                            $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                        @endphp

                                        @if ($prod->estado_desc == '0')

                                            @if ($prod->tipo_desc == '0')
                                                @php
                                                    $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                                                @endphp
                                                <span class="text-accent">
                                                    S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                                </span>
                                                <del class="font-size-sm text-muted">
                                                    S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                </del>

                                                @if ($prod->ModeloUnidad)
                                                    <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                @endif
                                            @else

                                                @php
                                                    $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                                                @endphp

                                                <span class="text-accent">
                                                    S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                                </span>
                                                <del class="font-size-sm text-muted">
                                                    S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                </del>

                                                @if ($prod->ModeloUnidad)
                                                    <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                @endif
                                            @endif

                                        @else
                                            <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                                            @if ($prod->ModeloUnidad)
                                                <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                            @endif
                                        @endif

                                    </div>
                                </div>
                                <div class="text-center d-sm-block d-lg-none d-xs-block mt-2">
                                    <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                        <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                                        Ver Detalles
                                    </button>
                                </div>
                            </div>
                            <div class="card-body card-body-hidden">
                                @hasrole('cliente')
                                    <button class="btn btn-primary btn-sm btn-block mt-2" type="button" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                        <i class="fas fa-shopping-cart font-size-sm mr-1 d-none d-sm-block"></i>
                                        Agregar al carrito
                                    </button>
                                @endhasrole
                                <div class="text-center mt-2">
                                    <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                        <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                                        Ver Detalles
                                    </button>
                                </div>
                            </div>
                        </div> --}}

                        <div class="col-12 px-2 mb-4">
                            <div class="card product-card h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                @if ($prod->estado_desc == '0')
                                    <span class="badge badge-danger badge-shadow">
                                        @if ($prod->tipo_desc == '0')
                                            S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                                        @else()
                                            -{{ $prod->descuento }}%
                                        @endif
                                    </span>
                                @endif

                                @guest
                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="modal" href="#InicioSesion">
                                        <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito"></i>
                                    </button>
                                @endguest
                                @hasrole('cliente')
                                    <button class="btn-wishlist btn-sm d-sm-block d-lg-none d-xs-block" type="button" style="margin-right: 2.5rem" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                        <i class="fas fa-shopping-cart"></i>
                                    </button>
                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Lista de Deseos" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                                        <i class="fas fa-heart"></i>
                                    </button>
                                @endhasrole

                                @if ($prod->PrimeraImagen)
                                    <span class="card-img-top d-block overflow-hidden">
                                        <img src="https://tiendas.ceamarket.com/img/{{ $prod->PrimeraImagen->ruta }}" onerror="this.src='/img/default-product.png'" alt="{{ $prod->ModeloProducto->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;">
                                    </span>
                                @else
                                    <span class="card-img-top d-block overflow-hidden">
                                        <img src="{{ asset("/img/default-product.png") }}" alt="Sin imagen" style="width: 100%; height: 12vw; object-fit: cover;">
                                    </span>
                                @endif

                                <div class="card-body py-2" style="border-radius: 10px !important;">

                                    <span class="product-meta d-block font-size-xs pb-1">
                                        {{ $prod->ModeloCategoria->nombre }}
                                    </span>

                                    <a class="pb-2" data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                        <h6 class="pb-1 mb-1" style="font-size: .79rem !important" >
                                            {{ $prod->ModeloProducto->nombre }}
                                        </h6>
                                    </a>
                                    <span class="product-meta d-block font-size-xs pb-1">
                                        Disponible:
                                        {{ $prod->stock . ' ' . $prod->ModeloUnidad->abreviatura }}
                                    </span>
                                    <div class="d-flex justify-content-between">
                                        <div class="product-price">

                                            @php
                                                $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                            @endphp

                                            @if ($prod->estado_desc == '0')

                                                @if ($prod->tipo_desc == '0')
                                                    @php
                                                        $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                                                    @endphp
                                                    <span class="text-accent">
                                                        S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                                    </span>

                                                    <del class="font-size-sm text-muted">
                                                        S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                    </del>

                                                    @if ($prod->ModeloUnidad)
                                                        <small class="text-muted"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                    @endif
                                                @else

                                                    @php
                                                        $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                                                    @endphp

                                                    <span class="text-accent">
                                                        S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                                    </span>

                                                    <del class="font-size-sm text-muted">
                                                        S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                    </del>

                                                    @if ($prod->ModeloUnidad)
                                                        <small class="text-muted"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                    @endif
                                                @endif

                                            @else

                                                <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                                                @if ($prod->ModeloUnidad)
                                                    <small class="text-muted"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                @endif

                                            @endif

                                        </div>
                                        {{-- <div class="star-rating">
                                            <i class="sr-star fas fa-star active"></i>
                                            <i class="sr-star fas fa-star active"></i>
                                            <i class="sr-star fas fa-star active"></i>
                                            <i class="sr-star fas fa-star active"></i>
                                            <i class="sr-star fas fa-star"></i>
                                        </div> --}}
                                    </div>
                                    <div class="text-center d-sm-block d-lg-none d-xs-block mt-2">
                                        <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                            <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                                            Ver Detalles
                                        </button>
                                    </div>
                                </div>
                                <div class="card-body card-body-hidden">
                                    @hasrole('cliente')
                                        <button class="btn btn-primary btn-sm btn-block mt-2" type="button" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                            <i class="fas fa-shopping-cart font-size-sm mr-1 d-none d-sm-block"></i>
                                            Agregar al carrito
                                        </button>
                                    @endhasrole
                                    <div class="text-center mt-2">
                                        <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                            <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                                            Ver Detalles
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <hr class="d-sm-none">
                        </div>

                        <footer class="d-flex justify-content-center pt-4">
                            <div class="media align-items-center">
                                @if ($prod->ModeloEmpresa->icono)
                                    <img class="rounded" width="50" src="https://tiendas.ceamarket.com/img/{{ $prod->ModeloEmpresa->icono }}" onerror="this.src='/img/default-store.png'" alt="logo"/>
                                @else
                                    <img class="rounded" width="50" src="/img/default-store.png" alt="logo"/>
                                @endif
                                <div class="media-body pl-3">
                                    <a href="/store/{{ $prod->ModeloEmpresa->slug }}">
                                        <h6 class="font-size-sm mb-n1">
                                            @if ( $prod->ModeloEmpresa->razon_social )
                                                {{ $prod->ModeloEmpresa->razon_social }}
                                            @else
                                                {{ $prod->ModeloEmpresa->nombre_comercial }}
                                            @endif
                                        </h6>
                                    </a>
                                    <span class="font-size-ms text-muted">{{ $prod->ModeloEmpresa->ModeloRubro->nombre }}</span>
                                </div>
                            </div>
                        </footer>
                    </blockquote>
                </div>

            @endforeach
        @else
        <center class="card col-12">
            <div class="col-12 justify-content-between align-items-center p-4">
                <h2 class="h3 mb-0 pt-1 mr-3">No hay productos similares</h2>
                <br>
                <span class="mt-2 pt-1 mr-3">Intente buscar por el nombre del producto</span>
            </div>
        </center>
        @endif

    </div>

</div>

@endsection

@section('modals')

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_web/ajaxIndex.js') }}"></script>

@endsection
