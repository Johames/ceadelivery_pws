@extends('principal')

@section('title_header')
    NO ENCONTRADO
@endsection


@section('content')
    <div class="container-fluid py-5 mb-lg-3 mt-5">
        <div class="row justify-content-center pt-lg-4 text-center mt-5">
            <div class="col-lg-5 col-md-7 col-sm-9">
                <h1 class="display-404">404</h1>
                <h2 class="h3 mb-4">
                    Parece que no podemos encontrar la página que está buscando
                </h2>
                <p class="font-size-md mb-4">
                    <u>
                        En su lugar, aquí hay algunos enlaces útiles:
                    </u>
                </p>
            </div>
        </div>
        <div class="row justify-content-center mb-4">
            <div class="col-xl-8 col-lg-10">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <a class="card h-100 border-0 box-shadow-sm" href="/">
                            <div class="card-body">
                                <div class="media align-items-center"><i class="cea-icon-home text-primary h4 mb-0"></i>
                                    <div class="media-body pl-3">
                                        <h5 class="font-size-sm mb-0">Inicio</h5>
                                        <span class="text-muted font-size-ms">
                                            Volver al inicio
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <a class="card h-100 border-0 box-shadow-sm" href="/buscador/Todos">
                            <div class="card-body">
                                <div class="media align-items-center"><i class="cea-icon-search text-success h4 mb-0"></i>
                                    <div class="media-body pl-3">
                                        <h5 class="font-size-sm mb-0">Buscar Productos</h5>
                                        <span class="text-muted font-size-ms">
                                            Buscar productos en todas las tiendas
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <a class="card h-100 border-0 box-shadow-sm" href="/preguntas_frecuentes">
                            <div class="card-body">
                                <div class="media align-items-center"><i class="cea-icon-help text-info h4 mb-0"></i>
                                    <div class="media-body pl-3">
                                        <h5 class="font-size-sm mb-0">Ayuda &amp; Soporte</h5><span class="text-muted font-size-ms">Visite nuestro centro de ayuda</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

