@extends('principal')

@section('title_header')
    {{ $titulo }}
@endsection

@section('css')
    {{-- @livewireStyles --}}
@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

    <div class="mt-4 pt-2"></div>

    <div id="contentLoader1" style="display: none;">
        <div class="loader">
            <div class="cargando">
                <i class="fas fa-spinner fa-spin fa-5x"></i>
            </div>
        </div>
    </div>

    <!-- Fondo Oscuro -->
    <div class="page-title-overlap bg-dark mt-5 pt-4">
        <div class="container d-lg-flex justify-content-center py-2">
        </div>
    </div>

    <!-- Contenido del Carrito -->
    <div class="container pb-5 mb-2 mb-md-4">
        <div class="row">
            <h2 class="text-white">{{ Session::get('idCarrito') }}</h2>
        </div>
        <div class="row">
            <section class="col-lg-8" id="lista_productos">

            </section>

            <!-- Total y Notas-->
            <aside class="col-lg-4 pt-2 pt-lg-0">
                <form method="POST" id="formulario_paso_uno" name="formulario_paso_uno">
                    <div class="cea-sidebar-static rounded-lg box-shadow-lg ml-lg-auto">
                        <div class="text-center mb-4 pb-3 border-bottom">
                            <h2 class="h6 mb-3 pb-1">Subtotal</h2>
                            <h3 class="font-weight-normal" id="sub_total">
                                cargando...
                            </h3>
                        </div>

                        <div class="form-group mb-4">
                            <label class="mb-3" for="order-comments">
                                <span class="font-weight-medium">Comentarios</span>
                            </label>
                            <textarea class="form-control" id="comentarios" name="comentarios" rows="3" id="order-comments"></textarea>
                            <input type="hidden" id="codigo" name="codigo" value="">
                            @csrf
                        </div>

                        <div class="accordion" id="order-options">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="accordion-heading">
                                        <a href="#promo-code" role="button" data-toggle="collapse"
                                                aria-expanded="true" aria-controls="promo-code">
                                                Aplicar Promoción
                                                <span class="accordion-indicator"></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="collapse show p-2" id="promo-code" data-parent="#order-options">
                                    <form class="card-body needs-validation" method="post" novalidate>
                                        <div class="form-group">
                                                <input class="form-control" type="text" placeholder="Código"
                                                    id="codigo_promocion">
                                                <div class="invalid-feedback">Por favor ingrese un código válido</div>
                                        </div>
                                        <button class="btn btn-outline-primary btn-block"
                                        id="btn_aplicar_codigo_promocion" type="button" onclick="aplicar_codigo_promocion()">
                                            Aplicar Promoción
                                        </button>
                                        <button class="btn btn-outline-danger btn-block"  id="btn_eliminar_codigo_promocion" type="button" onclick="eliminar_codigo_promocion()">
                                            <i class="fa fa-trash" aria-hidden="true"></i> Eliminar código</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 p-0 mt-3 mx-0">
                            <div class="form-group">
                                <div class="progress" id="div_barra_progress_paso_uno">
                                    <div id="barra_progress_paso_uno" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div id="contenedor_de_errores_paso_uno"></div>
                        </div>

                        <!-- Carrito 2 -->
                        <button class="btn btn-primary btn-shadow btn-block mt-4" type="submit">
                                <i class="fas fa-credit-card font-size-lg mr-2"></i>
                                Continuar
                        </button>
                    </div>
                </form>
            </aside>
        </div>
    </div>

@endsection

@section('modals')



@endsection

@section('js')

    <script src="{{ asset('ajax_web/ajaxPasoUno.js') }}"></script>

@endsection
