@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

{{-- --}}

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

    <div class="mt-4 pt-2"></div>

    <div id="contentLoader1" style="display: none;">
        <div class="loader">
            <div class="cargando">
                <i class="fas fa-spinner fa-spin fa-5x"></i>
            </div>
        </div>
    </div>

    <!-- Pasos del Pedido -->
    <div class="page-title-overlap bg-dark mt-5 pt-2">
        <div class="container d-lg-flex justify-content-center py-2 py-lg-3">
            <div class="media align-items-center">
                <img class="rounded" id="imgEmpresaPedido3" width="50" src="/img/default-store.png" alt="logo"/>
                <div class="media-body pl-3">
                    <a id="urlEmpresaPedido3" href="">
                        <h6 class="font-size-sm mb-n1 my-2 text-white"></h6>
                    </a>
                    <br>
                    <span id="rubroEmpresaPedido3" class="font-size-ms text-muted"></span>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                </div>
            </div>
        </div>
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="steps steps-light pt-2">
                <a class="step-item active" href="/mi/pedido/paso/1">
                    <div class="step-progress">
                        <span class="step-count">1</span>
                    </div>
                    <div class="step-label">
                        <i class="fas fa-shopping-cart"></i>
                        Carrito
                    </div>
                </a>
                <a class="step-item active" href="/mi/pedido/paso/2">
                    <div class="step-progress">
                        <span class="step-count">2</span>
                    </div>
                    <div class="step-label">
                        <i class="fas fa-map-marked-alt"></i>
                        Dirección de entrega
                    </div>
                </a>
                <a class="step-item active current" href="#">
                    <div class="step-progress">
                        <span class="step-count">3</span>
                    </div>
                    <div class="step-label">
                        <i class="cea-icon-card"></i>
                        Método de Pago
                    </div>
                </a>
                <a class="step-item current" href="#">
                    <div class="step-progress">
                        <span class="step-count">4</span>
                    </div>
                    <div class="step-label">
                        <i class="cea-icon-card"></i>
                        Facturación
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4">
        <div class="row">
            <section class="col-lg-8">
                <div class="justify-content-between align-items-center bg-light p-4 rounded-lg mb-3">
                    <h2 class="h4 pb-3 mb-2">Elegir un método de pago</h2>

                    {{-- <div class="accordion mb-2" id="metodo_pago" role="tablist">
                        <div class="card">
                            <div class="card-header" role="tab">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#tarjeta" data-toggle="collapse">
                                        <i class="fas fa-credit-card font-size-lg mr-2 mt-n1 align-middle"></i>
                                        Pagar con tarjetas
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="tarjeta" data-parent="#metodo_pago" role="tabpanel">
                                <div class="card-body">
                                    <p class="font-size-sm">
                                        Aceptamos las siguientes tarjetas:&nbsp;&nbsp;
                                        <img class="d-inline-block align-middle" src="{{ asset('img/cards.png') }}" style="width: 187px;" alt="Cerdit Cards">
                                    </p>
                                    <div class="card-wrapper"></div>
                                    <form class="interactive-credit-card row">
                                        @csrf
                                        <div class="form-group col-sm-6">
                                            <input class="form-control" type="text" name="number" placeholder="Card Number"
                                                required>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <input class="form-control" type="text" name="name" placeholder="Full Name"
                                                required>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <input class="form-control" type="text" name="expiry" placeholder="MM/YY"
                                                required>
                                        </div>
                                        <div class="form-group col-sm-3">
                                            <input class="form-control" type="text" name="cvc" placeholder="CVC" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <button class="btn btn-outline-primary btn-block mt-0" id="btn_pagar" type="submit">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="card">
                            <div class="card-header" role="tab">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#paypal" data-toggle="collapse">
                                        <i class="fab fa-paypal mr-2 align-middle"></i>
                                        Pagar con PayPal - Próximamente
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="paypal" data-parent="#metodo_pago" role="tabpanel">
                                <div class="card-body font-size-sm">
                                    <p><span class='font-weight-medium'>PayPal</span> - the safer, easier way to pay</p>
                                    <form class="row" method="post">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" type="email" placeholder="E-mail" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" type="password" placeholder="Password" required>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="d-flex flex-wrap justify-content-between align-items-center"><a
                                                    class="nav-link-style" href="#">Forgot password?</a>
                                                <button class="btn btn-primary" type="submit">Log In</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div> -->
                        <div class="card">
                            <div class="card-header" role="tab">
                                <h3 class="accordion-heading">
                                    <a href="#otros" data-toggle="collapse">
                                        <i class="fas fa-cash-register mr-2"></i>
                                        Otros métodos de pago
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse show" id="otros" data-parent="#metodo_pago" role="tabpanel">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover bg-light font-size-sm border-bottom">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle"></th>
                                                    <th class="align-middle">Metodo de Pago</th>
                                                    <th class="align-middle">Comisión</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tabla_metodos_pago">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="card">
                            <div class="card-header" role="tab">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#ceapuntos" data-toggle="collapse">
                                        <i class="fas fa-donate mr-2"></i>
                                        Pagar con CEAPUNTOS - Próximamente
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="ceapuntos" data-parent="#metodo_pago" role="tabpanel">
                                <div class="card-body">
                                    <p>Tienes <span class="font-weight-medium">&nbsp;000</span>&nbsp;CEAPUNTOS para gastar.</p>
                                    <div class="custom-control custom-checkbox d-block">
                                        <input class="custom-control-input" type="checkbox" id="use_points">
                                        <label class="custom-control-label" for="use_points">
                                            Usar mis CEAPUNTOS para pagar este pedido.
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div> --}}

                    <div class="table-responsive">
                        <table class="table table-hover bg-light font-size-sm border-bottom">
                            <thead>
                                <tr>
                                    <th class="align-middle" colspan="2">Metodo de Pago</th>
                                    {{-- <th class="align-middle">Comisión</th> --}}
                                </tr>
                            </thead>
                            <tbody id="tabla_metodos_pago">

                            </tbody>
                        </table>
                    </div>

                </div>

                <form id="formulario_paso_tres">
                    @csrf
                    <div class="d-lg-flex d-flex pt-4 mt-3">

                        <input type="hidden" name="pago_id" id="pago_id">

                        <div class="w-50 pr-3">
                            <a class="btn btn-outline-primary btn-block" href="/mi/pedido/paso/2">
                                <i class="fas fa-chevron-left mt-sm-0 mr-1"></i>
                                <span class="d-none d-sm-inline">Regresar a Método de Pago</span>
                                <span class="d-inline d-sm-none">Regresar</span>
                            </a>
                        </div>
                        <div class="w-50 pl-2">
                            <button type="submit" class="btn btn-primary btn-block">
                                <span class="d-none d-sm-inline">Completar Pedido</span>
                                <span class="d-inline d-sm-none">Completar Pedido</span>
                                <i class="fas fa-chevron-right mt-sm-0 ml-1"></i>
                            </button>
                        </div>
                    </div>
                </form>

            </section>
            <aside class="col-lg-4 pt-4 pt-lg-0">
                <div class="cea-sidebar-static rounded-lg box-shadow-lg ml-lg-auto">
                    <div class="widget mb-3" id="lista_productos_list">

                    </div>
                    <ul class="list-unstyled font-size-sm pb-2 border-bottom">
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Subtotal:</span>
                            <span class="text-right" id="sub_total">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Envío:</span>
                            <span class="text-right" id="envio">S/  0.<small>00</small></span>
                        </li>
                        {{-- <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Comisión metodo pago:</span>
                            <span class="text-right" id="pago">S/  0.<small>00</small></span>
                        </li> --}}
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">IGV:</span>
                            <span class="text-right">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Descuento:</span><span class="text-right">S/  0.<small>00</small></span>
                        </li>
                    </ul>
                    <h3 class="font-weight-normal text-center my-4" id="total">Cargando<small>...</small></h3>
                </div>
            </aside>
        </div>
    </div>

@endsection

@section('modals')

    {{-- Modal Pagar el pedido con App --}}
    <div class="modal fade" id="modal_pago_app" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0 justify-content-center" style="border: 0px !important">
                    <h4 class="modal-title">REGISTRAR PAGO CON APP</h4>
                </div>
                <div class="modal-body pt-1">
                    <div class="alert alert-info text-center" id="descripcionQR" role="alert" style="font-size: 15px">
                        Escanea el codigo para pagar tu pedido
                    </div>

                    <center>
                        <div class="card card-product mb-3" style="width: 230px">
                            <span class="card-img-top d-block overflow-hidden">
                                <img id="MetodoPagoQR" src="" onerror="this.src='/img/default_img.png'" alt="QRcode">
                            </span>
                        </div>
                    </center>

                    <div class="alert alert-warning text-center" role="alert" style="font-size: 14px">
                        Si realizaste tu pago, por favor sube una foto de la constancia.
                        <br><br>
                        Debe ser una constancia correcta, si no lo es el pedido será rechazado de inmediato.
                    </div>

                    <form novalidate id="formulario_pago_app" name="formulario_pago_app">
                        @csrf

                        <div class="col-12">
                            <label for="constancia">Constancia de Pago</label>
                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="constancia" name="constancia">
                                <label class="custom-file-label" for="constancia">Subir constancia de pago (.png, .jpg)...</label>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="progress" id="div_barra_progress_pago_app">
                                    <div id="barra_progress_pago_app" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_pago_app"></div>
                            </div>
                        </div>

                        <center class="mt-2">
                            <button class="btn btn-primary btn-shadow mt-2" type="submit">Pagar ahora</button>
                            <button class="btn btn-outline-primary btn-shadow mt-2" type="button" onclick="agregar()">Pagar al recibir mi pedido</button>
                            <button class="btn btn-outline-secondary btn-shadow mt-2" type="button" data-dismiss="modal">Cerrar</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Pagar el pedido con Tarjeta --}}
    <div class="modal fade" id="modal_pago_tarjeta" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                {{-- <div class="modal-header px-2 pt-2 pb-0 justify-content-center" style="border: 0px !important">
                    <h4 class="modal-title">REGISTRAR PAGO CON TARJETA</h4>
                </div> --}}
                <div class="modal-body" id="ContentModalTarjetas">

                </div>
                <center class="mb-3">
                    <i class="fas fa-lock"></i>
                    <span>Powered by</span>
                    <b>CULQI</b>
                </center>
            </div>
        </div>
    </div>

    {{-- Modal Pagar el pedido --}}
    <div class="modal fade" id="modal_pago_paypal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0 justify-content-center" style="border: 0px !important">
                    <h4 class="modal-title">REGISTRAR PAGO CON PAYPAL</h4>
                </div>
                <div class="modal-body pt-1">
                    <div class="alert alert-info text-center" id="descripcionQR" role="alert" style="font-size: 15px">
                        Escanea el codigo para pagar tu pedido
                    </div>

                    <center>
                        <div class="card card-product mb-3" style="width: 230px">
                            <span class="card-img-top d-block overflow-hidden">
                                <img id="MetodoPagoQR" src="" onerror="this.src='/img/default_img.png'" alt="QRcode">
                            </span>
                        </div>
                    </center>

                    <div class="alert alert-warning text-center" role="alert" style="font-size: 14px">
                        Si realizaste tu pago, por favor sube una foto de la constancia de dicho pago.
                    </div>

                    <form novalidate id="formulario_pago_paypal">
                        @csrf

                        <div class="col-12">
                            <label for="constancia">Constancia de Pago</label>
                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="" name="">
                                <label class="custom-file-label" for="constancia">Subir constancia de pago (.png, .jpg)...</label>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="progress" id="div_barra_progress_pago_paypal">
                                    <div id="barra_progress_pago_paypal" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_pago_paypal"></div>
                            </div>
                        </div>

                        <center class="mt-2">
                            <button class="btn btn-primary btn-shadow" type="submit">Pagar ahora</button>
                            <button class="btn btn-outline-secondary btn-shadow" type="button" data-dismiss="modal">Cerrar</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')


    <!-- Incluyendo .js de Culqi JS -->
    <script src="https://checkout.culqi.com/v2"></script>

    <script src="{{ asset('ajax_web/ajaxPasoTres.js') }}"></script>

@endsection
