@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

{{-- --}}

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

<!-- Pasos del Pedido -->
<div class="page-title-overlap bg-dark">
    <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
         <div class="steps steps-light pt-2">
              <a class="step-item active" href="/mi/pedido/paso/1">
                   <div class="step-progress">
                        <span class="step-count">1</span>
                   </div>
                   <div class="step-label">
                        <i class="cea-icon-cart"></i>
                        Carrito
                   </div>
              </a>
              <a class="step-item active" href="/mi/pedido/paso/2">
                   <div class="step-progress">
                        <span class="step-count">2</span>
                   </div>
                   <div class="step-label">
                        <i class="cea-icon-user-circle"></i>
                        Dirección de Entrega
                   </div>
              </a>
              <a class="step-item active" href="/mi/pedido/paso/3">
                   <div class="step-progress">
                        <span class="step-count">3</span>
                   </div>
                   <div class="step-label">
                        <i class="cea-icon-package"></i>
                        Método de Envio
                   </div>
              </a>
              <a class="step-item active" href="/mi/pedido/paso/4">
                   <div class="step-progress">
                        <span class="step-count">4</span>
                   </div>
                   <div class="step-label">
                        <i class="cea-icon-card"></i>
                        Método de Pago
                   </div>
              </a>
              <a class="step-item active current" href="#">
                   <div class="step-progress">
                        <span class="step-count">5</span>
                   </div>
                   <div class="step-label">
                        <i class="cea-icon-check-circle"></i>
                        Pedido Copletado
                   </div>
              </a>
         </div>
    </div>
</div>

<!-- Contenido de la Pagina -->
<div class="container pb-5 mb-2 mb-md-4">
    <div class="row">
         <section class="col-lg-8">
              <div class="row mx-n2" id="lista_productos">

              </div>
              <div class="d-none d-lg-flex pt-4">
                   <div class="w-50 pl-2">
                        <a class="btn btn-primary btn-block" href="#">
                             <span class="d-none d-sm-inline">Finalizar</span>
                             <span class="d-inline d-sm-none">Finalizar</span>
                             <i class="cea-icon-arrow-right mt-sm-0 ml-1"></i>
                        </a>
                   </div>
              </div>
         </section>
         <!-- Sidebar-->
         <aside class="col-lg-4 pt-4 pt-lg-0">
            <div class="cea-sidebar-static rounded-lg box-shadow-lg ml-lg-auto">

                 <ul class="list-unstyled font-size-sm pb-2 border-bottom">
                      <li class="d-flex justify-content-between align-items-center">
                           <span class="mr-2">Subtotal:</span>
                           <span class="text-right" id="sub_total">S/  0.<small>00</small></span>
                      </li>
                      <li class="d-flex justify-content-between align-items-center">
                           <span class="mr-2">Envío:</span>
                           <span class="text-right" id="envio">S/  0.<small>00</small></span>
                      </li>
                      <li class="d-flex justify-content-between align-items-center">
                           <span class="mr-2">Comisión metodo pago:</span>
                           <span class="text-right" id="pago">S/  0.<small>00</small></span>
                       </li>
                      <li class="d-flex justify-content-between align-items-center">
                           <span class="mr-2">IGV:</span>
                           <span class="text-right">S/  0.<small>00</small></span>
                      </li>
                      <li class="d-flex justify-content-between align-items-center">
                           <span class="mr-2">Descuento:</span><span class="text-right">S/  0.<small>00</small></span>
                      </li>
                 </ul>
                 <h3 class="font-weight-normal text-center my-4" id="total">Cargando<small>...</small></h3>

            </div>
         </aside>
    </div>
    <!-- Navigation (mobile)-->
    <div class="row d-lg-none">
         <div class="col-lg-8">
              <div class="d-flex pt-4 mt-3">
                   <div class="w-50 pl-2">
                        <a class="btn btn-primary btn-block" href="modelo_carroto6.html">
                             <span class="d-none d-sm-inline">Finalizar</span>
                             <span class="d-inline d-sm-none">Finalizar</span>
                             <i class="cea-icon-arrow-right mt-sm-0 ml-1"></i>
                        </a>
                   </div>
              </div>
         </div>
    </div>
</div>

@endsection

@section('modals')



@endsection

@section('js')



<script src="{{ asset('ajax_web/ajaxPasoCinco.js') }}"></script>

@endsection
