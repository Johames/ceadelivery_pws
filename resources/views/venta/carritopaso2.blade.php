@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

    <style>
        #map {
            height: 300px;
            width: 100%;
        }

        .map-responsive {
            overflow: hidden;
            padding-bottom: 300px;
            position: relative;
            height: 0;
        }
    </style>

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

    <div class="mt-4 pt-2"></div>

    <div id="contentLoader1" style="display: none;">
        <div class="loader">
            <div class="cargando">
                <i class="fas fa-spinner fa-spin fa-5x"></i>
            </div>
        </div>
    </div>

    <!-- Pasos del Pedido -->
    <div class="page-title-overlap bg-dark mt-5 pt-2">
        <div class="container d-lg-flex justify-content-center py-2 py-lg-3">
            <div class="media align-items-center">
                <img class="rounded" id="imgEmpresaPedido2" width="50" src="/img/default-store.png" onerror="this.src='/img/default-store.png'" alt="logo"/>
                <div class="media-body pl-3">
                    <a id="urlEmpresaPedido2" href="">
                        <h6 class="font-size-sm mb-n1 my-2 text-white"></h6>
                    </a>
                    <br>
                    <span id="rubroEmpresaPedido2" class="font-size-ms text-muted"></span>
                </div>
            </div>
        </div>
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="steps steps-light pt-2">
                <a class="step-item active" href="/mi/pedido/paso/1">
                    <div class="step-progress">
                        <span class="step-count">1</span>
                    </div>
                    <div class="step-label">
                        <i class="fas fa-shopping-cart"></i>
                        Carrito
                    </div>
                </a>
                <a class="step-item active current" href="#">
                    <div class="step-progress">
                        <span class="step-count">2</span>
                    </div>
                    <div class="step-label">
                        <i class="fas fa-map-marked-alt"></i>
                        Dirección de entrega
                    </div>
                </a>
                <a class="step-item current" href="#">
                    <div class="step-progress">
                        <span class="step-count">3</span>
                    </div>
                    <div class="step-label">
                        <i class="cea-icon-card"></i>
                            Método de Pago
                    </div>
                </a>
                <a class="step-item current" href="#">
                    <div class="step-progress">
                        <span class="step-count">4</span>
                    </div>
                    <div class="step-label">
                        <i class="cea-icon-card"></i>
                        Facturación
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!-- Page Content-->
    <div class="container pb-5 mb-2 mb-md-4">
        <div class="row">
            <section class="col-lg-8">
                <div class="justify-content-between align-items-center bg-light p-4 rounded-lg mb-3">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="account-fn" class="font-size-md my-2">
                                <strong>Selecione su dirección de entrega</strong>
                            </label>
                            <div class="input-group">
                                <select class="form-control custom-select" id="select_modal_direcciones">

                                </select>
                                <div class="input-group-prepend">
                                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal_direcciones" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px;">
                                        <i class="fas fa-plus-circle"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <label class="font-size-md my-2">
                            <strong>Selecione su método de envío</strong>
                        </label>
                        <div class="table-responsive">
                            <table class="table table-hover bg-light font-size-sm border-bottom">
                                <thead>
                                    <tr>
                                        <th class="align-middle" colspan="2">Método de Entrega</th>
                                        <th class="align-middle">Tiempo Estimado</th>
                                        <th class="align-middle">Costo Envío</th>
                                    </tr>
                                </thead>
                                <tbody id="tabla_metodos_envio">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <form id="formulario_paso_dos">
                    <div class="row px-3 pt-4 mt-3">
                        @csrf
                        <input type="hidden" name="direcion_id" id="direcion_id">
                        <input type="hidden" name="envio_id" id="envio_id">

                        <div class="col-md-12 p-0">
                            <div class="form-group">
                                <div class="progress" id="div_barra_progress_paso_dos">
                                    <div id="barra_progress_paso_dos" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 p-0">
                            <div id="contenedor_de_errores_paso_dos"></div>
                        </div>

                        <div class="w-50 pr-3">
                            <a class="btn btn-outline-primary btn-block" href="/mi/pedido/paso/1">
                                <i class="fas fa-chevron-left mt-sm-0 mr-1"></i>
                                <span class="d-none d-sm-inline">Regresar al Carrito</span>
                                <span class="d-inline d-sm-none">Regresar</span>
                            </a>
                        </div>
                        <div class="w-50 pl-2">
                            <button type="submit" class="btn btn-primary btn-block" data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" title="Importante" data-content="Antes de continuar verifique que su dirección de entrega sea la correcta.">
                                <span class="d-none d-sm-inline">Continuar a Método de Envío</span>
                                <span class="d-inline d-sm-none">Continuar</span>
                                <i class="fas fa-chevron-right mt-sm-0 ml-1"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </section>

            <!-- Sidebar-->
            <aside class="col-lg-4 pt-4 pt-lg-0">
                <div class="cea-sidebar-static rounded-lg box-shadow-lg ml-lg-auto">
                    <div class="widget mb-3" id="lista_productos_list">
                    </div>
                    <ul class="list-unstyled font-size-sm pb-2 border-bottom">
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Subtotal:</span>
                            <span class="text-right" id="sub_total">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Envío:</span>
                            <span class="text-right" id="envio">S/  0.<small>00</small></span>
                        </li>
                        {{-- <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Comisión metodo pago:</span>
                            <span class="text-right" id="pago">S/  0.<small>00</small></span>
                        </li> --}}
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">IGV:</span>
                            <span class="text-right">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Descuento:</span>
                            <span class="text-right">S/  0.<small>00</small></span>
                        </li>
                    </ul>
                    <h3 class="font-weight-normal text-center my-4" id="total">Cargando<small>...</small></h3>

                </div>
            </aside>
        </div>
    </div>

@endsection

@section('modals')

    <div class="modal fade" id="modal_direcciones" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <form class="needs-validaton" id="formulario_direcciones">
            @csrf
            <input type="hidden" id="direciones_id" name="direciones_id">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">AGREGAR O MODIFICAR DIRECCIONES</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Dirección</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" id="direccion" name="direccion" placeholder="Ejemplo: Jr. direccion #nro - ciudad" data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" title="Importante" data-content="En su direccion indique al final la ciudad de esta direccion (direccion - ciudad) y luego click sobre el boton naranja para marcar la direccion en el mapa.">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Marcar en mapa" id="btn_buscar_direccion" type="button" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px;">
                                                <i class="fas fa-map-marked-alt"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="referencia">Referencia</label>
                                    <input class="form-control" type="text" id="referencia" name="referencia" placeholder="Cerca de..." >
                                </div>
                            </div>

                            <div class="alert alert-warning alert-with-icon mx-3 mb-1" role="alert">
                                <div class="alert-icon-box">
                                    <i class="alert-icon cea-icon-security-announcement"></i>
                                </div>
                                La ubicación del maracador no siempre es exacta, por favor de click sobre la ubicacion exacta de su direccion para colocar el marcador ahi.
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" id="coordenadas" name="coordenadas" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="map-responsive">
                                    <div id="map"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="progress" id="div_barra_progress_direcciones">
                                        <div id="barra_progress_direcciones" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_direcciones"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary btn-shadow btn-sm" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('js')

    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCb1-FEYYbdk2hfSG-TO5gCVm7t1j5PJfg'></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/noisy/1.2/jquery.noisy.min.js'></script>
    <script src="{{ asset('/funciones/mapas/map_marker.js') }}"></script>

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('ajax_web/ajaxPasoDos.js') }}"></script>

@endsection
