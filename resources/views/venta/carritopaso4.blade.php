@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

{{-- --}}

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

    <div class="mt-4 pt-2"></div>

    <div id="contentLoader1" style="display: none;">
        <div class="loader">
            <div class="cargando">
                <i class="fas fa-spinner fa-spin fa-5x"></i>
            </div>
        </div>
    </div>

    <!-- Pasos del Pedido -->
    <div class="page-title-overlap bg-dark mt-5 pt-5">
        <div class="container d-lg-flex justify-content-center py-2">
            <div class="media align-items-center">
                <img class="rounded" id="imgEmpresaPedido4" width="50" src="/img/default-store.png" onerror="this.src='/img/default-store.png'" alt="logo"/>
                <div class="media-body pl-3">
                    <a id="urlEmpresaPedido4">
                        <h6 class="font-size-sm mb-n1 my-2 text-white"></h6>
                    </a>
                    <br>
                    <span id="rubroEmpresaPedido4" class="font-size-ms text-muted"></span>
                </div>
            </div>
        </div>
        <div class="container d-lg-flex justify-content-between py-2 py-lg-3">
            <div class="steps steps-light pt-2">
                <a class="step-item active current" href="#">
                    <div class="step-progress">
                        <span class="step-count">1</span>
                    </div>
                    <div class="step-label">
                        <i class="fas fa-shopping-cart"></i>
                        Carrito
                    </div>
                </a>
                <a class="step-item active current" href="">
                    <div class="step-progress">
                        <span class="step-count">2</span>
                    </div>
                    <div class="step-label">
                        <i class="fas fa-map-marked-alt"></i>
                        Dirección de entrega
                    </div>
                </a>
                <a class="step-item active current" href="#">
                    <div class="step-progress">
                        <span class="step-count">3</span>
                    </div>
                    <div class="step-label">
                        <i class="cea-icon-card"></i>
                        Método de Pago
                    </div>
                </a>
                <a class="step-item active current" href="#">
                    <div class="step-progress">
                        <span class="step-count">4</span>
                    </div>
                    <div class="step-label">
                        <i class="cea-icon-card"></i>
                        Facturación
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!-- Contenido de la Pagina -->
    <div class="container pb-5 mb-2 mb-md-4">
        <div class="row">
            <section class="col-lg-8">
                <div class="justify-content-between align-items-center bg-light p-4 rounded-lg mb-3">
                    <form novalidate id="formulario_paso_cuatro">
                            @csrf
                        <h2 class="h6 pb-1 mb-2">Tipo de Comprobante</h2>
                        <div class="pb-2 justify-content-between align-items-center">
                            <div class="custom-control custom-option custom-control-inline mb-2">
                                <input type="radio" class="custom-control-input" id="boleta" name="comprobante" value="1" checked>
                                <label for="boleta" class="custom-option-label">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Boleta
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
                            </div>
                            <div class="custom-control custom-option custom-control-inline mb-2">
                                <input type="radio" class="custom-control-input" id="factura" name="comprobante" value="2">
                                <label for="factura" class="custom-option-label">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Factura
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </label>
                            </div>
                        </div>

                        <h2 class="h6 mt-3 pb-1 mb-2">Datos de Facturación</h2>
                        <div class="row">
                            <div class="col-12 form-group invali">
                                <label for="validationTooltip01">Cliente</label>
                                <input class="form-control" type="text" id="cliente" name="cliente" placeholder="Nombre o Razon Social">
                                <div class="valid-tooltip">Muy Bien!</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 form-group">
                                <label for="validationTooltip01">Nro. Documento</label>
                                <input minlength="8" maxlength="11" class="form-control" type="text" id="nro_documento" name="nro_documento" placeholder="RUC o DNI">
                                <div class="valid-tooltip">Muy Bien!</div>
                            </div>
                            <div class="col-6 form-group">
                                <label for="validationTooltip02">Nro. Teléfono</label>
                                <input maxlength="15" class="form-control" type="text" id="telefono" name="telefono" placeholder="Nro. Teléfono">
                                <div class="valid-tooltip">Muy Bien!</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 form-group">
                                <label for="validationTooltip01">Dirección</label>
                                <input maxlength="100" class="form-control" type="text" id="direccion" name="direccion" placeholder="Dirección">
                                <div class="valid-tooltip">Muy Bien!</div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="contenedor_de_errores_paso_cuatro"></div>
                        </div>

                        <div class="d-lg-flex d-flex pt-4">
                            <div class="w-50 pl-2">
                                <button class="btn btn-primary btn-block" type="submit">
                                    <span class="d-none d-sm-inline">Finalizar</span>
                                    <span class="d-inline d-sm-none">Finalizar</span>
                                    <i class="fas fa-chevron-right mt-sm-0 ml-1"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

            <!-- Sidebar-->
            <aside class="col-lg-4 pt-2 pt-lg-0">
                <div class="cea-sidebar-static rounded-lg box-shadow-lg ml-lg-auto">
                    <div class="pt-0 mb-1">
                        <div class="p-2">
                            <h5 class="card-title pb-2">Datos de Facturación</h5>
                            <ul class="list-unstyled font-size-sm">
                                <li class="d-flex justify-content-between align-items-center border-top p-1">
                                    <span class="mr-2"> Tipo de comprobante: </span>
                                    <span class="text-right">
                                        <strong id="view_comprobante"> Cargando... </strong>
                                    </span>
                                </li>
                                <li class="d-flex justify-content-between align-items-center border-top p-1">
                                    <span class="mr-4">Cliente:</span>
                                    <span class="text-right">
                                        <strong id="view_cliente"> Cargando... </strong>
                                    </span>
                                </li>
                                <li class="d-flex justify-content-between align-items-center border-top p-1">
                                    <span class="mr-2">Nro. Doc:</span>
                                    <span class="text-right">
                                        <strong id="view_nro_documento"> Cargando... </strong>
                                    </span>
                                </li>
                                <li class="d-flex justify-content-between align-items-center border-top p-1">
                                    <span class="mr-2"> Teléfono: </span>
                                    <span class="text-right" >
                                        <strong id="view_telefono"> Cargando... </strong>
                                    </span>
                                </li>
                                <li class="d-flex justify-content-between align-items-center border-top p-1">
                                    <span class="mr-2"> Dirección: </span>
                                    <span class="text-right">
                                        <strong id="view_direccion"> Cargando... </strong>
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="mt-0 pt-0 mb-1" id="lista_productos">

                    </div>

                    <ul class="list-unstyled font-size-sm py-2 border-bottom">
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Subtotal:</span>
                            <span class="text-right" id="sub_total">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Envío:</span>
                            <span class="text-right" id="envio">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Comisión metodo pago:</span>
                            <span class="text-right" id="pago">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">IGV:</span>
                            <span class="text-right">S/  0.<small>00</small></span>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span class="mr-2">Descuento:</span><span class="text-right">S/  0.<small>00</small></span>
                        </li>
                    </ul>
                    <h3 class="font-weight-normal text-center my-4" id="total">Cargando<small>...</small></h3>

                </div>
            </aside>
        </div>
    </div>

@endsection

@section('modals')

    {{-- Modal Pagar el pedido --}}
    <div class="modal fade" id="modal_pago_pedido" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content" style="border-radius: 20px !important;">
                <div class="modal-header px-2 pt-2 pb-0 justify-content-center" style="border: 0px !important">
                    <h4 class="modal-title">REGISTRO DE PAGO</h4>
                </div>
                <div class="modal-body pt-1">
                    <div class="alert alert-info text-center" id="descripcionQR" role="alert" style="font-size: 15px">
                        Escanea el codigo para pagar tu pedido
                    </div>

                    <center>
                        <div class="card card-product mb-3" style="width: 230px">
                            <span class="card-img-top d-block overflow-hidden">
                                <img id="MetodoPagoQR" src="" onerror="this.src='/img/default_img.png'" alt="Product">
                            </span>
                        </div>
                    </center>

                    <div class="alert alert-warning text-center" role="alert" style="font-size: 14px">
                        Si realizaste tu pago, por favor sube una foto de la constancia de dicho pago.
                    </div>

                    <form novalidate id="formulario_pago_pedido">
                        @csrf

                        <div class="col-12">
                            <label for="constancia">Constancia de Pago</label>
                            <div class="custom-file mb-2">
                                <input type="file" class="custom-file-input" id="constancia4" name="constancia4">
                                <label class="custom-file-label" for="constancia">Subir constancia de pago (.png, .jpg)...</label>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-12">
                                <div class="progress" id="div_barra_progress_pago_pedido">
                                    <div id="barra_progress_pago_pedido" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_pago_pedido"></div>
                            </div>
                        </div>

                        <center class="mt-2">
                            <button class="btn btn-outline-primary btn-shadow" type="button" data-dismiss="modal">Cancelar</button>
                            <button class="btn btn-primary btn-shadow" type="submit">Registrar Pago</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ asset('resources/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
    <script src="{{ asset('ajax_web/ajaxPasoCuatro.js') }}"></script>

@endsection
