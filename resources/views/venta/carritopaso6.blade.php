@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

{{-- --}}

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

    <div class="container pb-5 pt-5 mb-sm-4 mt-5">
        <div class="pt-3">
            <div class="card py-3 mt-sm-3">
                <div class="card-body text-center">
                    <h2 class="h4">Gracias por completar su pedido.</h2>
                    <p class="font-size-sm mb-2">Su pedido será procesado y enviado por la tienda correspondiente.</p>
                    <br><br>
                    <p class="font-size-sm mb-2">
                        <h3 class="text-center">Importante</h3>
                        <h6 class="text-center">
                            Asegúrese de guardar su nro. de pedido, por su seguridad solo con este podrá recibirlo.
                        </h6>
                        <br>
                        <h3 class='font-weight-medium'>
                            {{ $codigo }}
                            &nbsp;&nbsp;
                            <i class="fas fa-qrcode" onclick="mostrar_qr_pedido('{{ $codigo }}')" style="cursor: pointer;"></i>
                        </h3>
                        <br>
                        <p class="font-size-sm">También puede ver este codigo en su historial de compras, en el detalle de su pedido.</p>
                    </p>
                    <br>
                    <p class="font-size-sm">Se le enviará un correo con la confirmación de su pedido.</p>
                    <a class="btn btn-outline-primary mt-3 mr-3" href="{{ route('Inicio') }}">
                        <i class="fas fa-shopping-cart"></i>
                        Continuar Comprando
                    </a>
                    <a class="btn btn-primary mt-3 mr-3" href="{{ route('AdmHistorialClient') }}">
                        <i class="fas fa-history"></i>
                        &nbsp;Ver sus Pedidos
                    </a>
                    {{-- <a class="btn btn-outline-primary mt-3" href="#">
                        <i class="fas fa-star"></i>
                        &nbsp;Calificar Tienda
                    </a> --}}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modals')



@endsection

@section('js')

{{-- --}}

@endsection
