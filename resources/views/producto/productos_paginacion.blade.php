<div class="row pt-1 mx-n2">

    @if (count($productos) == 0 )
        <div class="col-lg-12">
            <div class="alert alert-info mt-2" role="alert">

                <p class="pt-3 mb-2">No se han encontrado productos...</p>
            </div>
        </div>
    @endif

    @foreach ($productos as $prod)
    <div class="col-lg-3 col-md-4 col-sm-6 px-2 mb-4">
        <div class="card product-card">

            @if ($prod->estado_desc == '0')
            <span class="badge badge-danger badge-shadow">

                @if ($prod->tipo_desc == '0')
                S/  -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                @else()
                -{{ $prod->descuento }}%
                @endif

            </span>
            @endif

            <button class="btn-wishlist bg-warning text-white btn-sm d-sm-none d-lg-none d-xs-block" onclick="mostrar_producto_empresa({{ $prod->producto_detalle_id }})" type="button">
                <i class="fas fa-edit"></i>
            </button>

            <button class="btn-wishlist bg-info text-white btn-sm d-sm-none d-lg-none d-xs-block" onclick="mostar_producto_imagen({{ $prod->producto_detalle_id }})" type="button"
                style="margin-right: 2.5rem">
                <i class="fas fa-image"></i>
            </button>

            <button class="btn-wishlist bg-danger text-white  btn-sm d-sm-none d-lg-none d-xs-block" onclick="eliminar_producto_empresa({{ $prod->producto_detalle_id }})" type="button"
                style="margin-right: 5rem">
                <i class="fas fa-trash"></i>
            </button>


            @if ($prod->estado == '0')

            <button class="btn-wishlist bg-danger text-white  btn-sm d-sm-none d-lg-none d-xs-block"  onclick="desactivar_producto_empresa({{ $prod->producto_detalle_id }})" type="button"
                style="margin-right: 7.5rem">
                <i class="fas fa-times"></i>
            </button>
            @endif

            @if ($prod->estado == '1')

            <button class="btn-wishlist bg-success text-white  btn-sm d-sm-none d-lg-none d-xs-block" onclick="activar_producto_empresa({{ $prod->producto_detalle_id }})" type="button"
                style="margin-right: 7.5rem">
                <i class="fas fa-check"></i>
            </button>
            @endif

            @php
                $ruta_img = null;
                $coun_img = 0;
            @endphp

            @foreach ($prod->ModeloProductoImagenes as $imagen)
                @php
                    $coun_img++
                @endphp

                @if ($coun_img <= 1)
                    @php
                        $ruta_img = $imagen->ruta;
                    @endphp
                @endif
            @endforeach

            @if ($ruta_img)
                <span class="card-img-top d-block overflow-hidden">
                    <img src="{{ asset("img/$ruta_img") }}" alt="Product">
                </span>
            @else
                <span class="card-img-top d-block overflow-hidden">
                    <img src="{{ asset("img/default_img.png") }}" alt="Sin imagen">
                </span>
            @endif


            <div class="card-body py-2">

                @if ($prod->estado == '0')
                    <small class="badge badge-success my-3" style="font-size: 11px;">Activo</small>

                @endif

                @if ($prod->estado == '1')
                    <small class="badge badge-danger my-3" style="font-size: 11px;">Desactivo</small>
                @endif

                <br><br>

                <h3 class="product-title font-size-sm">
                    <a data-toggle="modal" href="#DetalleProducto">{{ $prod->ModeloProducto->nombre }}</a>
                </h3>

                <span class="product-meta d-block font-size-xs pb-1">
                    Disponible: {{ $prod->stock . ' ' . $prod->ModeloUnidad->abreviatura }}
                </span>

                <div class="d-flex justify-content-between">
                    <div class="product-price">
                        {{-- <span class="text-accent">S/  154.<small>00</small></span> --}}

                        {{-- <span class="text-accent">S/  {{ $prod->precio }} </span> --}}
                        @php
                        $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                        @endphp



                        @if ($prod->estado_desc == '0')

                        @if ($prod->tipo_desc == '0')
                            @php
                                $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                            @endphp
                            <span class="text-accent">
                                S/
                                {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                            </span>

                            <del class="font-size-sm text-muted">
                                S/  {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                            </del>
                        @else

                        @php
                        $precios_array_desc_por = explode(".", number_format((float)($prod->precio -
                        ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                        @endphp

                        <span class="text-accent">
                            S/  {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                        </span>
                        <del class="font-size-sm text-muted">
                            S/  {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                        </del>
                        @endif

                        @else
                        <span class="text-accent">S/
                            {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>
                        @endif


                    </div>

                </div>
                <div class="text-center d-sm-none d-lg-none d-xs-block">
                    {{-- <a class="btn btn-outline-primary btn-block btn-sm mr-4" href="#DetalleProducto"
                        data-toggle="modal">
                        <i class="fas fa-eye align-middle mr-1"></i>
                        Ver Detalles
                    </a> --}}
                </div>
            </div>
            <div class="card-body card-body-hidden">

                <div class="row">
                    <div class="col-md-6">
                        {{-- <button class="btn btn-outline-danger btn-sm" type="button"><i class="cea-icon-trash"></i></button> --}}
                        <button class="btn btn-outline-warning btn-sm btn-block mt-2 mb-2"
                                data-toggle="tooltip" data-placement="top" title="Editar"
                                onclick="mostrar_producto_empresa({{ $prod->producto_detalle_id }})" type="button">
                            <i class="fas fa-edit font-size-sm mr-1"></i>
                        </button>
                        <button class="btn btn-outline-info btn-sm btn-block mt-2 mb-2"
                                data-toggle="tooltip" data-placement="top" title="Agregar Foto"
                                onclick="mostar_producto_imagen({{ $prod->producto_detalle_id }})" type="button">
                            <i class="fas fa-image font-size-sm mr-1"></i>
                        </button>
                    </div>
                    <div class="col-md-6">

                        <button class="btn btn-outline-danger btn-sm btn-block mt-2 mb-2"
                                data-toggle="tooltip" data-placement="top" title="Eliminar"
                                onclick="eliminar_producto_empresa({{ $prod->producto_detalle_id }})" type="button">
                            <i class="fas fa-trash font-size-sm mr-1"></i>
                        </button>

                        @if ($prod->estado == '0')
                            <button class="btn btn-outline-danger btn-sm btn-block mt-2 mb-2"
                            data-toggle="tooltip" data-placement="top" title="Desactivar"
                            onclick="desactivar_producto_empresa({{ $prod->producto_detalle_id }})" type="button">
                                <i class="fas fa-times font-size-sm mr-1"></i>
                            </button>
                        @endif

                        @if ($prod->estado == '1')
                        <button class="btn btn-outline-success btn-sm btn-block mt-2 mb-2"
                                data-toggle="tooltip" data-placement="top" title="Activar"
                                onclick="activar_producto_empresa({{ $prod->producto_detalle_id }})" type="button">
                            <i class="fas fa-check-circle font-size-sm mr-1"></i>
                        </button>
                        @endif
                    </div>
                </div>
                {{-- <button class="btn btn-primary btn-sm btn-block mt-2 mb-2" type="button">
                    <i class="fas fa-shopping-cart font-size-sm mr-1"></i>
                    Agregar al Carrito
                </button>
                <div class="text-center">
                    <a class="btn btn-outline-primary btn-block btn-sm mr-4" href="#DetalleProducto"
                        data-toggle="modal">
                        <i class="fas fa-eye align-middle mr-1"></i>
                        Ver Detalles
                    </a>
                </div> --}}
            </div>
        </div>
        <hr class="d-sm-none">
    </div>
    @endforeach

</div>

<div class="row my-4">

    {!! $productos->links() !!}
</div>

