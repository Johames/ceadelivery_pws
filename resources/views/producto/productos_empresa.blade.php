@extends('principal2')

@section('title_header')
    {{ $titulo }}
@endsection

@section('css')



@endsection

@section('faded')

    class="bg-faded-success"

@endsection

@section('viewsidebar')
    <a class="navbar-toggler" href="#sideNav" data-toggle="sidebar">
        <span class="navbar-toggler-icon"></span>
    </a>
@endsection

@section('menulateral')

    <aside class="cea-sidebar cea-sidebar-fixed" id="sideNav" style="padding-top: 5rem;">
        <button class="close" type="button" data-dismiss="sidebar" aria-label="Close">
            <span class="d-inline-block font-size-xs font-weight-normal align-middle">Cerrar Menú</span>
            <span class="d-inline-block align-middle ml-2" aria-hidden="true">&times;</span>
        </button>
        <div class="cea-sidebar-inner">

            <h4 class="mt-3 px-2">
                @if ( $empresa->razon_social )
                    <center>{{ $empresa->razon_social }}</center>
                @else
                    <center>{{ $empresa->nombre_comercial }}</center>
                @endif
            </h4>

            <div class="navbar-tool mb-2">
                <div class="input-group-overlay d-lg-flex mx-4">
                    <input class="form-control prepended-form-control appended-form-control" id="buscarEnTienda" type="text" placeholder="Buscar en esta tienda">
                    <div class="input-group-append-overlay" style="background-color: white;">
                        <button class="btn btn-primary" type="submit"  onclick="buscarProd('{{ $empresa->slug }}')" data-dismiss="sidebar" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">
                            <i class="fas fa-search white" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>

            <input type="hidden" value="{{ $empresa->empresa_id }}" id="empresa_id">
            <input type="hidden" value="{{ $empresa->slug }}" id="empresa_slug">
            <input type="hidden" value="{{ $empresa->coordenadas }}" id="coordenadas">
            <input type="hidden" value="{{ $empresa->direccion }}" id="direccion">

            <ul class="nav nav-tabs nav-justified mt-2 mb-0 border-top" role="tablist" style="min-height: 3rem;">
                <li class="nav-item">
                    <a class="nav-link font-weight-medium active" href="#categories" data-toggle="tab" role="tab">Categorias</a>
                </li>
            </ul>
            <div class="cea-sidebar-body pt-3 pb-0 mb-1 border-bottom" data-simplebar>
                <div class="tab-content">
                    <div class="sidebar-nav tab-pane fade show active" id="categories" role="tabpanel">
                        <div class="widget widget-categories">
                            <div class="accordion">
                                <div class="card border-bottom">
                                    <div class="card-header" id="mostrarTodos">
                                        <h3 class="accordion-heading font-size-base px-grid-gutter">
                                            <a class="collapsed py-3 d-none d-md-block" href="javascript:mostrarProductosSinCategoria()">
                                                <span class="d-flex align-items-center">
                                                    <i class="ceai-discount font-size-lg text-danger mt-n1 mr-2"></i>
                                                    Todos los Productos
                                                </span>
                                            </a>
                                            <a class="collapsed py-3 d-block d-md-none" data-dismiss="sidebar" href="javascript:mostrarProductosSinCategoria()">
                                                <span class="d-flex align-items-center">
                                                    <i class="ceai-discount font-size-lg text-danger mt-n1 mr-2"></i>
                                                    Todos los Productos
                                                </span>
                                            </a>
                                        </h3>
                                    </div>
                                </div>
                                @foreach ($categorias as $cat)
                                    <div class="card border-bottom">
                                        <div class="card-header" id="{{ $cat->nombre . '_' . $cat->categoria_id }}">
                                            <h3 class="accordion-heading font-size-base px-grid-gutter">
                                                <a class="collapsed py-3 d-none d-md-block" href="javascript:mostrarProductosCategoria('{{ $cat->nombre }}', {{ $cat->categoria_id }})">
                                                    <span class="d-flex align-items-center">
                                                        <i class="ceai-discount font-size-lg text-danger mt-n1 mr-2"></i>
                                                        {{ trim($cat->nombre) }}
                                                    </span>
                                                </a>
                                                <a class="collapsed py-3 d-block d-md-none" data-dismiss="sidebar" href="javascript:mostrarProductosCategoria('{{ $cat->nombre }}', {{ $cat->categoria_id }})">
                                                    <span class="d-flex align-items-center">
                                                        <i class="ceai-discount font-size-lg text-danger mt-n1 mr-2"></i>
                                                        {{ trim($cat->nombre) }}
                                                    </span>
                                                </a>
                                            </h3>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mx-2 mb-2">
                <div class="card-body">
                    <ul class="list-unstyled mb-0 pb-0">
                        <li class="media pb-2 border-bottom">
                            <i class="fas fa-map-marker-alt font-size-lg mt-4 mb-0 text-primary"></i>
                            <div class="media-body pl-3">
                                <span class="d-block text-heading font-size-sm pt-2">
                                    @if ($empresa->ModeloUbigeo)
                                        <b>{{ $empresa->ModeloUbigeo->nombre }}</b>
                                    @else
                                        <b>Sin Definir</b>
                                    @endif
                                </span>
                                <span class="d-block text-heading font-size-sm pt-2">
                                    @if ($empresa->direccion)
                                        {{ $empresa->direccion }}
                                    @else
                                        Sin Definir
                                    @endif
                                </span>
                            </div>
                        </li>
                        @if (count($empresa->ModeloContactos) > 0)
                            <li class="media pt-1">
                                <i class="fas fa-headset font-size-lg mt-2 mb-0 text-primary"></i>
                                <div class="media-body pl-3 pt-2">
                                    @foreach ($empresa->ModeloContactos as $telefono)
                                        @if ($telefono->tipo == 0)
                                            <a href="tel:+{{ $telefono->nombre }}" class="d-block text-heading font-size-sm">
                                                {{ $telefono->nombre }}
                                            </a>
                                        @else
                                            <p>No definido</p>
                                        @endif
                                    @endforeach
                                </div>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </aside>

@endsection

@section('content')

    {{-- Seccion de Imagenes de la Empresa --}}
    <div class="box-shadow-lg px-0 pb-0 pt-4 mb-0 mt-5">

        @if ($empresa->baner)
            <img src="https://tiendas.ceamarket.com/img/{{ $empresa->baner }}" onerror="this.src='/img/default-store-portada.png'" class="card-img-top tamano_imagen" alt="Banner Empresa">
        @else
            <img src="/img/default-store-portada.png" class="card-img-top tamano_imagen" alt="Banner Empresa">
        @endif

        <div class="" style="position: relative; background: rgba(0, 0, 0, 0.6); padding-left: 20px;">
            <div class="px-4 pb-0 up_margin" style="padding-top: 30px;">
                <div class="media align-items-center pb-5">
                    <div class="img-thumbnail rounded-circle position-relative tamano_icono">
                        {{-- <span class="badge badge-warning ml-3 mt-4" data-toggle="tooltip" title="Reward points">RPO</span> --}}

                        @if ($empresa->icono)
                            <img class="rounded-circle" style="height: 150px; width: 150px;" src="https://tiendas.ceamarket.com/img/{{ $empresa->icono }}" onerror="this.src='/img/default-store.png'" alt="{{ $empresa->razon_social }}">
                        @else
                            <img class="rounded-circle" src="/img/default-store.png" alt="{{ $empresa->razon_social }}">
                        @endif

                    </div>
                    <div class="media-body pl-3">

                        @php
                            $total_star_perfil = 0;
                            $count_star_perfil = 0;
                            $promedio_star_perfil = 0;
                        @endphp

                        @foreach ($empresa->ModeloValoracionEmpresas as $star_perfil)
                            @php
                                $total_star_perfil += $star_perfil->estrellas;
                                $count_star_perfil ++;
                                $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                            @endphp
                        @endforeach
                        <div class="star-rating mr-2 py-2">

                            @for ($e = 0; $e < 5; $e++)
                                @if ($e < floor($promedio_star_perfil))
                                    <i class="sr-star fas fa-star active tamano_estrella"></i>

                                @elseif($e == floor($promedio_star_perfil))

                                    @if (is_float($promedio_star_perfil))
                                        @php
                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                        @endphp

                                        @if (intval($star_array_perfil[1]) >= 50 )
                                            <i class="sr-star fas fa-star-half-alt active tamano_estrella"></i>
                                        @else
                                            <i class="sr-star fas fa-star tamano_estrella"></i>
                                        @endif
                                    @else
                                        <i class="sr-star fas fa-star tamano_estrella"></i>
                                    @endif
                                @else
                                    <i class="sr-star fas fa-star tamano_estrella"></i>
                                @endif
                            @endfor

                        </div>

                        @if ( $empresa->razon_social )
                            <h2 class="text-white tamano_text">{{ $empresa->razon_social }}</h2>
                        @else
                            <h2 class="text-white">{{ $empresa->nombre_comercial }}</h2>
                        @endif
                        <h4 class="font-size-lg text-white">{{ $empresa->ModeloRubro->nombre }}</h4>
                        <div class="row">
                            <button class="btn btn-info btn-sm d-none d-sm-block mr-2" onclick="mostrar_tienda_detalle('{{ $empresa->slug }}')">{{-- data-target="#InformacionTienda" data-toggle="modal" --}}
                                <i class="fas fa-info-circle" aria-hidden="true"></i> Información de la tienda
                            </button>
                            <button class="btn btn-warning btn-sm d-none d-sm-block mr-2" onclick="mostrar_tienda_valor('{{ $empresa->slug }}')">{{-- data-target="#ResenasTienda" data-toggle="modal" --}}
                                <i class="fas fa-star" aria-hidden="true"></i> Reseñas y calificaciones
                            </button>

                            <button class="btn btn-info btn-pill btn-icon d-block d-sm-none btn-sm mr-2" onclick="mostrar_tienda_detalle('{{ $empresa->slug }}')"> {{-- data-target="#InformacionTienda" data-toggle="modal" --}}
                                <i class="fas fa-info-circle" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-warning btn-pill btn-icon d-block d-sm-none btn-sm mr-2" onclick="mostrar_tienda_valor('{{ $empresa->slug }}')">{{-- data-target="#ResenasTienda" data-toggle="modal" --}}
                                <i class="fas fa-star" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mx-3" id="contenidoEmpresa">

        <!-- Mes Vendidos y Descuentos -->
        <section class="container-fluid p-0 mt-2 separar">
            <div class="row p-0">
                @if (count($descProds) > 1)
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                @else
                <div class="col-12">
                @endif
                    <!-- Productos mas Vendidos -->
                    @if (count($productos_mas_vendidos) > 1)
                        <section class="container-fluid bg-light mt-3 p-4" style="border-radius: 10px;">
                            <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-2 mb-1">
                                <h2 class="h3 mb-0 pt-1 mr-3">Más Vendidos</h2>
                            </div>
                            <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:1}, &quot;720&quot;:{&quot;items&quot;:2}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:2}, &quot;1300&quot;:{&quot;items&quot;:3}, &quot;1500&quot;:{&quot;items&quot;:3}}}">
                                    @foreach ($productos_mas_vendidos as $prod)
                                        <div class="col-6 col-sm-4 col-md-2 col-lg-1 col-xl-1 mb-4 pl-0">
                                            <div class="card product-card card-static pb-3 m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                                @if ($prod->ModeloDetalleProducto->estado_desc == '0')
                                                    <span class="badge badge-danger badge-shadow">
                                                        @if ($prod->ModeloDetalleProducto->tipo_desc == '0')
                                                            S/ -{{ number_format((float)$prod->ModeloDetalleProducto->descuento, 2, '.', '') }}
                                                        @else()
                                                            -{{ $prod->ModeloDetalleProducto->descuento }}%
                                                        @endif
                                                    </span>
                                                @endif

                                                @guest
                                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="modal" href="#InicioSesion">
                                                        <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito"></i>
                                                    </button>
                                                @endguest

                                                @hasrole('cliente')
                                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito" onclick="addCarrito({{ $prod->ModeloDetalleProducto->producto_detalle_id }})">
                                                        <i class="fas fa-shopping-cart"></i>
                                                    </button>
                                                @endhasrole

                                                @if ($prod->ModeloDetalleProducto->PrimeraImagen)
                                                    <span class="card-img-top d-block overflow-hidden" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $prod->ModeloDetalleProducto->producto_detalle_id }})">
                                                        <img src="https://tiendas.ceamarket.com/img/{{ $prod->ModeloDetalleProducto->PrimeraImagen->ruta }}" alt="{{ $empresa->slug }} - {{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}" onerror="this.src='/img/default-product.png'"  data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;">
                                                    </span>
                                                @else
                                                    <span class="card-img-top d-block overflow-hidden" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $prod->ModeloDetalleProducto->producto_detalle_id }})">
                                                        <img src="{{ asset("img/default-product.png") }}" alt="Sin imagen"  data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;">
                                                    </span>
                                                @endif

                                                <div class="card-body py-2">

                                                    {{-- <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->ModeloDetalleProducto->producto_detalle_id }})">
                                                        @if (strlen($prod->ModeloDetalleProducto->ModeloProducto->nombre) > 35)
                                                            <h6 class="pb-0 mb-0" style="font-size: .79rem !important" data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}">
                                                                {{ ucwords(mb_strtolower(substr($prod->ModeloDetalleProducto->ModeloProducto->nombre, 0, 35), 'UTF-8')) }}...
                                                            </h6>
                                                        @else
                                                            <h6 class="pb-0 mb-0" style="font-size: .79rem !important">{{ ucwords(mb_strtolower($prod->ModeloDetalleProducto->ModeloProducto->nombre), 'UTF-8') }}</h6>
                                                        @endif
                                                    </a> --}}

                                                    <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->ModeloDetalleProducto->producto_detalle_id }})">
                                                        <h6 class="pb-1 mb-1" style="font-size: .79rem !important" >
                                                            {{ $prod->ModeloDetalleProducto->ModeloProducto->nombre }}
                                                        </h6>
                                                    </a>

                                                    <div class="product-price" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $prod->ModeloDetalleProducto->producto_detalle_id }})">

                                                        @php
                                                            $precios_array = explode(".", number_format((float)$prod->ModeloDetalleProducto->precio, 2, '.', ''));
                                                        @endphp

                                                        @if ($prod->ModeloDetalleProducto->estado_desc == '0')

                                                            @if ($prod->ModeloDetalleProducto->tipo_desc == '0')

                                                                @php
                                                                    $precios_array_desc_soles = explode(".", number_format((float)$prod->ModeloDetalleProducto->precio - $prod->ModeloDetalleProducto->descuento, 2, '.', ''));
                                                                @endphp

                                                                <span class="text-accent" style="font-size: .77rem !important;">
                                                                    S/ {{ $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                                                </span>

                                                                <del class="text-muted" style="font-size: .77rem !important;">
                                                                    S/ {{ $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                                </del>

                                                                @if ($prod->ModeloDetalleProducto->ModeloUnidad)
                                                                    <small class="text-muted" style="font-size: .77rem !important;">x {{ $prod->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</small>
                                                                @endif

                                                            @else

                                                                @php
                                                                    $precios_array_desc_por = explode(".", number_format((float)($prod->ModeloDetalleProducto->precio - ($prod->ModeloDetalleProducto->precio * $prod->ModeloDetalleProducto->descuento/100)), 2, '.', ''));
                                                                @endphp

                                                                <span class="text-accent" style="font-size: .67rem !important;">
                                                                    S/ {{ $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                                                </span>

                                                                <del class="text-muted" style="font-size: .67rem !important;">
                                                                    S/ {{ $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                                </del>

                                                                @if ($prod->ModeloDetalleProducto->ModeloUnidad)
                                                                    <small class="text-muted" style="font-size: .67rem !important;"> x {{ $prod->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</small>
                                                                @endif

                                                            @endif

                                                        @else

                                                            <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                                                            @if ($prod->ModeloDetalleProducto->ModeloUnidad)
                                                                <small class="text-muted">x {{ $prod->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</small>
                                                            @endif

                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    @endif
                </div>
                @if (count($productos_mas_vendidos) > 1)
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                @else
                <div class="col-12">
                @endif
                    <!-- Productos con Descuentos -->
                    @if (count($descProds) > 1)
                        <section class="container-fluid bg-light mt-3 p-4" style="border-radius: 10px;">
                            <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-2 mb-1">
                                <h2 class="h3 mb-0 pt-1 mr-3">Descuentos</h2>
                            </div>
                            <div class="cea-carousel cea-controls-static cea-controls-outside cea-dots-enabled pt-2">
                                <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 2, &quot;gutter&quot;: 16, &quot;controls&quot;: true, &quot;autoHeight&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1}, &quot;480&quot;:{&quot;items&quot;:1}, &quot;720&quot;:{&quot;items&quot;:2}, &quot;991&quot;:{&quot;items&quot;:2}, &quot;1140&quot;:{&quot;items&quot;:2}, &quot;1300&quot;:{&quot;items&quot;:3}, &quot;1500&quot;:{&quot;items&quot;:3}}}">
                                    @foreach ($descProds as $prod)
                                        <div class="col-6 col-sm-4 col-md-2 col-lg-1 col-xl-1 mb-4 pl-0">
                                            <div class="card product-card card-static pb-3 m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                                                @if ($prod->estado_desc == '0')
                                                    <span class="badge badge-danger badge-shadow">
                                                        @if ($prod->tipo_desc == '0')
                                                            S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                                                        @else()
                                                            -{{ $prod->descuento }}%
                                                        @endif
                                                    </span>
                                                @endif

                                                @guest
                                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="modal" href="#InicioSesion">
                                                        <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito"></i>
                                                    </button>
                                                @endguest

                                                @hasrole('cliente')
                                                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                                        <i class="fas fa-shopping-cart"></i>
                                                    </button>
                                                @endhasrole

                                                @if ($prod->PrimeraImagen)
                                                    <span class="card-img-top d-block overflow-hidden" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                                        <img src="https://tiendas.ceamarket.com/img/{{ $prod->PrimeraImagen->ruta }}" alt="{{ $empresa->slug }} - {{ $prod->ModeloProducto->nombre }}" onerror="this.src='/img/default-product.png'" data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloProducto->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;">
                                                    </span>
                                                @else
                                                    <span class="card-img-top d-block overflow-hidden" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                                        <img src="{{ asset("img/default-product.png") }}" alt="Sin imagen" data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloProducto->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;">
                                                    </span>
                                                @endif

                                                <div class="card-body py-2">

                                                    {{-- <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                                        @if (strlen($prod->ModeloProducto->nombre) > 35)
                                                            <h6 class="pb-0 mb-0" style="font-size: .79rem !important" data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloProducto->nombre }}">
                                                                {{ ucwords(mb_strtolower(substr($prod->ModeloProducto->nombre, 0, 35), 'UTF-8')) }}...
                                                            </h6>
                                                        @else
                                                            <h6 class="pb-0 mb-0" style="font-size: .79rem !important">{{ ucwords(mb_strtolower($prod->ModeloProducto->nombre), 'UTF-8') }}</h6>
                                                        @endif
                                                    </a> --}}

                                                    <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                                        <h6 class="pb-1 mb-1" style="font-size: .79rem !important" >
                                                            {{ $prod->ModeloProducto->nombre }}
                                                        </h6>
                                                    </a>

                                                    <div class="product-price" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">

                                                        @php
                                                            $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                                        @endphp

                                                        @if ($prod->tipo_desc == '0')

                                                            @php
                                                                $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                                                            @endphp

                                                            <span class="text-accent" style="font-size: .77rem !important;">
                                                                S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                                            </span>

                                                            <del class="text-muted" style="font-size: .77rem !important;">
                                                                S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                            </del>

                                                            @if ($prod->ModeloUnidad)
                                                                <small class="text-muted" style="font-size: .77rem !important;"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                            @endif

                                                        @else

                                                            @php
                                                                $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                                                            @endphp

                                                            <span class="text-accent" style="font-size: .67rem !important;">
                                                                S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                                            </span>

                                                            <del class="text-muted" style="font-size: .67rem !important;">
                                                                S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                                            </del>

                                                            @if ($prod->ModeloUnidad)
                                                                <small class="text-muted" style="font-size: .67rem !important;"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                                            @endif

                                                        @endif

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </section>
                    @endif
                </div>
            </div>
        </section>

        <div>
            <section class="container-fluid bg-light mt-3 pb-4 mb-4" style="border-radius: 10px;" id="">

                <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-3 mb-4" id="NombreSeccionCat">

                </div>

                <input type="hidden" id="NombreCat">
                <input type="hidden" id="CatProdId">
                <input type="hidden" id="Page" value="1">

                <div class="row pt-1 mx-2" id="prodsCat">

                </div>

                <div class="col-12" id="botonVerMas">
                    <div class="align-content-between">
                        <button class="btn btn-info btn-block btn-xl" onclick="mostrarProductosCategoriaPage()">MOSTRAR MÁS PRODUCTOS</button>
                    </div>
                </div>

            </section>
        </div>

    </div>


    @if (count($pagos) > 0)

        @if (count($pagos) < 5)
            <section class="container-fluid my-3 px-3 mx-0">
                <div class="cea-carousel border-right">
                    <div class="cea-carousel-inner" data-carousel-options="{ &quot;nav&quot;: false, &quot;controls&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2},&quot;360&quot;:{&quot;items&quot;:2},&quot;600&quot;:{&quot;items&quot;:{{ count($pagos) }}},&quot;991&quot;:{&quot;items&quot;:{{ count($pagos) }}},&quot;1200&quot;:{&quot;items&quot;:{{ count($pagos) }}}} }">
                        @foreach ($pagos as $pago)
                            <div>
                                <div class="d-block bg-white border py-1 px-1" style="margin-right: -.0625rem;">
                                    {{-- <img class="d-block mx-auto" src="https://tiendas.ceamarket.com/img/{{ $pago->ModeloMetodoPago->icono }}" onerror="this.src='/img/cards.png'" style="width: 150px; height: 90px;" alt="{{ $pago->ModeloMetodoPago->nombre }}"> --}}
                                    <div class="justify-content-center text-center pt-2">
                                        <i class="{{ $pago->ModeloMetodoPago->icono }} fa-2x"></i>
                                    </div>
                                    <div class="card-body text-center">
                                        <p class="mb-1"><strong>{{ $pago->ModeloMetodoPago->nombre }}</strong></p>
                                        <p class="mb-1">{{ $pago->descripcion }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @else
            <section class="container-fluid my-3">
                <div class="cea-carousel border-right">
                    <div class="cea-carousel-inner" data-carousel-options="{ &quot;nav&quot;: false, &quot;controls&quot;: false, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;loop&quot;: true, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2},&quot;360&quot;:{&quot;items&quot;:2},&quot;600&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:5}} }">
                        @foreach ($pagos as $pago)
                            <div>
                                <div class="d-block bg-white border py-1 px-1" style="margin-right: -.0625rem;">
                                    {{-- <img class="d-block mx-auto" src="https://tiendas.ceamarket.com/img/{{ $pago->ModeloMetodoPago->icono }}" onerror="this.src='/img/cards.png'" style="width: 150px; height: 90px;" alt="{{ $pago->ModeloMetodoPago->nombre }}"> --}}
                                    <div class="justify-content-center text-center pt-2">
                                        <i class="{{ $pago->ModeloMetodoPago->icono }} fa-2x"></i>
                                    </div>
                                    <div class="card-body text-center">
                                        <p class="mb-1"><strong>{{ $pago->ModeloMetodoPago->nombre }}</strong></p>
                                        <p class="mb-1">{{ $pago->descripcion }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
        @endif

    @endif

@endsection

@section('socialbuttons')

    <div class="social-bar">
        @foreach ($empresa->ModeloRedesSociales as $redes_nav)
            <a href="{{ $redes_nav->url }}" class="icon {{ $redes_nav->color }}" target="_blank">
                <i class="{{ $redes_nav->icono }}"></i>
                <span class="text">&nbsp; &nbsp; &nbsp; Visítanos en {{ $redes_nav->nombre }}</span>
            </a>
        @endforeach
    </div>

    {{-- <div class="social-bar-left">
        <a href="#OtrasTiendas" data-toggle="modal" class="icon-left bg-primary">
            <span class="text">Ver Otras Tiendas &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
            <i class="fas fa-store"></i>
        </a>
        <a href="#InformacionTienda" data-toggle="modal" class="icon-left bg-info">
            <span class="text">Información de la Tienda &nbsp; &nbsp;</span>
            <i class="fas fa-info-circle"></i>
        </a>

        <a href="#ResenasTienda" data-toggle="modal" class="icon-left bg-warning">
            <span class="text">Reseñas de la Tienda &nbsp; &nbsp;</span>
            <i class="fas fa-star"></i>
        </a>
    </div> --}}

@endsection

@section('modals')

    {{-- Modal Otras Tiendas --}}
    {{-- <div id="OtrasTiendas" class="modal fade modal-right" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content">
                <div class="modal-header-lateral bg-dark">
                    <h5 class="modal-title" style="color: white;">Visitar Otras Tiendas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color: white;">&times;</span>
                    </button>
                </div>
                <div class="modal-body lateral bg-faded-success">
                    <!-- <div class="col-12 mb-2 p-2">
                        <div class="input-group-overlay d-lg-none my-2">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Buscar una Tienda">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="input-group-overlay d-none d-lg-block">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Buscar una Tienda">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-12 px-2 mb-4">
                        @foreach($otras as $emp)
                            <a href="/store/{{ $emp->slug }}">
                                <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-2 mr-xl-0 pb-2 border-bottom" style="cursor: pointer;">
                                    <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                        <span class="d-inline-block mx-auto mr-sm-2">
                                            @if($emp->icono)
                                                <img src="https://tiendas.ceamarket.com/img/{{ $emp->icono }}" width="130" alt="Product">
                                            @else
                                                <img src="https://tiendas.ceamarket.com/img/default-store.png" width="130" alt="Product">
                                            @endif
                                        </span>
                                        <div class="media-body pt-2">
                                            <h4 class="product-title font-size-base mb-2">
                                                <span>{{ $emp->nombre_comercial }}</span>
                                            </h4>
                                            <div class="font-size-sm mb-2">
                                                {{ $emp->ModeloRubro->nombre }}
                                            </div>
                                            <hr class="mr-2">
                                            <div class="tab-pane fade active show mt-2" role="tabpanel">
                                                @foreach ($emp->ModeloRedesSociales as $redes)
                                                    <a class="social-btn sb-round sb-outline {{ $redes->color }} mb-2" href="{{ $redes->url }}" target="_blank">
                                                        <i class="{{ $redes->icono }}"></i>
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="d-sm-none">
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    {{-- Modal Otras Tiendas --}}
    <div id="ResenasTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content" id="detalle_resena">

            </div>
        </div>
    </div>

    {{-- Modal Otras Tiendas --}}
    <div id="InformacionTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño" role="document">
            <div class="modal-content" id="detalleTienda">

            </div>
        </div>
    </div>

    {{-- Modal Promociones --}}
    @if (count($promociones) > 0)
        <a href="#Promociones" data-toggle="modal">
            <div class="support pulse-button">
                <span class="full_help">
                    <span class="fas fa-ad"></span>
                </span>
            </div>
        </a>

        <div class="modal fade" id="Promociones" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Promociones y Descuentos</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <section class="cea-carousel">
                            <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 1, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 5000, &quot;nav&quot;: false, &quot;responsive&quot;: {&quot;0&quot;: {&quot;nav&quot;: true, &quot;controls&quot;: false}, &quot;576&quot;: {&quot;nav&quot;: false, &quot;controls&quot;: true}}}">
                                @foreach ($promociones as $prom)
                                    <div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-lg">
                                                    <div class="py-2 my-2 my-md-0 py-md-2 px-2 ml-md-2 text-center text-sm-left">
                                                        <span class="badge badge-danger badge-shadow mb-3">Usa el código: {{ $prom->codigo }}</span>
                                                        <h5 class="mb-4">{{ $prom->titulo }}</h5>
                                                        <h6 class="font-size-lg font-weight-light mb-2" id="view_descripcion">{{ $prom->descripcion }}</h6>
                                                        <br><br>
                                                        <span class="font-size-ms text-muted">
                                                            @if ($prom->tipo == 0)
                                                                <p><strong>S/  {{ $prom->descuento }}</strong> de Descuento</p>
                                                            @else
                                                                <p><strong>{{ $prom->descuento }}%</strong> de Descuento</p>
                                                            @endif
                                                        </span>
                                                        <span class="align-center font-size-ms text-muted" style="font-size: 9px !important;">
                                                            Promoción Válida Hasta: <strong id="view_fecha_fin">{{ $prom->fecha_fin }}</strong>
                                                        </span>
                                                    </div>
                                                    <img class="d-block ml-auto" src="https://tiendas.ceamarket.com/img/{{ $prom->img_prom }}" onerror="this.src='img/default-ads.png'" alt="Promocion" style="height: 300px !important;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection

@section('js')

    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCb1-FEYYbdk2hfSG-TO5gCVm7t1j5PJfg'></script>

    <script src="{{ asset('/funciones/mapas/map_marker.js') }}"></script>

    <script src="{{ asset('/ajax_web/ajaxProductoEmpresa.js') }}"></script>

@endsection
