@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')
<style>
    @media screen and (max-width: 576px) {
        .tamano_imagen {
            background-color: #26a69a !important;
            height: 250px !important;
            border-top-left-radius: 0px !important;
            border-top-right-radius: 0px !important;
        }
        .up_margin {
            margin-top: -11.4rem !important;
        }
        .tamano_icono {
            width: 6rem !important;
        }
        .tamano_text {
            font-size: 1rem !important;
        }
        .tamano_estrella {
            font-size: 1rem !important;
        }
    }

    @media screen and (min-width : 576px) and (max-width : 818px) {
        .tamano_imagen {
            background-color: #26a69a !important;
            height: 300px !important;
            border-top-left-radius: 0px !important;
            border-top-right-radius: 0px !important;
        }
        .up_margin {
            margin-top: -10.3rem !important;
        }
        .tamano_icono {
            width: 7rem !important;
        }
        .tamano_text {
            font-size: 1.3rem !important;
        }
        .tamano_estrella {
            font-size: 1.3rem !important;
        }
    }

    @media screen and (min-width : 818px) and (max-width : 1920px) {
        .tamano_imagen {
            background-color: #26a69a !important;
            height: 420px !important;
            border-top-left-radius: 0px !important;
            border-top-right-radius: 0px !important;
        }
        .up_margin {
            margin-top: -11.5rem !important;
        }
        .tamano_icono {
            width: 10rem !important;
        }
        .tamano_text {
            font-size: 2rem !important;
        }
        .tamano_estrella {
            font-size: 1.8rem !important;
        }
    }

    @-webkit-keyframes pulse {
        to {
            box-shadow: 0 0 0 35px rgba(3, 169, 244, 0);
        }
    }
    @-moz-keyframes pulse {
        to {
            box-shadow: 0 0 0 35px rgba(3, 169, 244, 0);
        }
    }
    @-ms-keyframes pulse {
        to {
            box-shadow: 0 0 0 35px rgba(3, 169, 244, 0);
        }
    }
    @keyframes pulse {
        to {
            box-shadow: 0 0 0 35px rgba(3, 169, 244, 0);
        }
    }

</style>
@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('carroucel')

<!--<div class="box-shadow-lg px-0 pt-0 pb-0 mb-0 mb-lg-0">
    <img src="img/contacts/chicago.jpg" class="card-img-top" alt="Card image" style="background-color: #26a69a; height: 550px !important; border-top-left-radius: 0px !important; border-top-right-radius: 0px !important;">
    <div class="px-4 pb-2" style="margin-top: -11.5rem;">
        <div class="media align-items-center pb-3">
            <div class="img-thumbnail rounded-circle position-relative" style="width: 10rem;">
                <span class="badge badge-warning ml-3 mt-4" data-toggle="tooltip" title="Reward points">384</span>
                <img class="rounded-circle" src="img/shop/account/avatar.jpg" alt="Susan Gardner">
            </div>
            <div class="media-body pl-3">
                <div class="star-rating pb-2">
                    <i class="sr-star fas fa-star active font-size-lg"></i>
                    <i class="sr-star fas fa-star active font-size-lg"></i>
                    <i class="sr-star fas fa-star active font-size-lg"></i>
                    <i class="sr-star fas fa-star active font-size-lg"></i>
                    <i class="sr-star fas fa-star font-size-lg"></i>
                </div>
                <h2 class="text-white">Nombre de la Empresa</h2>
                <h4 class="font-size-lg text-white">Rubro de la Empresa</h4>
                <strong class="font-size-sm text-white">correo_tienda@gmail.com</strong>
            </div>
        </div>
    </div>
</div>-->

@endsection

@section('content')
<div class="box-shadow-lg px-0 pt-0 pb-0 mb-0 mb-lg-0">


    @if ($empresa->baner)
        <img src="/img/{{ $empresa->baner }}" class="card-img-top tamano_imagen" alt="Card image">
    @else
    <img src="/img/default-store-portada.png" class="card-img-top tamano_imagen" alt="Card image">
    @endif

    <div class="container">
        <div class="px-4 pb-2 up_margin">
            <div class="media align-items-center pb-3">
                <div class="img-thumbnail rounded-circle position-relative tamano_icono">
                    {{-- <span class="badge badge-warning ml-3 mt-4" data-toggle="tooltip" title="Reward points">RPO</span> --}}

                    @if ($empresa->icono)
                        <img class="rounded-circle" src="/img/{{ $empresa->icono }}" alt="Susan Gardner">
                    @else
                        <img class="rounded-circle" src="/img/default-store.png" alt="Susan Gardner">
                    @endif

                </div>
                <div class="media-body pl-3">

                    @php
                        $total_star_perfil = 0;
                        $count_star_perfil = 0;
                        $promedio_star_perfil = 0;
                    @endphp

                    @foreach ($empresa->ModeloValoracionEmpresas as $star_perfil)
                        @php
                            $total_star_perfil += $star_perfil->estrellas;
                            $count_star_perfil ++;
                            $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                        @endphp
                    @endforeach
                    <div class="star-rating mr-2 py-2">

                        @for ($e = 0; $e < 5; $e++)
                            @if ($e < floor($promedio_star_perfil))
                                <i class="sr-star fas fa-star active tamano_estrella"></i>

                            @elseif($e == floor($promedio_star_perfil))

                                @if (is_float($promedio_star_perfil))
                                    @php
                                        $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                    @endphp

                                    @if (intval($star_array_perfil[1]) >= 50 )
                                        <i class="sr-star fas fa-star-half-alt active tamano_estrella"></i>
                                    @else
                                        <i class="sr-star fas fa-star tamano_estrella"></i>
                                    @endif
                                @else
                                    <i class="sr-star fas fa-star tamano_estrella"></i>
                                @endif
                            @else
                                <i class="sr-star fas fa-star tamano_estrella"></i>
                            @endif
                        @endfor

                    </div>



                    @if ( $empresa->nombre_comercial )
                        <h2 class="text-white tamano_text">{{ $empresa->nombre_comercial }}</h2>
                    @else
                     <h2 class="text-white">{{ $empresa->razon_social }}</h2>
                    @endif
                    <h4 class="font-size-lg text-white">{{ $empresa->ModeloRubro->nombre }}</h4>
                    <div class="row">
                        <button class="btn btn-info btn-sm d-none d-sm-block mr-2" data-target="#InformacionTienda" data-toggle="modal">
                            <i class="fas fa-info-circle" aria-hidden="true"></i> Información de la tienda
                        </button>
                        <button class="btn btn-warning btn-sm d-none d-sm-block mr-2" data-target="#ResenasTienda" data-toggle="modal">
                            <i class="fas fa-star" aria-hidden="true"></i> Reseñas y calificaciones
                        </button>

                        <button class="btn btn-info btn-pill btn-icon d-block d-sm-none btn-sm mr-2" data-target="#InformacionTienda" data-toggle="modal">
                            <i class="fas fa-info-circle" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-warning btn-pill btn-icon d-block d-sm-none btn-sm mr-2" data-target="#ResenasTienda" data-toggle="modal">
                            <i class="fas fa-star" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Fondo Oscuro -->
{{-- <div class="bg-dark pt-1 pb-3">
    <div class="container pt-1 pb-3 pt-lg-1 pb-lg-4">
        <ul class="nav nav-tabs media-tabs nav-justified mt-3 pb-3" role="tablist" style="width: 100%; flex-wrap: unset; overflow-x: auto;">
            <li class="nav-item">
                <a class="nav-link active" href="#" data-toggle="tab" role="tab">
                    <div class="media align-items-center">
                        <div class="media-tab-media mr-3">
                            <i class="cea-icon-cart"></i>
                        </div>
                        <div class="media-body">
                            <div class="media-tab-subtitle text-muted font-size-xs mb-1">Categoría</div>
                            <h6 class="media-tab-title text-nowrap text-white mb-0">Shopping cart</h6>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tab" role="tab">
                    <div class="media align-items-center">
                        <div class="media-tab-media mr-3">
                            <i class="cea-icon-wallet"></i>
                        </div>
                        <div class="media-body">
                            <div class="media-tab-subtitle text-muted font-size-xs mb-1">Categoría</div>
                            <h6 class="media-tab-title text-nowrap text-white mb-0">Payment method</h6>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tab" role="tab">
                    <div class="media align-items-center">
                        <div class="media-tab-media mr-3">
                            <i class="cea-icon-package"></i>
                        </div>
                        <div class="media-body">
                            <div class="media-tab-subtitle text-muted font-size-xs mb-1">Categoría</div>
                            <h6 class="media-tab-title text-nowrap text-white mb-0">Delivery info</h6>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#" data-toggle="tab" role="tab">
                    <div class="media align-items-center">
                        <div class="media-tab-media mr-3">
                            <i class="cea-icon-check"></i>
                        </div>
                        <div class="media-body">
                            <div class="media-tab-subtitle text-muted font-size-xs mb-1">Categoría</div>
                            <h6 class="media-tab-title text-nowrap text-white mb-0">Confirmation</h6>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div> --}}

<section class="bg-dark py-4">
    <div class="container" data-simplebar>
        <div class="d-flex justify-content-between">
            <a class="d-block py-3 pr-sm-3 pr-lg-5 mr-5" href="/store/{{ $empresa->slug }}/categoria">
                <img class="d-block mx-auto mb-3" width="60" src="/img/food-delivery/icons/noodles.svg" alt="categoria"/>
                <h6 class="font-size-base text-center text-nowrap text-white">Todos</h6>
            </a>
            @foreach ($categorias as $cat)
                <a class="d-block py-3 pr-sm-3 pr-lg-5 mr-5" href="/store/{{ $empresa->slug }}/categoria/{{ $cat->slug }}">
                    <img class="d-block mx-auto mb-3" width="60" src="/img/food-delivery/icons/noodles.svg" alt="categoria"/>
                    <h6 class="font-size-base text-center text-nowrap text-white">{{ $cat->nombre }}</h6>
                </a>
            @endforeach

        </div>
    </div>
</section>

<!-- Contenido -->
<div class="container pb-5 mb-2 mb-md-4">

    <!-- Products grid-->
    <div class="row pt-3 mx-n2">

        @foreach ($productos as $prod)
            <div class="col-6 col-sm-4 col-md-3 col-lg-2 col-xl-2 px-2 mb-4">
                <div class="card product-card" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">

                    @if ($prod->estado_desc == '0')
                        <span class="badge badge-danger badge-shadow">

                            @if ($prod->tipo_desc == '0')
                                S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                            @else()
                                -{{ $prod->descuento }}%
                            @endif

                        </span>
                    @endif

                    @hasrole('cliente')
                        <button class="btn-wishlist btn-sm d-sm-block d-lg-none d-xs-block" type="button" style="margin-right: 2.5rem" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                            <i class="fas fa-shopping-cart"></i>
                        </button>
                        <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Lista de Deseos" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                            <i class="fas fa-heart"></i>
                        </button>
                    @endhasrole

                    @php
                        $ruta_img = null;
                        $coun_img = 0;
                    @endphp

                    @foreach ($prod->ModeloProductoImagenes as $imagen)
                        @php
                            $coun_img++
                        @endphp

                        @if ($coun_img <= 1)
                            @php
                                $ruta_img = $imagen->ruta;
                            @endphp
                        @endif
                    @endforeach

                    @if ($ruta_img)
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="{{ asset("img/$ruta_img") }}" alt="Product" style="height: 224px;">
                        </span>
                    @else
                        <span class="card-img-top d-block overflow-hidden">
                            <img src="{{ asset("img/default-product.png") }}" alt="Sin imagen">
                        </span>
                    @endif

                    <div class="card-body py-2" style="border-radius: 10px !important;">

                        <h3 class="product-title font-size-sm">
                            <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                @if (strlen($prod->ModeloProducto->nombre) > 15)
                                    <h6 data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloProducto->nombre }}">{{ substr($prod->ModeloProducto->nombre, 0, 15) }}...</h6>
                                @else
                                    <h6>{{ $prod->ModeloProducto->nombre }}</h6>
                                @endif
                            </a>
                        </h3>
                        <span class="product-meta d-block font-size-xs pb-1">
                            Disponible:
                            {{ $prod->stock . ' ' . $prod->ModeloUnidad->abreviatura }}
                        </span>
                        <div class="d-flex justify-content-between">
                            <div class="product-price">

                                @php
                                    $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                @endphp

                                @if ($prod->estado_desc == '0')

                                    @if ($prod->tipo_desc == '0')
                                        @php
                                            $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                                        @endphp
                                        <span class="text-accent">
                                            S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                        </span>

                                        <del class="font-size-sm text-muted">
                                            S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                        </del>
                                    @else

                                        @php
                                            $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                                        @endphp

                                        <span class="text-accent">
                                            S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                        </span>
                                        <del class="font-size-sm text-muted">
                                            S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                        </del>
                                    @endif

                                @else
                                    <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>
                                @endif

                            </div>
                            {{-- <div class="star-rating">
                                <i class="sr-star fas fa-star active"></i>
                                <i class="sr-star fas fa-star active"></i>
                                <i class="sr-star fas fa-star active"></i>
                                <i class="sr-star fas fa-star active"></i>
                                <i class="sr-star fas fa-star"></i>
                            </div> --}}
                        </div>
                        <div class="text-center d-sm-block d-lg-none d-xs-block mt-2">
                            <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                <i class="fas fa-eye align-middle mr-1"></i>
                                Ver Detalles
                            </button>
                        </div>
                    </div>
                    <div class="card-body card-body-hidden">
                        @hasrole('cliente')
                        <button class="btn btn-primary btn-sm btn-block mt-2" type="button" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                            <i class="fas fa-shopping-cart font-size-sm mr-1"></i>
                            Agregar al carrito
                        </button>
                        @endhasrole
                        <div class="text-center mt-2">
                            <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                <i class="fas fa-eye align-middle mr-1"></i>
                                Ver Detalles
                            </button>
                        </div>
                    </div>
                </div>
                <hr class="d-sm-none">
            </div>
        @endforeach
    </div>

    <!-- Banners -->
    {{-- <div class="row py-sm-2">
        <div class="col-md-8 mb-4">
            <div class="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-lg">
                <div class="py-4 my-2 my-md-0 py-md-5 px-4 ml-md-3 text-center text-sm-left">
                    <h4 class="font-size-lg font-weight-light mb-2">Converse All Star</h4>
                    <h3 class="mb-4">Make Your Day Comfortable</h3>
                    <a class="btn btn-primary btn-shadow btn-sm" href="#">Shop Now</a>
                </div>
                <img class="d-block ml-auto" src="img/shop/catalog/banner.jpg" alt="Shop Converse">
            </div>
        </div>
        <div class="col-md-4 mb-4">
            <div class="d-flex flex-column h-100 bg-size-cover bg-position-center rounded-lg py-4"
                style="background-image: url(img/blog/banner-bg.jpg);">
                <div class="py-4 my-2 px-4 text-center">
                    <h5 class="mb-2">Your Add Banner Here</h5>
                    <p class="font-size-sm text-muted">Hurry up to reserve your spot</p>
                    <a class="btn btn-primary btn-shadow btn-sm" href="#">Contact us</a>
                </div>
            </div>
        </div>
    </div> --}}

    <hr class="my-3">
    <nav class="d-flex justify-content-between pt-2" aria-label="Page navigation">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-chevron-left mr-2"></i>Prev</a></li>
        </ul>
        <ul class="pagination">
            <li class="page-item d-sm-none"><span class="page-link page-link-static">1 / 5</span></li>
            <li class="page-item active d-none d-sm-block" aria-current="page"><span class="page-link">1<span
                        class="sr-only">(current)</span></span></li>
            <li class="page-item d-none d-sm-block"><a class="page-link" href="#">2</a></li>
            <li class="page-item d-none d-sm-block"><a class="page-link" href="#">3</a></li>
            <li class="page-item d-none d-sm-block"><a class="page-link" href="#">4</a></li>
            <li class="page-item d-none d-sm-block"><a class="page-link" href="#">5</a></li>
        </ul>
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="#" aria-label="Next">Next<i
                        class="fas fa-chevron-right ml-2"></i></a></li>
        </ul>
    </nav>

</div>

@endsection

@section('socialbuttons')

<div class="social-bar">
    @foreach ($empresa->ModeloRedesSociales as $redes_nav)
        <a href="{{ $redes_nav->url }}" class="icon {{ $redes_nav->color }}" target="_blank">
            <i class="{{ $redes_nav->icono }}"></i>
            <span class="text">&nbsp; &nbsp; &nbsp; Visítanos en {{ $redes_nav->nombre }}</span>
        </a>
    @endforeach
</div>

<div class="social-bar-left">
    <a href="#OtrasTiendas" data-toggle="modal" class="icon-left bg-primary">
        <span class="text">Ver Otras Tiendas &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
        <i class="fas fa-store"></i>
    </a>
    <a href="#InformacionTienda" data-toggle="modal" class="icon-left bg-info">
        <span class="text">Información de la Tienda &nbsp; &nbsp;</span>
        <i class="fas fa-info-circle"></i>
    </a>

    <a href="#ResenasTienda" data-toggle="modal" class="icon-left bg-warning">
        <span class="text">Reseñas de la Tienda &nbsp; &nbsp;</span>
        <i class="fas fa-star"></i>
    </a>
</div>

@if (count($promociones) > 0)
    <a href="#Promociones" data-toggle="modal">
        <div class="support">
            <span class="full_help">
                <span class="fas fa-ad"></span>
            </span>
        </div>
    </a>
@endif

@endsection

@section('modals')
{{-- Modal Otras Tiendas --}}
<div id="OtrasTiendas" class="modal fade modal-right" tabindex="-1" role="dialog">
    <div class="modal-dialog tamaño" role="document">
        <div class="modal-content">
            <div class="modal-header-lateral bg-dark">
                <h5 class="modal-title" style="color: white;">Visitar Otras Tiendas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body lateral bg-faded-success">
                {{-- <div class="col-12 mb-2 p-2">
                    <div class="input-group-overlay d-lg-none my-2">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Buscar una Tienda">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="input-group-overlay d-none d-lg-block">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="Buscar una Tienda">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-12 px-2 mb-4">
                    @foreach($otras as $emp)
                        <a href="/store/{{ $emp->slug }}">
                            <div class="bg-faded-success rounded-lg justify-content-between pt-2 pl-2 mb-4 mr-2 mr-xl-0 pb-2 border-bottom" style="cursor: pointer;">
                                <div class="media media-ie-fix d-block d-sm-flex text-center text-sm-left">
                                    <span class="d-inline-block mx-auto mr-sm-2">
                                        @if($emp->icono)
                                            <img src="/img/{{ $emp->icono }}" width="130" alt="Product">
                                        @else
                                            <img src="/img/default-store.png" width="130" alt="Product">
                                        @endif
                                    </span>
                                    <div class="media-body pt-2">
                                        <h4 class="product-title font-size-base mb-2">
                                            <span>{{ $emp->nombre_comercial }}</span>
                                        </h4>
                                        <div class="font-size-sm mb-2">
                                            {{ $emp->ModeloRubro->nombre }}
                                        </div>
                                        <hr class="mr-2">
                                        <div class="tab-pane fade active show mt-2" role="tabpanel">
                                            @foreach ($emp->ModeloRedesSociales as $redes)
                                                <a class="social-btn sb-round sb-outline {{ $redes->color }} mb-2" href="{{ $redes->url }}" target="_blank">
                                                    <i class="{{ $redes->icono }}"></i>
                                                </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <hr class="d-sm-none">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Otras Tiendas --}}
<div id="ResenasTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
    <div class="modal-dialog tamaño" role="document">
        <div class="modal-content">
            <div class="modal-header-lateral bg-dark">
                <h5 class="modal-title" style="color: white;">Visitar Otras Tiendas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body lateral bg-faded-success">
                <div class="col-12 mb-2 p-2">
                    <div class="card">
                        <div class="card-header" id="RedesHeading">
                            @php
                                $total_star = 0;
                                $count_star = 0;
                                $promedio_star = 0;
                            @endphp
                            @foreach ($empresa->ModeloValoracionEmpresas as $star)
                                @php
                                    $total_star += $star->estrellas;
                                    $count_star ++;
                                    $promedio_star = ($total_star/$count_star);
                                @endphp
                            @endforeach
                            <div class="star-rating mr-2 py-2">



                                @for ($e = 0; $e < 5; $e++)
                                    @if ($e < floor($promedio_star))
                                        <i class="sr-star fas fa-star active" style="font-size: 1.8rem;"></i>

                                    @elseif($e == floor($promedio_star))

                                        @if (is_float($promedio_star))
                                            @php
                                                $star_array = explode(".", number_format((float)$promedio_star, 2, '.', ''));
                                            @endphp

                                            @if (intval($star_array[1]) >= 50 )
                                                <i class="sr-star fas fa-star-half-alt active" style="font-size: 1.8rem;"></i>
                                            @else
                                                  <i class="sr-star fas fa-star" style="font-size: 1.8rem;"></i>
                                            @endif
                                        @else
                                            <i class="sr-star fas fa-star" style="font-size: 1.8rem;"></i>
                                        @endif
                                    @else
                                        <i class="sr-star fas fa-star" style="font-size: 1.8rem;"></i>
                                    @endif
                                @endfor

                            </div>
                            <br>
                            <span class="d-inline-block align-middle">{{ $promedio_star }} Calificación general</span>
                        </div>
                        <div class="card-body" style="overflow-y: auto;" >
                            <div class="widget widget-cart px-3 pt-2 pb-3">
                                <div style="height: 30rem;" data-simplebar data-simplebar-auto-hide="false">
                                    @foreach ($empresa->ModeloValoracionEmpresas as $valoracion)
                                    <div class="widget pt-2 pb-2 border-top border-bottom">
                                        <div class="media align-items-center">

                                            <div class="img-thumbnail d-block mr-2 rounded-circle position-relative" style="width: 3.3rem;">
                                                @if ($valoracion->ModeloPersona->avatar)
                                                    <img class="rounded-circle" src="/img/{{ $valoracion->ModeloPersona->avatar }}" alt="Cliente">
                                                @else
                                                    <img class="rounded-circle" src="{{ asset('img/default_img.png') }}" alt="Cliente">
                                                @endif
                                            </div>
                                            <div class="media-body">
                                                <h6 class="widget-product-title">
                                                    <span>{{ $valoracion->ModeloPersona->nombres . ' ' . $valoracion->ModeloPersona->apellidos }}</span>
                                                </h6>
                                                <div class="star-rating pt-2 mb-2">

                                                    @for ($i = 0; $i < 5; $i++)
                                                        @if ($i < $valoracion->estrellas)
                                                            <i class="sr-star fas fa-star active"></i>
                                                        @else
                                                            <i class="sr-star fas fa-star"></i>

                                                        @endif
                                                    @endfor

                                                </div>
                                                <br>
                                                <small>
                                                    {{ $valoracion->resena }}
                                                </small>
                                            </div>

                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Otras Tiendas --}}
<div id="InformacionTienda" class="modal fade modal-left" tabindex="-1" role="dialog">
    <div class="modal-dialog tamaño" role="document">
        <div class="modal-content">
            <div class="modal-header-lateral bg-dark">
                <h5 class="modal-title" style="color: white;">Información de la Tienda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: white;">&times;</span>
                </button>
            </div>
            <div class="modal-body lateral bg-faded-success">
                <div class="col-12 px-2 mb-4">
                    <!-- Accordion made of cards -->
                    <div class="accordion" id="accordion">
                        <div class="card">
                            <div class="card-header" id="InfoHeading">
                                <h3 class="accordion-heading">
                                    <a href="#Informacion" role="button" data-toggle="collapse" aria-expanded="true"
                                        aria-controls="Informacion">
                                        Información General
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse show" id="Informacion" aria-labelledby="InfoHeading"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="list-unstyled mb-0">
                                                <li class="media pt-1 pb-2 border-bottom">
                                                    <i class="fas fa-id-card font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Ruc</span>
                                                        <span
                                                            class="d-block text-heading font-size-sm">{{ $empresa->ruc }}</span>
                                                    </div>
                                                </li>
                                                <li class="media pt-1 pb-2 border-bottom">
                                                    <i class="fas fa-signature font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Razón Social</span>
                                                        <span class="d-block text-heading font-size-sm">
                                                            @if ($empresa->razon_social)
                                                            {{ $empresa->razon_social }}
                                                            @else
                                                            ----------
                                                            @endif
                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="media pt-1 pb-2 border-bottom">
                                                    <i class="fas fa-signature font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Nombre Comercial</span>
                                                        <span class="d-block text-heading font-size-sm">

                                                            @if ($empresa->nombre_comercial)
                                                            {{ $empresa->nombre_comercial }}
                                                            @else
                                                            ----------
                                                            @endif

                                                        </span>
                                                    </div>
                                                </li>
                                                <li class="media pt-1 pb-2 border-bottom">
                                                    <i class="fas fa-user-tie font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Representante Legal</span>
                                                        <span class="d-block text-heading font-size-sm">


                                                            @if ($empresa->representante_legal)
                                                            {{ $empresa->representante_legal }}
                                                            @else
                                                            ----------
                                                            @endif

                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="ContactoHeading">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#InformacionContacto" role="button"
                                        data-toggle="collapse" aria-expanded="true" aria-controls="InformacionContacto">
                                        Información de Contacto
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="InformacionContacto" aria-labelledby="ContactoHeading"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <ul class="list-unstyled mb-0">
                                                @if (count($empresa->ModeloContactos) > 0)
                                                <li class="media pt-1 pb-2 border-bottom">
                                                    <i class="fas fa-headset font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-primary">Número de
                                                            Teléfono</span>

                                                        @foreach ($empresa->ModeloContactos as $telefono)
                                                            @if ($telefono->tipo == 0)
                                                                <a href="tel:+{{ $telefono->nombre }}" class="d-block text-heading font-size-sm">{{ $telefono->nombre }}</a>
                                                            @endif
                                                        @endforeach


                                                    </div>
                                                </li>
                                                @endif

                                                <li class="media pt-2m pb-3 border-bottom">
                                                    <i
                                                        class="fas fa-map-marker-alt font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-primary">Dirección</span>
                                                        <span class="d-block text-heading font-size-sm">
                                                            @if ($empresa->direccion)
                                                            {{ $empresa->direccion }}
                                                            @else
                                                            ----------
                                                            @endif
                                                        </span>
                                                    </div>
                                                </li>

                                                @if (count($empresa->ModeloContactos) > 0)

                                                <li class="media pt-1 pb-2 border-bottom">
                                                    <i
                                                        class="fas fa-envelope-open-text font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-primary">Correo
                                                            Electrónico</span>

                                                        @foreach ($empresa->ModeloContactos as $correo)
                                                            @if ($correo->tipo == 1)
                                                                <a href="mailto:{{ $correo->nombre }}" data-toggle="tooltip"
                                                                    data-placement="right" title=""
                                                                    data-original-title="{{ $correo->area }}"
                                                                    class="d-block text-heading font-size-sm">
                                                                    {{ $correo->nombre }}
                                                                </a>
                                                            @endif
                                                        @endforeach

                                                    </div>
                                                </li>
                                                @endif

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="HorarioHeading">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#Horarios" role="button" data-toggle="collapse"
                                        aria-expanded="true" aria-controls="Horarios">
                                        Horarios de Atención
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="Horarios" aria-labelledby="HorarioHeading"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <div class="table-responsive mt-2">
                                        <table class="table table-hover bg-light font-size-sm border-bottom">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle">Días</th>
                                                    <th class="align-middle">Turno</th>
                                                    <th class="align-middle">Entrada</th>
                                                    <th class="align-middle">Salida</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($empresa->ModeloHorarios as $horario)
                                                <tr>
                                                    <td class="align-middle">{{ $horario->dia }}</td>
                                                    <td class="align-middle">
                                                        @if ($horario->turnos == '1')
                                                        Un solo turno
                                                        @else
                                                        Mañana y Tarde
                                                        @endif
                                                    </td>
                                                    <td class="align-middle">
                                                        @if ($horario->turnos == '1')
                                                        {{ $horario->hora_abre_am }}
                                                        @else
                                                        {{ $horario->hora_abre_am . ' - ' . $horario->hora_cierre_am }}
                                                        @endif
                                                    </td>
                                                    <td class="align-middle">
                                                        @if ($horario->turnos == '1')
                                                        {{ $horario->hora_cierre_pm }}
                                                        @else
                                                        {{ $horario->hora_abre_pm . ' - ' . $horario->hora_cierre_pm }}
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="MetodoEnvioHeading">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#MetodoEnvio" role="button" data-toggle="collapse"
                                        aria-expanded="true" aria-controls="MetodoEnvio">
                                        Métodos de Envío
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="MetodoEnvio" aria-labelledby="MetodoEnvioHeading"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <div class="table-responsive mt-2">
                                        <table class="table table-hover bg-light font-size-sm border-bottom">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle">Método</th>
                                                    <th class="align-middle">Llegada</th>
                                                    <th class="align-middle">Costo</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($empresa->ModeloDetalleMetodoEnvios as $detalle_envio)
                                                <tr>
                                                    <td class="align-middle">
                                                        <div class="media align-items-center">
                                                            <div class="media-body pl-3">
                                                                <span
                                                                    class="text-dark font-weight-medium">{{ $detalle_envio->ModeloMetodoEnvio->nombre }}</span>
                                                                <br>
                                                                <span
                                                                    class="text-muted">{{ $detalle_envio->descripcion }}</span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="align-middle">{{ $detalle_envio->llegada }}</td>
                                                    <td class="align-middle"> S/
                                                        {{ number_format((float)$detalle_envio->precio, 2, '.', '') }}
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="MetodoPagoHeading">
                                <h3 class="accordion-heading">
                                    <a class="collapsed" href="#MetodoPago" role="button" data-toggle="collapse"
                                        aria-expanded="true" aria-controls="MetodoPago">
                                        Métodos de Pago
                                        <span class="accordion-indicator"></span>
                                    </a>
                                </h3>
                            </div>
                            <div class="collapse" id="MetodoPago" aria-labelledby="MetodoPagoHeading"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <div class="table-responsive mt-2">
                                        <table class="table table-hover bg-light font-size-sm border-bottom">
                                            <thead>
                                                <tr>
                                                    <th class="align-middle">Método</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                @foreach ($empresa->ModeloDetalleMetodoPagos as $detalle_pagos)
                                                <tr>
                                                    <td class="align-middle">
                                                        <div class="media align-items-center">
                                                            <div class="media-body pl-3">
                                                                <span
                                                                    class="text-dark font-weight-medium">{{ $detalle_pagos->ModeloMetodoPago->nombre }}</span>
                                                                <br>
                                                                <span
                                                                    class="text-muted">{{ $detalle_pagos->descripcion }}
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Promociones --}}
@if (count($promociones) > 0)
<div class="modal fade" id="Promociones" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Descuentos y Promociones</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <spanaria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body font-size-sm">
                <section class="cea-carousel">
                    <div class="cea-carousel-inner" data-carousel-options="{&quot;items&quot;: 1, &quot;mode&quot;: &quot;gallery&quot;, &quot;nav&quot;: false, &quot;responsive&quot;: {&quot;0&quot;: {&quot;nav&quot;: true, &quot;controls&quot;: false}, &quot;576&quot;: {&quot;nav&quot;: false, &quot;controls&quot;: true}}}">
                        @foreach ($promociones as $prom)
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="d-sm-flex justify-content-between align-items-center bg-secondary overflow-hidden rounded-lg">
                                            <div class="py-2 my-2 my-md-0 py-md-2 px-2 ml-md-2 text-center text-sm-left">
                                                <span class="badge badge-danger badge-shadow mb-3">Usa el código: {{ $prom->codigo }}</span>
                                                <h5 class="mb-4">{{ $prom->titulo }}</h5>
                                                <h6 class="font-size-lg font-weight-light mb-2" id="view_descripcion">{{ $prom->descripcion }}</h6>
                                                <br><br>
                                                <span class="font-size-ms text-muted">
                                                    @if ($prom->tipo == 0)
                                                        <p><strong>S/  {{ $prom->porcentaje }}</strong> de Descuento</p>
                                                    @else
                                                        <p><strong>{{ $prom->porcentaje }}%</strong> de Descuento</p>
                                                    @endif
                                                </span>
                                                <span class="align-center font-size-ms text-muted" style="font-size: 9px !important;">
                                                    Promoción Válida Hasta: <strong id="view_fecha_fin">{{ $prom->fecha_fin }}</strong>
                                                </span>
                                            </div>
                                            <img class="d-block ml-auto" src="{{ asset('img/default-ads.png') }}" alt="Promocion">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endif

@endsection

@section('js')

    <script src="{{ asset('/ajax_web/ajaxProductoEmpresa.js') }}"></script>

@endsection
