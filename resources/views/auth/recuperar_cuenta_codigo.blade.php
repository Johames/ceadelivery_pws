@extends('principal')

@section('title_header')
Recuperar mi cuenta
@endsection

@section('carroucel')

<!-- Rubros -->
<section class="container my-5 pt-5">

    



   
    
    <div class="row justify-content-center">
        <div class="col-md-8">


            <div class="card border-0 box-shadow my-5">
                <div class="card-body p-5">
                    <h2 class="text-center py-2">Recupera tu cuenta</h2>
                    <p class="text-center m-1">Comprueba si recibiste un correo electrónico con un código de 6 dígitos</p>
                    <p class="text-center mb-4">
                        <small>Enviamos el código a: <b>{{ $email }}</b></small>
                    </p>

                    <form class="form" method="POST" action="/recover/codigo/validate" autocomplete="off">
                        @csrf
                        <input type="hidden" name="email" id="email" value="{{ $email }} ">
                        <div class="form-inline justify-content-center mt-2 mb-4">
                            
                            <input class="form-control form-control-lg mx-1 code" type="text" name="code_1" id="code_1" maxlength = "1" placeholder="#" style="width: 56px">
                            <input class="form-control form-control-lg mx-1 code" type="text" name="code_2" id="code_2" maxlength = "1" placeholder="#" style="width: 56px">
                            <input class="form-control form-control-lg mx-1 code" type="text" name="code_3" id="code_3" maxlength = "1" placeholder="#" style="width: 56px">
                            <input class="form-control form-control-lg mx-1 code" type="text" name="code_4" id="code_4" maxlength = "1" placeholder="#" style="width: 56px">
                            <input class="form-control form-control-lg mx-1 code" type="text" name="code_5" id="code_5" maxlength = "1" placeholder="#" style="width: 56px">
                            <input class="form-control form-control-lg mx-1 code" type="text" name="code_6" id="code_6" maxlength = "1" placeholder="#" style="width: 56px">
                        </div>
                        
                        <div id="contenedor_de_errores_recover">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul class="m-0">
                                    @foreach ($errors->all() as $error)
                                        <li><small>{{ $error }}</small></li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>
        
                        <center>
                            {{-- <button class="btn btn-outline-warning btn-shadow" type="button">Limpiar</button> --}}
                            <button class="btn btn-primary btn-shadow" type="submit">Continuar</button>
                        </center>
                    </form>
                    
                </div>
            </div>

            
        </div>
    </div>
</section>

@endsection

@section('content')

@endsection

@section('modals')

@endsection

@section('js')
<script>

    function code_recover() {

        $(".code").bind('paste', function(e) {
           
            var pastedData = e.originalEvent.clipboardData.getData('text');

            for (var i = 0; i < pastedData.length; i++) {
        
               $("#code_" + (i + 1)).val(pastedData.charAt(i).toUpperCase());
            }
            
        });
     
        $(".code").on('keyup', function () {
            if (this.value.length == this.maxLength) {
                $(this).next('.code').focus();
            }
        });

        $(".code").on('change', function () {
            
                $(this).val($(this).val().toUpperCase());
            
        });
        
    }

    code_recover();

</script>
@endsection
