@extends('principal')

@section('title_header')
Recuperar mi cuenta
@endsection

@section('carroucel')

<!-- Rubros -->
<section class="container my-5 pt-5">
    <h2 class="text-center py-2 mt-5">Elije tu nueva contraseña</h2>
    <p  class="text-center py-2"> Crea una contraseña nueva de ocho caracteres como mínimo.<br> <small class="text-info">Una contraseña segura tiene una combinación de letras, números y signos de puntuación</small></p>
    <div class="row justify-content-center">
        <div class="col-md-5">
            <form class="form" method="POST" action="/recover/reset/validate" autocomplete="off">
                @csrf
                <input type="hidden" name="email" value="{{ $email }}">
                <input type="hidden" name="codigo_recuperacion" value="{{ $codigo_recuperacion }}">

                <div class="form-group">
                    <div class="password-toggle">
                        <input class="form-control" type="password" name="password" id="password" placeholder="Ingrese su nueva contraseña...">
                        <label class="password-toggle-btn">
                            <input class="custom-control-input" type="checkbox">
                            <i class="fas fa-eye password-toggle-indicator" aria-hidden="true"></i>
                            <span class="sr-only">Ver contraseña

                            </span>
                        </label>
                    </div>
                </div>

                <div id="contenedor_de_errores_recover">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="m-0">
                            @foreach ($errors->all() as $error)
                                <li><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>

                <button class="btn btn-primary btn-block btn-shadow" type="submit">Recuperar</button>
                
            </form>
        </div>
    </div>
</section>

@endsection

@section('content')

@endsection

@section('modals')

@endsection

@section('js')

@endsection
