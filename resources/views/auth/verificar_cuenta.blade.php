@extends('principal')

@section('title_header')
Activar mi cuenta
@endsection

@section('carroucel')

<!-- Rubros -->
<section class="container my-5 pt-5">
    {{-- <h2 class="text-center py-2 mt-5">CONFIRMA Y ACTIVA TU CUENTA</h2> --}}
    {{-- <h2 class="text-center py-2 mt-5">Confirma y activa tu cuenta</h2>
    <h2><span class="font-weight-light">Even More</span> Mobile Friendly <span class="font-weight-light">Interface</span></h2>
    <p  class="text-center py-2 h text-muted">
        Bienvenido:  <b>{{ Auth::user()->ModeloPersona->nombres . ' ' .  Auth::user()->ModeloPersona->apellidos }}</b>
        ahora solo queda activar tu cuenta
        <br>
        Te hemos enviado un mensaje a tu correo: <b>{{ Auth::user()->email }}</b>
    </p> --}}
    <div class="row justify-content-center my-5">
        <div class="col-md-10 ">

            <div class="card border-0 box-shadow">
                <div class="card-body p-5">

                    <h2 class="" style="">
                        <center>
                            <span class="font-weight-light">Bienvenido: </span>
                            <span style="letter-spacing: 2px;">{{ Auth::user()->ModeloPersona->nombres . ' ' .  Auth::user()->ModeloPersona->apellidos }}</span>

                        </center>
                    </h2>
                    <h5 class="mx-5 my-2 text-center">
                        <span class="font-weight-light">Revise su bandeja de entrada de</span>

                        {{ Auth::user()->email }}
                        <span class="font-weight-light">para activar su cuenta.</span>
                    </h5>
                    <p class="mx-5 my text-center">
                        <span class="font-weight-light text-warning">Si no lo ve, verifique la carpeta <b>SPAM</b>,
                        ya que a veces nuestros correos electrónicos de activación de cuentas se atascan allí.</span>
                    </p>
                    <center>
                        <img src="https://beefree.io/wp-content/themes/bee2017/img/beepro-signup/bee-plane.gif" width="120">


                    </center>
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form" method="POST" autocomplete="off">
                                @csrf
                                <input type="hidden" name="email" id="email" value="{{ Auth::user()->email }}">
                                <div class="form-inline justify-content-center mt-2 mb-4">

                                    {{-- <a href="#" id="btn_paste" class="btn btn-outline-info btn-icon btn" data-toggle="tooltip" data-placement="bottom" title="Pegar código">
                                        <i class="fas fa-paste"></i>
                                    </a> --}}
                                    <input class="form-control form-control-lg mx-1 code" data-toggle="tooltip" data-placement="bottom" title="CTRL + V para pegar aquí codigo" type="text" name="code_1" id="code_1" maxlength = "1" placeholder="#" style="width: 56px">
                                    <input class="form-control form-control-lg mx-1 code" data-toggle="tooltip" data-placement="bottom" title="CTRL + V para pegar aquí codigo" type="text" name="code_2" id="code_2" maxlength = "1" placeholder="#" style="width: 56px">
                                    <input class="form-control form-control-lg mx-1 code" data-toggle="tooltip" data-placement="bottom" title="CTRL + V para pegar aquí codigo" type="text" name="code_3" id="code_3" maxlength = "1" placeholder="#" style="width: 56px">
                                    <input class="form-control form-control-lg mx-1 code" data-toggle="tooltip" data-placement="bottom" title="CTRL + V para pegar aquí codigo" type="text" name="code_4" id="code_4" maxlength = "1" placeholder="#" style="width: 56px">
                                    <input class="form-control form-control-lg mx-1 code" data-toggle="tooltip" data-placement="bottom" title="CTRL + V para pegar aquí codigo" type="text" name="code_5" id="code_5" maxlength = "1" placeholder="#" style="width: 56px">
                                    <input class="form-control form-control-lg mx-1 code" data-toggle="tooltip" data-placement="bottom" title="CTRL + V para pegar aquí codigo" type="text" name="code_6" id="code_6" maxlength = "1" placeholder="#" style="width: 56px">

                                </div>

                                <div id="contenedor_de_errores_recover">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul class="m-0">
                                            @foreach ($errors->all() as $error)
                                                <li><small>{{ $error }}</small></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>

                                <center>
                                    {{-- <button class="btn btn-outline-warning btn-shadow" type="button">Limpiar</button> --}}
                                    <button class="btn btn-accent btn-shadow" type="button" onclick="reenviar_confirmacion()">Volver a enviar correo</button>
                                    <button class="btn btn-primary btn-shadow" type="button" id="btn_activar_cuenta">Activar mi cuenta</button>
                                </center>
                            </form>
                        </div>
                    </div>
                </div>
              </div>

        </div>
    </div>
</section>

@endsection

@section('content')

@endsection

@section('modals')

@endsection

@section('js')

<script>

    $('[data-toggle="tooltip"]').tooltip();

    $("#btn_activar_cuenta").on("click", function (e) {
        activar_cuenta(e);
    });


    function code_recover() {

        $(".code").bind('paste', function(e) {

            var pastedData = e.originalEvent.clipboardData.getData('text');

            for (var i = 0; i < pastedData.length; i++) {

               $("#code_" + (i + 1)).val(pastedData.charAt(i).toUpperCase());
            }

        });

        $(".code").on('keyup', function () {
            if (this.value.length == this.maxLength) {
                $(this).next('.code').focus();
            }
        });

        $(".code").on('change', function () {

                $(this).val($(this).val().toUpperCase());

        });

    }

    function activar_cuenta(e) {

        var cargando = Swal.fire({
                title: '¡Enviando!',
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });

        e.preventDefault();

        var _token = $("input[name='_token']").val();

        var email = $("#email").val();

        var c1 = $("#code_1").val(); var c2 = $("#code_2").val(); var c3 = $("#code_3").val();
        var c4 = $("#code_4").val(); var c5 = $("#code_5").val(); var c6 = $("#code_6").val();

        var codigo = c1 + c2 + c3 + c4 + c5 + c6;

        console.log(codigo);

        $.ajax({
            url: '/verificar/comprobar',
            type: "POST",
            data: { _token: _token, codigo: codigo, email: email},
            dataType: "json",
            success: function (datos) {

                cargando.close();

                if (datos.status) {
                    sw_success(datos.message);

                    window.history.back();

                } else {
                    sw_error(datos.message);
                }

            },
            error: function (jqXhr) {

                var errors = jqXhr.responseJSON;

                console.log(errors);

                $.each(errors.errors, function (key, value) {

                    console.log(key);

                    if (key === 'codigo') {
                        sw_error('Ingrese un código');
                    }
                })


                // sw_error("Pongase en contacto con info@ceamarket.com");
                // setTimeout('recargar()',2000);

            }
        });

    }

    function reenviar_confirmacion() {

        var cargando = Swal.fire({
                title: '¡Enviando!',
                allowOutsideClick: false,
                onBeforeOpen: () => {
                    Swal.showLoading()
                },
            });

        $.get("/verificar/reenviar", function (data, textStatus, jqXHR) {
            data = JSON.parse(data);

            cargando.close();

            console.log(data);

            if (data.status) {

                Swal.fire({ title: "Exito", text: data.message, timer: 4000,  icon: "success" });

            } else {
               Swal.fire({ title: "Error", text: data.message, timer: 4000,  icon: "error" });
            }



        });
    }

    function recargar() {
        location.reload()
    }

    code_recover();

</script>
@endsection
