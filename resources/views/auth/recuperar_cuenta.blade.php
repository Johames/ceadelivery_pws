@extends('principal')

@section('title_header')
Recuperar mi cuenta
@endsection

@section('carroucel')

<!-- Rubros -->
<section class="container my-5 pt-5">
    <h2 class="text-center py-2 mt-5">Recupera tu cuenta</h2>
    <p  class="text-center py-2">Ingresa tu correo electrónico para buscar tu cuenta.</p>
    <div class="row justify-content-center">
        <div class="col-md-5">
            <form class="form" method="POST" action="/recover" autocomplete="off">
                @csrf
                <div class="form-group">
                    
                    <input class="form-control" type="email" name="email" id="email" placeholder="ejemplo@gmail.com">
                </div>
                

                <div id="contenedor_de_errores_recover">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="m-0">
                            @foreach ($errors->all() as $error)
                                <li><small>{{ $error }}</small></li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>

                <button class="btn btn-primary btn-block btn-shadow" type="submit">Recuperar</button>
                
            </form>
        </div>
    </div>
</section>

@endsection

@section('content')

@endsection

@section('modals')

@endsection

@section('js')
    {{-- <script src="{{ asset('ajax_web/recover.js') }}"></script> --}}
@endsection
