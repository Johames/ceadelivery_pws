@extends('principal3')

@section('title_header')
    Página en Mantenimiento
@endsection


@section('content')
    <div class="container-fluid py-5 mb-lg-3 mt-5">
        <div class="row justify-content-center pt-lg-4 text-center mt-5">
            <div class="col-lg-5 col-md-7 col-sm-9">
                <h1 class="display-404">T_T</h1>
                <h2 class="h3 mb-4">
                    Nuestra página se encuentra en mantenimiento.
                </h2>
                <p class="font-size-md mb-4">
                    <u>
                        Por favor espere, en breve estaremos en linea
                    </u>
                </p>
            </div>
        </div>
    </div>
@endsection

