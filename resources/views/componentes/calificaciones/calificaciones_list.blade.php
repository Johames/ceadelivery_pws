@if (count($valor_emp) > 0)

    @foreach ($valor_emp as $valor)

        <div class="card col-12 mb-3">
            <div class="product-review pt-2 pr-3 pl-3">
                <div class="d-flex mb-3">
                    <div class="media media-ie-fix align-items-center mr-4 pr-2">
                        @if($valor->ModeloEmpresa->icono)
                            <img class="rounded-circle" width="50" src="https://tiendas.ceamarket.com/img/{{ $valor->ModeloEmpresa->icono }}" alt="empresa"/>
                        @else
                            <img class="rounded-circle" width="50" src="{{ asset("/img/default-store.png") }}" alt="empresa"/>
                        @endif
                        <div class="media-body pl-3">
                            <h6 class="font-size-sm mb-0">
                                @if ($valor->ModeloEmpresa->razon_social)
                                    {{ $valor->ModeloEmpresa->razon_social }}
                                @else
                                    {{ $valor->ModeloEmpresa->nombre_comercial }}
                                @endif
                            </h6>
                            <span class="font-size-ms text-muted">{{ $valor->created_at }}</span>
                        </div>
                    </div>
                    <div>
                        @php
                            $promedio_star_perfil = $valor->estrellas;
                        @endphp
                        <div class="star-rating">
                            @for ($e = 0; $e < 5; $e++)
                                @if ($e < floor($promedio_star_perfil))
                                    <i class="sr-star fas fa-star active tamano_estrella"></i>

                                @elseif($e == floor($promedio_star_perfil))

                                    @if (is_float($promedio_star_perfil))
                                        @php
                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                        @endphp

                                        @if (intval($star_array_perfil[1]) >= 50 )
                                            <i class="sr-star fas fa-star-half-alt active tamano_estrella"></i>
                                        @else
                                            <i class="sr-star fas fa-star tamano_estrella"></i>
                                        @endif
                                    @else
                                        <i class="sr-star fas fa-star tamano_estrella"></i>
                                    @endif
                                @else
                                    <i class="sr-star fas fa-star tamano_estrella"></i>
                                @endif
                            @endfor
                            <div class="mt-1 pt-2 font-size-ms text-muted">{{ $valor->estrellas }} Estrellas</div>
                        </div>
                    </div>
                </div>
                <p class="font-size-md mb-2">
                    {{ $valor->resena }}
                </p>
                {{-- <div class="text-nowrap pb-2">
                    <button type="button" class="btn btn-outline-danger btn-sm btn-icon pt-1 pb-1 pr-2 pl-2 mr-2">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-outline-warning btn-sm btn-icon pt-1 pb-1 pr-2 pl-2 mr-2">
                        <i class="fas fa-pen-alt"></i>
                    </button>
                </div> --}}
            </div>
        </div>

    @endforeach

@elseif(count($valor_prod) > 0)

    @foreach ($valor_prod as $valor)

        <div class="card col-12 mb-3">
            <div class="product-review pt-2 pr-3 pl-3">
                <div class="d-flex mb-3">
                    <div class="media media-ie-fix align-items-center mr-4 pr-2">
                        @if ($valor->ModeloDetalleProducto->PrimeraImagen)
                            <img class="rounded-circle" width="50" src="https://tiendas.ceamarket.com/img/{{  $valor->ModeloDetalleProducto->PrimeraImagen->ruta }}" alt="Producto"/>
                        @else
                            <img class="rounded-circle" width="50" src="{{ asset("/img/default-product.png") }}" alt="Producto"/>
                        @endif
                        <div class="media-body pl-3">
                            <h6 class="font-size-sm mb-0">{{ $valor->ModeloDetalleProducto->ModeloProducto->nombre }}</h6>
                            <span class="font-size-ms text-muted">{{ $valor->created_at }}</span>
                        </div>
                    </div>
                    <div>
                        @php
                            $promedio_star_perfil = $valor->estrellas;
                        @endphp
                        <div class="star-rating">
                            @for ($e = 0; $e < 5; $e++)
                                @if ($e < floor($promedio_star_perfil))
                                    <i class="sr-star fas fa-star active tamano_estrella"></i>

                                @elseif($e == floor($promedio_star_perfil))

                                    @if (is_float($promedio_star_perfil))
                                        @php
                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                        @endphp

                                        @if (intval($star_array_perfil[1]) >= 50 )
                                            <i class="sr-star fas fa-star-half-alt active tamano_estrella"></i>
                                        @else
                                            <i class="sr-star fas fa-star tamano_estrella"></i>
                                        @endif
                                    @else
                                        <i class="sr-star fas fa-star tamano_estrella"></i>
                                    @endif
                                @else
                                    <i class="sr-star fas fa-star tamano_estrella"></i>
                                @endif
                            @endfor
                            <div class="mt-1 pt-2 font-size-ms text-muted">{{ $valor->estrellas }} Estrellas</div>
                        </div>
                    </div>
                </div>
                <p class="font-size-md mb-2">
                    {{ $valor->resena }}
                </p>
                {{-- <div class="text-nowrap pb-2">
                    <button type="button" class="btn btn-outline-danger btn-sm btn-icon pt-1 pb-1 pr-2 pl-2 mr-2">
                        <i class="fas fa-trash"></i>
                    </button>
                    <button type="button" class="btn btn-outline-warning btn-sm btn-icon pt-1 pb-1 pr-2 pl-2 mr-2">
                        <i class="fas fa-pen-alt"></i>
                    </button>
                </div> --}}
            </div>
        </div>

    @endforeach

@elseif(count($valor_deliv) > 0)

    @foreach ($valor_deliv as $valor)

        <div class="card col-12 mb-3">
            <div class="product-review pt-2 pr-3 pl-3">
                <div class="d-flex mb-3">
                    <div class="media media-ie-fix align-items-center mr-4 pr-2">
                        @if($valor->ModeloDeliverista->foto)
                            <img class="rounded-circle" width="50" src="https://api.ceamarket.com/img/{{ $valor->ModeloDeliverista->foto }}" alt="deliverista"/>
                        @else
                            <img class="rounded-circle" width="50" src="{{ asset("/img/default_img.png") }}" alt="deliverista"/>
                        @endif
                        <div class="media-body pl-3">
                            <h6 class="font-size-sm mb-0">
                                {{ $valor->ModeloDeliverista->nombres . " " . $valor->ModeloDeliverista->apellidos }}
                            </h6>
                            <span class="font-size-ms text-muted">{{ $valor->created_at }}</span>
                        </div>
                    </div>
                    <div>
                        @php
                            $promedio_star_perfil = $valor->estrellas;
                        @endphp
                        <div class="star-rating">
                            @for ($e = 0; $e < 5; $e++)
                                @if ($e < floor($promedio_star_perfil))
                                    <i class="sr-star fas fa-star active tamano_estrella"></i>

                                @elseif($e == floor($promedio_star_perfil))

                                    @if (is_float($promedio_star_perfil))
                                        @php
                                            $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                        @endphp

                                        @if (intval($star_array_perfil[1]) >= 50 )
                                            <i class="sr-star fas fa-star-half-alt active tamano_estrella"></i>
                                        @else
                                            <i class="sr-star fas fa-star tamano_estrella"></i>
                                        @endif
                                    @else
                                        <i class="sr-star fas fa-star tamano_estrella"></i>
                                    @endif
                                @else
                                    <i class="sr-star fas fa-star tamano_estrella"></i>
                                @endif
                            @endfor
                            <div class="mt-1 pt-2 font-size-ms text-muted">{{ $valor->estrellas }} Estrellas</div>
                        </div>
                    </div>
                </div>
                <p class="font-size-md mb-2">
                    {{ $valor->resena }}
                </p>
            </div>
        </div>

    @endforeach

@else

    <div class="card col-12 mb-3">
        <div class="product-review pt-2 pr-3 pl-3">
            <p class="font-size-md mb-2">
                No se encontraron calificaciones
            </p>
        </div>
    </div>

@endif

