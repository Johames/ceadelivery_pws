@if (count($redes) > 0)

    @foreach ($redes as $item)

        <a class="social-btn sb-round sb-outline {{ $item->color }} mb-2" href="{{ $item->url }}" target="_blank">
            <i class="{{ $item->icono }}"></i>
        </a>

    @endforeach

@endif

<br><br>

<a class="btn btn-outline-primary btn-sm mb-2" href="/ver/blogs" target="_blank">
    Visita nuestro Blog
</a>
