
<div class="modal-header-lateral bg-dark">
    <h4 class="widget-title text-white py-0">
        DETALLE DE LA NOTIFICACIÓN
    </h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" style="color: white;">&times;</span>
    </button>
</div>


<div class="modal-body lateral bg-faded-success">
    <div class="col-12 px-2 mb-4">

        <div class="card mt-3 mb-3" style="max-width: 540px;">
            <div class="row no-gutters">
                <div class="col-3">
                    <img src="{{ asset('/img/notificaciones-push.jpg') }}" class="rounded-left" alt="Card image" style="height: 110px; margin-top: 5px; margin-left: 5px; margin-bottom: 5px;"/>
                </div>
                <div class="col-9">
                    <div class="card-body">
                        <div class="" style="padding-bottom: 7px;">
                            <h3 class="font-size-lg mb-0">{{ $notificacion->titulo }}</h3>
                            <span class="font-size-ms text-muted">{{ $notificacion->created_at }}</span>
                         </div>
                        <p class="card-text font-size-sm text-muted">
                            {{ $notificacion->descripcion }}
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="accordion" id="accordion">
            <div class="card">
                @foreach ($detalle as $detail)
                <div class="card-header" id="DetPedNotifHeading">
                    <h3 class="accordion-heading">
                        <a class="collapsed" href="#DetPedNotificacion" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="DetPedNotificacion">
                            <p class="mb-0">{{ $detail->codigo }}</p>
                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse show" id="DetPedNotificacion" aria-labelledby="DetPedNotifHeading" data-parent="#accordion">
                    <div class="card-body">
                        <div class="col-12 px-2 mb-4">
                            <div class="col-12 px-0">

                                @if ($detail->motivo_rechazo != "")
                                    <div class="alert alert-danger alert-with-icon" role="alert">
                                        <div class="alert-icon-box">
                                            <i class="alert-icon fas fa-times-circle"></i>
                                        </div>
                                        @if ($detail->estado == 0)
                                            <span class="badge badge-warning px-5 p-2">Pedido Pendiente</span>
                                        @elseif ($detail->estado == 1)
                                            <span class="badge badge-success px-5 p-2">Pedido en Proceso</span>
                                        @elseif ($detail->estado == 2)
                                            <span class="badge badge-success px-5 p-2">Pedido Confirmado</span>
                                        @elseif ($detail->estado == 3)
                                            <span class="badge badge-success px-5 p-2">Su Pedido esta en Camino</span>
                                        @elseif ($detail->estado == 4)
                                            <span class="badge badge-primary px-5 p-2">Pedido Entregado</span>
                                        @elseif ($detail->estado == 5)
                                            <span class="badge badge-danger px-5 p-2">Pedido Rechazado</span>
                                        @endif
                                        <br><br>
                                        {{ $detail->motivo_rechazo }}
                                    </div>
                                @else
                                    <div class="mb-3">
                                        @if ($detail->estado == 0)
                                            <span class="badge badge-warning px-5 p-2">Pedido Pendiente</span>
                                        @elseif ($detail->estado == 1)
                                            <span class="badge badge-success px-5 p-2">Pedido en Proceso</span>
                                        @elseif ($detail->estado == 2)
                                            <span class="badge badge-success px-5 p-2">Pedido Confirmado</span>
                                        @elseif ($detail->estado == 3)
                                            <span class="badge badge-success px-5 p-2">Su Pedido esta en Camino</span>
                                        @elseif ($detail->estado == 4)
                                            <span class="badge badge-primary px-5 p-2">Pedido Entregado</span>
                                        @elseif ($detail->estado == 5)
                                            <span class="badge badge-danger px-5 p-2">Pedido Rechazado</span>
                                        @endif
                                    </div>
                                @endif

                                @if ($detail->estado > 3)
                                    <span class="badge badge-pill badge-primary p-2 px-4 font-size-sm mb-2" style="cursor: pointer;" onclick="mostrar_modal_valoracion('emp', {{ $detail->ModeloEmpresa->empresa_id }})">
                                        Calificar Empresa
                                    </span>
                                    @if ($detail->deliverista_id)
                                        <span class="badge badge-pill badge-info p-2 px-4 font-size-sm mb-2" style="cursor: pointer;" onclick="mostrar_modal_valoracion('deliv', {{ $detail->deliverista_id }})">
                                            Calificar Deliverista
                                        </span>
                                    @endif
                                @endif

                                <div class="card">
                                    <div class="card-img-top overflow-hidden"  style="background: url('https://tiendas.ceamarket.com/img/{{ $detail->ModeloEmpresa->baner }}') no-repeat center center; background-size: cover; height: 120px;">

                                    </div>
                                    <div class="card-body">
                                        <ul class="list-unstyled mb-0">
                                            <li class="media ">
                                                <i class="fas fa-user font-size-lg mt-2 mb-0 text-primary"></i>
                                                <div class="media-body pl-3">
                                                    <span class="font-size-ms text-muted">Nombre de la Tienda</span>
                                                    @if ($detail->ModeloEmpresa->razon_social)
                                                        <span class="d-block text-heading font-size-sm">
                                                            {{ $detail->ModeloEmpresa->razon_social }}
                                                        </span>
                                                    @else
                                                        <span class="d-block text-heading font-size-sm">
                                                            {{ $detail->ModeloEmpresa->nombre_comercial }}
                                                        </span>
                                                    @endif

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 bg-white mt-3">

                                <ul class="list-unstyled font-size-sm pb-2 pt-2 mt-2 border-top border-bottom">
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span class="mr-2">Subtotal:</span>
                                        @php
                                            $subtotal_array = explode(".", number_format((float)$detail->total_pagar, 2, '.', ''));
                                        @endphp
                                        <span class="text-right">
                                            S/ {{ $subtotal_array[0] }}.<small>{{ $subtotal_array[1] }}</small>
                                        </span>
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span class="mr-2">Delivery:</span>
                                        @php
                                            $delivery_array = explode(".", number_format((float)$detail->precio_envio, 2, '.', ''));
                                        @endphp
                                        <span class="text-right">
                                            S/ {{ $delivery_array[0] }}.<small>{{ $delivery_array[1] }}</small>
                                        </span>
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span class="mr-2">IGV:</span>
                                        @php
                                            $igv_array = explode(".", number_format((float)$detail->igv, 2, '.', ''));
                                        @endphp
                                        <span class="text-right">
                                            S/ {{ $igv_array[0] }}.<small>{{ $igv_array[1] }}</small>
                                        </span>
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span class="mr-2">Descuento:</span>
                                        @php
                                            $descuento_array = explode(".", number_format((float)$detail->descuento, 2, '.', ''));
                                        @endphp
                                        <span class="text-right">
                                            S/ {{ $descuento_array[0] }}.<small>{{ $descuento_array[1] }}</small>
                                        </span>
                                    </li>
                                </ul>
                                @php
                                    $total_array = explode(".", number_format((float)$detail->total_pagar - $detail->descuento + $detail->precio_envio, 2, '.', ''));
                                @endphp
                                <h3 class="font-weight-normal text-center mb-3">
                                    S/ {{ $total_array[0] }}.<small>{{ $total_array[1] }}</small>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>


