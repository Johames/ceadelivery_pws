@if (count($notificaciones) > 0)
    @foreach ($notificaciones as $not)
        <div class="media mb-4 pb-2 border-bottom" style="cursor: pointer;" onclick="mostrar_notificacion('{{ $not->notificacion_id }}')">
            <i class="img-thumbnail fas fa-bell fa-3x text-success mb-2 mr-2"></i>
            {{-- <img class="rounded-circle" width="50" src="path-to-picture" alt="Laura Willson"/> --}}
            <div class="media-body pl-3">
                @if ($not->visto == 0)
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <h6 class="font-size-md mb-0"><strong>{{ $not->titulo }}</strong></h6>
                        <a class="nav-link-style font-size-sm font-weight-medium" href="#"></a>
                    </div>
                    <p class="font-size-md mb-1"><strong>{{ $not->descripcion }}</strong></p>
                    <span class="font-size-ms text-muted">
                        <i class="fas fa-clock align-middle mr-2"></i>
                        <strong>{{ $not->created_at }}</strong>
                    </span>
                @else
                    <div class="d-flex justify-content-between align-items-center mb-2">
                        <h6 class="font-size-md mb-0">{{ $not->titulo }}</h6>
                        <a class="nav-link-style font-size-sm font-weight-medium" href="#">
                            <i class="fas fa-check-double mr-2"></i> Visto
                        </a>
                    </div>
                    <p class="font-size-md mb-1">{{ $not->descripcion }}</p>
                    <span class="font-size-ms text-muted">
                        <i class="fas fa-clock align-middle mr-2"></i>
                        {{ $not->created_at }}
                    </span>
                @endif
            </div>
        </div>
    @endforeach
@else
    <div class="media card_notificaciones card-light">
        <div class="media-body pl-3">
            <div class="d-flex justify-content-between align-items-center mb-2">
                <h6 class="font-size-md mb-0">No hay notificaciones nuevas</h6>
            </div>
        </div>
    </div>
@endif

