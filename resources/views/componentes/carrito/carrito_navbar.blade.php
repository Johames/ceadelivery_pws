@php
    $sub_total = 0;
    $total = 0;
@endphp

@if (count($pedidos) > 1)

    {{-- MÁS DE UN PEDIDO --}}
    {{-- Listar pedidos --}}
    <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="#modal_carrito" data-toggle="modal">
        <span class="navbar-tool-label bg-info">{{ count($pedidos) }}</span>
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
    </a>

    <a class="navbar-tool-text" href="#modal_carrito" data-toggle="modal">
        <small>Mi carrito</small> Ver

    </a>


@else

{{-- SOLO UN PEDIDO --}}
{{-- listar de productos --}}

    @foreach ($pedidos as $pedido_head)
        @php
            $count_productos = count($pedido_head->ModeloDetallePedidos);
            $sub_total_head = 0;
        @endphp

        @foreach ($pedido_head->ModeloDetallePedidos as $detalle_pedido_head)
            @php
            $sub_total_head += $detalle_pedido_head->precio * $detalle_pedido_head->cantidad;
            @endphp
        @endforeach
    @endforeach

    <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="#modal_carrito" data-toggle="modal">
        <span class="navbar-tool-label bg-primary">{{ $count_productos }}</span>
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
    </a>

    <a class="navbar-tool-text" href="#modal_carrito" data-toggle="modal">
        <small>Mi carrito</small> S/  {{ number_format((float) $sub_total_head, 2, '.', '') }}
    </a>

@endif
