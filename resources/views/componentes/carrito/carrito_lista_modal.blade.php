{{-- ******* LISTAR ******* --}}
@php
    $sub_total = 0;
    $total = 0;
@endphp

@if (count($pedidos) > 1)

    {{-- INICO  MUCHOS PEDIDOS --}}
    @foreach ($pedidos as $pedido)

        <div class="accordion" id="accordionExample">

            <!-- Card -->
            <div class="card mt-2">
                <div class="card-header" id="headingOne">
                    <h3 class="accordion-heading">
                        <a href="#collapse_{{ $pedido->pedido_id }}" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapse_{{ $pedido->pedido_id }}">

                            @php
                                $sub_total_prev = 0;
                            @endphp

                            @foreach ($pedido->ModeloDetallePedidos as $dp_prev)
                                @php
                                $sub_total_prev += $dp_prev->precio * $dp_prev->cantidad;
                                @endphp
                            @endforeach

                            <span class="badge badge-primary">
                                {{ count($pedido->ModeloDetallePedidos) }} productos
                            </span>
                            <span class="badge badge-accent">
                                PASO {{ $pedido->paso }}
                            </span>
                            @if ($pedido->paso > 3)
                                <span class="badge badge-info">
                                    Confirme su pedido por favor
                                </span>
                            @endif

                            <br>

                            <span class="font-size-md">
                                {{ ($pedido->ModeloEmpresa->nombre_comercial) }}
                            </span>
                            <br>

                            <span class="text-muted font-size-xs">
                                Sub total: S/ {{  number_format((float) $sub_total_prev, 2, '.', '') }}
                            </span>

                            <span class="accordion-indicator"></span>
                        </a>
                    </h3>
                </div>
                <div class="collapse " id="collapse_{{ $pedido->pedido_id }}" aria-labelledby="headingOne"
                    data-parent="#accordionExample">
                    <div class="card-body">
                        @php
                            $sub_total = 0;
                        @endphp

                        @foreach ($pedido->ModeloDetallePedidos as $detalle_pedido)

                            @php
                                $detalle_producto = $detalle_pedido->ModeloDetalleProducto;
                                $producto = $detalle_producto->ModeloProducto;
                                $producto_img = $detalle_producto->PrimeraImagen;

                                $class
                            @endphp

                            @if ($pedido->paso < 4)
                            <div class="widget-cart-item pb-2 border-bottom">
                            @else
                            <div class="widget-cart pb-2 border-bottom">
                            @endif
                                @if ($pedido->paso < 4)
                                    <button class="close text-danger" type="button" onclick="deleteProductDirecto({{ $detalle_pedido->detalle_id }})" aria-label="Eliminar">
                                        <i class="fas fa-trash fa-xs" aria-hidden="true"></i>
                                    </button>
                                @endif
                                <div class="media align-items-center">
                                    <p class="d-block mr-2">

                                        @if ($producto_img)
                                            <img width="64" src="https://tiendas.ceamarket.com/img/{{ $producto_img->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" />
                                        @else
                                            <img width="64" src="/img/default-product.png" alt="Product" />
                                        @endif

                                    </p>
                                    <div class="media-body">
                                        <h6 class="widget-product-title">
                                            <p>
                                                {{ ($producto->nombre) }}
                                            </p>
                                        </h6>
                                        <div class="widget-product-meta">

                                            @php
                                                $precio_array = explode(".", number_format((float)
                                                $detalle_pedido->precio, 2, '.', ''));
                                            @endphp

                                            <span class="text-accent">S/ {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small></span>

                                            <span class="text-muted">x {{ $detalle_pedido->cantidad }}</span>
                                            <span class="text-muted"> = S/ {{ number_format((float) $detalle_pedido->precio * $detalle_pedido->cantidad, 2, '.', '') }}</span>
                                            @php
                                                $sub_total += $detalle_pedido->precio * $detalle_pedido->cantidad;
                                            @endphp
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach



                        <div class="font-size-sm mr-2 py-2 text-center">
                            <span class="text-muted">Subtotal:</span>
                            @php
                                $sub_total = number_format((float) $sub_total, 2, '.', '');
                                $sub_array = explode(".", $sub_total);
                            @endphp

                            <span class="text-accent font-size-base ml-1">S/ {{ $sub_array[0] }}.<small>{{ $sub_array[1] }}</small></span>
                        </div>
                        <a class="btn btn-block btn-outline-primary btn-sm" href="/redireccionar/pedido/{{ $pedido->codigo }}">
                            Continuar Compra
                            <i class="fas fa-chevron-right ml-1 mr-n1"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>

    @endforeach
    {{-- FINAL --}}
@else

    {{-- INICO  DE UN SOLO PEDIDO --}}
    @foreach ($pedidos as $pedido)

        <div class="card m-2">
            <div class="card-header">
                <span class="badge badge-primary">
                    {{ count($pedido->ModeloDetallePedidos) }} productos
                </span>
                <span class="badge badge-accent">
                    PASO {{ $pedido->paso }}
                </span>
                @if ($pedido->paso > 3)
                    <span class="badge badge-info">
                        Confirme su pedido por favor
                    </span>
                @endif
                <br>

                <strong class="font-size-md">
                    {{ ($pedido->ModeloEmpresa->nombre_comercial) }}
                </strong>
                <br>
            </div>
            <div class="card-body p-2">
                @foreach ($pedido->ModeloDetallePedidos as $detalle_pedido)

                    @php
                        $detalle_producto = $detalle_pedido->ModeloDetalleProducto;
                        $producto = $detalle_producto->ModeloProducto;
                        $producto_img = $detalle_producto->PrimeraImagen;
                    @endphp

                    @if ($pedido->paso < 4)
                    <div class="widget-cart-item pb-2 border-bottom">
                    @else
                    <div class="widget-cart pb-2 border-bottom">
                    @endif
                        @if ($pedido->paso < 4)
                            <button class="close text-danger" onclick="deleteProductDirecto({{ $detalle_pedido->detalle_id }})" type="button" aria-label="Eliminar">
                                <i class="fas fa-trash fa-xs" aria-hidden="true"></i>
                            </button>
                        @endif
                        <div class="media align-items-center">
                            <p class="d-block mr-2">

                                @if ($producto_img)
                                    <img width="64" src="https://tiendas.ceamarket.com/img/{{ $producto_img->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" />
                                @else
                                    <img width="64" src="/img/default-product.png" alt="Product" />
                                @endif

                            </p>
                            <div class="media-body">
                                <h6 class="widget-product-title">
                                    <p>
                                        {{ ($producto->nombre) }}
                                    </p>

                                </h6>
                                <div class="widget-product-meta">

                                    @php
                                        $precio_array = explode(".", number_format((float) $detalle_pedido->precio, 2, '.', ''));
                                    @endphp

                                    <span class="text-accent mr-2">S/ {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small></span>

                                    <span class="text-muted mr-2">x {{ $detalle_pedido->cantidad }}</span>
                                    <span class="text-muted mr-2">= S/ {{  number_format((float) $detalle_pedido->precio * $detalle_pedido->cantidad, 2, '.', '') }}</span>
                                    @php
                                        $sub_total += $detalle_pedido->precio * $detalle_pedido->cantidad;
                                    @endphp
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
            <div class="card-footer">
                <div class="font-size-sm mr-2 py-2 text-center">
                    <span class="text-muted">Subtotal:</span>

                    @php
                        $sub_total = number_format((float) $sub_total, 2, '.', '');
                        $sub_array = explode(".", $sub_total);
                    @endphp

                    <span class="text-accent font-size-base ml-1">S/ {{ $sub_array[0] }}.<small>{{ $sub_array[1] }}</small></span>
                </div>

                <a class="btn btn-block btn-outline-primary btn-sm" href="/redireccionar/pedido/{{ $pedido->codigo }}">
                    Continuar Compra
                    <i class="fas fa-chevron-right ml-1 mr-n1"></i>
                </a>
            </div>
        </div>

    @endforeach
    {{-- FINAL --}}
@endif
