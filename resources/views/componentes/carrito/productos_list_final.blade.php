@foreach ($pedido_detalle as $producto)
<div class="media align-items-center py-1 border-bottom  border-top">
    <p class="d-block mr-2 mb-0" >

        @php
            $producto_img = $producto->ModeloDetalleProducto->PrimeraImagen;
        @endphp

        @if ($producto_img)
            <img width="64" src="https://tiendas.ceamarket.com/img/{{ $producto_img->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" />
        @else
            <img width="64" src="/img/default-product.png" alt="Product" />
        @endif

    </p>
    <div class="media-body">
         <h6 class="widget-product-title">
            <p>
                {{ $producto->ModeloDetalleProducto->ModeloProducto->nombre }}
            </p>
         </h6>
         <div class="widget-product-meta">

            @php
                $precio_array = explode(".", number_format((float) $producto->precio, 2, '.', ''));
            @endphp

              <span class="text-accent mr-2">S/  {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small></span>
              <span class="text-muted mr-2">x {{ $producto->cantidad }}</span>
              <span class="text-muted mr-2">= S/  {{  number_format((float) $producto->precio * $producto->cantidad, 2, '.', '') }}</span>
         </div>
    </div>
</div>
@endforeach
