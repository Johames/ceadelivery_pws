<div class="d-flex justify-content-center justify-content-sm-between align-items-center pb-2 pb-sm-2">
    <div class="d-sm-flex pb-3">
        @if ($filtro == 1)
            <a class="btn btn-icon nav-link-style disabled bg-light text-dark opacity-100 mr-2"  href="javascript:listar_productos(1, 1)">
                <i class="fas fa-th-list"></i>
                &nbsp;Ver en Lista
            </a>
            <a class="btn btn-icon nav-link-style nav-link-light" href="javascript:listar_productos(0, 2)">
                <i class="fas fa-th-large"></i>
                &nbsp;Ver en Cuadrícula
            </a>
        @elseif($filtro == 2)
            <a class="btn btn-icon nav-link-style nav-link-light" href="javascript:listar_productos(1, 1)">
                <i class="fas fa-th-list"></i>
                &nbsp;Ver en Lista
            </a>
            <a class="btn btn-icon nav-link-style disabled bg-light text-dark opacity-100 mr-2" href="javascript:listar_productos(0, 2)">
                <i class="fas fa-th-large"></i>
                &nbsp;Ver en Cuadrícula
            </a>
        @endif

    </div>
    <div class="d-flex pb-3">
        <div class="media align-items-center">
            <div class="media-body pr-3 text-right">
                <a id="urlEmpresaPedido1">
                    <h6 class="font-size-sm mb-n1 my-2 text-white"></h6>
                </a>
                <br>
                <span id="rubroEmpresaPedido1" class="font-size-ms text-muted"></span>
            </div>
            <img class="rounded d-none d-sm-block" id="imgEmpresaPedido1" width="50" src="/img/default-store.png" onerror="this.src='/img/default-store.png'" alt="logo"/>
        </div>
    </div>
</div>

<div class="row pt-1 mx-n2">

    @if (count($pedido_detalle) > 0)

        @if ($filtro == 1)

            <div class="table-responsive">
                <table class="table table-hover bg-light font-size-sm border-bottom" id="tablePedido">
                    <thead>
                        <tr>
                            <th class="align-middle">Producto</th>
                            <th class="align-middle">Cantidad</th>
                            <th class="align-middle">Costo</th>
                            <th class="align-middle">Color</th>
                            <th class="align-middle">Marca</th>
                            <th class="align-middle">Talla</th>
                            <th class="align-middle">Opc.</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pedido_detalle as $producto)
                            <tr class="tr_pedidos">
                                <td class="align-middle">
                                    <div class="media align-items-center">
                                        <div class="img-thumbnail rounded-circle position-relative" style="width: 3.3rem;">
                                            @php
                                                $producto_img = $producto->ModeloDetalleProducto->PrimeraImagen;
                                            @endphp

                                            @if ($producto_img)
                                                <img class="rounded-circle" src="https://tiendas.ceamarket.com/img/{{ $producto_img->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" />
                                            @else
                                                <img class="rounded-circle" src="/img/default-product.png" alt="Product" />
                                            @endif
                                        </div>
                                        <div class="media-body pl-3">
                                            {{-- @if ($producto->ModeloDetalleProducto->codigo_ariculo_real)
                                                <span class="text-dark font-weight-medium">
                                                    {{ $producto->ModeloDetalleProducto->codigo_ariculo_real }}
                                                </span>
                                                <br>
                                            @endif --}}

                                            <b class="text-primary" style="cursor: pointer;" onclick="mostrar_producto_detalle({{ $producto->ModeloDetalleProducto->producto_detalle_id }})">
                                                @if (strlen($producto->ModeloDetalleProducto->ModeloProducto->nombre) > 20)
                                                    {{ substr($producto->ModeloDetalleProducto->ModeloProducto->nombre, 1, 20) }}...
                                                @else
                                                    {{ $producto->ModeloDetalleProducto->ModeloProducto->nombre }}
                                                @endif
                                            </b>
                                            <br>
                                            <small>{{ $producto->ModeloDetalleProducto->ModeloCategoria->nombre }}</small>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-prepend">
                                            <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="restar({{ $producto->detalle_id }})">
                                                <i class="fas fa-minus-circle"></i>
                                            </a>
                                        </div>
                                        <input disabled="disabled" class="form-control form-control-sm text-center" type="text" id="cantidad_{{ $producto->detalle_id }}" value="{{ $producto->cantidad }}" style="width: 10px">
                                        <div class="input-group-append">
                                            <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="sumar({{ $producto->detalle_id }})">
                                                <i class="fas fa-plus-circle"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle">
                                    @php
                                        $precio_array = explode(".", number_format((float) $producto->precio, 2, '.', ''));
                                    @endphp



                                    @php
                                        $precio_referencial_array = explode(".", number_format((float) $producto->precio_referencial, 2, '.', ''));
                                    @endphp

                                    @if ($producto->precio != $producto->precio_referencial)
                                        <del class="font-size-sm text-muted"><small>S/  {{ $precio_referencial_array[0] }}.{{ $precio_referencial_array[1] }}</small> </del> <br>

                                    @endif

                                    <span class="text-accent">S/  {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small></span>
                                    <span>
                                        &nbsp;
                                    </span> x
                                    <b data-toggle="tooltip" data-placement="bottom" title="{{ $producto->ModeloDetalleProducto->ModeloUnidad->nombre }}">
                                        {{ $producto->ModeloDetalleProducto->ModeloUnidad->abreviatura }}
                                    </b>
                                </td>


                                <td class="align-middle">
                                    @if ($producto->ModeloDetalleProducto->ModeloColor)
                                        <span class="d-block font-size-xs pb-1">
                                            <b>{{ $producto->ModeloDetalleProducto->ModeloColor->nombre }}</b>
                                        </span>
                                    @else
                                        <span class="d-block font-size-xs pb-1">
                                            <b>-</b>
                                        </span>
                                    @endif
                                </td>
                                <td class="align-middle">
                                    @if ($producto->ModeloDetalleProducto->ModeloMarca)
                                        <span class="d-block font-size-xs pb-1">
                                            <b>{{ $producto->ModeloDetalleProducto->ModeloMarca->nombre }}</b>
                                        </span>
                                    @else
                                        <span class="d-block font-size-xs pb-1">
                                            <b>-</b>
                                        </span>
                                    @endif
                                </td>
                                <td class="align-middle">
                                    @if ($producto->ModeloDetalleProducto->ModeloTalla)
                                        <span class="d-block font-size-xs pb-1">
                                            <b>{{ $producto->ModeloDetalleProducto->ModeloTalla->nombre }}</b>
                                        </span>
                                    @else
                                        <span class="d-block font-size-xs pb-1">
                                            <b>-</b>
                                        </span>
                                    @endif
                                </td>
                                <td class="align-middle">
                                    <button class="btn-wishlist btn-sm" type="button" onclick="eliminar({{ $producto->detalle_id }})" data-toggle="tooltip" data-placement="bottom" title="Quitar del Carrito">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="row my-4">

                {!! $pedido_detalle->links() !!}

            </div>

        @elseif ($filtro == 2)

            @foreach ($pedido_detalle as $producto)

                <div class="col-lg-3 col-md-4 col-sm-6 px-2 mb-4">
                    <div class="card product-card">
                        <button class="btn-wishlist btn-sm" type="button" onclick="eliminar({{ $producto->detalle_id }})"data-toggle="tooltip" data-placement="bottom" title="Quitar del Carrito">
                            <i class="fas fa-trash-alt"></i>
                        </button>

                        <p class="d-block mr-2 mb-0" >

                            @php
                                $producto_img = $producto->ModeloDetalleProducto->PrimeraImagen;
                            @endphp

                            @if ($producto_img)
                                <img src="https://tiendas.ceamarket.com/img/{{ $producto_img->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" />
                            @else
                                <img src="/img/default-product.png" alt="Product" />
                            @endif

                        </p>
                        <div class="card-body py-2">
                            <h3 class="product-title font-size-md">
                                <a href="#" onclick="mostrar_producto_detalle({{ $producto->ModeloDetalleProducto->producto_detalle_id }})">
                                    {{ $producto->ModeloDetalleProducto->ModeloProducto->nombre }}
                                </a>
                            </h3>

                            @php
                                $precio_array = explode(".", number_format((float) $producto->precio, 2, '.', ''));
                            @endphp

                            <span class="text-accent">S/ {{ $precio_array[0] }}.<small>{{ $precio_array[1] }}</small></span>
                            <span>&nbsp;</span>

                            @php
                                $precio_referencial_array = explode(".", number_format((float) $producto->precio_referencial, 2, '.', ''));
                            @endphp

                            @if ($producto->precio != $producto->precio_referencial)
                                <small>
                                    <del class="font-size-sm text-muted"><small>S/ {{ $precio_referencial_array[0] }}.{{ $precio_referencial_array[1] }}</small> </del>
                                </small>
                            @endif
                            x
                            <b>{{ $producto->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</b>
                            <hr class="my-2">
                            <span class="d-block font-size-xs pb-1">
                                Categoria:
                                <b>{{ $producto->ModeloDetalleProducto->ModeloCategoria->nombre }}</b>
                            </span>

                            <span class="d-block font-size-xs pb-1">
                                Unidad:
                                <b>{{ $producto->ModeloDetalleProducto->ModeloUnidad->nombre }}</b>
                            </span>

                            @if ($producto->ModeloDetalleProducto->ModeloColor)
                                <span class="d-block font-size-xs pb-1">
                                    Color:
                                    <b>{{ $producto->ModeloDetalleProducto->ModeloColor->nombre }}</b>
                                </span>
                            @endif

                            @if ($producto->ModeloDetalleProducto->ModeloMarca)
                                <span class="d-block font-size-xs pb-1">
                                    Marca:
                                    <b>{{ $producto->ModeloDetalleProducto->ModeloMarca->nombre }}</b>
                                </span>
                            @endif

                            @if ($producto->ModeloDetalleProducto->ModeloTalla)
                                <span class="d-block font-size-xs pb-1">
                                    Talla:
                                    <b>{{ $producto->ModeloDetalleProducto->ModeloTalla->nombre }}</b>
                                </span>
                            @endif


                            <hr class="my-2">
                            <div class="d-flex justify-content-between">
                                <div class="input-group input-group-sm">
                                    <div class="input-group-prepend">
                                        <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="restar({{ $producto->detalle_id }})">
                                            <i class="fas fa-minus-circle"></i>
                                        </a>
                                    </div>
                                    <input disabled="disabled" class="form-control form-control-sm text-center" type="text" id="cantidad_{{ $producto->detalle_id }}" value="{{ $producto->cantidad }}" style="width: 10px">
                                    <div class="input-group-append">
                                        <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="sumar({{ $producto->detalle_id }})">
                                            <i class="fas fa-plus-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="d-sm-none">
                </div>

            @endforeach

        @endif

    @endif

</div>
