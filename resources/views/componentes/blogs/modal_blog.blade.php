@if ($blogs)

<div class="modal-header p-0 m-0">
    <div class="card-img-top overflow-hidden"  style="background: url('https://admin.ceamarket.com/img/{{ $blogs->banner }}') no-repeat center center; background-size: cover; height: 230px; border-radius: 0px;">
        <div style="height: 170px"></div>
        <div class="align-content-between" style="position: fixed; background: rgba(0, 0, 0, 0.6); padding-left: 20px; height: 60px;">
            <div class="px-4 pb-0">
                <p class="text-white pl-3">
                    <strong>
                        {{ $blogs->titulo }}
                    </strong>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal-body bg-faded-success p-0 m-0">
    <div class="card" style="border-radius: 0px;">
        <div class="card-body">
            <div class="row col-12">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <ul class="list-unstyled mb-0">
                        <li class="media pb-2 border-bottom">
                            <div class="media-body">
                                <span class="font-size-ms text-muted">Categoría</span>
                                <span class="d-block text-heading font-size-sm">
                                    {{ $blogs->categoria }}
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <ul class="list-unstyled mb-0">
                        <li class="media pb-2 border-bottom">
                            <div class="media-body">
                                <span class="font-size-ms text-muted">Autor</span>
                                <span class="d-block text-heading font-size-sm">
                                    {{ $blogs->autor }}
                                </span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="align-content-center px-3 py-2">
                {!! $blogs->contenido !!}
            </div>

            @if (count($blogs->ModeloComentariosBlogs) < 0)

                <div class="col-12 accordion mt-0 mb-3" id="accordion">
                    <div class="card">

                        <div class="card-header" id="ValoracionHeading">
                            <h3 class="accordion-heading">
                                <a class="collapsed" href="#Valoracion" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="Valoracion">
                                    <p class="mb-0">Comentarios del Blog: </p>
                                    <span class="accordion-indicator"></span>
                                </a>
                            </h3>
                        </div>
                        <div class="collapse" id="Valoracion" aria-labelledby="ValoracionHeading" data-parent="#accordion">
                            <div class="card-body">
                                <div class="col-12 px-2 mb-2">

                                    {{-- <div class="col-12 p-0">
                                        <div class="container" id="reviews">
                                            <!-- <div class="row pb-3">
                                                <div class="col-12">
                                                    @php
                                                        $total_star_perfil = 0;
                                                        $count_star_perfil = 0;
                                                        $promedio_star_perfil = 0;
                                                    @endphp

                                                    @foreach ($prod->ModeloValoracionProductos as $star_perfil)
                                                        @php
                                                            $total_star_perfil += $star_perfil->estrellas;
                                                            $count_star_perfil ++;
                                                            $promedio_star_perfil = ($total_star_perfil/$count_star_perfil);
                                                        @endphp
                                                    @endforeach
                                                    <div class="star-rating">

                                                        @for ($e = 0; $e < 5; $e++)
                                                            @if ($e < floor($promedio_star_perfil))
                                                                <i class="sr-star fas fa-star active" style="font-size: 24px;"></i>

                                                            @elseif($e == floor($promedio_star_perfil))

                                                                @if (is_float($promedio_star_perfil))
                                                                    @php
                                                                        $star_array_perfil = explode(".", number_format((float)$promedio_star_perfil, 2, '.', ''));
                                                                    @endphp

                                                                    @if (intval($star_array_perfil[1]) >= 50 )
                                                                        <i class="sr-star fas fa-star-half-alt active" style="font-size: 24px;"></i>
                                                                    @else
                                                                        <i class="sr-star fas fa-star" style="font-size: 24px;"></i>
                                                                    @endif
                                                                @else
                                                                    <i class="sr-star fas fa-star" style="font-size: 24px;"></i>
                                                                @endif
                                                            @else
                                                                <i class="sr-star fas fa-star" style="font-size: 24px;"></i>
                                                            @endif
                                                        @endfor

                                                    </div><br>
                                                    <span class="d-inline-block align-middle mt-3">
                                                        Promedio general: <strong>{{ round($promedio_star_perfil, 2) }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <hr class="mt-2 mb-0"> -->
                                            <div class="row">
                                                <div class="col-md-12">

                                                    @foreach ($blogs->ModeloComentariosBlogs as $item)

                                                        <div class="product-review border-bottom pb-2 mt-3">
                                                            <div class="d-flex mb-3">
                                                                <div class="media media-ie-fix align-items-center mr-4 pr-2">
                                                                    <img class="rounded-circle" width="50" src="/img/{{ $item->ModeloPersona->avatar }}" onerror="this.src='/img/default-product.png'" alt="Cliente">
                                                                    <div class="media-body pl-3">
                                                                        <h6 class="font-size-sm mb-0">{{ $item->ModeloPersona->nombres . " " . $item->ModeloPersona->apellidos }}</h6>
                                                                        <span class="font-size-ms text-muted">{{ $item->created_at }}</span>
                                                                    </div>
                                                                </div>
                                                                <div>
                                                                    <h6></h6>
                                                                    <div class="star-rating mt-1 pt-2">
                                                                        @for ($i = 0; $i < 5; $i++)
                                                                            @if ($i < $item->estrellas)
                                                                                <i class="sr-star fas fa-star active"></i>
                                                                            @else
                                                                                <i class="sr-star fas fa-star"></i>

                                                                            @endif
                                                                        @endfor
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <p class="col-12 font-size-md mb-2">
                                                                {{ $item->resena }}
                                                            </p>
                                                        </div>

                                                    @endforeach

                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@if ($blogs->url)
<div class="modal-footer bg-faded-success p-2">
    <ul class="list-unstyled mb-0">
        <li class="media pb-2">
            <div class="media-body pt-2">
                <a href="{{ $blogs->url }}" target="_blank" class="d-block text-heading font-size-sm">
                    {{ $blogs->url }}
                </a>
            </div>
        </li>
    </ul>
</div>
@endif

@endif
