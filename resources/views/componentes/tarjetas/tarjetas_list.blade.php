@if (count($tarjetas) > 0)

    <div class="table-responsive">
        <table class="table table-hover bg-light font-size-sm border-bottom">
            <thead>
                <tr>
                    <th class="align-middle">Tarjeta</th>
                    <th class="align-middle">Brand</th>
                    <th class="align-middle">Tipo</th>
                    <th class="align-middle">Opciones</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($tarjetas as $card)

                    <tr class="tr_pedidos" >
                        <td class="align-middle" style="width: 70%">
                            <h5><b>{{ $card->card_number }}</b></h4>
                        </td>
                        <td class="align-middle" style="width: 35px !important;">
                            @if ($card->card_brand == "Visa")
                                <span class="badge badge-info px-2 p-2">{{ $card->card_brand }}</span>
                            @elseif($card->card_brand == "Mastercard")
                                <span class="badge badge-danger px-2 p-2">{{ $card->card_brand }}</span>
                            @elseif($card->card_brand == "Amex")
                                <span class="badge badge-warning px-2 p-2">{{ $card->card_brand }}</span>
                            @elseif($card->card_brand == "Diners")
                                <span class="badge badge-info px-2 p-2">{{ $card->card_brand }}</span>
                            @endif
                        </td>
                        <td class="align-middle" style="width: 35px !important;">
                            <span class="badge badge-success px-2 p-2">
                                {{ $card->card_type }}
                            </span>
                        </td>
                        <td class="align-middle">
                            <div class="media align-items-center">
                                <button class="btn btn-outline-danger btn-icon mb-2 mr-2" type="button" onclick="eliminar_tarjetas('{{ $card->card_id }}')">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                @endforeach

            </tbody>
        </table>
    </div>

@else

    <div class="table-responsive">
        <table class="table table-hover bg-light font-size-sm border-bottom" id="tablePedido">
            <thead>
                <tr>
                    <th class="align-middle">Tarjeta</th>
                    <th class="align-middle">Brand</th>
                    <th class="align-middle">Tipo</th>
                    <th class="align-middle">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <tr class="tr_direcciones">
                    <td class="align-middle text-center" colspan="4">
                        <div class="media align-items-center">
                            <div class="media-body pl-3">
                                <span class="text-dark font-weight-medium">No hay tarjetas guardadas</span>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

@endif
