<div class="accordion" id="accordion">
    @if (count($tarjetas) > 0)
        <div class="card">
            <div class="card-header" id="TarjHeading">
                <h3 class="accordion-heading">
                    <a href="#tarjetas" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="tarjetas">
                        Tarjetas Guardadas
                        <span class="accordion-indicator"></span>
                    </a>
                </h3>
            </div>
            <div class="collapse show p-3" id="tarjetas" aria-labelledby="TarjHeading" data-parent="#accordion">
                <div class="table-responsive">
                    <table class="table table-hover bg-light font-size-sm border-bottom">
                        <tbody>
                            @foreach ($tarjetas as $card)
                                <tr onclick="selectCard('{{ $card->card_id }}')" style="cursor: pointer;">
                                    <td>
                                        <div class="custom-control custom-radio">
                                            <input class="custom-control-input" type="radio" id="tarjeta_{{ $card->card_id }}" name="tarjeta_id" value="{{ $card->card_id }}">
                                            <label class="custom-control-label" for="tarjeta_{{ $card->card_id }}"></label>
                                        </div>
                                    </td>
                                    <td class="align-middle">
                                        <span class="text-dark font-weight-medium">{{ $card->card_number }}</span>
                                        <br>
                                        <span class="text-muted">
                                            {{ $card->card_brand }} / {{ $card->card_type }}
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <center>
                    <button class="btn btn-primary btn-shadow mt-2" id="btn_card_pago">Realizar Pago</button>
                    <button class="btn btn-outline-secondary btn-shadow mt-2" id="cancelCardPago" type="button" data-dismiss="modal">Cancelar</button>
                    <i id="spinner_guard" style="display: none;" class="fa fa-spinner fa-pulse fa-3x fa-fw mt-2"></i>
                </center>
            </div>
        </div>
    @endif

    <div class="card">
        <div class="card-header" id="NuevHeading">
            <h3 class="accordion-heading">
                <a href="#nuevas" role="button" data-toggle="collapse" aria-expanded="true" aria-controls="nuevas">
                    Nueva Tarjeta
                    <span class="accordion-indicator"></span>
                </a>
            </h3>
        </div>
        @if (count($tarjetas) > 0)
        <div class="collapse p-3" id="nuevas" aria-labelledby="NuevHeading" data-parent="#accordion">
        @else
        <div class="collapse show p-3" id="nuevas" aria-labelledby="NuevHeading" data-parent="#accordion">
        @endif

            <p class="font-size-sm">
                Aceptamos las siguientes tarjetas:&nbsp;&nbsp;
                <img class="d-inline-block align-middle" src="{{ asset('img/cards.png') }}" style="width: 187px;" alt="Cerdit Cards">
            </p>

            <div class="card px-3 pt-3 pb-1 m-0">
                <div class="row">
                    <div class="form-group col-12">
                        <input class="form-control" type="text" maxlength="50" name="cardEmail" data-culqi="card[email]" id="card[email]" placeholder="Correo Electrónico">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-12">
                        <input class="form-control" type="text" maxlength="20" name="cardNumero" data-culqi="card[number]" id="card[number]" placeholder="Número de la tarjeta">
                    </div>
                </div>

                <div class="row">
                    <div class="col-5 form-group">
                        <label for="mes">Mes Venc.</label>
                        <select class="form-control custom-select" name="cardMes" data-culqi="card[exp_month]" id="card[exp_month]">
                            <option value="01">Enero</option>
                            <option value="02">Febrero</option>
                            <option value="03">Marzo</option>
                            <option value="04">Abril</option>
                            <option value="05">Mayo</option>
                            <option value="06">Junio</option>
                            <option value="07">Julio</option>
                            <option value="08">Agosto</option>
                            <option value="09">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>
                        </select>
                    </div>
                    <div class="col-4 form-group">
                        <label for="year">Año Venc.</label>
                        <select class="form-control custom-select" name="cardYear" data-culqi="card[exp_year]" id="card[exp_year]">
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                            <option value="2024">2024</option>
                            <option value="2025">2025</option>
                            <option value="2026">2026</option>
                            <option value="2027">2027</option>
                            <option value="2028">2028</option>
                            <option value="2029">2029</option>
                            <option value="2030">2030</option>
                            <option value="2031">2031</option>
                            <option value="2032">2032</option>
                            <option value="2033">2033</option>
                            <option value="2034">2034</option>
                            <option value="2035">2035</option>
                            <option value="2036">2036</option>
                            <option value="2037">2037</option>
                            <option value="2038">2038</option>
                            <option value="2039">2039</option>
                            <option value="2040">2040</option>
                            <option value="2041">2041</option>
                            <option value="2042">2042</option>
                            <option value="2043">2043</option>
                            <option value="2044">2044</option>
                            <option value="2045">2045</option>
                            <option value="2046">2046</option>
                            <option value="2047">2047</option>
                            <option value="2048">2048</option>
                            <option value="2049">2049</option>
                            <option value="2050">2050</option>
                        </select>
                    </div>
                    <div class="col-3 form-group">
                        <label for="cvc">CVV</label>
                        <input class="form-control" type="text" maxlength="4" name="cardCVV" data-culqi="card[cvv]" id="card[cvv]" placeholder="123">
                    </div>
                </div>
                <div class="form-group">
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" id="guardar" name="guardar" onchange="guardarCard()">
                        <label class="custom-control-label" for="guardar">Guardar mi tarjeta</label>
                    </div>
                </div>
            </div>


            <div class="form-group mt-2">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="terminos">
                    <label class="custom-control-label" for="terminos">Acepto los <a href="">Términos y Condiciones</a></label>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div id="contenedor_de_errores_pago_tarjeta"></div>
                </div>
            </div>

            <center class="border-top pt-3">
                <button class="btn btn-primary btn-shadow mt-2" id="btn_pago">Realizar Pago</button>
                <button class="btn btn-outline-secondary btn-shadow mt-2" id="cancelarTarjeta" type="button" data-dismiss="modal">Cancelar</button>
                <i id="spinner" style="display: none;" class="fa fa-spinner fa-pulse fa-3x fa-fw mt-2"></i>
            </center>
        </div>
    </div>
</div>
