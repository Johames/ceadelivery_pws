@if ($metodos_envio)

    @foreach ($metodos_envio as $envio)

        <tr class="tr_metodos_envio" onclick="selectMetodo('{{ $envio->metodo_envio_emp_id }}')" style="cursor: pointer;">
            <td>
                <div class="custom-control custom-radio mb-4">
                    <input class="custom-control-input" type="radio" id="metodos_envio_{{ $envio->metodo_envio_emp_id }}" name="metodo_envio_emp_id" value="{{ $envio->metodo_envio_emp_id }}">
                    <label class="custom-control-label" for="metodos_envio_{{ $envio->metodo_envio_emp_id }}"></label>
                </div>
            </td>
            <td class="align-middle">
                <span class="text-dark font-weight-medium">{{ $envio->ModeloMetodoEnvio->nombre }}</span>
                <br>
                <span class="text-muted">
                    @if ($envio->descripcion)
                        {{ $envio->descripcion }}
                    @else
                        Sin descripción
                    @endif
                </span>
            </td>
            <td class="align-middle">{{ $envio->llegada }}</td>
            <td class="align-middle">S/ {{ $envio->precio }}</td>
        </tr>

        {{ $envio }}

    @endforeach

@endif
