@if ($metodos_pago)

    @foreach ($metodos_pago as $metodo)

        <tr class="tr_metodos_pago" onclick="selectMetodo('{{ $metodo->metodo_pago_emp_id }}', '{{ $metodo->ModeloMetodoPago->tipo }}', '{{ $metodo->ModeloMetodoPago->referencia }}')" style="cursor: pointer;">
            <td>
                <div class="custom-control custom-radio mb-4">
                    <input class="custom-control-input" type="radio" id="metodos_pago_{{ $metodo->ModeloMetodoPago->tipo }}" name="metodo_pago_emp_id" value="{{ $metodo->metodo_pago_emp_id }}">
                    <label class="custom-control-label" for="metodos_pago_{{ $metodo->ModeloMetodoPago->tipo }}"></label>
                </div>
            </td>
            <td class="align-middle">
                <span class="text-dark font-weight-medium">{{ $metodo->ModeloMetodoPago->nombre }}</span>
                <br>
                <span class="text-muted">
                    @if ($metodo->descripcion)
                        {{ $metodo->descripcion }}
                    @else
                        Sin descripción
                    @endif
                </span>
            </td>
            {{-- <td class="align-middle">
                @if ($metodo->precio_comision)
                    {{ $metodo->precio_comision }}
                @else
                    0.00
                @endif
                 %
            </td> --}}
        </tr>

    @endforeach

@endif

