{{-- {{ $deseos }} --}}

@foreach ($deseos as $deseo)

    <div class="modal-header">
        <h4 class="modal-title">{{ $deseo->nombre }}</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body font-size-sm">
        <div class="row">

            @if (count($deseo->ModeloDeseosProductos) > 0)

                @foreach ($deseo->ModeloDeseosProductos as $deseosProd)

                    <div class="col-6 col-sm-4 col-md-4 col-lg-3 col-xl-3 mb-4 pl-0">
                        <div class="card product-card card-static pb-3 m-1 h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                            @if ($deseosProd->ModeloDetalleProducto->estado_desc == '0')
                                <span class="badge badge-danger badge-shadow">
                                    @if ($deseosProd->ModeloDetalleProducto->tipo_desc == '0')
                                        S/ -{{ number_format((float)$deseosProd->ModeloDetalleProducto->descuento, 2, '.', '') }}
                                    @else()
                                        -{{ $deseosProd->ModeloDetalleProducto->descuento }}%
                                    @endif
                                </span>
                            @endif

                            @hasrole('cliente')
                                <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito" onclick="addCarrito({{ $deseosProd->ModeloDetalleProducto->producto_detalle_id }})">
                                    <i class="fas fa-shopping-cart"></i>
                                </button>
                            @endhasrole

                            @if ($deseosProd->ModeloDetalleProducto->PrimeraImagen)
                                <span class="card-img-top d-block overflow-hidden">
                                    <img src="https://tiendas.ceamarket.com/img/{{ $deseosProd->ModeloDetalleProducto->PrimeraImagen->ruta }}" onerror="this.src='/img/default-product.png'" alt="Product" style="width: 100%; height: 12vw; object-fit: cover;">
                                </span>
                            @else
                                <span class="card-img-top d-block overflow-hidden">
                                    <img src="{{ asset("/img/default-product.png") }}" alt="Sin imagen" style="width: 100%; height: 12vw; object-fit: cover;">
                                </span>
                            @endif

                            <div class="card-body py-2">

                                <span class="product-meta d-block font-size-xs pb-1">
                                    {{ $deseosProd->ModeloDetalleProducto->ModeloCategoria->nombre }}
                                </span>

                                <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $deseosProd->ModeloDetalleProducto->producto_detalle_id }})">
                                    <h6 class="pb-1 mb-1" style="font-size: .89rem !important" >
                                        {{ $deseosProd->ModeloDetalleProducto->ModeloProducto->nombre }}
                                    </h6>
                                </a>

                                <span class="product-meta d-block font-size-xs pb-1">
                                    Disponible:
                                    {{ $deseosProd->ModeloDetalleProducto->stock . ' ' . $deseosProd->ModeloDetalleProducto->ModeloUnidad->abreviatura }}
                                </span>
                                <div class="product-price">
                                    @php
                                        $precios_array = explode(".", number_format((float)$deseosProd->ModeloDetalleProducto->precio, 2, '.', ''));
                                    @endphp

                                    @if ($deseosProd->ModeloDetalleProducto->estado_desc == '0')

                                        @if ($deseosProd->ModeloDetalleProducto->tipo_desc == '0')
                                            @php
                                                $precios_array_desc_soles = explode(".", number_format((float)$deseosProd->ModeloDetalleProducto->precio - $deseosProd->ModeloDetalleProducto->descuento, 2, '.', ''));
                                            @endphp
                                            <span class="text-accent">
                                                S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                            </span>
                                            <del class="font-size-sm text-muted">
                                                S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                            </del>

                                            @if ($deseosProd->ModeloDetalleProducto->ModeloUnidad)
                                                <small class="text-muted">x {{ $deseosProd->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</small>
                                            @endif
                                        @else

                                            @php
                                                $precios_array_desc_por = explode(".", number_format((float)($deseosProd->ModeloDetalleProducto->precio - ($deseosProd->ModeloDetalleProducto->precio*$deseosProd->ModeloDetalleProducto->descuento/100)), 2, '.', ''));
                                            @endphp

                                            <span class="text-accent">
                                                S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                            </span>
                                            <del class="font-size-sm text-muted">
                                                S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                            </del>

                                            @if ($deseosProd->ModeloDetalleProducto->ModeloUnidad)
                                                <small class="text-muted">x {{ $deseosProd->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</small>
                                            @endif
                                        @endif

                                    @else
                                        <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                                        @if ($deseosProd->ModeloDetalleProducto->ModeloUnidad)
                                            <small class="text-muted">x {{ $deseosProd->ModeloDetalleProducto->ModeloUnidad->abreviatura }}</small>
                                        @endif
                                    @endif

                                </div>
                                <br>
                                <div class="col-12 pt-3 border-top">
                                    <a class="text-center" href="/store/{{ $deseosProd->ModeloDetalleProducto->ModeloEmpresa->slug }}" style="font-size: .9rem;">
                                        @if ($deseosProd->ModeloDetalleProducto->ModeloEmpresa->razon_social)
                                            <h6>{{ $deseosProd->ModeloDetalleProducto->ModeloEmpresa->razon_social }}</h6>
                                        @else
                                            <h6>{{ $deseosProd->ModeloDetalleProducto->ModeloEmpresa->nombre_comercial }}</h6>
                                        @endif
                                    </a>
                                </div>
                            </div>
                            <div class="product-floating-btn">
                                @hasrole('cliente')
                                    <button class="btn-wishlist btn-shadow btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Eliminar de la lista de deseos" onclick="eliminar_detalle_deseos('{{ $deseosProd->prod_deseo_id }}', '{{ $deseo->deseos_id }}')">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                @endhasrole
                            </div>
                        </div>
                    </div>

                @endforeach

            @else
                <div class="text-center">
                    <span>No hay productos en esta lista de deseos.</span>
                </div>
            @endif
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary btn-sm" type="reset" data-dismiss="modal">Cerrar</button>
    </div>

@endforeach
