@if (count($deseos) > 0)

    <div class="row justify-content-center px-0 mx-0">
        <div class="col-12">
            <div class="row">
                @foreach ($deseos as $deseo)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4 mb-3">
                        <div class="card h-100 border-0 box-shadow-sm">
                            <div class="card-body">
                                <div class="media align-items-center">
                                    <div class="media-body pl-3">
                                        <div onclick="mostrar_detalle_deseo('{{ $deseo->deseos_id }}')" style="cursor: pointer;">
                                            <h5 class="font-size-sm mb-0">{{ $deseo->nombre }}</h5>
                                            <span class="text-muted font-size-ms">
                                                @if ($deseo->descripcion)
                                                    {{ $deseo->descripcion }}
                                                @else
                                                    Sin descripción
                                                @endif
                                            </span>
                                        </div>
                                        <div class=" justify-content-center pt-3" style="position: relative !important;">
                                            <button type="button" onclick="mostrar_deseo('{{ $deseo->deseos_id }}')" class="btn btn-outline-danger btn-sm btn-icon pt-1 pb-1 pr-2 pl-2 mr-2">
                                                <i class="fas fa-pen-alt"></i>
                                            </button>
                                            <button type="button" onclick="eliminar_lista_deseo('{{ $deseo->deseos_id }}')" class="btn btn-outline-warning btn-sm btn-icon pt-1 pb-1 pr-2 pl-2 mr-2">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@else

    <div class="row justify-content-center px-0 mx-0">
        <div class="card col-12 mb-3">
            <div class="product-review pt-2 pr-3 pl-3">
                <p class="font-size-md mb-2">
                    No se encontraron listas de deseos
                </p>
            </div>
        </div>
    </div>

@endif

