@if (count($direcciones) > 0)

    <thead>
        <tr>
            <th class="align-middle">Dirección</th>
            <th class="align-middle">Coordenadas</th>
            <th class="align-middle">Principal</th>
            <th class="align-middle">Estado</th>
            <th class="align-middle">Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($direcciones as $dir)
            <tr class="tr_direcciones">
                <td class="align-middle">
                    <div class="media align-items-center">
                        <div class="media-body pl-3">
                            <span class="text-dark font-weight-medium">{{ $dir->direccion }}</span>
                            <br>
                            @if ($dir->referencia)
                                <span class="text-muted">{{ $dir->referencia }}</span>
                            @else
                                <span class="text-muted">Sin referencia.</span>
                            @endif
                        </div>
                    </div>
                </td>
                <td class="align-middle">{{ $dir->coordenadas }}</td>
                <td class="align-middle">
                    @if ($dir->principal == 0)
                        <span class="badge badge-success px-2 p-2">Principal</span>
                    @else
                        <span class="badge badge-warning px-2 p-2">Secundaria</span>
                    @endif
                </td>
                <td class="align-middle">
                    @if ($dir->estado == 0)
                        <span class="badge badge-success px-2 p-2">Activo</span>
                    @else
                        <span class="badge badge-danger px-2 p-2">Inactivo</span>
                    @endif
                </td>
                <td class="align-middle">
                    <div class="media align-items-center">
                        @if ($dir->estado == 0)
                            <button class="btn btn-outline-warning btn-sm mb-2 mr-2" type="button" onclick="mostar_direcciones('{{ $dir->direciones_id }}')">
                                <i class="fas fa-pen-alt"></i>
                            </button>
                            <button class="btn btn-outline-danger btn-sm mb-2 mr-2" type="button" onclick="desactivar_direcciones('{{ $dir->direciones_id }}')">
                                <i class="fas fa-times"></i>
                            </button>
                        @else
                            <button class="btn btn-outline-warning btn-sm mb-2 mr-2" type="button" onclick="mostar_direcciones('{{ $dir->direciones_id }}')">
                                <i class="fas fa-pen-alt"></i>
                            </button>
                            <button class="btn btn-outline-success btn-sm mb-2 mr-2" type="button" onclick="activar_direcciones('{{ $dir->direciones_id }}')">
                                <i class="fas fa-check"></i>
                            </button>
                        @endif
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>

@else
    <thead>
        <tr>
            <th class="align-middle">Dirección</th>
            <th class="align-middle">Coordenadas</th>
            <th class="align-middle">Principal</th>
            <th class="align-middle">Estado</th>
            <th class="align-middle">Opciones</th>
        </tr>
    </thead>
    <tbody>
        <tr class="tr_direcciones">
            <td class="align-middle text-center" colspan="5">
                <div class="media align-items-center">
                    <div class="media-body pl-3">
                        <span class="text-dark font-weight-medium">No hay direcciones inactivas</span>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
@endif
