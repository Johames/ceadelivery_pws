<section class="container-fluid bg-light mt-2 mb-3 pb-2 mx-0 px-0" style="border-radius: 10px;">
    <div class="d-flex flex-wrap justify-content-between align-items-center pt-1 border-bottom pb-4 mb-4">
        <h2 class="h3 mb-0 pt-3 mr-3">Esta buscando: "{{ $text }}"</h2>
        <div class="pt-3">
            <a class="btn btn-danger btn-sm" href="javascript:mostrarProductosSinCategoria()">
                Limpiar busqueda
            </a>
        </div>
    </div>
    <div class="row pt-1 mx-2">
        @if ($buscado)

            @foreach ($buscado as $prod)

                {{-- <div class="col-6 col-sm-4 col-md-3 col-lg-3 col-xl-3 mb-4 pl-0"> --}}
                <div class="col-6 col-sm-4 col-md-3 col-lg-3 col-xl-2 mb-4 pl-0">
                    <div class="card product-card h-100 pb-3 m-1" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                        @if ($prod->estado_desc == '0')
                            <span class="badge badge-danger badge-shadow">
                                @if ($prod->tipo_desc == '0')
                                    S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                                @else()
                                    -{{ $prod->descuento }}%
                                @endif
                            </span>
                        @endif

                        @guest
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="modal" href="#InicioSesion">
                                <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito"></i>
                            </button>
                        @endguest
                        @hasrole('cliente')
                            <button class="btn-wishlist btn-sm d-sm-block d-lg-none d-xs-block" type="button" data-toggle="tooltip" data-placement="left" title="Agregar al carrito" style="margin-right: 2.5rem" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                <i class="fas fa-shopping-cart"></i>
                            </button>
                            <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Lista de Deseos" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                                <i class="fas fa-heart"></i>
                            </button>
                        @endhasrole

                        @php
                            $ruta_img = null;
                            $coun_img = 0;
                        @endphp

                        @foreach ($prod->ModeloProductoImagenes as $imagen)
                            @php
                                $coun_img++
                            @endphp

                            @if ($coun_img <= 1)
                                @php
                                    $ruta_img = $imagen->ruta;
                                @endphp
                            @endif
                        @endforeach

                        @if ($ruta_img)
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="https://tiendas.ceamarket.com/img/{{ $ruta_img }}" onerror="this.src='/img/default-product.png'" alt="Product" style="width: 100%; height: 12vw; object-fit: cover;">
                            </span>
                        @else
                            <span class="card-img-top d-block overflow-hidden">
                                <img src="{{ asset("img/default-product.png") }}" alt="Sin imagen" style="width: 100%; height: 12vw; object-fit: cover;">
                            </span>
                        @endif

                        <div class="card-body py-2">
                            <span class="product-meta d-block font-size-xs pb-1">
                                {{ $prod->ModeloCategoria->nombre }}
                            </span>

                            {{-- <a data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                @if (strlen($prod->ModeloProducto->nombre) > 35)
                                    <h6 class="pb-0 mb-0" style="font-size: .79rem !important" data-toggle="tooltip" data-placement="bottom" title="{{ $prod->ModeloProducto->nombre }}">
                                        {{ ucwords(mb_strtolower(substr($prod->ModeloProducto->nombre, 0, 35), 'UTF-8')) }}...
                                    </h6>
                                @else
                                    <h6 class="pb-0 mb-0" style="font-size: .79rem !important">{{ ucwords(mb_strtolower($prod->ModeloProducto->nombre), 'UTF-8') }}</h6>
                                @endif
                            </a> --}}

                            <a class="pb-2" data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                <h6 class="pb-1 mb-1" style="font-size: .79rem !important" >
                                    {{ $prod->ModeloProducto->nombre }}
                                </h6>
                            </a>

                            <span class="product-meta d-block font-size-xs pb-1">
                                Disponible:
                                {{ $prod->stock . ' ' . $prod->ModeloUnidad->abreviatura }}
                            </span>
                            <div class="product-price">
                                @php
                                    $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                                @endphp

                                @if ($prod->estado_desc == '0')

                                    @if ($prod->tipo_desc == '0')
                                        @php
                                            $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                                        @endphp
                                        <span class="text-accent">
                                            S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                        </span>
                                        <del class="font-size-sm text-muted">
                                            S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                        </del>

                                        @if ($prod->ModeloUnidad)
                                            <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                        @endif
                                    @else

                                        @php
                                            $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                                        @endphp

                                        <span class="text-accent">
                                            S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                        </span>
                                        <del class="font-size-sm text-muted">
                                            S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                        </del>

                                        @if ($prod->ModeloUnidad)
                                            <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                        @endif
                                    @endif

                                @else
                                    <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                                    @if ($prod->ModeloUnidad)
                                        <small class="text-muted">x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                    @endif
                                @endif

                            </div>
                        </div>
                        <div class="text-center d-sm-block d-lg-none d-xs-block mt-2">
                            <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                                Ver Detalles
                            </button>
                        </div>
                        {{-- <div class="product-floating-btn">
                            <button class="btn-wishlist btn-shadow btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Ver Detalles" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                                <i class="fas fa-eye"></i>
                            </button>
                            @hasrole('cliente')
                                <button class="btn-wishlist btn-shadow btn-sm" type="button" data-toggle="tooltip" data-placement="bottom" title="Agregar a Lista de Deseos" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                                    <i class="fas fa-heart"></i>
                                </button>
                            @endhasrole
                        </div> --}}
                        <div class="card-body card-body-hidden">
                            @hasrole('cliente')
                                <button class="btn btn-primary btn-sm btn-block mt-2" type="button" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                                    <i class="fas fa-shopping-cart font-size-sm mr-1 d-none d-sm-block"></i>
                                    Agregar al carrito
                                </button>
                            @endhasrole
                            <div class="text-center mt-2">
                                <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                                    <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                                    Ver Detalles
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        @endif
    </div>
</section>
