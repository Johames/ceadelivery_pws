@if (count($productos) <= 0)
@else
    @foreach ($productos as $prod)
        <div class="col-6 col-sm-4 col-md-3 col-lg-3 col-xl-2 px-2 mb-4">
            <div class="card product-card h-100" style="-webkit-box-shadow: 2px 2px 5px rgb(211, 210, 210); -moz-box-shadow: 2px 2px 5px rgb(211, 211, 211); filter: shadow(color=rgb(211, 211, 211), direction=135, strength=2);">
                @if ($prod->estado_desc == '0')
                    <span class="badge badge-danger badge-shadow">
                        @if ($prod->tipo_desc == '0')
                            S/ -{{ number_format((float)$prod->descuento, 2, '.', '') }}
                        @else()
                            -{{ $prod->descuento }}%
                        @endif
                    </span>
                @endif

                @guest
                    <button class="btn-wishlist btn-sm" type="button" data-toggle="modal" href="#InicioSesion">
                        <i class="fas fa-shopping-cart" data-toggle="tooltip" data-placement="bottom" title="Agregar al Carrito"></i>
                    </button>
                @endguest
                @hasrole('cliente')
                    <button class="btn-wishlist btn-sm d-sm-block d-lg-none d-xs-block" type="button" style="margin-right: 2.5rem" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                        <i class="fas fa-shopping-cart"></i>
                    </button>
                    <button class="btn-wishlist btn-sm" type="button" data-toggle="tooltip" data-placement="left" title="Agregar a Lista de Deseos" onclick="mostrar_modal_elegir_deseos({{ $prod->producto_detalle_id }})">
                        <i class="fas fa-heart"></i>
                    </button>
                @endhasrole

                @if ($prod->PrimeraImagen)
                    <span class="card-img-top d-block overflow-hidden">
                        <img src="https://tiendas.ceamarket.com/img/{{ $prod->PrimeraImagen->ruta }}" onerror="this.src='/img/default-product.png'" alt="{{ $prod->ModeloProducto->nombre }}" style="width: 100%; height: 12vw; object-fit: cover;">
                    </span>
                @else
                    <span class="card-img-top d-block overflow-hidden">
                        <img src="{{ asset("/img/default-product.png") }}" alt="Sin imagen" style="width: 100%; height: 12vw; object-fit: cover;">
                    </span>
                @endif

                <div class="card-body py-2" style="border-radius: 10px !important;">

                    <a class="pb-2" data-toggle="modal" href="#" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }})">
                        <h6 class="pb-1 mb-1" style="font-size: .79rem !important" >
                            {{ $prod->ModeloProducto->nombre }}
                        </h6>
                    </a>

                    <span class="product-meta d-block font-size-xs pb-1">
                        Disponible:
                        {{ $prod->stock . ' ' . $prod->ModeloUnidad->abreviatura }}
                    </span>
                    <div class="d-flex justify-content-between">
                        <div class="product-price">

                            @php
                                $precios_array = explode(".", number_format((float)$prod->precio, 2, '.', ''));
                            @endphp

                            @if ($prod->estado_desc == '0')

                                @if ($prod->tipo_desc == '0')
                                    @php
                                        $precios_array_desc_soles = explode(".", number_format((float)$prod->precio - $prod->descuento, 2, '.', ''));
                                    @endphp
                                    <span class="text-accent">
                                        S/ {{  $precios_array_desc_soles[0] }}.<small>{{  $precios_array_desc_soles[1] }}</small>
                                    </span>

                                    <del class="font-size-sm text-muted">
                                        S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                    </del>

                                    @if ($prod->ModeloUnidad)
                                        <small class="text-muted"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                    @endif
                                @else

                                    @php
                                        $precios_array_desc_por = explode(".", number_format((float)($prod->precio - ($prod->precio*$prod->descuento/100)), 2, '.', ''));
                                    @endphp

                                    <span class="text-accent">
                                        S/ {{  $precios_array_desc_por[0] }}.<small>{{  $precios_array_desc_por[1] }}</small>
                                    </span>

                                    <del class="font-size-sm text-muted">
                                        S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small>
                                    </del>

                                    @if ($prod->ModeloUnidad)
                                        <small class="text-muted"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                    @endif
                                @endif

                            @else

                                <span class="text-accent">S/ {{  $precios_array[0] }}.<small>{{  $precios_array[1] }}</small></span>

                                @if ($prod->ModeloUnidad)
                                    <small class="text-muted"> x {{ $prod->ModeloUnidad->abreviatura }}</small>
                                @endif

                            @endif

                        </div>
                        {{-- <div class="star-rating">
                            <i class="sr-star fas fa-star active"></i>
                            <i class="sr-star fas fa-star active"></i>
                            <i class="sr-star fas fa-star active"></i>
                            <i class="sr-star fas fa-star active"></i>
                            <i class="sr-star fas fa-star"></i>
                        </div> --}}
                    </div>
                    <div class="text-center d-sm-block d-lg-none d-xs-block mt-2">
                        <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                            <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                            Ver Detalles
                        </button>
                    </div>
                </div>
                <div class="card-body card-body-hidden">
                    @hasrole('cliente')
                        <button class="btn btn-primary btn-sm btn-block mt-2" type="button" onclick="addCarrito({{ $prod->producto_detalle_id }})">
                            <i class="fas fa-shopping-cart font-size-sm mr-1 d-none d-sm-block"></i>
                            Agregar al carrito
                        </button>
                    @endhasrole
                    <div class="text-center mt-2">
                        <button class="btn btn-outline-primary btn-block btn-sm mr-4" onclick="mostrar_producto_detalle({{ $prod->producto_detalle_id }}, 'inicio')">
                            <i class="fas fa-eye align-middle mr-1 d-none d-sm-block"></i>
                            Ver Detalles
                        </button>
                    </div>
                </div>
            </div>
            <hr class="d-sm-none">
        </div>
    @endforeach
@endif
