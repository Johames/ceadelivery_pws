@if (count($pedidos) > 0)

    <div class="table-responsive">
        <table class="table table-hover bg-light font-size-sm border-bottom" id="tablePedido">
            <thead>
                <tr>
                    <th class="align-middle">Tienda</th>
                    <th class="align-middle">Estado</th>
                    <th class="align-middle">Llegada</th>
                    <th class="align-middle">Total</th>
                    <th class="align-middle">Opciones</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($pedidos as $ped)

                    <tr class="tr_pedidos" >
                        <td class="align-middle">
                            <div class="media align-items-center">
                                <div class="img-thumbnail rounded-circle position-relative" style="width: 3.3rem;">
                                    @if ($ped->ModeloEmpresa->icono)
                                        <img class="rounded-circle" src="https://tiendas.ceamarket.com/img/{{ $ped->ModeloEmpresa->icono }}" alt="tienda" onerror="this.src='/img/default-store.png'">
                                    @else
                                        <img class="rounded-circle" src="/img/default-store.png" alt="tienda">
                                    @endif
                                </div>
                                <div class="media-body pl-3">
                                    <span class="text-dark font-weight-medium">{{ $ped->ModeloEmpresa->ruc }}</span>
                                    <br>
                                    <a href="/store/"{{ $ped->ModeloEmpresa->slug }}>
                                        @if ($ped->ModeloEmpresa->razon_social)
                                            <span class="text-muted">{{ $ped->ModeloEmpresa->razon_social }}</span>
                                        @else
                                            <span class="text-muted">{{ $ped->ModeloEmpresa->nombre_comercial }}</span>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </td>
                        <td class="align-middle" style="width: 35px !important;">
                            @if ($ped->estado == 0)
                                <span class="badge badge-warning px-2 p-2">Pendiente</span>
                            @elseif ($ped->estado == 1)
                                <span class="badge badge-success px-2 p-2">En Proceso</span>
                            @elseif ($ped->estado == 2)
                                <span class="badge badge-success px-2 p-2">Confirmado</span>
                            @elseif ($ped->estado == 3)
                                <span class="badge badge-success px-2 p-2">En Camino</span>
                            @elseif ($ped->estado == 4)
                                <span class="badge badge-primary px-2 p-2">Entregado</span>
                            @elseif ($ped->estado == 5)
                                <span class="badge badge-danger px-2 p-2">Rechazado</span>
                            @endif
                        </td>
                        <td class="align-middle">Aprox. {{ $ped->fecha_pedido }}</td>
                        <td class="align-middle">S/ {{ $ped->total_pagar + $ped->igv + $ped->precio_envio - $ped->descuento }}</td>
                        <td class="align-middle">
                            <div class="media align-items-center">
                                <button class="btn btn-outline-info btn-icon mb-2 mr-2" type="button" onclick="mostrar_detalle_pedido('{{ $ped->pedido_id }}')">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button class="btn btn-outline-warning btn-icon mb-2 mr-2" type="button" onclick="mostrar_discusion_pedido('{{ $ped->pedido_id }}')">
                                    <i class="fas fa-comments"></i>
                                </button>
                            </div>
                        </td>
                    </tr>

                @endforeach

            </tbody>
        </table>
    </div>

    <div class="row my-4">

        {!! $pedidos->links() !!}

    </div>

@else

    <div class="table-responsive">
        <table class="table table-hover bg-light font-size-sm border-bottom" id="tablePedido">
            <thead>
                <tr>
                    <th class="align-middle">Tienda</th>
                    <th class="align-middle">Estado</th>
                    <th class="align-middle">Llegada</th>
                    <th class="align-middle">Total</th>
                    <th class="align-middle">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <tr class="tr_direcciones">
                    <td class="align-middle text-center" colspan="5">
                        <div class="media align-items-center">
                            <div class="media-body pl-3">
                                <span class="text-dark font-weight-medium">No hay pedidos en este estado</span>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

@endif
