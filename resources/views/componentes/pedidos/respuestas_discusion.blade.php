@if ($respuestas)

    @foreach ($respuestas->ModeloRespuestaReclamos as $resp)

        @if ($resp->tipo == 1)

            <div class="media text-right" style="margin: 10px !important; " >
                <div class="media-body">
                    <div class="card text-primary border-primary" >
                        <div class="card-body">
                            <span class="font-size-sm text-muted">{{ $resp->contenido }}</span>
                        </div>
                    </div>
                </div>
                <img class="rounded-circle ml-3" src="{{ asset('/img/default_img.png') }}" style="height: 55px; width: 55px;"/>
            </div>

        @else

            <div class="media" style="margin: 10px !important; ">
                <img class="rounded-circle mr-3" src="{{ asset('/img/default-store.png') }}" style="height: 55px; width: 55px;"/>
                <div class="media-body">
                    <div class="card text-primary border-success">
                        <div class="card-body">
                            <span class="font-size-sm text-muted">{{ $resp->contenido }}</span>
                        </div>
                    </div>
                </div>
            </div>

        @endif

    @endforeach

@else

    <div class="media text-center m-5">
        <div class="media-body">
            <div class="card text-primary border-primary" >
                <div class="card-body">
                    <span class="font-size-sm text-muted">No hay mensajes aun...</span>
                    <br>
                    <span class="font-size-sm text-muted">Envíe un mensaje para iniciar la conversación</span>
                </div>
            </div>
        </div>
    </div>

@endif
