

    <div class="modal-header px-2 pt-2 pb-2">
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3"></div>
        <center class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-6 align-center">
            <img class="rounded-circle mb-2" src="https://tiendas.ceamarket.com/img/{{ $discuss->ModeloEmpresa->icono }}" style="height: 100px; width: 100px;">
            <br>
            <div>
                <div class="h5 text-center" id="nombre_valor">
                    Pedido Nro:
                    <strong><a href="#" onclick="mostrar_qr_pedido('{{ $discuss->codigo }}')">{{ $discuss->codigo }}</a></strong>
                </div>
            </div>
        </center>
        <div class="col-1 col-sm-1 col-md-2 col-lg-3 col-xl-3">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <div class="modal-body">

        <label for="account-phone"><strong>Mensajes: </strong></label>

        <div style="background-color: #f5f8f9; width: 100%; height: 300px; overflow: auto; border-radius: 10px;" id="contentMessages">

        </div>

        @if ($discuss->estado < 4)

            <form class="mt-4" id="formulario_discuss" name="formulario_discuss">
                @csrf
                <input type="hidden" name="pedido_id" value="{{ $discuss->pedido_id }}">
                <div class="input-group"  >
                    <input   class="form-control" id="mensaje" name="mensaje" type="text" placeholder="Escriba su mensaje aquí..."/>
                    <div class="input-group-append">
                        <button class="btn btn-outline-primary" type="submit">
                            <i class="far fa-paper-plane"></i>
                        </button>
                    </div>
                </div>

                <div class="col-md-12 p-0 mt-3">
                    <div class="form-group">
                        <div class="progress" id="div_barra_progress_discuss">
                            <div id="barra_progress_discuss" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 p-0 mt-3">
                    <div id="contenedor_de_errores_discuss"></div>
                </div>
            </form>

        @else

            <div class="media text-center mt-4">
                <div class="media-body">
                    <div class="card text-primary border-primary" >
                        <div class="card-body">
                            <span class="font-size-sm text-muted">
                                Su pedido se encuentra
                                @if ($discuss->estado == 4)
                                    <strong>Entregado</strong>,
                                @elseif($discuss->estado == 5)
                                    <strong>Rechazado</strong>,
                                @endif
                                cualquier reclamo debe hacerse directamente en la tienda.
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        @endif

    </div>
