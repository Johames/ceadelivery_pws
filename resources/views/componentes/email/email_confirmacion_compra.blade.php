<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
    <head>

    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width" name="viewport"/>

    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>

    <title></title>

    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        table, td, tr {
            vertical-align: top;
            border-collapse: collapse;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }
    </style>
    <style id="media-query" type="text/css">
            @media (max-width: 660px) {

                .block-grid, .col {
                    min-width: 320px !important;
                    max-width: 100% !important;
                    display: block !important;
                }

                .block-grid {
                    width: 100% !important;
                }

                .col {
                    width: 100% !important;
                }

                .col_cont {
                    margin: 0 auto;
                }

                img.fullwidth,
                img.fullwidthOnMobile {
                    max-width: 100% !important;
                }

                .no-stack .col {
                    min-width: 0 !important;
                    display: table-cell !important;
                }

                .no-stack.two-up .col {
                    width: 50% !important;
                }

                .no-stack .col.num2 {
                    width: 16.6% !important;
                }

                .no-stack .col.num3 {
                    width: 25% !important;
                }

                .no-stack .col.num4 {
                    width: 33% !important;
                }

                .no-stack .col.num5 {
                    width: 41.6% !important;
                }

                .no-stack .col.num6 {
                    width: 50% !important;
                }

                .no-stack .col.num7 {
                    width: 58.3% !important;
                }

                .no-stack .col.num8 {
                    width: 66.6% !important;
                }

                .no-stack .col.num9 {
                    width: 75% !important;
                }

                .no-stack .col.num10 {
                    width: 83.3% !important;
                }

                .video-block {
                    max-width: none !important;
                }

                .mobile_hide {
                    min-height: 0px;
                    max-height: 0px;
                    max-width: 0px;
                    display: none;
                    overflow: hidden;
                    font-size: 0px;
                }

                .desktop_hide {
                    display: block !important;
                    max-height: none !important;
                }
            }
        </style>
    </head>
    <body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;">
        <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="table-layout: fixed; vertical-align: top; min-width: 320px; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; width: 100%;" valign="top" width="100%">
            <tbody>
                <tr style="vertical-align: top;" valign="top">
                    <td style="word-break: break-word; vertical-align: top;" valign="top">
                        <div style="background-color:transparent;">
                            <div class="block-grid" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #ffde79;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#ffde79;">
                                    <div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:20px; padding-right: 0px; padding-left: 0px;">

                                                <div style="background-color: white;margin-left: 5px;margin-right: 5px;">
                                                    <div class="block-grid" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                                        <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                                            <div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
                                                                <div class="col_cont" style="width:100% !important;">
                                                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:20px; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">
                                                                        <div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;">
                                                                            <a href="https://ceamarket.com" style="outline:none" tabindex="-1" target="_blank">
                                                                                <img align="center" alt="Logo Cea Market" border="0" class="center autowidth" src="{{ asset('/img/mail/mail_logo_cea_market.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 251px; display: block;" title="Logo" width="251">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="color:#0c2b5b;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:15px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #0c2b5b; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 22px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 26px; margin: 0;">
                                                            <span style="font-size: 22px;">
                                                                <strong>
                                                                    Estimado
                                                                    @if (strlen($data->ModeloPersona->ruc_dni) > 8)
                                                                        @if ($data->ModeloPersona->razon_social)
                                                                            {{ $data->ModeloPersona->razon_social }}
                                                                        @else
                                                                            {{ $data->ModeloPersona->nombre_comercial }}
                                                                        @endif
                                                                    @else
                                                                        {{ $data->ModeloPersona->nombres }} {{ $data->ModeloPersona->apellidos }}
                                                                    @endif,
                                                                </strong>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div style="color:#0c2b5b;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #0c2b5b; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                    <p style="font-size: 34px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 41px; margin: 0;">
                                                        <span style="font-size: 34px;">
                                                            <strong>Tu pedido ha sido registrado</strong>
                                                        </span>
                                                    </p>
                                                    </div>
                                                </div>
                                                <div style="color:#555555;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="line-height: 1.2; font-size: 12px; color: #555555; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14px;">
                                                        <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                            Se ha notificado a la tienda correspondiente la cual se encargará de los siguientes pasos del pedido así como también de su entrega en la dirección respectiva.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid three-up" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #8cc0e8;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#8cc0e8;">
                                    <div class="col num4" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 212px; width: 213px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">
                                                <div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;">
                                                    <img align="center" alt="Image" border="0" class="center autowidth" src="{{ asset('/img/mail/icon-01_2.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 24px; display: block;" title="Image" width="24"/>
                                                </div>
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:20px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.5; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 21px;">
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            Nro. Pedido:
                                                        </p>
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            <strong>{{ $data->codigo }}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num4" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 212px; width: 213px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">
                                                <div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;">
                                                    <img align="center" alt="Image" border="0" class="center autowidth" src="{{ asset('/img/mail/icon-02_2.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 22px; display: block;" title="Image" width="22"/>
                                                </div>
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.5; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 21px;">
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            Fecha Emisión:
                                                        </p>
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            <strong>{{ $data->fecha_pedido }}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num4" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 212px; width: 213px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:15px; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">
                                                <div align="center" class="img-container center autowidth" style="padding-right: 0px;padding-left: 0px;">
                                                    <img align="center" alt="Image" border="0" class="center autowidth" src="{{ asset('/img/mail/icon-03_2.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; width: 100%; max-width: 25px; display: block;" title="Image" width="25"/>
                                                </div>
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.5; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 21px;">
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            Monto Total:
                                                        </p>
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            <strong>S/ {{ $data->total_pagar + $data->ModeloDetalleMetodoEnvio->precio + $data->ModeloDetalleMetodoPago->precio_comision + $data->igv - $data->descuento }}</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid mixed-two-up" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #5c98c7;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#5c98c7;">
                                    <div class="col num9" style="display: table-cell; vertical-align: top; min-width: 320px; max-width: 477px; width: 480px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:10px; padding-right: 30px; padding-left: 30px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:0px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 20px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 24px; margin: 0;">
                                                            <span style="font-size: 20px;">
                                                                <strong>Tienda:</strong>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:0px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.5; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 21px;">
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 21px; margin: 0;">
                                                            @if ($data->ModeloEmpresa->razon_social)
                                                                {{ $data->ModeloEmpresa->razon_social }}
                                                            @else
                                                                {{ $data->ModeloEmpresa->nombre_comercial }}
                                                            @endif,
                                                        </p>
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 21px; margin: 0;">
                                                            {{ $data->ModeloEmpresa->direccion }},
                                                        </p>
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: left; mso-line-height-alt: 21px; margin: 0;">
                                                            @if (count($data->ModeloEmpresa->ModeloContactos) > 0)
                                                                @foreach ($data->ModeloEmpresa->ModeloContactos as $telefono)
                                                                    @if ($telefono->tipo == 0)
                                                                        {{ $telefono->nombre }}
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num3" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 159px; width: 160px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:30px; padding-bottom:10px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.5; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 21px;">
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            Monto Total:
                                                        </p>
                                                        <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;">
                                                            <strong>
                                                                S/ {{ $data->total_pagar + $data->ModeloDetalleMetodoEnvio->precio + $data->ModeloDetalleMetodoPago->precio_comision + $data->igv - $data->descuento }}
                                                            </strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid five-up no-stack" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #6da3cd;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#6da3cd;">
                                    <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                            <strong>Nro.</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num4" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 212px; width: 213px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                            <strong>DESCRIPCIÓN</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                            <strong>UND.</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                            <strong>CANT.</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;"><strong>PRECIO</strong></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if ($data->ModeloDetallePedidos)

                            @php
                                $count = 1;
                            @endphp

                            @foreach ($data->ModeloDetallePedidos as $item)

                                <div style="background-color:transparent;">
                                    <div class="block-grid five-up no-stack" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #5c98c7;">
                                        <div style="border-collapse: collapse;display: table;width: 100%;background-color:#5c98c7;">
                                            <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                                <div class="col_cont" style="width:100% !important;">
                                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:1px solid #6DA3CD; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                        <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                            <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                                <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                                    {{ $count }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col num4" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 212px; width: 213px;">
                                                <div class="col_cont" style="width:100% !important;">
                                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:1px solid #6DA3CD; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                        <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:5px;padding-right:5px;padding-bottom:0px;padding-left:5px;">
                                                            <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                                <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 17px; margin: 0;">
                                                                    <strong>
                                                                        {{ $item->ModeloDetalleProducto->Modeloproducto->nombre }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:0px;padding-right:5px;padding-bottom:5px;padding-left:5px;">
                                                            <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                                <p style="font-size: 10px; line-height: 1.2; word-break: break-word; text-align: left; mso-line-height-alt: 12px; margin: 0;">
                                                                    <span style="font-size: 10px;">
                                                                        {{ $item->ModeloDetalleProducto->descripcion }}
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                                <div class="col_cont" style="width:100% !important;">
                                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:1px solid #6DA3CD; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                        <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                            <div style="line-height: 1.2; font-size: 12px; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14px;">
                                                                <p style="text-align: center; line-height: 1.2; word-break: break-word; font-size: 14px; mso-line-height-alt: 17px; margin: 0;">
                                                                    <span style="font-size: 14px;">
                                                                        <strong>
                                                                            {{ $item->ModeloDetalleProducto->ModeloUnidad->abreviatura }}
                                                                        </strong>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                                <div class="col_cont" style="width:100% !important;">
                                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:1px solid #6DA3CD; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                        <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                            <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                                <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                                    <strong>
                                                                        {{ $item->cantidad }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col num2" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 106px; width: 106px;">
                                                <div class="col_cont" style="width:100% !important;">
                                                    <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:1px solid #6DA3CD; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                        <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                            <div style="font-size: 14px; line-height: 1.2; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                                <p style="font-size: 14px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 17px; margin: 0;">
                                                                    <strong>
                                                                        S/ {{ $item->precio }}
                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @php
                                    $count++;
                                @endphp

                            @endforeach

                        @endif

                        <div style="background-color:transparent;">
                            <div class="block-grid two-up" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #5c98c7;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#5c98c7;">
                                    <div class="col num6" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 318px; width: 320px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:25px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.5;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.5; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 21px;">
                                                        <p style="font-size: 16px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 24px; margin: 0;">
                                                            <span style="font-size: 16px;">Método de Pago:</span>
                                                            <br/>
                                                            <span style="font-size: 20px;">
                                                                <strong>{{ $data->ModeloDetalleMetodoPago->ModeloMetodoPago->nombre }}</strong>
                                                            </span>
                                                        </p>
                                                        @if ($data->voucher)
                                                            <p style="font-size: 14px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 21px; margin: 0;"> </p>
                                                            <p style="font-size: 12px; line-height: 1.5; word-break: break-word; text-align: center; mso-line-height-alt: 18px; margin: 0;">
                                                                <span style="font-size: 12px;">
                                                                    <u>puede ver su constancia de pago</u>&nbsp;
                                                                    <strong>
                                                                        @if (file_exists(public_path().'/img/'.$data->voucher))
                                                                            <a href="{{ 'https://ceamarket.com/img/'.$data->voucher }}">aquí.</a>
                                                                            {{-- <a href="{{ 'http://127.0.0.1:8000/img/'.$data->voucher }}">aquí.</a> --}}
                                                                        @else
                                                                            <a href="https://tiendas.ceamarket.com/img/{{ $data->voucher }}">aquí.</a>
                                                                        @endif
                                                                    </strong>
                                                                </span>
                                                            </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col num6" style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 318px; width: 320px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#ffffff;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.8;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.8; color: #ffffff; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 25px;">
                                                        <p style="font-size: 16px; line-height: 1.8; word-break: break-word; text-align: center; mso-line-height-alt: 29px; margin: 0;">
                                                            <span style="font-size: 16px;">
                                                                @php
                                                                    $subtotal_array = explode(".", number_format((float) $data->total_pagar, 2, '.', ''));
                                                                @endphp

                                                                Subtotal :
                                                                <strong>
                                                                    S/ {{ $subtotal_array[0] }}.<small>{{ $subtotal_array[1] }}</small>
                                                                </strong>
                                                            </span>
                                                            <br/>
                                                            <span style="font-size: 16px;">
                                                                @php
                                                                    $delivery_array = explode(".", number_format((float) $data->precio_envio, 2, '.', ''));
                                                                @endphp

                                                                Delivery :
                                                                <strong>
                                                                    S/ {{ $delivery_array[0] }}.<small>{{ $delivery_array[1] }}</small>
                                                                </strong>
                                                            </span>
                                                            <br/>
                                                            <span style="font-size: 16px;">
                                                                @php
                                                                    $comision_array = explode(".", number_format((float) $data->precio_comision, 2, '.', ''));
                                                                @endphp

                                                                Comisión :
                                                                <strong>
                                                                    S/ {{ $comision_array[0] }}.<small>{{ $comision_array[1] }}</small>
                                                                </strong>
                                                            </span>
                                                            <br />
                                                            <span style="font-size: 16px;">
                                                                @php
                                                                    $igv_array = explode(".", number_format((float) $data->igv, 2, '.', ''));
                                                                @endphp

                                                                I.G.V &nbsp; &nbsp; &nbsp;:
                                                                <strong>
                                                                    S/ {{ $igv_array[0] }}.<small>{{ $igv_array[1] }}</small>
                                                                </strong>
                                                            </span>
                                                            <br />
                                                            <span style="font-size: 16px;">
                                                                @php
                                                                    $descuento_array = explode(".", number_format((float) $data->descuento, 2, '.', ''));
                                                                @endphp

                                                                Descuento :
                                                                <strong>
                                                                    S/ {{ $descuento_array[0] }}.<small>{{ $descuento_array[1] }}</small>
                                                                </strong>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: #5c98c7;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:#5c98c7;">
                                    <div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:2px solid #FFFFFF; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#555555;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="line-height: 1.2; font-size: 12px; color: #555555; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 14px;">
                                                        <p style="line-height: 1.2; word-break: break-word; mso-line-height-alt: 14px; margin: 0;">
                                                            <span style="color: #ffffff;">
                                                                <strong>Gracias por su Compra</strong>
                                                            </span>
                                                        </p>
                                                        <p style="line-height: 1.2; word-break: break-word; mso-line-height-alt: 14px; margin: 0;"> </p>
                                                        <p style="line-height: 1.2; word-break: break-word; mso-line-height-alt: 14px; margin: 0;">
                                                            <span style="color: #ffffff;">
                                                                Atentamente,
                                                            </span>
                                                        </p>
                                                        <p style="line-height: 1.2; word-break: break-word; mso-line-height-alt: 14px; margin: 0;">
                                                            <span style="color: #ffffff;">
                                                                <strong>
                                                                    <a href="https://ceamarket.com/store/{{ $data->ModeloEmpresa->slug }}">
                                                                        @if ($data->ModeloEmpresa->razon_social)
                                                                            {{ $data->ModeloEmpresa->razon_social }}
                                                                        @else
                                                                            {{ $data->ModeloEmpresa->nombre_comercial }}
                                                                        @endif
                                                                    </a> y <a href="https://ceamarket.com">ceamarket.com</a>
                                                                </strong>
                                                            </span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                                <table cellpadding="0" cellspacing="0" class="social_icons" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" valign="top" width="100%">
                                                    <tbody>
                                                        <tr style="vertical-align: top;" valign="top">
                                                            <td style="word-break: break-word; vertical-align: top; padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;" valign="top">
                                                                <table align="center" cellpadding="0" cellspacing="0" class="social_table" role="presentation" style="table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-tspace: 0; mso-table-rspace: 0; mso-table-bspace: 0; mso-table-lspace: 0;" valign="top">
                                                                    <tbody>
                                                                        <tr align="center" style="vertical-align: top; display: inline-block; text-align: center;" valign="top">
                                                                            <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 10px; padding-left: 0;" valign="top">
                                                                                <a href="https://es-la.facebook.com/ceatecsoft/" target="_blank">
                                                                                    <img alt="Facebook" height="32" src="{{ asset('/img/mail/facebook.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="Facebook" width="32"/>
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 10px; padding-left: 0;" valign="top">
                                                                                <a href="https://twitter.com/ceatecsoft" target="_blank">
                                                                                    <img alt="Twitter" height="32" src="{{ asset('/img/mail/twitter.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="Twitter" width="32"/>
                                                                                </a>
                                                                            </td>
                                                                            <td style="word-break: break-word; vertical-align: top; padding-bottom: 5px; padding-right: 10px; padding-left: 0;" valign="top">
                                                                                <a href="https://www.youtube.com/channel/UCB7NwF5KHkDshY6OolsXEPA" target="_blank">
                                                                                    <img alt="YouTube" height="32" src="{{ asset('/img/mail/youtube.png') }}" style="text-decoration: none; -ms-interpolation-mode: bicubic; height: auto; border: 0; display: block;" title="YouTube" width="32"/>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:transparent;">
                            <div class="block-grid" style="min-width: 320px; max-width: 640px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; Margin: 0 auto; background-color: transparent;">
                                <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                                    <div class="col num12" style="min-width: 320px; max-width: 640px; display: table-cell; vertical-align: top; width: 640px;">
                                        <div class="col_cont" style="width:100% !important;">
                                            <div style="border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                                <div style="color:#555555;font-family:Lato, Tahoma, Verdana, Segoe, sans-serif;line-height:1.2;padding-top:10px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
                                                    <div style="font-size: 14px; line-height: 1.2; color: #555555; font-family: Lato, Tahoma, Verdana, Segoe, sans-serif; mso-line-height-alt: 17px;">
                                                        <p style="font-size: 11px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 13px; margin: 0;">
                                                            <span style="font-size: 11px;">Recibiste este email desde  <b>ceamarket</b></span>
                                                        </p>
                                                        <p style="font-size: 11px; line-height: 1.2; word-break: break-word; text-align: center; mso-line-height-alt: 13px; margin: 0;">
                                                            <span style="font-size: 11px;">visítenos en https://ceamarket.com</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
