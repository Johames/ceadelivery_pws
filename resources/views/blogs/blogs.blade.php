@extends('principal')

@section('title_header')
 Blogs
@endsection

@section('carroucel')

    {{--  --}}

@endsection

@section('content')

    <br class="mt-5 pt-5"><br><br>

    @if ($blogs)

        <section class="container-fluid mt-2 pt-2 pb-2 mb-4">
            <div class="row justify-content-between">
                <div class="col-12 pt-3 pb-2">
                    <h2 class="text-center">Revise los temas de nuestro blog</h2>
                </div>
                @foreach ($blogs as $item)

                    <div class="col-12 col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 mt-3">
                        <article class="card">
                            <div class="blog-entry-thumb">
                                <img class="card-img-top" src="https://admin.ceamarket.com/img/{{ $item->banner }}" alt="{{ $item->titulo }}" style="height: 270px;">
                            </div>
                            <div class="card-body">
                                <h2 class="h6 blog-entry-title">
                                    <a href="javascript:mostrarModalBlog({{ $item->blog_id }})">{{ $item->titulo }}</a>
                                </h2>
                                <p class="font-size-sm">
                                    {!! str_limit(strip_tags($item->contenido), $limit = 100, $end = '...') !!}
                                </p>
                                <p class="btn-tag mr-2 mb-2">{{ $item->categoria }}</p>
                            </div>
                            <div class="card-footer d-flex align-items-center font-size-xs">
                                <div class="blog-entry-meta-link">
                                    <div class="blog-entry-author-ava">
                                        <img src="{{ asset('img/default_img.png') }}" alt="Emma Gallaher">
                                    </div>
                                    {{ $item->autor }}
                                </div>
                            <div class="ml-auto text-nowrap">
                                    <p class="blog-entry-meta-link text-nowrap">{{ $item->created_at }}</p>
                                    <span class="blog-entry-meta-divider mx-2"></span>
                                    <h6 class="blog-entry-meta-link text-nowrap" data-toggle="tooltip" data-placement="bottom" title="Comentarios">
                                        <i class="fas fa-comment-alt"></i>
                                        {{ count($item->ModeloComentariosBlogs) }}
                                    </h6>
                            </div>
                            </div>
                        </article>
                    </div>

                @endforeach

            </div>
        </section>

    @endif

@endsection

@section('pagos')

    {{--  --}}

@endsection

@section('blogs')

    {{--  --}}

@endsection

@section('modals')

    {{-- Contenido del Blog --}}
    <div id="modalBlog" class="modal fade modal-right" tabindex="-1" role="dialog">
        <div class="modal-dialog tamaño_blog" role="document">
            <div class="modal-content modal-dialog-scrollable" id="contentModalBlog">

            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_web/ajaxBlog.js') }}"></script>

@endsection

