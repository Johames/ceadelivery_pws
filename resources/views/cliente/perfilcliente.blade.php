@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

    <!-- Fondo Oscuro -->
    <div class="page-title-overlap bg-dark mt-5 pt-5">
        <div class="container d-lg-flex justify-content-between"></div>
    </div>

    <!-- Contenido de la Pagina -->
    <div class="container pb-5 mb-2 mb-md-3">
        <div class="row">

            @include('secciones.admin_client.aside')

            <section class="col-lg-8">
                <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-3">
                    <div class="media align-items-center">
                        <img src="/img/default_img.png" id="icono_info" class="img-thumbnail" width="120" alt="Mi perfil">
                        <span class="badge badge-light p-2" style="margin-top: 4.3rem; margin-left: -1.2rem; cursor: pointer;" data-target="#modal_avatar" data-toggle="modal">
                            <i  data-toggle="tooltip" title="Cambiar foto perfil" class="fas fa-camera"></i>
                        </span>
                        <div class="media-body pl-3 col-xs-12">
                            <div class="p mb-0 font-size-ms">
                                <h4>PERFIL DE USUARIO</h4>
                            </div>
                            <div class="row justify-content-between mx-2">
                                <button class="btn btn-warning mt-3 mt-sm-0" id="btn_modificar_perfil" type="button">Modificar Perfil</button>
                                <button class="btn btn-info mt-3 mt-sm-0" type="button" onclick="mostrarCiudades()">Cambiar Ciudad</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-0 m-0">
                    <div class="card col-12">
                        <div class="card-body">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="row">
                                            <ul class="col-12 list-unstyled mb-0">
                                                <li class="media pb-3 border-bottom">
                                                    <i class="cea-icon-location font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">DNI</span>
                                                        <span class="d-block text-heading font-size-sm" id="view_ruc_dni"></span>
                                                    </div>
                                                </li>
                                                <li class="media pt-2 pb-3 border-bottom">
                                                    <i class="cea-icon-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Nombres</span>
                                                        <span class="d-block text-heading font-size-sm" id="view_nombres_razon_social"></span>
                                                    </div>
                                                </li>
                                                <li class="media pt-2m">
                                                    <i class="cea-icon-mail font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Representante Legal</span>
                                                        <span class="d-block text-heading font-size-sm" id="view_representante_legal"></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="row">
                                            <ul class="col-12 list-unstyled mb-0">
                                                <li class="media pb-3 border-bottom">
                                                    <i class="cea-icon-location font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">N° Teléfono</span>
                                                        <span class="d-block text-heading font-size-sm" id="view_telefono"></span>
                                                    </div>
                                                </li>
                                                <li class="media pt-2 pb-3 border-bottom">
                                                    <i class="cea-icon-phone font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Apellidos</span>
                                                        <span class="d-block text-heading font-size-sm" id="view_apellidos_nombre_comercial"></span>
                                                    </div>
                                                </li>
                                                <li class="media pt-2m">
                                                    <i class="cea-icon-mail font-size-lg mt-2 mb-0 text-primary"></i>
                                                    <div class="media-body pl-3">
                                                        <span class="font-size-ms text-muted">Correo Electrónico</span>
                                                        <span class="d-block text-heading font-size-sm" id="view_correo">{{ Auth::user()->email }}</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection

@section('modals')

    <div class="modal fade" id="modal_cliente_perfil" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <form class="needs-validation" id="formulario_cliente_perfil">
            @csrf
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">MODIFICAR PERFIL DE USUARIO</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="account-fn">Nro de DNI</label>
                                    <input class="form-control" type="number" id="ruc_dni" name="ruc_dni" placeholder="10484325290" disabled>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="account-email">Teléfono</label>
                                    <input class="form-control" type="tel" id="telefono_perfil" name="telefono_perfil" placeholder="Nro Telefono" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label>Rellenar según sea el caso</label>
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="account-email">Nombres</label>
                                                    <input class="form-control" type="text" id="nombres" name="nombres" placeholder="No llenar si es RUC">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="account-phone">Apellidos</label>
                                                    <input class="form-control" type="text" id="apellidos" name="apellidos" placeholder="No llenar si es RUC">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="account-email">Representante Legal</label>
                                    <input class="form-control" type="text" id="representante_legal" name="representante_legal" placeholder="Rellenar si tuviera">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary btn-shadow btn-sm" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="modal fade" id="modal_avatar" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">
        <form class="needs-validation was-validated" id="formulario_avatar" name="formulario_avatar">
            @csrf
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Subir mi foto de perfil</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="cea-file-drop-area">
                                    <div class="cea-file-drop-icon cea-icon-cloud-upload"></div>
                                    <span class="cea-file-drop-message">Arrastra y suelta aquí para subir</span>
                                    <input type="file" class="cea-file-drop-input" id="avatar" name="avatar">
                                    <button type="button" class="cea-file-drop-btn btn btn-primary btn-sm">O seleccione archivo</button>
                                </div>
                            </div>

                            <div class="col-sm-12"><br></div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="progress" id="div_barra_progress_avatar">
                                        <div id="barra_progress_avatar" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div id="contenedor_de_errores_avatar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary btn-shadow btn-sm" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxPerfil.js') }}"></script>

@endsection
