@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

<!-- Fondo Oscuro -->
<div class="page-title-overlap bg-dark mt-5 pt-5">
    <div class="container d-lg-flex justify-content-between"></div>
</div>

<!-- Contenido de la Pagina -->
<div class="container pb-5 mb-2 mb-md-3">
    <div class="row">

        @include('secciones.admin_client.aside')

        <section class="col-lg-8">
            <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-3">
                <div class="media align-items-center">
                    <div class="media-body pl-3">
                        <div class="p mb-0 font-size-ms">
                            <h4>CEA Puntos</h4>
                            <span class="text-muted">Administra la configuración de CEA Puntos</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </section>
    </div>
</div>

@endsection

@section('modals')

@endsection

@section('js')
{{-- <script type="text/javascript" src="{{ asset('js/ajaxBackupsjdhsajkhdjkashdjkashkdjs.js') }}">
</script> --}}
@endsection
