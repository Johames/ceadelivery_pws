@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

<style>
    #map {
        height: 300px;
        width: 100%;
    }

    .map-responsive {
        overflow: hidden;
        padding-bottom: 300px;
        position: relative;
        height: 0;
    }
</style>

@endsection

@section('faded')

    class="bg-faded-success"

@endsection

@section('content')

    <!-- Page Title-->
    <div class="page-title-overlap bg-dark mt-5 pt-5">
        <div class="container d-lg-flex justify-content-between"></div>
    </div>

    <!-- Contenido de la Pagina -->
    <div class="container pb-5 mb-2 mb-md-3">
        <div class="row">

            @include('secciones.admin_client.aside')

            <section class="col-lg-8">
                <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-4">
                    <div class="media align-items-center">
                        <div class="media-body pl-3">
                            <div class="p mb-0 font-size-ms">
                                <h4>Administrar Direcciones</h4>
                            </div>
                            <div class="custom-control custom-option custom-control-inline mb-2">
                                <input type="radio" class="custom-control-input" id="activas" name="filtro" value="Activas" checked>
                                <label for="activas" class="custom-option-label">Activas</label>
                            </div>
                            <div class="custom-control custom-option custom-control-inline mb-2">
                                <input type="radio" class="custom-control-input" id="inactivas" name="filtro" value="Inactivas">
                                <label for="inactivas" class="custom-option-label">Inactivas</label>
                            </div>
                        </div>
                        <div class="d-flex flex-wrap justify-content-between align-items-right">
                            <button class="btn btn-primary mt-3 mt-sm-0" type="button" data-toggle="modal" data-target="#modal_direcciones">Agregar Dirección</button>
                        </div>
                    </div>
                </div>
                <div class="row p-0 m-0">
                    <div class="table-responsive mt-2">
                        <table class="table table-hover bg-light font-size-sm border-bottom" id="tabla_direcciones">

                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection

@section('modals')

    <div class="modal fade" id="modal_direcciones" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <form class="needs-validaton" id="formulario_direcciones">
            @csrf
            <input type="hidden" id="direciones_id" name="direciones_id">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">AGREGAR O MODIFICAR DIRECCIONES</h4>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body font-size-sm">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="direccion">Dirección</label>
                                    <div class="input-group">
                                        <input class="form-control" type="text" id="direccion" name="direccion" placeholder="Ejemplo: Jr. direccion #nro - ciudad" data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" title="Importante" data-content="En su direccion indique al final la ciudad de esta direccion (direccion - ciudad) y luego click sobre el boton naranja para marcar la direccion en el mapa.">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Marcar en mapa" id="btn_buscar_direccion" type="button" style="border-top-right-radius: 5px; border-bottom-right-radius: 5px;">
                                                <i class="fas fa-map-marked-alt"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="referencia">Referencia</label>
                                    <input class="form-control" type="text" id="referencia" name="referencia" placeholder="Cerca de..." >
                                </div>
                            </div>

                            <div class="alert alert-warning alert-with-icon mx-3 mb-1" role="alert">
                                <div class="alert-icon-box">
                                    <i class="alert-icon cea-icon-security-announcement"></i>
                                </div>
                                La ubicación del maracador no siempre es exacta, por favor de click sobre la ubicacion exacta de su direccion para colocar el marcador ahi.
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" id="coordenadas" name="coordenadas" >
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="map-responsive">
                                    <div id="map"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="progress" id="div_barra_progress_direcciones">
                                        <div id="barra_progress_direcciones" class="progress-bar progress-bar-striped bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="contenedor_de_errores_direcciones"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cerrar</button>
                        <button class="btn btn-primary btn-shadow btn-sm" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('js')

    <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCb1-FEYYbdk2hfSG-TO5gCVm7t1j5PJfg'></script>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/noisy/1.2/jquery.noisy.min.js'></script>
    <script src="{{ asset('/funciones/mapas/map_marker.js') }}"></script>

    <script src="{{ asset('/funciones/axfile.js') }}"></script>

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxDirecciones.js') }}"></script>

@endsection
