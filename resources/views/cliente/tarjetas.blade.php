@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

    {{--  --}}

@endsection

@section('faded')

    class="bg-faded-success"

@endsection

@section('content')

    <!-- Page Title-->
    <div class="page-title-overlap bg-dark mt-5 pt-5">
        <div class="container d-lg-flex justify-content-between"></div>
    </div>

    <!-- Contenido de la Pagina -->
    <div class="container pb-5 mb-2 mb-md-3">
        <div class="row">

            @include('secciones.admin_client.aside')

            <section class="col-lg-8">
                <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-4">
                    <div class="media align-items-center">
                        <div class="media-body pl-3">
                            <div class="p mb-0 font-size-ms">
                                <h4>Administrar Medios de Pago</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row p-0 m-0">
                    <div class="table-responsive mt-2">
                        <table class="table table-hover bg-light font-size-sm border-bottom" id="tabla_tarjetas">

                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection

@section('modals')

    {{--  --}}

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxTarjetas.js') }}"></script>

@endsection
