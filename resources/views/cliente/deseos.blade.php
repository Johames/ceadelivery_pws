@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

<!-- Fondo Oscuro -->
<div class="page-title-overlap bg-dark mt-5 pt-5">
    <div class="container d-lg-flex justify-content-between"></div>
</div>

<!-- Contenido de la Pagina -->
<div class="container pb-5 mb-2 mb-md-3">
    <div class="row">

        @include('secciones.admin_client.aside')

        <section class="col-lg-8">
            <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-3">
                <div class="media align-items-center">
                    <div class="media-body pl-3">
                        <div class="p mb-0 font-size-ms">
                            <h4>LISTAS DE DESEOS</h4>
                            <span class="text-muted">Administra tus listas de deseos</span>
                        </div>
                    </div>
                    <div class="d-flex flex-wrap justify-content-between align-items-right">
                        <button class="btn btn-primary mt-3 mt-sm-0" type="button" data-toggle="modal" data-target="#modal_deseos">Agregar lista de deseos</button>
                    </div>
                </div>
            </div>
            <div class="col-12 px-0 mx-0">
                <div class="accordion mt-2" id="accordion">
                </div>
            </div>
        </section>
    </div>
</div>

@endsection

@section('modals')

    <div class="modal fade" id="modal_detalle_deseos" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content" id="detailDeseos">

            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxDeseos.js') }}"></script>

@endsection
