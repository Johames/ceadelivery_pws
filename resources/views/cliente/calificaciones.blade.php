@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

<!-- Fondo Oscuro -->
<div class="page-title-overlap bg-dark mt-5 pt-5">
    <div class="container d-lg-flex justify-content-between"></div>
</div>

<!-- Contenido de la Pagina -->
<div class="container pb-5 mb-2 mb-md-3">
    <div class="row">

        @include('secciones.admin_client.aside')

        <section class="col-lg-8">
            <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-3">
                <div class="media align-items-center">
                    <div class="media-body pl-3">
                        <div class="p mb-0 font-size-ms">
                            <h4>Reseñas y Calificaciones</h4>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="Empresa" name="filtro" value="Empresas" checked>
                            <label for="Empresa" class="custom-option-label">De Empresas</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="Producto" name="filtro" value="Productos">
                            <label for="Producto" class="custom-option-label">De Productos</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="Deliverista" name="filtro" value="Deliverista">
                            <label for="Deliverista" class="custom-option-label">De Deliveristas</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0" id="lstsValoraciones">

            </div>
        </section>
    </div>
</div>

@endsection

@section('modals')

@endsection

@section('js')

    <script src="{{ asset('/ajax_admin/client/ajaxCalificaciones.js') }}"></script>

@endsection
