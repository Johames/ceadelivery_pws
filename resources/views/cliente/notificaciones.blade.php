@extends('principal')

@section('title_header')
     {{ $titulo }}
@endsection

@section('css')

@endsection

@section('faded')

class="bg-faded-success"

@endsection

@section('content')

<!-- Fondo Oscuro -->
<div class="page-title-overlap bg-dark mt-5 pt-5">
    <div class="container d-lg-flex justify-content-between"></div>
</div>

<!-- Contenido de la Pagina -->
<div class="container pb-5 mb-2 mb-md-3">
    <div class="row">

        @include('secciones.admin_client.aside')

        <section class="col-lg-8">
            <div class="bg-secondary box-shadow-lg rounded-lg p-4 mb-3">
                <div class="media align-items-center">
                    <div class="media-body pl-3">
                        <div class="p mb-0 font-size-ms">
                            <h4>Notificaciones</h4>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="leido" name="filtro" value="NoLeido" checked>
                            <label for="leido" class="custom-option-label">No Vistos</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="noleido" name="filtro" value="Leido">
                            <label for="noleido" class="custom-option-label">Vistos</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-12 p-0 m-0">

                <ul class="col-12 list-unstyled bg-light p-3" id="lstsNotificaciones">
                </ul>

            </div>
        </section>
    </div>
</div>

@endsection

@section('modals')

    <div class="modal" tabindex="-1" id="modal_notificacion" role="dialog">
        <div class="modal-dialog" role="document" >
            <div class="modal-content" style="border-radius: 15px !important;  ">

                <div class="modal-header">
                    <h5 class="modal-title">NOTIFICACION NUEVA</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="listar_notificaciones()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" id="detallNotificacion">

                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxNotificaciones.js') }}"></script>

@endsection
