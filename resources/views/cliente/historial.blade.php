@extends('principal')

@section('title_header')
{{ $titulo }}
@endsection

@section('css')

@endsection

@section('carroucel')

@endsection

@section('content')

<!-- Fondo Oscuro -->
<div class="page-title-overlap bg-dark mt-5 pt-5">
    <div class="container d-lg-flex justify-content-between"></div>
</div>

<!-- Contenido de la Pagina -->
<div class="container pb-5 mb-2 mb-md-3">
    <div class="row">

        @include('secciones.admin_client.aside')

        <section class="col-lg-8">
            <div class="bg-secondary box-shadow-lg box-shadow-lg rounded-lg p-4 mb-4">
                <div class="media align-items-center">
                    <div class="media-body pl-3">
                        <div class="p mb-0 font-size-ms">
                            <h4>Lista de Pedidos</h4>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="proceso" name="filtro" value="EnProceso" checked>
                            <label for="proceso" class="custom-option-label">En Proceso</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="confirmado" name="filtro" value="Confirmado">
                            <label for="confirmado" class="custom-option-label">Confirmados</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="camino" name="filtro" value="EnCamino">
                            <label for="camino" class="custom-option-label">En Camino</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="entregado" name="filtro" value="Entregado">
                            <label for="entregado" class="custom-option-label">Entregados</label>
                        </div>
                        <div class="custom-control custom-option custom-control-inline mb-2">
                            <input type="radio" class="custom-control-input" id="rechazado" name="filtro" value="Rechazado">
                            <label for="rechazado" class="custom-option-label">Rechazados</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row p-0 m-0" id="listapedidos">

            </div>
        </section>
    </div>
</div>

@endsection

@section('modals')

<div class="modal fade" id="DetallePedido" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content" id="detailsPedido">

        </div>
    </div>
</div>

<div class="modal fade" id="DiscusionPedido" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content" id="discussPedido">

        </div>
    </div>
</div>

@endsection

@section('js')

    <script src="{{ asset('/funciones/crud.js') }}"></script>
    <script src="{{ asset('/ajax_admin/client/ajaxHistorial.js') }}"></script>

@endsection
