<!DOCTYPE html>
<html>

    @section('htmlheader')
        @include('secciones.head')
    @show

    <body @yield('faded') style="background-color: #fafaf8;">

        <header class="bg-light box-shadow-sm fixed-top"><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
            <div class="navbar-sticky bg-light">

                @yield('header_nv_1')

                <div class="navbar navbar-expand-lg navbar-light">
                    <div class="container">

                        <a class="navbar-brand d-none d-sm-block mr-3 flex-shrink-0" href="{{ route('Inicio') }}">
                            <img width="110" src="{{ asset('/img/cea_market_logo.svg') }}" alt="Cea Market">
                        </a>
                        <a class="navbar-brand d-sm-none mr-2" href="{{ route('Inicio') }}">
                            <img src="{{ asset('img/cea_market_icon.svg') }}" style="height: 45px !important;" alt="Cea Market">
                        </a>

                        {{-- Buscador --}}
                        <div class="input-group-overlay mx-4 col-md-6 d-none d-md-block">
                            <input class="form-control prepended-form-control appended-form-control" type="text" placeholder="Buscar Productos en todas las tiendas">
                            <div class="input-group-append-overlay" style="background-color: white;">
                                <button class="btn btn-outline-primary" type="submit" style="border-top-left-radius: 0px; border-bottom-left-radius: 0px;">
                                    <i class="fas fa-search white" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>

                        <div class="navbar-toolbar d-flex flex-shrink-0 align-items-center">

                            <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2 d-block d-md-none">
                                <div class="navbar-tool-icon-box">
                                    <i class="navbar-tool-icon fas fa-search" aria-hidden="true"></i>
                                </div>
                            </a>

                            @guest

                                <a class="navbar-tool ml-1 ml-lg-0 mr-n1 mr-lg-2">
                                    <div class="navbar-tool-icon-box">
                                        <i class="navbar-tool-icon fas fa-user" aria-hidden="true"></i>
                                    </div>
                                    <div class="navbar-tool-text ml-n3">
                                        <small>Por favor</small>Ingresar
                                    </div>
                                </a>

                                <!-- CARRITO INVITADO -->
                                <div class="navbar-tool dropdown ml-3">
                                    <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" data-toggle="modal" href="#InicioSesion">
                                        <i class="navbar-tool-icon fas fa-shopping-cart"></i>
                                    </a>
                                    <a class="navbar-tool-text" href="javascript:void(0);">
                                        <small>Mi Carrito</small>S/  0.00
                                    </a>
                                </div>

                            @endguest

                        </div>
                    </div>
                </div>
            </div>
        </header>

        @yield('content')

        @include('secciones.js')

    </body>

</html>

