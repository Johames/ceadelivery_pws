@extends('principal')

@section('title_header')
    {{ $titulo }}
@endsection

@section('css')

    {{--  --}}

@endsection

@section('faded')

    class="bg-faded-success"

@endsection

@section('content')

    <!-- Page Title-->
    <div class="page-title-overlap bg-dark mt-5 pt-5">
        <div class="container d-lg-flex justify-content-between"></div>
    </div>

    <!-- Contenido de la Pagina -->
    <div class="container-fluid pb-5 mb-2 mb-md-3">
        <div class="row">

            {{-- @include('secciones.admin_client.aside_faq') --}}
            @if (count($preguntas) > 0)
                <aside class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 pt-0">
                    <div class="cea-sidebar-static rounded-lg box-shadow-lg px-0 py-0 mb-5" id="aside_client" style="max-width: 42rem !important;">

                        <div class="bg-secondary border-top px-4 py-3">
                            <h3 class="font-size-sm mb-0 text-muted">Preguntas Frecuentes</h3>
                        </div>
                        <ul class="list-unstyled mb-0">
                            @foreach ($preguntas as $item)
                                <li class="border-top border-bottom mb-0">
                                    <a class="nav-link-style d-flex align-items-center px-4 py-3" href="/preguntas_frecuentes/{{ $item->slug }}">
                                        <i class="fas fa-question-circle opacity-60 mr-2"></i>
                                        {{ $item->pregunta }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            @else
                <aside class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 pt-0">
                    <div class="cea-sidebar-static rounded-lg box-shadow-lg px-0 py-0 mb-5" id="aside_client" style="max-width: 42rem !important;">
                        <div class="bg-secondary border-top px-4 py-3">
                            <h3 class="font-size-sm mb-0 text-muted">No hay Preguntas Frecuentes</h3>
                        </div>
                    </div>
                </aside>
            @endif

            @if ($respuesta)
            <section class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
                <div class="bg-secondary box-shadow-lg rounded-lg p-4">
                    <div class="media align-items-center">
                        <div class="media-body pl-3">
                            <div class="font-size-ms">
                                <h4>{{ $respuesta->pregunta }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row m-0 p-0">
                    <div class="col-12 bg-light box-shadow rounded m-0 p-3">
                        {!! $respuesta->respuesta !!}
                    </div>
                </div>
            </section>
            @else
            <section class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
                <div class="bg-secondary box-shadow-lg rounded-lg p-4">
                    <div class="media align-items-center">
                        <div class="media-body pl-3">
                            <div class="font-size-ms">
                                <h4>Preguntas Frecuentes</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container-fluid">
                        <div class="row justify-content-center pt-lg-4 text-center">
                            <div class="col-11">
                                <h1 class="display-404">¿?</h1>
                                <h2 class="h3 mb-4">
                                    Selecciona una pregunta para ver la respuesta sugerida
                                </h2>
                                <p class="font-size-md mb-4">
                                    <u>
                                        O en su lugar, aquí hay algunos enlaces útiles:
                                    </u>
                                </p>
                            </div>
                        </div>
                        <div class="row justify-content-center mb-4">
                            <div class="col-xl-8 col-lg-10">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                        <a class="card h-100 border-0 box-shadow-sm" href="/">
                                            <div class="card-body">
                                                <div class="media align-items-center"><i class="cea-icon-home text-primary h4 mb-0"></i>
                                                    <div class="media-body pl-3">
                                                        <h5 class="font-size-sm mb-0">Inicio</h5>
                                                        <span class="text-muted font-size-ms">
                                                            Volver al inicio
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                                        <a class="card h-100 border-0 box-shadow-sm" href="/buscador/Todos">
                                            <div class="card-body">
                                                <div class="media align-items-center"><i class="cea-icon-search text-success h4 mb-0"></i>
                                                    <div class="media-body pl-3">
                                                        <h5 class="font-size-sm mb-0">Buscar Productos</h5>
                                                        <span class="text-muted font-size-ms">
                                                            Buscar en todas las tiendas
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            @endif

        </div>
    </div>

@endsection

@section('modals')

    <a href="javascript:modalAyudanos()">
        <div class="support_ubicacion" data-toggle="tooltip" data-placement="left" title="Ayudanos a mejorar" style="padding-top: .9rem !important; padding-left: 1.2rem !important;">
            <span class="full_help">
                <i class="fas fa-comments"></i>
            </span>
        </div>
    </a>

@endsection

@section('js')

    <script src="{{ asset('/ajax_web/ajaxPreguntas.js') }}"></script>

@endsection
