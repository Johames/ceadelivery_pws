<!DOCTYPE html>
<html>

    @section('htmlheader')
        @include('secciones.head')
    @show

    <body @yield('faded') style="background-color: #fafaf8;">

        @include('secciones.navbar')

        @yield('carroucel')

        @yield('content')

        {{-- @include('secciones.menu') --}}

        @include('secciones.menubottom')

        @yield('modals')

        @include('secciones.modals')

        <!-- Boton para ir Arriba -->
        <a class="btn-scroll-top" href="#top" data-scroll>
            <span class="btn-scroll-top-tooltip text-muted font-size-sm mr-2">Subir</span>
            <i class="btn-scroll-top-icon fas fa-chevron-up"> </i>
        </a>

        @yield('socialbuttons')

        @include('secciones.servicios')

        <footer class="bg-dark">

            @include('secciones.footer')

        </footer>

        @include('secciones.js')

    </body>

</html>
