-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: cea_delivery
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cd_categoria`
--

DROP TABLE IF EXISTS `cd_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_categoria` (
  `categoria_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_categoria`
--

LOCK TABLES `cd_categoria` WRITE;
/*!40000 ALTER TABLE `cd_categoria` DISABLE KEYS */;
INSERT INTO `cd_categoria` VALUES (1,'Vidrieria','1'),(2,'Plastiqueria','1');
/*!40000 ALTER TABLE `cd_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_color`
--

DROP TABLE IF EXISTS `cd_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(45) DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_color`
--

LOCK TABLES `cd_color` WRITE;
/*!40000 ALTER TABLE `cd_color` DISABLE KEYS */;
INSERT INTO `cd_color` VALUES (1,'Negro','1'),(2,'Blanco','1'),(3,'Rojo','1'),(4,'Amarillo','1'),(5,'Celeste','1'),(6,'Verde','1'),(7,'Azul','1');
/*!40000 ALTER TABLE `cd_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_deseos`
--

DROP TABLE IF EXISTS `cd_deseos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_deseos` (
  `deseos_id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `nombre` varchar(500) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`deseos_id`),
  KEY `fk_cd_deseos_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_deseos_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_deseos`
--

LOCK TABLES `cd_deseos` WRITE;
/*!40000 ALTER TABLE `cd_deseos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_deseos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_detalle_pedido`
--

DROP TABLE IF EXISTS `cd_detalle_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_detalle_pedido` (
  `detalle_id` int(11) NOT NULL AUTO_INCREMENT,
  `pedido_id` int(11) NOT NULL,
  `producto_detalle_id` int(11) NOT NULL,
  `nombre_producto` varchar(600) DEFAULT NULL,
  `cantidad` double DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `igv` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`detalle_id`),
  KEY `fk_cd_detalle_cd_pedido1_idx` (`pedido_id`),
  KEY `fk_cd_detalle_cd_productos1_idx` (`producto_detalle_id`),
  CONSTRAINT `fk_cd_detalle_cd_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `cd_pedido` (`pedido_id`),
  CONSTRAINT `fk_cd_detalle_cd_productos1` FOREIGN KEY (`producto_detalle_id`) REFERENCES `cd_producto_detalle` (`producto_detalle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_detalle_pedido`
--

LOCK TABLES `cd_detalle_pedido` WRITE;
/*!40000 ALTER TABLE `cd_detalle_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_detalle_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_direciones`
--

DROP TABLE IF EXISTS `cd_direciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_direciones` (
  `direciones_id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lgn` varchar(100) NOT NULL,
  `referencia` varchar(1000) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`direciones_id`),
  KEY `fk_cd_direciones_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_direciones_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_direciones`
--

LOCK TABLES `cd_direciones` WRITE;
/*!40000 ALTER TABLE `cd_direciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_direciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_empresa`
--

DROP TABLE IF EXISTS `cd_empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_empresa` (
  `empresa_id` int(11) NOT NULL AUTO_INCREMENT,
  `rubro_id` int(11) NOT NULL,
  `razon_social` varchar(1000) DEFAULT NULL,
  `nombre_comercial` varchar(1000) DEFAULT NULL,
  `representante_legal` varchar(600) DEFAULT NULL,
  `ruc` varchar(11) DEFAULT NULL,
  `descripcion` varchar(2500) DEFAULT NULL,
  `icono` varchar(500) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `direccion` varchar(500) DEFAULT NULL,
  `referencia` varchar(1000) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lgn` varchar(100) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `facebook` varchar(145) DEFAULT NULL,
  `twitter` varchar(145) DEFAULT NULL,
  `instagram` varchar(145) DEFAULT NULL,
  `google+` varchar(145) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`empresa_id`),
  KEY `fk_cd_empresa_cd_rubro1_idx` (`rubro_id`),
  CONSTRAINT `fk_cd_empresa_cd_rubro1` FOREIGN KEY (`rubro_id`) REFERENCES `cd_rubro` (`rubro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_empresa`
--

LOCK TABLES `cd_empresa` WRITE;
/*!40000 ALTER TABLE `cd_empresa` DISABLE KEYS */;
INSERT INTO `cd_empresa` VALUES (1,1,'Johames SAC','Johames SAC','Johann James Valles Paz','10484325290',NULL,NULL,NULL,'Jr. Por Ahi','Por Ahi','-67136782','-8743782','917993330','johames.15@gmail.com','facebook.com','twitter.com','instagram.com','google.com','1'),(2,2,'Vidrieria Valles','Vidrieria Valles','Johann James Valles Paz','10484325291',NULL,NULL,NULL,'Jr. Por Alla','Por Alla','-8764367','-64367821','917993331','johann.valles @upeu.edu.pe','facebook.com','twitter.com','instagram.com','google.com','1'),(7,2,'Nakamura SAC','Nakamura SAC','No Recuerdo','01234567890',NULL,NULL,NULL,'Jr. Por Allu','Por Allu','-46783264','-674783264','917993330','nakamura@gmail.com','facebook.com','twitter.com','instagram.com','google.com','1'),(8,1,'Licoreria SAC','Licoreria SAC','No Recuerdo','12345678900',NULL,NULL,NULL,'Jr. Por Alla','Por Alla','-64873264','-873426344','917993330','licoreria@gmail.com','facebook.com','twitter.com','instagram.com','google.com','1'),(9,2,'Publicidad SAC','Publicidad SAC','No Recuerdo','23456789001',NULL,NULL,NULL,'Jr. Por Ahi','Por Ahi','-8648372647','-894623764923','917993330','publicidad@gmail.com','facebook.com','twitter.com','instagram.com','google.com','1');
/*!40000 ALTER TABLE `cd_empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_horario`
--

DROP TABLE IF EXISTS `cd_horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_horario` (
  `horario_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `dia` varchar(45) NOT NULL,
  `turnos` varchar(1) DEFAULT NULL,
  `hora_abre_am` varchar(10) DEFAULT NULL,
  `hora_cierre_am` varchar(10) DEFAULT NULL,
  `hora_abre_pm` varchar(10) DEFAULT NULL,
  `hora_cierre_pm` varchar(10) DEFAULT NULL,
  `descripcion` varchar(450) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`horario_id`),
  KEY `fk_cd_horario_cd_empresa1_idx` (`empresa_id`),
  CONSTRAINT `fk_cd_horario_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_horario`
--

LOCK TABLES `cd_horario` WRITE;
/*!40000 ALTER TABLE `cd_horario` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_marca`
--

DROP TABLE IF EXISTS `cd_marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_marca` (
  `marca_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`marca_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_marca`
--

LOCK TABLES `cd_marca` WRITE;
/*!40000 ALTER TABLE `cd_marca` DISABLE KEYS */;
INSERT INTO `cd_marca` VALUES (1,'Rey','1');
/*!40000 ALTER TABLE `cd_marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_menu`
--

DROP TABLE IF EXISTS `cd_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `fk_cd_menu_cd_menu1_idx` (`submenu_id`),
  CONSTRAINT `fk_cd_menu_cd_menu1` FOREIGN KEY (`submenu_id`) REFERENCES `cd_menu` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_menu`
--

LOCK TABLES `cd_menu` WRITE;
/*!40000 ALTER TABLE `cd_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_metodo_pago`
--

DROP TABLE IF EXISTS `cd_metodo_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_metodo_pago` (
  `metodo_pago_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`metodo_pago_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_metodo_pago`
--

LOCK TABLES `cd_metodo_pago` WRITE;
/*!40000 ALTER TABLE `cd_metodo_pago` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_metodo_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_metodo_pago_emp`
--

DROP TABLE IF EXISTS `cd_metodo_pago_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_metodo_pago_emp` (
  `metodo_pago_emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `metodo_pago_id` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`metodo_pago_emp_id`),
  KEY `fk_cd_metodo_pago_emp_cd_empresa1_idx` (`empresa_id`),
  KEY `fk_cd_metodo_pago_emp_cd_metodo_pago1_idx` (`metodo_pago_id`),
  CONSTRAINT `fk_cd_metodo_pago_emp_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`),
  CONSTRAINT `fk_cd_metodo_pago_emp_cd_metodo_pago1` FOREIGN KEY (`metodo_pago_id`) REFERENCES `cd_metodo_pago` (`metodo_pago_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_metodo_pago_emp`
--

LOCK TABLES `cd_metodo_pago_emp` WRITE;
/*!40000 ALTER TABLE `cd_metodo_pago_emp` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_metodo_pago_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_notificacion`
--

DROP TABLE IF EXISTS `cd_notificacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_notificacion` (
  `notificacion_id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `titulo` varchar(450) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fecha_emision` varchar(10) DEFAULT NULL,
  `fecha_vencimiento` varchar(10) DEFAULT NULL,
  `hora_entrega` varchar(10) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`notificacion_id`),
  KEY `fk_cd_notificacion_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_notificacion_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_notificacion`
--

LOCK TABLES `cd_notificacion` WRITE;
/*!40000 ALTER TABLE `cd_notificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_notificacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_pedido`
--

DROP TABLE IF EXISTS `cd_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_pedido` (
  `pedido_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `metodo_pago_emp_id` int(11) DEFAULT NULL,
  `direciones_id` int(11) NOT NULL,
  `fecha_pedido` varchar(10) DEFAULT NULL,
  `fecha_entrega` varchar(10) DEFAULT NULL,
  `extras` varchar(1000) DEFAULT NULL,
  `comentarios` varchar(600) DEFAULT NULL,
  `total_pagar` double DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `igv` double DEFAULT NULL,
  `fecha_entregado` varchar(10) DEFAULT NULL,
  `hora entregado` varchar(10) DEFAULT NULL,
  `estado` varchar(1) NOT NULL DEFAULT '1' COMMENT '1: Pendiente\n2: En Proceso\n3: Aceptado\n4: Rechazado\n5: Entregado',
  PRIMARY KEY (`pedido_id`),
  KEY `fk_cd_pedido_cd_empresa1_idx` (`empresa_id`),
  KEY `fk_cd_pedido_cd_metodo_pago_emp1_idx` (`metodo_pago_emp_id`),
  KEY `fk_cd_pedido_cd_direciones1_idx` (`direciones_id`),
  KEY `fk_cd_pedido_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_pedido_cd_direciones1` FOREIGN KEY (`direciones_id`) REFERENCES `cd_direciones` (`direciones_id`),
  CONSTRAINT `fk_cd_pedido_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`),
  CONSTRAINT `fk_cd_pedido_cd_metodo_pago_emp1` FOREIGN KEY (`metodo_pago_emp_id`) REFERENCES `cd_metodo_pago_emp` (`metodo_pago_emp_id`),
  CONSTRAINT `fk_cd_pedido_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_pedido`
--

LOCK TABLES `cd_pedido` WRITE;
/*!40000 ALTER TABLE `cd_pedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_persona`
--

DROP TABLE IF EXISTS `cd_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_persona` (
  `persona_id` int(11) NOT NULL AUTO_INCREMENT,
  `ruc_dni` varchar(11) NOT NULL,
  `nombres` varchar(600) DEFAULT NULL,
  `apellidos` varchar(600) DEFAULT NULL,
  `razon_social` varchar(1000) DEFAULT NULL,
  `representante_legal` varchar(450) DEFAULT NULL,
  `nombre_comercial` varchar(1000) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`persona_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_persona`
--

LOCK TABLES `cd_persona` WRITE;
/*!40000 ALTER TABLE `cd_persona` DISABLE KEYS */;
INSERT INTO `cd_persona` VALUES (1,'4843229','Johann James','Valles Paz','-','Johann James Valles Paz','-','917993330','1');
/*!40000 ALTER TABLE `cd_persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_privilegios`
--

DROP TABLE IF EXISTS `cd_privilegios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_privilegios` (
  `privilegios_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`privilegios_id`),
  KEY `fk_cd_menu_has_cd_roles_cd_roles1_idx` (`roles_id`),
  KEY `fk_cd_menu_has_cd_roles_cd_menu1_idx` (`menu_id`),
  CONSTRAINT `fk_cd_menu_has_cd_roles_cd_menu1` FOREIGN KEY (`menu_id`) REFERENCES `cd_menu` (`menu_id`),
  CONSTRAINT `fk_cd_menu_has_cd_roles_cd_roles1` FOREIGN KEY (`roles_id`) REFERENCES `cd_roles` (`roles_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_privilegios`
--

LOCK TABLES `cd_privilegios` WRITE;
/*!40000 ALTER TABLE `cd_privilegios` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_privilegios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_prod_deseo`
--

DROP TABLE IF EXISTS `cd_prod_deseo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_prod_deseo` (
  `prod_deseo_id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_detalle_id` int(11) NOT NULL,
  `deseos_id` int(11) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`prod_deseo_id`),
  KEY `fk_cd_productos_has_cd_deseos_cd_deseos1_idx` (`deseos_id`),
  KEY `fk_cd_productos_has_cd_deseos_cd_productos_idx` (`producto_detalle_id`),
  CONSTRAINT `fk_cd_productos_has_cd_deseos_cd_deseos1` FOREIGN KEY (`deseos_id`) REFERENCES `cd_deseos` (`deseos_id`),
  CONSTRAINT `fk_cd_productos_has_cd_deseos_cd_productos` FOREIGN KEY (`producto_detalle_id`) REFERENCES `cd_producto_detalle` (`producto_detalle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_prod_deseo`
--

LOCK TABLES `cd_prod_deseo` WRITE;
/*!40000 ALTER TABLE `cd_prod_deseo` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_prod_deseo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_producto`
--

DROP TABLE IF EXISTS `cd_producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_producto` (
  `producto_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(600) DEFAULT NULL,
  `descripcion` varchar(1000) DEFAULT NULL,
  `marca_id` int(11) NOT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`producto_id`),
  KEY `fk_cd_producto_cd_marca1_idx` (`marca_id`),
  CONSTRAINT `fk_cd_producto_cd_marca1` FOREIGN KEY (`marca_id`) REFERENCES `cd_marca` (`marca_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_producto`
--

LOCK TABLES `cd_producto` WRITE;
/*!40000 ALTER TABLE `cd_producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_producto_detalle`
--

DROP TABLE IF EXISTS `cd_producto_detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_producto_detalle` (
  `producto_detalle_id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `unidad_id` int(11) NOT NULL,
  `talla_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `precio` double DEFAULT NULL,
  `stock` double DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `desc_porcentaje` double DEFAULT NULL,
  `estado_desc` varchar(1) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`producto_detalle_id`),
  KEY `fk_cd_productos_cd_categoria1_idx` (`categoria_id`),
  KEY `fk_cd_productos_cd_empresa1_idx` (`empresa_id`),
  KEY `fk_cd_producto_detalle_cd_producto1_idx` (`producto_id`),
  KEY `fk_cd_producto_detalle_cd_talla1_idx` (`talla_id`),
  KEY `fk_cd_producto_detalle_cd_color1_idx` (`color_id`),
  KEY `fk_cd_producto_detalle_cd_unidad1_idx` (`unidad_id`),
  CONSTRAINT `fk_cd_producto_detalle_cd_color1` FOREIGN KEY (`color_id`) REFERENCES `cd_color` (`color_id`),
  CONSTRAINT `fk_cd_producto_detalle_cd_producto1` FOREIGN KEY (`producto_id`) REFERENCES `cd_producto` (`producto_id`),
  CONSTRAINT `fk_cd_producto_detalle_cd_talla1` FOREIGN KEY (`talla_id`) REFERENCES `cd_talla` (`talla_id`),
  CONSTRAINT `fk_cd_producto_detalle_cd_unidad1` FOREIGN KEY (`unidad_id`) REFERENCES `cd_unidad` (`unidad_id`),
  CONSTRAINT `fk_cd_productos_cd_categoria1` FOREIGN KEY (`categoria_id`) REFERENCES `cd_categoria` (`categoria_id`),
  CONSTRAINT `fk_cd_productos_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_producto_detalle`
--

LOCK TABLES `cd_producto_detalle` WRITE;
/*!40000 ALTER TABLE `cd_producto_detalle` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_producto_detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_producto_imagen`
--

DROP TABLE IF EXISTS `cd_producto_imagen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_producto_imagen` (
  `producto_imagen_id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_detalle_id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `ruta` varchar(1000) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`producto_imagen_id`),
  KEY `fk_cd_producto_imagen_cd_producto_detalle1_idx` (`producto_detalle_id`),
  CONSTRAINT `fk_cd_producto_imagen_cd_producto_detalle1` FOREIGN KEY (`producto_detalle_id`) REFERENCES `cd_producto_detalle` (`producto_detalle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_producto_imagen`
--

LOCK TABLES `cd_producto_imagen` WRITE;
/*!40000 ALTER TABLE `cd_producto_imagen` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_producto_imagen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_promociones`
--

DROP TABLE IF EXISTS `cd_promociones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_promociones` (
  `promociones_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `titulo` varchar(500) DEFAULT NULL,
  `descripcion` varchar(2000) DEFAULT NULL,
  `extras` varchar(2000) DEFAULT NULL,
  `fecha_inicio` varchar(10) DEFAULT NULL,
  `fecha_fin` varchar(10) DEFAULT NULL,
  `codigo` varchar(100) DEFAULT NULL,
  `porcentaje` varchar(10) DEFAULT NULL,
  `texto` varchar(100) DEFAULT NULL,
  `tipo` varchar(1) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`promociones_id`),
  KEY `fk_cd_promociones_cd_empresa1_idx` (`empresa_id`),
  CONSTRAINT `fk_cd_promociones_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_promociones`
--

LOCK TABLES `cd_promociones` WRITE;
/*!40000 ALTER TABLE `cd_promociones` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_promociones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_reciente`
--

DROP TABLE IF EXISTS `cd_reciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_reciente` (
  `reciente_id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) NOT NULL,
  `producto_detalle_id` int(11) NOT NULL,
  PRIMARY KEY (`reciente_id`),
  KEY `fk_cd_reciente_cd_persona1_idx` (`persona_id`),
  KEY `fk_cd_reciente_cd_producto_detalle1_idx` (`producto_detalle_id`),
  CONSTRAINT `fk_cd_reciente_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`),
  CONSTRAINT `fk_cd_reciente_cd_producto_detalle1` FOREIGN KEY (`producto_detalle_id`) REFERENCES `cd_producto_detalle` (`producto_detalle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_reciente`
--

LOCK TABLES `cd_reciente` WRITE;
/*!40000 ALTER TABLE `cd_reciente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_reciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_roles`
--

DROP TABLE IF EXISTS `cd_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_roles` (
  `roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_roles`
--

LOCK TABLES `cd_roles` WRITE;
/*!40000 ALTER TABLE `cd_roles` DISABLE KEYS */;
INSERT INTO `cd_roles` VALUES (1,'Administrador','1'),(2,'Editor','1'),(3,'Delivery','1'),(4,'Cliente','1');
/*!40000 ALTER TABLE `cd_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_rubro`
--

DROP TABLE IF EXISTS `cd_rubro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_rubro` (
  `rubro_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`rubro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_rubro`
--

LOCK TABLES `cd_rubro` WRITE;
/*!40000 ALTER TABLE `cd_rubro` DISABLE KEYS */;
INSERT INTO `cd_rubro` VALUES (1,'Vidrieria','1'),(2,'Plastiqueria','1');
/*!40000 ALTER TABLE `cd_rubro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_talla`
--

DROP TABLE IF EXISTS `cd_talla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_talla` (
  `talla_id` int(11) NOT NULL AUTO_INCREMENT,
  `talla` varchar(100) DEFAULT NULL,
  `descripcion` varchar(450) DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`talla_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_talla`
--

LOCK TABLES `cd_talla` WRITE;
/*!40000 ALTER TABLE `cd_talla` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_talla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_unidad`
--

DROP TABLE IF EXISTS `cd_unidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_unidad` (
  `unidad_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `abreviatura` varchar(50) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`unidad_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_unidad`
--

LOCK TABLES `cd_unidad` WRITE;
/*!40000 ALTER TABLE `cd_unidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_unidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_usuario_entrega`
--

DROP TABLE IF EXISTS `cd_usuario_entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_usuario_entrega` (
  `usuario_entrega_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `pedido_id` int(11) NOT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`usuario_entrega_id`),
  KEY `fk_users_has_cd_pedido_cd_pedido1_idx` (`pedido_id`),
  KEY `fk_users_has_cd_pedido_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_cd_pedido_cd_pedido1` FOREIGN KEY (`pedido_id`) REFERENCES `cd_pedido` (`pedido_id`),
  CONSTRAINT `fk_users_has_cd_pedido_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_usuario_entrega`
--

LOCK TABLES `cd_usuario_entrega` WRITE;
/*!40000 ALTER TABLE `cd_usuario_entrega` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_usuario_entrega` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_valoracion_emp`
--

DROP TABLE IF EXISTS `cd_valoracion_emp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_valoracion_emp` (
  `valor_emp_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `estrellas` int(11) DEFAULT NULL,
  `resena` varchar(5000) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`valor_emp_id`),
  KEY `fk_cd_valoracion_emp_cd_empresa1_idx` (`empresa_id`),
  KEY `fk_cd_valoracion_emp_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_valoracion_emp_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`),
  CONSTRAINT `fk_cd_valoracion_emp_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_valoracion_emp`
--

LOCK TABLES `cd_valoracion_emp` WRITE;
/*!40000 ALTER TABLE `cd_valoracion_emp` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_valoracion_emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cd_valoracion_prod`
--

DROP TABLE IF EXISTS `cd_valoracion_prod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `cd_valoracion_prod` (
  `valor_prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_detalle_id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `estrellas` int(11) DEFAULT NULL,
  `resena` varchar(5000) DEFAULT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`valor_prod_id`),
  KEY `fk_cd_valoracion_prod_cd_productos1_idx` (`producto_detalle_id`),
  KEY `fk_cd_valoracion_prod_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_valoracion_prod_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`),
  CONSTRAINT `fk_cd_valoracion_prod_cd_productos1` FOREIGN KEY (`producto_detalle_id`) REFERENCES `cd_producto_detalle` (`producto_detalle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cd_valoracion_prod`
--

LOCK TABLES `cd_valoracion_prod` WRITE;
/*!40000 ALTER TABLE `cd_valoracion_prod` DISABLE KEYS */;
/*!40000 ALTER TABLE `cd_valoracion_prod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  `roles_id` int(11) NOT NULL,
  `email` varchar(450) NOT NULL,
  `password` varchar(500) NOT NULL,
  `api_token` varchar(80) NOT NULL,
  `created_at` varchar(200) NOT NULL,
  `updated_at` varchar(200) NOT NULL,
  `estado` varchar(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_cd_usuarios_cd_roles1_idx` (`roles_id`),
  KEY `fk_users_cd_empresa1_idx` (`empresa_id`),
  KEY `fk_users_cd_persona1_idx` (`persona_id`),
  CONSTRAINT `fk_cd_usuarios_cd_roles1` FOREIGN KEY (`roles_id`) REFERENCES `cd_roles` (`roles_id`),
  CONSTRAINT `fk_users_cd_empresa1` FOREIGN KEY (`empresa_id`) REFERENCES `cd_empresa` (`empresa_id`),
  CONSTRAINT `fk_users_cd_persona1` FOREIGN KEY (`persona_id`) REFERENCES `cd_persona` (`persona_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,NULL,4,'johann241@hotmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','dfghmasuytegwhdeuiwnckjsxmdmcx','2020-07-06','2020-07-06','1'),(2,NULL,1,1,'johames.15@gmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','sdhiar237rg23r872gdyg6732gd32yh','2020-07-06','2020-07-06','1'),(3,NULL,1,2,'editor.emp1@gmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','sjkdg6g3e8736tdg378dg87j,5326g5','2020-07-06','2020-07-06','1'),(4,NULL,1,3,'delivery.emp1@gmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','had7832hr8732djh8,32ihe327euehf','2020-07-06','2020-07-06','1'),(5,NULL,2,1,'johann.valles@gmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','nsiuand8hr87gfb872gf387g8237gdf','2020-07-06','2020-07-06','1'),(6,NULL,2,2,'editor.emp2@gmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','wdn78gqr62y4gr7gf2763rgf763gsjh','2020-07-06','2020-07-06','1'),(7,NULL,2,3,'delivery.emp2@gmail.com','$2y$10$2MBXEEzSpSy4eFmOVFYJ7eOw1rnjtlMqbF64wuhYi8c4r5MI/Upxe','nf7824rkf74g872iu8gf37fg382gf3de','2020-07-06','2020-07-06','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-08 15:34:30
