-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema cea_delivery
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema cea_delivery
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `cea_delivery` DEFAULT CHARACTER SET utf8 ;
USE `cea_delivery` ;

-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_rubro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_rubro` (
  `rubro_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(500) NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`rubro_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_empresa` (
  `empresa_id` INT NOT NULL AUTO_INCREMENT,
  `rubro_id` INT NOT NULL,
  `slug` VARCHAR(120) NULL,
  `razon_social` VARCHAR(1000) NULL,
  `nombre_comercial` VARCHAR(1000) NULL,
  `representante_legal` VARCHAR(600) NULL,
  `ruc` VARCHAR(11) NULL,
  `descripcion` VARCHAR(2500) NULL,
  `icono` VARCHAR(500) NULL,
  `color` VARCHAR(100) NULL,
  `direccion` VARCHAR(500) NULL,
  `referencia` VARCHAR(1000) NULL,
  `lat` VARCHAR(100) NULL,
  `lgn` VARCHAR(100) NULL,
  `telefono` VARCHAR(15) NULL,
  `correo` VARCHAR(100) NULL,
  `facebook` VARCHAR(145) NULL,
  `twitter` VARCHAR(145) NULL,
  `instagram` VARCHAR(145) NULL,
  `google+` VARCHAR(145) NULL,
  `baner` VARCHAR(165) NULL,
  `estado` VARCHAR(1) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `cea_puntos` CHAR(1) NULL,
  PRIMARY KEY (`empresa_id`),
  INDEX `fk_cd_empresa_cd_rubro1_idx` (`rubro_id` ASC) ,
  CONSTRAINT `fk_cd_empresa_cd_rubro1`
    FOREIGN KEY (`rubro_id`)
    REFERENCES `cea_delivery`.`cd_rubro` (`rubro_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_persona` (
  `persona_id` INT NOT NULL AUTO_INCREMENT,
  `ruc_dni` VARCHAR(11) NOT NULL,
  `nombres` VARCHAR(600) NULL,
  `apellidos` VARCHAR(600) NULL,
  `razon_social` VARCHAR(1000) NULL,
  `representante_legal` VARCHAR(450) NULL,
  `nombre_comercial` VARCHAR(1000) NULL,
  `telefono` VARCHAR(15) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`persona_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_categoria` (
  `categoria_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(500) NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`categoria_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_marca`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_marca` (
  `marca_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(500) NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`marca_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_producto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_producto` (
  `producto_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(600) NULL,
  `descripcion` VARCHAR(1000) NULL,
  `marca_id` INT NOT NULL,
  `estado` VARCHAR(1) NULL,
  PRIMARY KEY (`producto_id`),
  INDEX `fk_cd_producto_cd_marca1_idx` (`marca_id` ASC) ,
  CONSTRAINT `fk_cd_producto_cd_marca1`
    FOREIGN KEY (`marca_id`)
    REFERENCES `cea_delivery`.`cd_marca` (`marca_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_talla`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_talla` (
  `talla_id` INT NOT NULL AUTO_INCREMENT,
  `talla` VARCHAR(100) NULL,
  `descripcion` VARCHAR(450) NULL,
  `estado` VARCHAR(1) NULL,
  PRIMARY KEY (`talla_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_color`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_color` (
  `color_id` INT NOT NULL AUTO_INCREMENT,
  `color` VARCHAR(45) NULL,
  `estado` VARCHAR(1) NULL,
  PRIMARY KEY (`color_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_unidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_unidad` (
  `unidad_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(500) NOT NULL,
  `abreviatura` VARCHAR(50) NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`unidad_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_producto_detalle`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_producto_detalle` (
  `producto_detalle_id` INT NOT NULL AUTO_INCREMENT,
  `slug` VARCHAR(120) NULL,
  `producto_id` INT NOT NULL,
  `empresa_id` INT NOT NULL,
  `categoria_id` INT NOT NULL,
  `unidad_id` INT NOT NULL,
  `talla_id` INT NULL,
  `color_id` INT NULL,
  `precio` DOUBLE NULL,
  `stock` DOUBLE NULL,
  `descuento` DOUBLE NULL,
  `desc_porcentaje` DOUBLE NULL,
  `estado_desc` VARCHAR(1) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`producto_detalle_id`),
  INDEX `fk_cd_productos_cd_categoria1_idx` (`categoria_id` ASC) ,
  INDEX `fk_cd_productos_cd_empresa1_idx` (`empresa_id` ASC) ,
  INDEX `fk_cd_producto_detalle_cd_producto1_idx` (`producto_id` ASC) ,
  INDEX `fk_cd_producto_detalle_cd_talla1_idx` (`talla_id` ASC) ,
  INDEX `fk_cd_producto_detalle_cd_color1_idx` (`color_id` ASC) ,
  INDEX `fk_cd_producto_detalle_cd_unidad1_idx` (`unidad_id` ASC) ,
  CONSTRAINT `fk_cd_productos_cd_categoria1`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `cea_delivery`.`cd_categoria` (`categoria_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_productos_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_producto_detalle_cd_producto1`
    FOREIGN KEY (`producto_id`)
    REFERENCES `cea_delivery`.`cd_producto` (`producto_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_producto_detalle_cd_talla1`
    FOREIGN KEY (`talla_id`)
    REFERENCES `cea_delivery`.`cd_talla` (`talla_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_producto_detalle_cd_color1`
    FOREIGN KEY (`color_id`)
    REFERENCES `cea_delivery`.`cd_color` (`color_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_producto_detalle_cd_unidad1`
    FOREIGN KEY (`unidad_id`)
    REFERENCES `cea_delivery`.`cd_unidad` (`unidad_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_horario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_horario` (
  `horario_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `dia` VARCHAR(45) NOT NULL,
  `turnos` VARCHAR(1) NULL,
  `hora_abre_am` VARCHAR(10) NULL,
  `hora_cierre_am` VARCHAR(10) NULL,
  `hora_abre_pm` VARCHAR(10) NULL,
  `hora_cierre_pm` VARCHAR(10) NULL,
  `descripcion` VARCHAR(450) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`horario_id`),
  INDEX `fk_cd_horario_cd_empresa1_idx` (`empresa_id` ASC) ,
  CONSTRAINT `fk_cd_horario_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`users` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `persona_id` INT NULL,
  `empresa_id` INT NULL,
  `email` VARCHAR(60) NOT NULL,
  `email_verified_at` TIMESTAMP NULL,
  `password` VARCHAR(80) NOT NULL,
  `remenber_token` VARCHAR(120) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_cd_empresa1_idx` (`empresa_id` ASC) ,
  INDEX `fk_users_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_users_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_metodo_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_metodo_pago` (
  `metodo_pago_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  `logo` VARCHAR(45) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`metodo_pago_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_metodo_pago_emp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_metodo_pago_emp` (
  `metodo_pago_emp_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `metodo_pago_id` INT NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`metodo_pago_emp_id`),
  INDEX `fk_cd_metodo_pago_emp_cd_empresa1_idx` (`empresa_id` ASC) ,
  INDEX `fk_cd_metodo_pago_emp_cd_metodo_pago1_idx` (`metodo_pago_id` ASC) ,
  CONSTRAINT `fk_cd_metodo_pago_emp_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_metodo_pago_emp_cd_metodo_pago1`
    FOREIGN KEY (`metodo_pago_id`)
    REFERENCES `cea_delivery`.`cd_metodo_pago` (`metodo_pago_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_direciones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_direciones` (
  `direciones_id` INT NOT NULL AUTO_INCREMENT,
  `persona_id` INT NOT NULL,
  `direccion` VARCHAR(500) NOT NULL,
  `lat` VARCHAR(100) NOT NULL,
  `lgn` VARCHAR(100) NOT NULL,
  `referencia` VARCHAR(1000) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`direciones_id`),
  INDEX `fk_cd_direciones_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_direciones_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_metodo_envio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_metodo_envio` (
  `metodo_envio_id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `descripcion` VARCHAR(45) NULL,
  `estado` CHAR(1) NULL,
  PRIMARY KEY (`metodo_envio_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_metodo_envio_emp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_metodo_envio_emp` (
  `metodo_envio_emp_id` INT NOT NULL AUTO_INCREMENT,
  `metodo_envio_id` INT NOT NULL,
  `empresa_id` INT NOT NULL,
  `precio` VARCHAR(45) NULL,
  `estado` CHAR(1) NULL,
  PRIMARY KEY (`metodo_envio_emp_id`),
  INDEX `fk_cd_metodo_envio_emp_cd_metodo_envio1_idx` (`metodo_envio_id` ASC) ,
  INDEX `fk_cd_metodo_envio_emp_cd_empresa1_idx` (`empresa_id` ASC) ,
  CONSTRAINT `fk_cd_metodo_envio_emp_cd_metodo_envio1`
    FOREIGN KEY (`metodo_envio_id`)
    REFERENCES `cea_delivery`.`cd_metodo_envio` (`metodo_envio_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_metodo_envio_emp_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_pedido` (
  `pedido_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `persona_id` INT NOT NULL,
  `users_id` BIGINT(20) NULL,
  `metodo_pago_emp_id` INT NULL,
  `metodo_envio_emp_id` INT NULL,
  `direciones_id` INT NOT NULL,
  `fecha_pedido` VARCHAR(10) NULL,
  `fecha_entrega` VARCHAR(10) NULL,
  `extras` VARCHAR(1000) NULL,
  `comentarios` VARCHAR(600) NULL,
  `total_pagar` DOUBLE NULL,
  `codigo_descuento` VARCHAR(120) NULL,
  `descuento` DOUBLE NULL,
  `igv` DOUBLE NULL,
  `precio_envio` DOUBLE NULL,
  `fecha_entregado` VARCHAR(10) NULL,
  `hora entregado` VARCHAR(10) NULL,
  `puntos_ganados` VARCHAR(45) NULL,
  `cangear_puntos` INT NULL,
  `estado` VARCHAR(1) NOT NULL DEFAULT '1' COMMENT '1: Pendiente\n2: En Proceso\n3: Aceptado\n4: Rechazado\n5: Entregado',
  PRIMARY KEY (`pedido_id`),
  INDEX `fk_cd_pedido_cd_empresa1_idx` (`empresa_id` ASC) ,
  INDEX `fk_cd_pedido_cd_metodo_pago_emp1_idx` (`metodo_pago_emp_id` ASC) ,
  INDEX `fk_cd_pedido_cd_direciones1_idx` (`direciones_id` ASC) ,
  INDEX `fk_cd_pedido_cd_persona1_idx` (`persona_id` ASC) ,
  INDEX `fk_cd_pedido_users1_idx` (`users_id` ASC) ,
  INDEX `fk_cd_pedido_cd_metodo_envio_emp1_idx` (`metodo_envio_emp_id` ASC) ,
  CONSTRAINT `fk_cd_pedido_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_pedido_cd_metodo_pago_emp1`
    FOREIGN KEY (`metodo_pago_emp_id`)
    REFERENCES `cea_delivery`.`cd_metodo_pago_emp` (`metodo_pago_emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_pedido_cd_direciones1`
    FOREIGN KEY (`direciones_id`)
    REFERENCES `cea_delivery`.`cd_direciones` (`direciones_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_pedido_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_pedido_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `cea_delivery`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_pedido_cd_metodo_envio_emp1`
    FOREIGN KEY (`metodo_envio_emp_id`)
    REFERENCES `cea_delivery`.`cd_metodo_envio_emp` (`metodo_envio_emp_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_deseos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_deseos` (
  `deseos_id` INT NOT NULL AUTO_INCREMENT,
  `persona_id` INT NOT NULL,
  `nombre` VARCHAR(500) NOT NULL,
  `descripcion` VARCHAR(500) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`deseos_id`),
  INDEX `fk_cd_deseos_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_deseos_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_notificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_notificacion` (
  `notificacion_id` INT NOT NULL AUTO_INCREMENT,
  `persona_id` INT NOT NULL,
  `titulo` VARCHAR(450) NULL,
  `descripcion` VARCHAR(500) NULL,
  `fecha_emision` VARCHAR(10) NULL,
  `fecha_vencimiento` VARCHAR(10) NULL,
  `hora_entrega` VARCHAR(10) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`notificacion_id`),
  INDEX `fk_cd_notificacion_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_notificacion_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_valoracion_prod`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_valoracion_prod` (
  `valor_prod_id` INT NOT NULL AUTO_INCREMENT,
  `producto_detalle_id` INT NOT NULL,
  `persona_id` INT NOT NULL,
  `estrellas` INT NULL,
  `resena` VARCHAR(5000) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`valor_prod_id`),
  INDEX `fk_cd_valoracion_prod_cd_productos1_idx` (`producto_detalle_id` ASC) ,
  INDEX `fk_cd_valoracion_prod_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_valoracion_prod_cd_productos1`
    FOREIGN KEY (`producto_detalle_id`)
    REFERENCES `cea_delivery`.`cd_producto_detalle` (`producto_detalle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_valoracion_prod_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_prod_deseo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_prod_deseo` (
  `prod_deseo_id` INT NOT NULL AUTO_INCREMENT,
  `producto_detalle_id` INT NOT NULL,
  `deseos_id` INT NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  INDEX `fk_cd_productos_has_cd_deseos_cd_deseos1_idx` (`deseos_id` ASC) ,
  INDEX `fk_cd_productos_has_cd_deseos_cd_productos_idx` (`producto_detalle_id` ASC) ,
  PRIMARY KEY (`prod_deseo_id`),
  CONSTRAINT `fk_cd_productos_has_cd_deseos_cd_productos`
    FOREIGN KEY (`producto_detalle_id`)
    REFERENCES `cea_delivery`.`cd_producto_detalle` (`producto_detalle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_productos_has_cd_deseos_cd_deseos1`
    FOREIGN KEY (`deseos_id`)
    REFERENCES `cea_delivery`.`cd_deseos` (`deseos_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_valoracion_emp`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_valoracion_emp` (
  `valor_emp_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `persona_id` INT NOT NULL,
  `estrellas` INT NULL,
  `resena` VARCHAR(5000) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`valor_emp_id`),
  INDEX `fk_cd_valoracion_emp_cd_empresa1_idx` (`empresa_id` ASC) ,
  INDEX `fk_cd_valoracion_emp_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_valoracion_emp_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_valoracion_emp_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_detalle_pedido`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_detalle_pedido` (
  `detalle_id` INT NOT NULL AUTO_INCREMENT,
  `pedido_id` INT NOT NULL,
  `producto_detalle_id` INT NOT NULL,
  `nombre_producto` VARCHAR(600) NULL,
  `cantidad` DOUBLE NULL,
  `precio` DOUBLE NULL,
  `descuento` DOUBLE NULL,
  `igv` DOUBLE NULL,
  `total` DOUBLE NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`detalle_id`),
  INDEX `fk_cd_detalle_cd_pedido1_idx` (`pedido_id` ASC) ,
  INDEX `fk_cd_detalle_cd_productos1_idx` (`producto_detalle_id` ASC) ,
  CONSTRAINT `fk_cd_detalle_cd_pedido1`
    FOREIGN KEY (`pedido_id`)
    REFERENCES `cea_delivery`.`cd_pedido` (`pedido_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_detalle_cd_productos1`
    FOREIGN KEY (`producto_detalle_id`)
    REFERENCES `cea_delivery`.`cd_producto_detalle` (`producto_detalle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_promociones`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_promociones` (
  `promociones_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `titulo` VARCHAR(500) NULL,
  `descripcion` VARCHAR(2000) NULL,
  `extras` VARCHAR(2000) NULL,
  `fecha_inicio` VARCHAR(10) NULL,
  `fecha_fin` VARCHAR(10) NULL,
  `codigo` VARCHAR(100) NULL,
  `porcentaje` VARCHAR(10) NULL,
  `texto` VARCHAR(100) NULL,
  `tipo` VARCHAR(1) NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`promociones_id`),
  INDEX `fk_cd_promociones_cd_empresa1_idx` (`empresa_id` ASC) ,
  CONSTRAINT `fk_cd_promociones_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_reciente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_reciente` (
  `reciente_id` INT NOT NULL AUTO_INCREMENT,
  `persona_id` INT NOT NULL,
  `producto_detalle_id` INT NOT NULL,
  PRIMARY KEY (`reciente_id`),
  INDEX `fk_cd_reciente_cd_persona1_idx` (`persona_id` ASC) ,
  INDEX `fk_cd_reciente_cd_producto_detalle1_idx` (`producto_detalle_id` ASC) ,
  CONSTRAINT `fk_cd_reciente_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cd_reciente_cd_producto_detalle1`
    FOREIGN KEY (`producto_detalle_id`)
    REFERENCES `cea_delivery`.`cd_producto_detalle` (`producto_detalle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_producto_imagen`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_producto_imagen` (
  `producto_imagen_id` INT NOT NULL AUTO_INCREMENT,
  `producto_detalle_id` INT NOT NULL,
  `descripcion` VARCHAR(100) NOT NULL,
  `ruta` VARCHAR(1000) NOT NULL,
  `estado` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`producto_imagen_id`),
  INDEX `fk_cd_producto_imagen_cd_producto_detalle1_idx` (`producto_detalle_id` ASC) ,
  CONSTRAINT `fk_cd_producto_imagen_cd_producto_detalle1`
    FOREIGN KEY (`producto_detalle_id`)
    REFERENCES `cea_delivery`.`cd_producto_detalle` (`producto_detalle_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`roles` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `slug` VARCHAR(60) NOT NULL,
  `descripction` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  `especial` ENUM('all-access', 'no-access') NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `cea_delivery`.`permissions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`permissions` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `slug` VARCHAR(60) NOT NULL,
  `description` TEXT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `cea_delivery`.`permission_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`permission_role` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `permission_id` BIGINT(20) NOT NULL,
  `role_id` BIGINT(20) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_permission_role_roles1_idx` (`role_id` ASC) ,
  INDEX `fk_permission_role_permissions1_idx` (`permission_id` ASC) ,
  CONSTRAINT `fk_permission_role_roles1`
    FOREIGN KEY (`role_id`)
    REFERENCES `cea_delivery`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permission_role_permissions1`
    FOREIGN KEY (`permission_id`)
    REFERENCES `cea_delivery`.`permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `cea_delivery`.`password_resets`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`password_resets` (
  `email` VARCHAR(100) NOT NULL,
  `token` VARCHAR(100) NULL,
  `created_at` TIMESTAMP NULL);


-- -----------------------------------------------------
-- Table `cea_delivery`.`permission_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`permission_user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `permission_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_permission_user_permissions1_idx` (`permission_id` ASC) ,
  INDEX `fk_permission_user_users1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_permission_user_permissions1`
    FOREIGN KEY (`permission_id`)
    REFERENCES `cea_delivery`.`permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_permission_user_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `cea_delivery`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `cea_delivery`.`role_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`role_user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `role_id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_role_user_roles1_idx` (`role_id` ASC) ,
  INDEX `fk_role_user_users1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_role_user_roles1`
    FOREIGN KEY (`role_id`)
    REFERENCES `cea_delivery`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_user_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `cea_delivery`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_codigo_descuento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_codigo_descuento` (
  `codigo_descuento_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NOT NULL,
  `nombre` VARCHAR(45) NULL,
  `codigo` VARCHAR(120) NULL,
  `tipo` CHAR(1) NULL,
  `monto` DOUBLE NULL,
  `estado` CHAR(1) NULL,
  PRIMARY KEY (`codigo_descuento_id`),
  INDEX `fk_cd_codigo_descuento_cd_empresa1_idx` (`empresa_id` ASC) ,
  CONSTRAINT `fk_cd_codigo_descuento_cd_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `cea_delivery`.`cd_empresa` (`empresa_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_cea_puntos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_cea_puntos` (
  `cd_cea_puntos_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NULL,
  `persona_id` INT NOT NULL,
  `total` VARCHAR(45) NULL,
  PRIMARY KEY (`cd_cea_puntos_id`),
  INDEX `fk_cd_cea_puntos_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_cea_puntos_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_cea_puntos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_cea_puntos` (
  `cd_cea_puntos_id` INT NOT NULL AUTO_INCREMENT,
  `empresa_id` INT NULL,
  `persona_id` INT NOT NULL,
  `total` VARCHAR(45) NULL,
  PRIMARY KEY (`cd_cea_puntos_id`),
  INDEX `fk_cd_cea_puntos_cd_persona1_idx` (`persona_id` ASC) ,
  CONSTRAINT `fk_cd_cea_puntos_cd_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `cea_delivery`.`cd_persona` (`persona_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cea_delivery`.`cd_detalle_cea_puntos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cea_delivery`.`cd_detalle_cea_puntos` (
  `cd_detalle_cea_puntos_id` INT NOT NULL AUTO_INCREMENT,
  `cea_puntos_id` INT NOT NULL,
  `puntos_ganados` INT NULL,
  `tipo_accion` CHAR(1) NULL,
  `id_accion` INT NULL,
  `fecha` TIMESTAMP NULL,
  PRIMARY KEY (`cd_detalle_cea_puntos_id`),
  INDEX `fk_cd_detalle_cea_puntos_cd_cea_puntos1_idx` (`cea_puntos_id` ASC) ,
  CONSTRAINT `fk_cd_detalle_cea_puntos_cd_cea_puntos1`
    FOREIGN KEY (`cea_puntos_id`)
    REFERENCES `cea_delivery`.`cd_cea_puntos` (`cd_cea_puntos_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
