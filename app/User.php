<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{

    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres', 'avatar','empresa_id', 'persona_id', 'email', 'password', 'codigo_recuperacion', 'codigo_confirmacion', 'fecha_recuperacion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ModeloPedidos(){
        return  $this->hasMany('App\Models\ModeloPedido', 'id', 'users_id');
    }

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

    public function ModeloEmpresa(){
        return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloComentariosBlogs(){
        return $this->hasMany('App\Models\ModeloComentarioBlog', 'id', 'users_id');
    }

    public function ModeloTickets(){
        return $this->hasMany('App\Models\ModeloTicket', 'id', 'users_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('withModeloPersona', function (Builder $builder) {
            $builder->with(['ModeloPersona']);
        });
    }
}
