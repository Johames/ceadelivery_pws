<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    //

    public static function saveImg($file, $id_table, $name_table, $name_id, $name_img )
    {


       $url_img = '';

       if ($id_table) {
            //Editando clientes

            $table_ruta_old = DB::table($name_table)->select($name_img . ' as old_foto')->where($name_id , $id_table)->first();

            if($file){

                $path = Storage::disk('public')->put( $name_table , $file);
                $delt = Storage::disk('public')->delete($table_ruta_old->old_foto);

                $url_img = $path;

            }else{

                $url_img =  $table_ruta_old->old_foto;

            }

       }else{
            //Agregando clientes

            if($file){
                $path = Storage::disk('public')->put( $name_table , $file);
                $url_img = $path;
            }else{
                $url_img = '';
            }
       }


       return $url_img;
    }

    public static function deleteImg($name_table, $name_img, $name_id, $id_table)
    {
        $table_ruta_old = DB::table($name_table)->select($name_img . ' as old_foto')->where($name_id , $id_table)->first();

        Storage::disk('public')->delete($table_ruta_old->old_foto);
    }


    public static function random_string(){
        $key = '';
        $keys = array_merge( range('a','z'), range(0,9) );

        for($i=0; $i<10; $i++){
            $key .= $keys[array_rand($keys)];
        }
        $key = $key . '_' . time();
        return $key;
    }


}
