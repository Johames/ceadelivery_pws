<?php

namespace App\Http\Controllers\utilidades;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Create extends Controller
{
    //Crear boton unitario
    public static function btn($accion, $id, $btn_name = '')
    {
        $btn = '';

        switch ($accion) {
          case 'eliminar':
            $btn = '<a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="eliminar'. $btn_name . '(' . $id . ')"><i class="fa fa-trash"></i></a> ';
            break;
          case 'activar':
            $btn = '<a href="javascript:void(0)" class="btn btn-success btn-sm" onclick="activar'. $btn_name . '(' . $id . ')"><i class="fa fa-check-circle"></i></a> ';
            break;
          case 'desactivar':
            $btn = '<a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="desactivar'. $btn_name . '(' . $id . ')"><i class="fa fa-times"></i></a> ';
            break;
          case 'editar':
            $btn = '<a href="javascript:void(0)" class="btn btn-warning btn-sm" onclick="mostrar'. $btn_name . '(' . $id . ')"><i class="fa fa-edit"></i></a> ';
            break;
          case 'mostrar':
            $btn = '<a href="javascript:void(0)" class="btn btn-info btn-xs" onclick="ver'. $btn_name . '(' . $id . ')"><i class="fa fa-edit"></i></a> ';
            break;
          default:
            break;
        }
        return $btn;
    }

    //Crear grupo de botones para "Acciones"
    public static function btns_array($estado, $id, $btns){

        $btns_html = '<center>';

        foreach ($btns as $array) {

            if ($array == 'estado') {

                if ($estado) {
                    $btns_html = $btns_html . Create::btn('activar', $id);
                }else{
                    $btns_html = $btns_html . Create::btn('desactivar', $id);
                }

            }else{

                $btns_html = $btns_html . Create::btn($array, $id);
            }
        }


        $btns_html = $btns_html . '</center>';

        return $btns_html;
    }

    public static function btns_two($estado, $id){

        $btns = '';

        if ($estado) {
            $btns = Create::btn('editar', $id) . Create::btn('activar', $id);
        }else{
            $btns = Create::btn('editar', $id) . Create::btn('desactivar', $id);
        }

        $btns = '<center>' . $btns . '</center>';

        return $btns;
    }

    public static function btns_tree($estado, $id, $btn_name = ''){

        $btns = '';

        if ($estado) {
            $btns = Create::btn('editar', $id, $btn_name) . Create::btn('activar', $id, $btn_name). Create::btn('eliminar', $id, $btn_name);
        }else{
            $btns = Create::btn('editar', $id, $btn_name) . Create::btn('desactivar', $id, $btn_name). Create::btn('eliminar', $id, $btn_name);
        }

        $btns = '<center>' . $btns . '</center>';

        return $btns;
    }

    public static function btns_car($id, $btn_name = ''){

        $btns = Create::btn('editar', $id, $btn_name) . Create::btn('eliminar', $id, $btn_name);

        // $btns = '<center>' . $btns . '</center>';

        return $btns;
    }

    public static function btns_pedido($id){

        $btns = '<a class="btn btn-info btn-xs" href="javascript:void(0)" onclick="ver_pedido('. $id .')"><i class="fas fa-eye"></i></a> '
              . '<a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminar_pedido(' . $id . ')"><i class="fa fa-trash"></i></a> ';
        return $btns;
    }

    public static function btns_pedido_detalle($id){

        $btns = '<a href="javascript:void(0)" class="btn btn-warning btn-xs" onclick="mostrar_pedido_detalle('. $id .')"><i class="fas fa-edit"></i></a> '
              . '<a href="javascript:void(0)" class="btn btn-danger btn-xs" onclick="eliminar_pedido_detalle(' . $id . ')"><i class="fa fa-trash"></i></a> ';
        return $btns;
    }

    public static function span($accion){

        $span = '';

        switch ($accion) {
            case 'activo':
                $span = '<span class="label bg-green"> Activo </span> ';
                break;
            case 'desactivo':
                $span = '<span class="label bg-red"> Deshabilitado </span> ';
                break;
            default:
                break;
        }
        return $span;
    }

    public static function span_pedido($estado){

        $span = '';

        switch ($estado) {
            case 0:
                $span = '<span class="label bg-yellow">En espera</span> ';
                break;
            case 1:
                $span = '<span class="label bg-primary">En Progreso</span> ';
                break;
            case 2:
                $span = '<span class="label bg-success">Completado</span> ';
                break;
            case 3:
                $span = '<span class="label bg-danger">Anulado</span> ';
                break;
            default:
                break;
        }
        return $span;
    }

    public static function spans_two($estado){

        $span = '';

        if ($estado) {
            $span = '<span class="label bg-red"> Deshabilitado </span> ';
        }else{
            $span = '<span class="label bg-green"> Activo </span> ';
        }

        return $span;
    }

    public static function img($ruta){

        $img = '<img src="' . $ruta . '" class="img-thumbnail" width="100">';
        return $img;
    }


    public static function spans_array($array_obj)
    {
        $colores = array("bg-green", "bg-red", "bg-olive", "bg-yellow", "bg-aqua", "bg-teal", "bg-navy", "bg-purple", "bg-orange", "bg-maroon","btn-adn","btn-bitbucket","btn-dropbox","btn-facebook","btn-flickr","btn-foursquare","btn-github","btn-google","btn-instagram","btn-linkedin","btn-microsoft","btn-odnoklassniki","btn-openid","btn-pinterest","btn-soundcloud","btn-tumblr","btn-twitter","btn-vimeo","btn-vk","btn-yahoo");
        $spans_html = '';
        $cont_array = 0;

        foreach ($array_obj as $spans) {
            $cont_array++;

            if ($cont_array <= 5){

                $spans_html .= '<span class="label '. $colores[array_rand($colores)] .'">' . $spans->span_name .'</span><br> ';
            }

        }

        if (count($array_obj) > 5) {
            $spans_html .= '<span class="label bg-gray"> Y ' . (count($array_obj) - 5)  . ' más ...</span><br> ';
        }

        ($cont_array <= 0) ? $spans_html = '<span class="label bg-gray">Ninguno</span> ' : $spans_html = $spans_html;

        return $spans_html;
    }

    public static function ul_array($array_obj, $name)
    {
        $lista_html = '<ul>';
        $cont_array = 0;

        foreach ($array_obj as $lista) {

            $cont_array++;
            $lista_html .= '<li>' . $lista->$name . '</li>';

        }

        ($cont_array <= 0) ? $lista_html = '<li> Ninguno </li>' : $lista_html = $lista_html;

        $lista_html .= '<ul>';

        return $lista_html;
    }


    public static function novedades_html($idnovedades, $foto ,$titulo, $descripcion, $fecha)
    {
        Carbon::setLocale('es');
        $fecha = Carbon::parse($fecha);

        $novedades_html = '<div class="row" id="lista_productos">'
                            . '<div class="col-md-12 my-3">'
                                . '<div class="card" style="width: 100%">'
                                    . '<div class="row">'
                                       .  '<div class="col-md-3">'
                                            . '<img class="card-img-top" src="/recursos/imagenes/admin/' . $foto . '" alt="Card image cap">'
                                       .  '</div>'
                                       .  '<div class="col-md-9">'
                                            . '<div class="card-body">'
                                                . '<h5 class="card-title m-2" style="color: #274685; font-weight: bold">' . $titulo . '</h5>'
                                               .  '<small class="m-2" style="color: #2e3c57; font-weight: bold"> ' . $fecha->format('d/m/Y') .' ➟ ' . $fecha->diffForHumans() .  '</small>'
                                                . '<p class="card-text m-2">' . Create::substr_tags($descripcion, 400) . '</p>'
                                                . '<a href="#" class="btn btn-outline-info" onclick="add()"><i class="fas fa-eye"></i> Ver más</a>'
                                            . '</div>'
                                        . '</div>'
                                    . '</div>'
                                . '</div>'
                            . '</div>'
                        . '</div>';

        return $novedades_html;
    }


    public static function producto_html($idproductos, $foto, $precio, $nombre, $categoria, $descripcion)
    {
        $producto_html = '<div class="col-md-4 my-3">'
        . '<div class="card" style="width: 18rem;">'
            . '<img class="card-img-top" src="/recursos/imagenes/admin/' . $foto . '" alt="Card image cap">'
            . '<div class="card-body">'
                . '<h4 class="m-2" style="color: #277e41; font-weight: bold"><sup>S/  </sup>' . number_format((float)$precio, 2, '.', '') . '</h4>'
                . '<h5 class="card-title m-2" style="color: #274685; font-weight: bold">' . $nombre . '</h5>'
                . '<small class="m-2" style="color: #2e3c57; font-weight: bold">' . $categoria . '</small>'
                . '<p class="card-text m-2">'. $descripcion . '</p>'
                . '<a href="javascript:void(0)" class="btn btn-block btn-outline-success" onclick="add(' . $idproductos . ',\''. $nombre .'\')"><i class="fas fa-cart-plus"></i> Agregar al carrito</a>'
            . '</div>'
        . '</div>'
    . '</div>';

        return $producto_html;
    }

    public static function row( $contenido)
    {
        $row_html = '<div class="row">' . $contenido . '</div>';

        return $row_html;
    }


    // TODO: I'm not claiming to have invented this,
    // but there is a very complete Text::truncate()
    // method in CakePHP which does what you want
    public static function substr_tags($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false){

        if (is_array($ending)) {
            extract($ending);
        }

        if ($considerHtml) {
            if (mb_strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
                return $text;
            }
            $totalLength = mb_strlen($ending);
            $openTags = array();
            $truncate = '';

            preg_match_all('/(<\/?([\w+]+)[^>]*>)?([^<>]*)/', $text, $tags, PREG_SET_ORDER);

            foreach ($tags as $tag) {
                if (!preg_match('/img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param/s', $tag[2])) {
                    if (preg_match('/<[\w]+[^>]*>/s', $tag[0])) {
                        array_unshift($openTags, $tag[2]);
                    } else if (preg_match('/<\/([\w]+)[^>]*>/s', $tag[0], $closeTag)) {
                        $pos = array_search($closeTag[1], $openTags);
                        if ($pos !== false) {
                            array_splice($openTags, $pos, 1);
                        }
                    }
                }
                $truncate .= $tag[1];

                $contentLength = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $tag[3]));
                if ($contentLength + $totalLength > $length) {
                    $left = $length - $totalLength;
                    $entitiesLength = 0;
                    if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $tag[3], $entities, PREG_OFFSET_CAPTURE)) {
                        foreach ($entities[0] as $entity) {
                            if ($entity[1] + 1 - $entitiesLength <= $left) {
                                $left--;
                                $entitiesLength += mb_strlen($entity[0]);
                            } else {
                                break;
                            }
                        }
                    }

                    $truncate .= mb_substr($tag[3], 0 , $left + $entitiesLength);
                    break;
                } else {
                    $truncate .= $tag[3];
                    $totalLength += $contentLength;
                }
                if ($totalLength >= $length) {
                    break;
                }
            }

        } else {
            if (mb_strlen($text) <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - strlen($ending));
            }
        }
        if (!$exact) {
            $spacepos = mb_strrpos($truncate, ' ');
            if (isset($spacepos)) {
                if ($considerHtml) {
                    $bits = mb_substr($truncate, $spacepos);
                    preg_match_all('/<\/([a-z]+)>/', $bits, $droppedTags, PREG_SET_ORDER);
                    if (!empty($droppedTags)) {
                        foreach ($droppedTags as $closingTag) {
                            if (!in_array($closingTag[1], $openTags)) {
                                array_unshift($openTags, $closingTag[1]);
                            }
                        }
                    }
                }
                $truncate = mb_substr($truncate, 0, $spacepos);
            }
        }

        $truncate .= $ending;

        if ($considerHtml) {
            foreach ($openTags as $tag) {
                $truncate .= '</'.$tag.'>';
            }
        }

        return $truncate;
    }

}
