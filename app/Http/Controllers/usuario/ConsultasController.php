<?php

namespace App\Http\Controllers\usuario;

use GuzzleHttp\Client;
use App\Http\Controllers\Controller;

class ConsultasController extends Controller
{

    public function consultaRUC($nro){

        $client = new Client();
        $res = $client->request('GET', 'http://consultardoc.ceatec.com.pe/index.php?ruc=' . $nro, ['verify' => false] );

        $data = $res->getBody();
        if ($data) {

            $cliente_data = json_decode($data);

            return json_encode($cliente_data);

        }

    }

    public function consultarDNI($nro){

        $client = new Client();
        $res = $client->request('GET', 'https://api.reniec.cloud/dni/' . $nro, ['verify' => false] );

        $data = $res->getBody();
        if ($data) {

            $cliente_data = json_decode($data);

            return json_encode($cliente_data);

        }

    }

}
