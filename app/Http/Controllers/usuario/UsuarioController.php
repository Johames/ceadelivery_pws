<?php

namespace App\Http\Controllers\usuario;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegistarClienteRequest;
use App\Http\Requests\RegistarEmpresaRequest;
use App\Http\Requests\UserGeneralRequest;
use App\Mail\ConfirmarCuentaMail;
use App\Models\ModeloEmpresa;
use App\Models\ModeloPersona;
use App\Models\ModeloRubro;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{

    public function getIndex()
    {
        return view('cliente.usuarios')->with('titulo', 'Lista de Usuarios');
    }

    public function getCrearRol($rol)
    {
        $role = Role::create(['name' => $rol]);
        return json_encode($role);
    }


    public function getCrearUsuariosPrueba()
    {
        $role_1 = Role::create(['name' => 'super_administrador']);
        $role_2 = Role::create(['name' => 'administrador_empresa']);
        $role_3 = Role::create(['name' => 'editor_empresa']);
        $role_4 = Role::create(['name' => 'deliverista_empresa']);
        $role_5 = Role::create(['name' => 'cliente']);
        $role_6 = Role::create(['name' => 'deliverista_externo']);

        $rubro_1 = ModeloRubro::create(['nombre' => 'Restaurante']);
        $rubro_2 = ModeloRubro::create(['nombre' => 'Delivery']);

        $empresa_1 = ModeloEmpresa::create(['nombre_comercial' => 'Polleria el Fogon Dorado', 'ruc' => '12345678910', 'rubro_id' => $rubro_1->id]);
        // $empresa_2 = ModeloEmpresa::create(['nombre_comercial' => 'Delivery Izzi',            'ruc' => '16645677710', 'rubro_id' => $rubro_2->id]);

        $persona_1 = ModeloPersona::create(['ruc_dni' => '72462226', 'nombres' => 'Edwin Alexander', 'apellidos' => 'Bautista Villegas']);
        $persona_2 = ModeloPersona::create(['ruc_dni' => '83745639', 'nombres' => 'Johan James',     'apellidos' => 'Valles Paz']);
        // $persona_3 = ModeloPersona::create(['ruc_dni' => '75678456', 'nombres' => 'Luis Eduardo',    'apellidos' => 'Gonzales Mori']);
        // $persona_4 = ModeloPersona::create(['ruc_dni' => '86573454', 'nombres' => 'Elena',           'apellidos' => 'Herrera Majuan']);
        $persona_5 = ModeloPersona::create(['ruc_dni' => '34567856', 'nombres' => 'Camilo',          'apellidos' => 'Salas Tanchiva']);
        $persona_6 = ModeloPersona::create(['ruc_dni' => '67456789', 'nombres' => 'Isai',            'apellidos' => 'Caruajulca Bravo']);

        $user_1 = User::create(['email' => 'edwinbautista@ceatec.com', 'password' => bcrypt('12345678'), 'persona_id' => $persona_1->id]);
        $user_2 = User::create(['email' => 'johan@ceatec.com',         'password' => bcrypt('12345678'), 'persona_id' => $persona_2->id]);
        $user_3 = User::create(['email' => 'empresa@gmail.com',        'password' => bcrypt('12345678'), 'empresa_id' => $empresa_1->id]);
        $user_4 = User::create(['email' => 'editor@gmail.com',         'password' => bcrypt('12345678'), 'empresa_id' => $empresa_1->id]);
        $user_5 = User::create(['email' => 'deliverista@gmail.com',    'password' => bcrypt('12345678'), 'empresa_id' => $empresa_1->id]);
        $user_6 = User::create(['email' => 'cliente@gmail.com',        'password' => bcrypt('12345678'), 'persona_id' => $persona_5->id]);
        // $user_7 = User::create(['email' => 'deliveristaex@ceatec.com', 'password' => bcrypt('12345678'), 'empresa_id' => $empresa_2->id]);


        $user_1->assignRole('super_administrador');
        $user_2->assignRole('super_administrador');
        $user_3->assignRole('administrador_empresa');
        $user_4->assignRole('editor_empresa');
        $user_5->assignRole('deliverista_empresa');
        $user_6->assignRole('cliente');
        // $user_7->assignRole('deliverista_externo');

        return json_encode($user_1);
    }

    public function getAsignarRol($rol)
    {

        $user = User::where('id', 1)->first();
        $user->assignRole($rol);

        return json_encode($user);
    }

    public function postRegistarCliente(RegistarClienteRequest $request)
    {
        $codigo = Str::upper(Str::random(6));

        $persona = new ModeloPersona;
        $persona->ruc_dni   = $request->input('ruc_dni');
        $persona->nombres   = $request->input('nombres');
        $persona->apellidos = $request->input('apellidos');
        $persona->estado    = 0; // Desactivo
        $persona->save();

        $user = new User;
        $user->email      = $request->input('email');
        $user->password   = bcrypt($request->input('password'));
        $user->persona_id = $persona->persona_id;
        // $user->token_api  = $this->generarToken();
        $user->codigo_confirmacion = $codigo;
        $user->save();

        //Asigno su rol por defecto
        $user->assignRole('cliente');


        if (Auth::attempt(['email' => $user->email, 'password' => $request->input('password'), 'estado' => 0])) {

            $data = [
                "nombres" => $persona->nombres . ' ' . $persona->apellidos,
                "codigo" => $codigo,
            ];

            Mail::to($user->email)->queue(new ConfirmarCuentaMail($data));


            if (Auth::check()) {
                return json_encode(['status' => true, 'message' => 'Exito en registarse']);
            }

        } else {

            return json_encode(['status' => false, 'message' => 'Sus credenciales no son válidas']);

        }


        // return json_encode(['status'=> true, 'message' => 'Exito']);

    }

    public function generarToken(){

        $token = bin2hex(random_bytes(45));

        $user = User::where('token_api', $token)->first();

        if($user){

            $this->generarToken();

        } else {

            return $token;

        }

    }


    /************************************* */
    /************************************* */
    /************************************* */
    /************************************* */
    public function getListarUsuariosEmpreas()
    {
        $empresa_id = Auth::user()->empresa_id;
        $usuarios = User::with('roles')->where('empresa_id', $empresa_id)->get();

        return json_encode(['usuarios' => $usuarios, 'id' => Auth::user()->id]);
    }

    public function postRegistarEditorEmpresa(UserGeneralRequest $request)
    {
        $nombres    = $request->input('nombres');
        $email      = $request->input('email');
        $password   = bcrypt($request->input('password'));
        $empresa_id = Auth::user()->empresa_id;

        $usuario = User::create(['nombres' => $nombres, 'email' => $email, 'password' => $password, 'empresa_id' => $empresa_id]);
        $usuario->assignRole('editor_empresa');

        return json_encode(['status' => true, 'message' => 'Éxito se registro su empresa']);
    }

    public function postRegistarDeliveristaEmpresa(UserGeneralRequest $request)
    {
        $nombres    = $request->input('nombres');
        $email      = $request->input('email');
        $password   = bcrypt($request->input('password'));
        $empresa_id = Auth::user()->empresa_id;

        $usuario = User::create(['nombres' => $nombres, 'email' => $email, 'password' => $password, 'empresa_id' => $empresa_id]);
        $usuario->assignRole('deliverista_empresa');

        return json_encode(['status' => true, 'message' => 'Éxito se registro su empresa']);
    }

    public function getMostarUsuarioEmpresa($id)
    {
        $usuario = User::where('id', $id)->first();

        if ($usuario) {
            if ($usuario->empresa_id == Auth::user()->empresa_id) {

                $usuario = User::where('id', $id)->where('empresa_id', Auth::user()->empresa_id)
                ->with('roles')
                // ->select('usuario_id as id','descripcion','precio','llegada','estado')
                ->first();
                return json_encode($usuario);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }
    }

    public function getEliminarUsuarioEmpresa($id)
    {
        $usuario = User::where('id', $id)->first();

        if ($usuario) {
            if ($usuario->empresa_id == Auth::user()->empresa_id) {

                $roles = DB::table('model_has_roles')->where('model_id',$usuario->id)->delete();
                $usuario = User::where('id', $id)->where('empresa_id', Auth::user()->empresa_id)->delete();

                return json_encode(['status' => true, 'message' => 'Se ha eliminado el usuario']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }
    }


    public function getDesactivarUsuarioEmpresa($id)
    {
        $usuario = User::where('id', $id)->first();

        if ($usuario) {
            if ($usuario->empresa_id == Auth::user()->empresa_id) {

                $usuario->estado = 1;
                $usuario->save();

                return json_encode(['status' => true, 'message' => 'Se ha desactivado el usuario']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }
    }

    public function getActivarUsuarioEmpresa($id)
    {
        $usuario = User::where('id', $id)->first();

        if ($usuario) {
            if ($usuario->empresa_id == Auth::user()->empresa_id) {

                $usuario->estado = 0;
                $usuario->save();

                return json_encode(['status' => true, 'message' => 'Se ha activado el usuario']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }
    }


}
