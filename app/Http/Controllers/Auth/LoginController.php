<?php

namespace App\Http\Controllers\Auth;

use App\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
     public function showLoginForm(){
          return view('auth/login');
     }

     public function LogIn(LoginRequest $request){

        $email    = $request->input('email');
        $password = $request->input('password');
        $remember = ($request->input('remember') == 'on') ? true : false;

        $user = User::where('email', $email)->first();

        $user = User::where('email', $email)->first();

        if ($user) {
            if ($user->hasRole(['cliente'])) {
                if (Auth::attempt(['email' => $email, 'password' => $password, 'estado' => 0], $remember)) {

                    if(Auth::check()){

                        $redirect = '';

                        if (Auth::user()->hasAnyRole(['cliente'])) {

                            if(Auth::user()->verificado == '0'){

                                $redirect = '';

                            } else if(Auth::user()->verificado == '1'){

                                $redirect = '/verificar';

                            } else {

                                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);

                            }

                        }

                        return json_encode(['status'=> true, 'message' => 'Exito', 'redirect' => $redirect]);
                    }

                }else{
                    return json_encode(['status'=> false, 'message' => 'Sus credenciales no son válidas']);
                }
            }else{
                return json_encode(['status'=> false, 'message' => 'Sus credenciales no son válidas']);
            }
        }else{
            return json_encode(['status'=> false, 'message' => 'Sus credenciales no son válidas']);
        }

        return json_encode(['email'=> $email, 'password' => $password, 'remember' => $remember]);
     }

     public function LogOut(){
          Auth::LogOut();
          session()->flush();
          return redirect('/');
     }

}
