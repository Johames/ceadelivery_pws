<?php

namespace App\Http\Controllers\deliverista;

use App\Http\Requests\ValoracionDeliveristaRequest;
use App\Models\ModeloValoracionDelivery;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\ModeloDeliverista;

class ValoracionDeliveristaController extends Controller
{

    public function datosDeliverista($id){

        $datos = ModeloDeliverista::where('deliverista_id', $id)->first();

        return json_encode($datos);

     }


     public function AgregarModificarResenaDelivery(ValoracionDeliveristaRequest $request){

        $persona_id                 = Auth::user()->persona_id;

        $deliverista_id             = $request->input('id_valoracion');
        $valoracion                 = $request->input('valoracion');
        $resena                     = $request->input('resena');

        $ver = ModeloValoracionDelivery::where('deliverista_id', $deliverista_id)
                                        ->where('persona_id', $persona_id)
                                        ->first();

        if(empty($ver)){

            $valor = ModeloValoracionDelivery::firstOrCreate(
                [
                    'persona_id'                => $persona_id,
                    'deliverista_id'            => $deliverista_id
                ],
                [
                    'estrellas'         => $valoracion,
                    'resena'            => $resena
                ]
            );

            return json_encode(['status' => true, 'message' => 'Valoración agregada']);

        } else {

            $valor = ModeloValoracionDelivery::find($ver->calificacion_delivery_id);

            $valor->estrellas       = $valoracion;
            $valor->resena          = $resena;
            $valor->save();

            return json_encode(['status' => true, 'message' => 'Valoración modificada']);

        }

     }

}
