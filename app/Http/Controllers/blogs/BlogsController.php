<?php

namespace App\Http\Controllers\blogs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloBlog;

class BlogsController extends Controller
{

    public function getIndex(){

        $blogs = ModeloBlog::with('ModeloComentariosBlogs')->where('estado', '0')
                ->orderBy('created_at','desc')
                ->get();

        return view('blogs.blogs', compact('blogs'));
    }

    public function getBlog($idBlog){

        $blogs = ModeloBlog::with('ModeloComentariosBlogs')->where('blog_id', $idBlog)->where('estado', '0')->first();

        return view('componentes.blogs.modal_blog', compact('blogs'));

    }

}
