<?php

namespace App\Http\Controllers\venta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FileController;
use App\Mail\ConfirmacionCompra;
use App\Http\Requests\CarritoRequest;
use App\Models\ModeloDetalleMetodoEnvio;
use App\Models\ModeloDetalleMetodoPago;
use App\Models\ModeloDetallePedido;
use App\Models\ModeloDetalleProducto;
use App\Models\ModeloDireccion;
use App\Models\ModeloMetodoEnvio;
use App\Models\ModeloNotificacion;
use App\Models\ModeloPedido;
use App\Models\ModeloPersona;
use App\Models\ModeloPromocion;
use App\User;
use Carbon\Carbon;
use Culqi\Culqi;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class CarritoController extends Controller
{

    protected $API_KEY = 'sk_test_17da4f1f91f81aeb';

    // FUNCION PARA AGREGAR PRODUCTO EN EL CARRITO (OK)
    public function postAddCarrito($id, $cant)
    {
        $persona_id = Auth::user()->persona_id;

        $producto_detalle = ModeloDetalleProducto::where('producto_detalle_id', $id)->first();
        $producto_detalle_precio = $this->getPrecio($producto_detalle->estado_desc, $producto_detalle->tipo_desc, $producto_detalle->descuento, $producto_detalle->precio);

        //Existe el producto a agregar
        if ($producto_detalle) {

            //Existe un pedido de esa empresa? SI
            $pedido = ModeloPedido::where('persona_id', $persona_id)
                                    ->where('empresa_id', $producto_detalle->empresa_id)
                                    ->where('estado', '0')
                                    ->where('paso', '<', 4)
                                    ->first();
            if ($pedido) {
                //Agregar producto al pedido

                $pedido_detalle_v = ModeloDetallePedido::where('pedido_id', $pedido->pedido_id)
                                    ->where('producto_detalle_id', $producto_detalle->producto_detalle_id)
                                    ->first();

                // Validar que no agrege el mismo producto otra ves
                if (!$pedido_detalle_v) {

                    // Validar stock del producto
                    if ($producto_detalle->stock >= $cant) {

                        $pedido_detalle = new ModeloDetallePedido;

                        $pedido_detalle->pedido_id                  = $pedido->pedido_id;
                        $pedido_detalle->producto_detalle_id        = $producto_detalle->producto_detalle_id;
                        $pedido_detalle->cantidad                   = $cant;
                        $pedido_detalle->precio                     = $producto_detalle_precio;
                        $pedido_detalle->precio_referencial         = $producto_detalle->precio;//precio original
                        $pedido_detalle->total                      = $cant * $producto_detalle_precio; // total
                        $pedido_detalle->save();


                        $pedido->total_pagar                        = $this->getTotal($pedido->codigo);
                        $pedido->save();


                        return json_encode(['status' => true, 'message' => 'Se ha agregado al carrito', 'codigo_pedido' => $pedido->codigo]);

                    }else{

                        return json_encode(['status' => false, 'message' => 'Stock no disponible']);
                    }
                }else{

                    // Validamos el stock del producto, y sumamos uno más al carrito
                    if ($this->getValidateStock($producto_detalle->producto_detalle_id, ($cant + $pedido_detalle_v->cantidad))) {

                        $pedido_detalle_v->cantidad                 = ($cant + $pedido_detalle_v->cantidad);
                        $pedido_detalle_v->precio                   = $producto_detalle_precio;
                        $pedido_detalle_v->precio_referencial       = $producto_detalle->precio;
                        $pedido_detalle_v->total                    = ($pedido_detalle_v->cantidad) * $producto_detalle_precio; //total
                        $pedido_detalle_v->save();

                        $pedido->total_pagar                        = $this->getTotal($pedido->codigo);
                        $pedido->save();

                        return json_encode(['status' => true, 'message' => 'Se sumo ' . $cant . ' más al carrito', 'codigo_pedido' => $pedido->codigo]);

                    }else{
                        return json_encode(['status' => false, 'message' => 'Stock llego al límite']);
                    }

                }

                //calcular descuento*

            }else{

                // Validar stock del producto
                if ($producto_detalle->stock >= $cant) {

                    $codigo_pedido = 'PED-' . Carbon::now()->format('YmdHms') . $persona_id . $producto_detalle->empresa_id;

                    //Crear un nuevo pedido
                    $nuevo_pedido = new  ModeloPedido;
                    $nuevo_pedido->persona_id       = $persona_id;
                    $nuevo_pedido->empresa_id       = $producto_detalle->empresa_id;
                    $nuevo_pedido->codigo           = $codigo_pedido;
                    $nuevo_pedido->save();
                    //$codigo = 'PED-' . Carbon::now()->format('YmdHms') . 10102;

                    //Agregamos el producto al detalle
                    $nuevo_pedido_detalle = new ModeloDetallePedido;
                    $nuevo_pedido_detalle->pedido_id                = $nuevo_pedido->pedido_id;
                    $nuevo_pedido_detalle->producto_detalle_id      = $producto_detalle->producto_detalle_id;
                    $nuevo_pedido_detalle->cantidad                 = $cant;
                    $nuevo_pedido_detalle->precio                   = $producto_detalle_precio;
                    $nuevo_pedido_detalle->precio_referencial       = $producto_detalle->precio;// precio original
                    $nuevo_pedido_detalle->total                    = $cant * $producto_detalle_precio;// total
                    $nuevo_pedido_detalle->save();

                    if ($nuevo_pedido->total_pagar) {
                        # code...
                        $nuevo_pedido->total_pagar += $producto_detalle_precio;
                    }else{
                        $nuevo_pedido->total_pagar = $producto_detalle_precio;
                    }

                    $nuevo_pedido->save();



                    return json_encode(['status' => true, 'message' => 'Se ha agregado al carrito, nuevo pedido', 'codigo_pedido' =>  $codigo_pedido]);

                }else{
                    return json_encode(['status' => false, 'message' => 'Stock llego al límite']);
                }

            }

        }else{

            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);

        }

    }

    // FUNCION PARA EDITAR CATIDAD DEL PRODUCTO EN EL CARRITO (OK) - [mejorar]
    public function getEditarCantidad($detalle_id, $cant)
    {
        if ($cant > 0) {
            $persona_id = Auth::user()->persona_id;

            $pedido_detalle = ModeloDetallePedido::where('detalle_id', $detalle_id)->first();

            // Existe el detalle pedido
            if ($pedido_detalle) {

                $pedido = ModeloPedido::where('persona_id', $persona_id)// El pedido es de la persona logueada?
                ->where('pedido_id', $pedido_detalle->pedido_id) // El pedido es el mismo que el del detalle?
                ->where('estado', '0') // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
                ->first();

                // Validar que solo pueda editar su pedido
                if ($pedido) {

                    $producto_detalle = ModeloDetalleProducto::where('producto_detalle_id', $pedido_detalle->producto_detalle_id)->first();
                    $producto_detalle_precio = $this->getPrecio($producto_detalle->estado_desc, $producto_detalle->tipo_desc, $producto_detalle->descuento, $producto_detalle->precio);

                    // Validar Stock del producto
                    if ($this->getValidateStock($pedido_detalle->producto_detalle_id, $cant)) {

                        $pedido_detalle->cantidad = $cant;
                        $pedido_detalle->precio = $producto_detalle_precio;
                        $pedido_detalle->precio_referencial = $producto_detalle->precio;
                        $pedido_detalle->total = ($pedido_detalle->cantidad) * $producto_detalle_precio; //total
                        $pedido_detalle->save();


                        $pedido->total_pagar = $this->getTotal($pedido->codigo);
                        $pedido->save();

                        // var suma = 1;
                        // suma = suma + 1;
                        // suma += 1;

                        return json_encode(['status' => true, 'message' => 'Se editó la cantidad']);
                    }else{
                        return json_encode(['status' => false, 'message' => 'Stock no disponible']);
                    }

                }else{
                    return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
                }
            }
        }else{
            return json_encode(['status' => false, 'message' => 'El stock no puede ser ' . $cant]);
        }
    }

    // FUNCION PARA ELIMINAR PRODUCTO DEL CARRITO
    public function getEliminar($detalle_id)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido_detalle = ModeloDetallePedido::where('detalle_id', $detalle_id)->first();

        // Existe el detalle pedido
        if ($pedido_detalle) {

            $pedido = ModeloPedido::with('ModeloDetallePedidos')
            ->where('persona_id', $persona_id)// El pedido es de la persona logueada?
            ->where('pedido_id', $pedido_detalle->pedido_id) // El pedido es el mismo que el del detalle?
            ->where('estado', '0') // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
            ->where('paso', '<', 4)
            ->first();



             // Validar que solo pueda editar su pedido y menor del paso 3 donde se paga
            if ($pedido) {

                // Eliminar del carrito
                $pedido_detalle->delete();

                //Actualizamos el total en la tabla
                $pedido->total_pagar = $this->getTotal($pedido->codigo);
                $pedido->save();
                $lasted = false;

                // El pedido tiene productos?
                if ($pedido->ModeloDetallePedidos) {

                    $num_productos = count($pedido->ModeloDetallePedidos);

                    if ($num_productos == 1) {
                        $lasted = true;

                        $pedido->delete();
                    }
                }

                return json_encode(['status' => true, 'message' => 'Se eliminó del carrito', 'lasted' => $lasted]);

            }else{
                return json_encode(['status' => false, 'message' => 'Su pedido ya fue pagado, no se puede modificar']);
            }
        } else {
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }


    }

    // COMPROBAR SI LA CANTIDAD SOCILICITA ESTA EN STOCK
    public function getValidateStock($id, $cant)
    {
        $producto_detalle = ModeloDetalleProducto::where('producto_detalle_id', $id)->first();

        $flat = false;

        //   100 <= 100
        if ($cant <= $producto_detalle->stock) {
            $flat = true;
        }

        return $flat;
    }

    // OBETENER EL PRECIO CUANDO TIENE DESCUENTO SEGUN EL TIPO DE DESCUENTO
    public function getPrecio($estado_desc, $tipo_desc, $descuento, $precio)
    {

        $precio_final = $precio;

        // Validar estado del descuento activo 0
        if ($estado_desc == '0') {

            //Según el tidpo de descuento
            switch ($tipo_desc) {

                case '0':

                    // Descuento por soles

                    $precio_final = $precio - $descuento;
                    break;

                case '1':

                    // Descuento por porcentaje

                    $precio_final = $precio - ($precio * ($descuento/100));
                    break;

                default:

                    $precio_final = $precio;
                    break;
            }

        }

       return number_format((float)$precio_final, 2, '.', '');

    }

    /*
        FUNCIONES GENERALES PARA TODOS LOS PASOS
    */

    // OK
    public function getPedidoTotales()
    {
        $persona_id = Auth::user()->persona_id;
        $codigo = session('codigo_pedido_activo');

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                    ->where('codigo', $codigo)
                                    ->first();

        // $totales = [];

        if ($pedido) {



            $pedidos_detalle = ModeloDetallePedido::where('pedido_id', $pedido->pedido_id)->get();

            // $metodo_envio = ModeloDetalleMetodoPago::where('metodo_pago_emp_id', $pedido->metodo_pago_emp_id)->first();

            $productos_total_desc = $pedidos_detalle->sum(function ($row) {
                return $row->precio * $row->cantidad;
            });

            $productos_total_orig = $pedidos_detalle->sum(function ($row) {
                return $row->precio_referencial * $row->cantidad;
            });

            $metodo_envio_precio  = $pedido->precio_envio == null ? 0 :  $pedido->precio_envio ;
            $metodo_pago_precio   = $pedido->precio_comision == null ? 0 : $pedido->precio_comision;
            $horro   = $productos_total_orig - $productos_total_desc;
            $total   = $productos_total_desc  + $metodo_envio_precio + $metodo_pago_precio;

            $totales = [
                "productos_total_desc" => (float)number_format($productos_total_desc, 2, '.', ''),
                "productos_total_orig" => (float)number_format($productos_total_orig, 2, '.', ''),
                "metodo_envio_precio"  => (float)number_format($metodo_envio_precio, 2, '.', ''),
                "metodo_pago_precio"   => (float)number_format($metodo_pago_precio, 2, '.', ''),
                "horro"                => (float)number_format($horro, 2, '.', ''),
                "total"                => (float)number_format($total, 2, '.', ''),
            ];


            return json_encode($totales);
        }else{

            return json_encode($pedido);
        }

    }

    // OK -> NO EN RUTA
    public function getTotal($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                    ->where('codigo', $codigo)
                                    ->first();
        if ($pedido) {



            $pedidos_detalle = ModeloDetallePedido::where('pedido_id', $pedido->pedido_id)->get();

            // $metodo_envio = ModeloDetalleMetodoPago::where('metodo_pago_emp_id', $pedido->metodo_pago_emp_id)->first();

            $productos_total_desc = $pedidos_detalle->sum(function ($row) {
                return $row->precio * $row->cantidad;
            });

            $total   = $productos_total_desc ;

            $totales = (float)number_format($total,  2, '.', '');


            return ($totales);
        }

    }

    /*
        PASO UNO
    */

    // Mostrar datos del paso uno [comentarios, coddigo promocion]
    public function getMostarPedidoCarrito()
    {
        $persona_id = Auth::user()->persona_id;
        $codigo     = session('codigo_pedido_activo');
        $today = Carbon::now()->format('Y-m-d');

        $pedido = ModeloPedido::with('ModeloEmpresa', 'ModeloEmpresa.ModeloRubro', 'ModeloDetalleMetodoPago', 'ModeloDetalleMetodoPago.ModeloMetodoPago', 'ModeloPersona')
                            ->where('persona_id', $persona_id)
                            ->where('codigo', $codigo)
                            ->where('estado', 0)
                            // ->select('codigo',
                            //          'comentarios'
                            //         'direciones_id as direccion',
                            //         'metodo_envio_emp_id as metodo_envio'
                            //         )
                            ->first();

        if($pedido->paso == 3){

            if ($pedido->codigo_descuento) {
                // Fecha de hoy

                $promociones = ModeloPromocion::where('codigo', $pedido->codigo_descuento)
                                            ->where('empresa_id', $pedido->empresa_id )
                                            ->where('fecha_inicio', '>=', $today )
                                            ->where('fecha_fin', '<=', $today )
                                            ->where('estado', '0')
                                            ->first();

                // El codigo cumple con las condicones de fecha ?
                if ($promociones) {

                    if ($promociones->tipo == 0) {
                        $pedido->descuento = $promociones->descuento;
                    }

                    if ($promociones->tipo == 1) {
                        $pedido->descuento = $pedido->total_pagar - ($pedido->total_pagar * ($promociones->descuento/100));
                    }

                    $pedido->save();

                } else {

                    $pedido->descuento = 0;
                    $pedido->save();

                }

            }

        }

        return json_encode($pedido);

    }

    // FUNCION PARA APLICAR UN CODIGO DE DESCUENTO A LA COMPRA
    public function getAplicarCodigoPromocion($codigo_promocion)
    {
        //
        // $querys = Curso::where('name', 'Like', '%' . $term . '%')->select('name as label')->get();

        $persona_id = Auth::user()->persona_id;
        $codigo_pedido     = session('codigo_pedido_activo');

        $pedido = ModeloPedido::where('persona_id', $persona_id)// El pedido es de la persona logueada?
                            ->where('codigo', $codigo_pedido)
                            ->where('estado', '0') // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
                            ->first();

        // Fecha de hoy
        $today = Carbon::now()->format('Y-m-d');

        // Existe un pedido pendiente
        if ($pedido) {

            $promociones = ModeloPromocion::where('codigo', $codigo_promocion)
            ->where('empresa_id', $pedido->empresa_id )
            ->where('fecha_inicio', '>=', $today )
            ->where('fecha_fin', '<=', $today )
            ->where('estado', '0')
            ->first();

            // El codigo cumple con las condicones de fecha ?
            if ($promociones) {

                $pedido->codigo_descuento = $codigo_promocion;
                $pedido->save();

                return json_encode(["status" => true, 'message' => 'Código de descuento aplicado']);

            }else{
                return json_encode(["status" => true, 'message' => 'Código no válido']);
            }

        }else{
            return json_encode(["status" => true, 'message' => 'Nesecita iniciar un pedido']);
        }

    }



    public function postGuardarPaso1(Request $request)
    {
        // GUARDAR COMENTARIOS
        $persona_id = Auth::user()->persona_id;
        $codigo = session('codigo_pedido_activo');

        $user = User::where('persona_id', $persona_id)
                    ->where('estado', '0')
                    ->where('verificado', '0')
                    ->first();

        $comentarios = $request->input('comentarios');

        if($user){

            $pedido = ModeloPedido::with('ModeloDetallePedidos')->where('persona_id', $persona_id)
                                    ->where('codigo', $codigo)
                                    ->where('estado', 0)
                                    ->first();
            if ($pedido) {

                $pedido->comentarios = $comentarios;
                $pedido->paso = 2;
                $pedido->save();

                return json_encode(['status' => true, 'message' => 'Se han guardado los cambios', 'codigo' => $codigo]);
            }

        } else {

            return json_encode(['status' => 'Confirmar', 'message' => 'Debe activar su email para continuar']);

        }

        // return json_encode(request()->all());
    }

     /*
        PASO DOS
    */

    public function postGuardarPaso2(Request $request)
    {
        // Direccion de Entrega
        $persona_id = Auth::user()->persona_id;
        $codigo = session('codigo_pedido_activo');


        $direcciones_id = $request->input('direcion_id'); //direccion_id
        $metodo_envio_emp_id = $request->input('envio_id');

        $pedido = ModeloPedido::with('ModeloDetallePedidos')->where('persona_id', $persona_id)
                                    ->where('codigo', $codigo)
                                    ->where('estado', 0)
                                    ->first();
        if ($pedido) {
            // Falta un if para verificar si existe el metodo de envio
            $metodo_envio = ModeloDetalleMetodoEnvio::where('metodo_envio_emp_id', $metodo_envio_emp_id)->first();

            if ($metodo_envio) {

                $pedido->direciones_id = $direcciones_id;
                $pedido->metodo_envio_emp_id = $metodo_envio_emp_id;
                $pedido->precio_envio = $metodo_envio->precio;
                $pedido->paso = 3;
                $pedido->save();

                return json_encode(['status' => true, 'message' => 'Se han guardado los cambios', 'codigo' => $codigo]);

            } else {

                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);

            }

        }

    }

    public function postGuardarPaso3(Request $request)
    {
        // Metodo de Envío
        $persona_id = Auth::user()->persona_id;
        $codigo = session('codigo_pedido_activo');
        $today = Carbon::now()->format('Y-m-d');

        $metodo_pago_emp_id = $request->input('pago_id'); //metodo de pago

        $pedido = ModeloPedido::with('ModeloDetallePedidos')->where('persona_id', $persona_id)
                                    ->where('codigo', $codigo)
                                    ->where('estado', 0)
                                    ->first();
        if ($pedido) {

            // Falta un if para verificar si existe el metodo de envio
            $metodo_pago = ModeloDetalleMetodoPago::where('metodo_pago_emp_id', $metodo_pago_emp_id)->first();

            // $errorStock = $this->comprobarStockVigente($pedido->pedido_id);

            // return $errorStock;

            // if ($errorStock->status) {

                if($metodo_pago){

                    $pedido->metodo_pago_emp_id = $metodo_pago_emp_id;
                    $pedido->precio_comision = $metodo_pago->precio_comision;
                    $pedido->paso = 4;
                    $pedido->save();


                    foreach($pedido->ModeloDetallePedidos as $detalle_pedido){

                        $producto = ModeloDetalleProducto::where('producto_detalle_id', $detalle_pedido->producto_detalle_id)->first();

                        $producto->stock = $producto->stock - $detalle_pedido->cantidad;
                        $producto->save();

                    }

                    return json_encode(['status' => true, 'message' => 'Se han guardado los cambios', 'codigo' => $codigo]);

                } else {

                    return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);

                }

                // return json_encode(['status' => false, 'message' => 'Si hay estock del ' . $errorStock->producto . '.']);

            // } else {

            //     return json_encode(['status' => false, 'message' => 'El estock del ' . $errorStock->producto . 'es menor que la cantidad solicitada']);

            // }

        }
    }

    //funcion comprobar stock de los productos
    public function comprobarStockVigente($idPedido) {

        $pedido = ModeloPedido::with('ModeloDetallePedidos')->where('pedido_id', $idPedido)->first();

        foreach ($pedido->ModeloDetallePedidos as $detalle) {

            $prod = ModeloDetalleProducto::with('ModeloProducto')->where('producto_detalle_id', $detalle->producto_detalle_id)->first();

            if ($detalle->cantidad > $prod->stock) {

                break;

                return json_encode(['estado' => false, 'producto' => $prod->ModeloProducto->nombre, 'stock' => $prod->stock]);

            }

        }

    }

    // OK
    public function getListarMetodosEnvio()
    {
        //
        $persona_id = Auth::user()->persona_id;
        $codigo     = session('codigo_pedido_activo');

        $pedido = ModeloPedido::where('persona_id', $persona_id)// El pedido es de la persona logueada?
                  ->where('codigo', $codigo) // Comprobar el codigo del pedido,
                  ->where('estado', '0') // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
                  ->first();

        if($pedido){

            $metodos_envio = ModeloDetalleMetodoEnvio::with('ModeloMetodoEnvio')->where('empresa_id', $pedido->empresa_id)->where('estado', 0)->get();

            // return json_encode($metodos_envio);
            return view('componentes.pasos.paso2', compact('metodos_envio'));
        }
    }

    // Agregar la imagen de la constancia de pago
    public function agregarConstancia(Request $request)
    {

        $this->validate($request, [
            'constancia' => 'required'
        ], [
          'constancia.required' => 'Seleccione la constancia de pago.'
        ]);

        $persona_id = Auth::user()->persona_id;
        $codigo = session('codigo_pedido_activo');

        $constancia      = $request->file('constancia');

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                            ->where('codigo', $codigo)
                            ->where('estado', 0)
                            ->first();

        if($pedido){

            if($constancia){

                $ruta_constancia = FileController::saveImg($constancia, null, 'cd_pedido', null, 'constancia');

                $pedido->voucher    = $ruta_constancia;
                $pedido->pagado     = 0;
                $pedido->save();

                return json_encode(['status' => true, 'message' => 'Constancia de pago registrada']);

            } else {
                return json_encode(['status' => false, 'message' => 'No hay imagen que gusradar.']);
            }

        } else {
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

    }

    public function postGuardarPaso4(Request $request)
    {
        // FACTURACION
        $persona_id = Auth::user()->persona_id;
        $correo     = Auth::user()->email;
        $codigo = session('codigo_pedido_activo');
        $today = Carbon::now()->format('Y-m-d');

        // $API_KEY = getenv("API_KEY_CULQUI");
        $culqi = new Culqi(array("api_key" => $this->API_KEY ));

        if($request->input('comprobante') == 1){

            $this->validate($request, [
                'comprobante' => 'required',
                'cliente' => 'required',
                'nro_documento' => 'required|min:8|max:11',
                'telefono' => 'required',
                'direccion' => 'required'
            ], [
              'comprobante.required' => 'Seleccione el tipo de comprobante',
              'cliente.required'  => 'Ingrese su nombre por favor',
              'nro_documento.min' => 'Su documento debe tener al menos 8 dígitos',
              'telefono.required' => 'Ingrese un número de telefono',
              'direccion.required' => 'Ingrese una dirección'
            ]);

        } else if($request->input('comprobante') == 2){

            $this->validate($request, [
                'comprobante' => 'required',
                'cliente' => 'required',
                'nro_documento' => 'required|min:11|max:11',
                'telefono' => 'required',
                'direccion' => 'required'
            ], [
              'comprobante.required' => 'Seleccione el tipo de comprobante',
              'cliente.required'  => 'Ingrese su nombre por favor',
              'nro_documento.min' => 'Su documento debe tener 11 dígitos',
              'telefono.required' => 'Ingrese un número de telefono',
              'direccion.required' => 'Ingrese una dirección'
            ]);

        } else {
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

        if(strlen($request->input('nro_documento')) != 8){
            if(strlen($request->input('nro_documento')) != 11){
                return json_encode(['status' => 'error', 'message' => 'Ingrese un numero de documento válido.']);
            }
        }

        $comprobante = $request->input('comprobante');
        $cliente = $request->input('cliente');
        $nro_documento = $request->input('nro_documento');
        $telefono = $request->input('telefono');
        $direccion = $request->input('direccion');

        $cargo      = [];

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                            ->where('codigo', $codigo)
                            ->where('estado', 0)
                            ->first();


        if($pedido->token_pago_card){

            try {
                $cargo = $culqi->Charges->get($pedido->token_pago_card);
            } catch (Exception $e) {
                return json_encode(['status' => false, 'message' => $e->getMessage()]);
            }

            if($cargo->capture){

                $pedido->cliente_facturacion            = $cliente;
                $pedido->tipo_documento_facturacion     = $comprobante;
                $pedido->nro_documento_facturacion      = $nro_documento;
                $pedido->nro_telefono_facturacion       = $telefono;
                $pedido->direccion_facturacion          = $direccion;
                $pedido->fecha_pedido                   = $today;
                $pedido->pagado                         = 0;
                $pedido->paso                           = 5;
                $pedido->estado                         = 1;
                $pedido->save();

                $notificacion = new ModeloNotificacion;
                $notificacion->empresa_id       = $pedido->empresa_id;
                $notificacion->tipo_accion      = 'pedido';
                $notificacion->titulo           = 'Nuevo pedido';
                $notificacion->descripcion      = 'Un nuevo pedido ha sido registrado.';
                $notificacion->id_accion        = $pedido->pedido_id;
                $notificacion->visto            = 0;
                $notificacion->estado           = 0; // Desactivo
                $notificacion->save();

                Mail::to($correo)->queue(new ConfirmacionCompra($pedido));

                return json_encode(['status' => true, 'message' => 'Se han guardado los cambios']);

            } else {

                try {
                    $capture = $culqi->Charges->capture($pedido->token_pago_card);
                } catch (Exception $e) {
                    return json_encode(['status' => false, 'message' => $e->getMessage()]);
                }

                if($capture->outcome){

                    $pedido->cliente_facturacion            = $cliente;
                    $pedido->tipo_documento_facturacion     = $comprobante;
                    $pedido->nro_documento_facturacion      = $nro_documento;
                    $pedido->nro_telefono_facturacion       = $telefono;
                    $pedido->direccion_facturacion          = $direccion;
                    $pedido->fecha_pedido                   = $today;
                    $pedido->pagado                         = 0;
                    $pedido->paso                           = 5;
                    $pedido->estado                         = 1;
                    $pedido->save();

                    $notificacion = new ModeloNotificacion;
                    $notificacion->empresa_id       = $pedido->empresa_id;
                    $notificacion->tipo_accion      = 'pedido';
                    $notificacion->titulo           = 'Nuevo pedido';
                    $notificacion->descripcion      = 'Un nuevo pedido ha sido registrado.';
                    $notificacion->id_accion        = $pedido->pedido_id;
                    $notificacion->visto            = 0;
                    $notificacion->estado           = 0; // Desactivo
                    $notificacion->save();

                    Mail::to($correo)->queue(new ConfirmacionCompra($pedido));

                    return json_encode(['status' => true, 'message' => 'Se han guardado los cambios']);

                } else {

                    $pedido->paso                   = 3;
                    $pedido->estado                 = 0;
                    $pedido->save();

                    return json_encode($capture);

                }

            }

        } else {

            $pedido->cliente_facturacion            = $cliente;
            $pedido->tipo_documento_facturacion     = $comprobante;
            $pedido->nro_documento_facturacion      = $nro_documento;
            $pedido->nro_telefono_facturacion       = $telefono;
            $pedido->direccion_facturacion          = $direccion;
            $pedido->fecha_pedido                   = $today;
            $pedido->paso                           = 5;
            $pedido->estado                         = 1;
            $pedido->save();

            $notificacion = new ModeloNotificacion;
            $notificacion->empresa_id       = $pedido->empresa_id;
            $notificacion->tipo_accion      = 'pedido';
            $notificacion->titulo           = 'Nuevo pedido';
            $notificacion->descripcion      = 'Un nuevo pedido ha sido registrado.';
            $notificacion->id_accion        = $pedido->pedido_id;
            $notificacion->visto            = 0;
            $notificacion->estado           = 0; // Desactivo
            $notificacion->save();

            Mail::to($correo)->queue(new ConfirmacionCompra($pedido));

            return json_encode(['status' => true, 'message' => 'Se han guardado los cambios']);

        }


    }

    public function getMostarFacturacion()
    {
        $persona_id = Auth::user()->persona_id;
        $codigo = session('codigo_pedido_activo');

        $pedido = ModeloPedido::with('ModeloPersona', 'ModeloDireccion')
                  ->where('persona_id', $persona_id)// El pedido es de la persona logueada?
                  ->where('codigo', $codigo) // Comprobar el codigo del pedido,
                  ->where('estado', '0') // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
                  ->first();


        // $persona = ModeloPersona::where('persona_id', $persona_id)->first();
        // $user->hasRole('writer');

        $tipo = 1;
        $cliente = '';

        if (strlen($pedido->ModeloPersona->ruc_dni) == 8) {

            $tipo = 1;
            $cliente = $pedido->ModeloPersona->nombres . ' ' . $pedido->ModeloPersona->apellidos;

        }else if (strlen($pedido->ModeloPersona->ruc_dni) == 11){

            $tipo = 2;

            if ($pedido->ModeloPersona->razon_social) {

                $cliente = $pedido->ModeloPersona->razon_social;
            }else{
                $cliente = $pedido->ModeloPersona->nombre_comercial;
            }

        }

        $datos = [
            "codigo"        => $codigo,
            "cliente"       => $cliente,
            "comprobante"   => $tipo,
            "nro_documento" => $pedido->ModeloPersona->ruc_dni,
            "direccion"     => $pedido->ModeloDireccion->direccion,
            "telefono"      => $pedido->ModeloPersona->telefono,
        ];

        return json_encode($datos);
    }

    // OK
    public function getListarMetodosPago()
    {
        //
        $persona_id = Auth::user()->persona_id;
        $codigo     = session('codigo_pedido_activo');

        $pedido = ModeloPedido::where('persona_id', $persona_id)// El pedido es de la persona logueada?
                  ->where('codigo', $codigo) // Comprobar el codigo del pedido,
                  ->where('estado', '0') // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
                  ->first();

        if($pedido){

            $metodos_pago = ModeloDetalleMetodoPago::with('ModeloMetodoPago')->where('empresa_id', $pedido->empresa_id)->where('estado', 0)->get();

            // return json_encode($metodos_pago);
            return view('componentes.pasos.paso3', compact('metodos_pago'));
        }
    }

    public function postGuardarPaso5(Request $request)
    {
        // Confirmar pedido
        // Metodo de Pago
        $persona_id = Auth::user()->persona_id;

        $codigo = $request->input('codigo');

        $pedido = ModeloPedido::with('ModeloDetallePedidos')->where('persona_id', $persona_id)
                                    ->where('codigo', $codigo)
                                    ->where('estado', 0)
                                    ->first();
        if ($pedido) {


            $pedido->estado = 1; //Cambia a estado pendiente
            $pedido->paso = 5;
            $pedido->save();

            return json_encode(['status' => true, 'message' => 'Se han guardado los cambios', 'codigo' => $codigo]);
        }
    }


    public function getListarPedidosPendientes()
    {
        $persona_id = Auth::user()->persona_id;

        $pedidos = ModeloPedido::where('persona_id', $persona_id)->where('estado', '0')->get();


        return json_encode($pedidos);
    }


    public function getMostarPedidoPeniente($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();

        return json_encode($pedido);
    }

    public function getListarProductosNav()
    {
        $persona_id = Auth::user()->persona_id;

        $pedidos = ModeloPedido::with('ModeloDetallePedidos')
                                ->where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->get();

        if ($pedidos) {

            $count_pedidos   = count($pedidos);// Número de pedidos
            $count_productos = 0;
            $subtotal = 0;

            foreach ($pedidos as $pedido) {

                $count_productos = count($pedido->ModeloDetallePedidos);

                foreach ($pedido->ModeloDetallePedidos as $detalle) {

                    $subtotal += $detalle->precio * $detalle->cantidad;


                }

                $pedido = 0;
            }

            $data = [
                "count_pedidos"   => $count_pedidos,
                "count_productos" => $count_productos,
                "subtotal"        => number_format((float) $subtotal, 2, '.', ''),
            ];

            return json_encode($data);

        }


    }

    public function getListarProductosModal()
    {
        $persona_id = Auth::user()->persona_id;

        $pedidos = ModeloPedido::with(
            'ModeloEmpresa:empresa_id,slug,nombre_comercial,icono',
            'ModeloDetallePedidos:detalle_id,pedido_id,producto_detalle_id,cantidad,precio,precio_referencial',
            'ModeloDetallePedidos.ModeloDetalleProducto:producto_detalle_id,producto_id',
            'ModeloDetallePedidos.ModeloDetalleProducto.ModeloProducto:producto_id,nombre',
            'ModeloDetallePedidos.ModeloDetalleProducto.PrimeraImagen:producto_imagen_id,producto_detalle_id,ruta'
        )
        ->where('persona_id', $persona_id)
        ->where('estado', '0')
        ->get();

        // $pedidos = json_encode($pedidos);
        // return json_encode($pedidos);
        return view('componentes.carrito.carrito_lista_modal', compact('pedidos'))->render();
    }

    // FUNCION PARA LISTAR LOS PORDUCTOS DEL CARRITO DE UN PEDIDO
    public function getListarProductosPedido(Request $request)
    {

        $persona_id = Auth::user()->persona_id;
        $codigo     = session('codigo_pedido_activo');

        //Listamos el pedido, comprobamos que este pendiente [estado = 0] y que sea de la persona logueada
        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();

        if($pedido){

            if (request()->has('filtro')) {

                $filtro = $request->filtro;

                switch ($request->filtro) {
                    case '1':

                        $pedido_detalle = ModeloDetallePedido::with('ModeloDetalleProducto:producto_detalle_id,producto_id,categoria_id,marca_id,unidad_id,color_id,talla_id',
                                                                    'ModeloDetalleProducto.ModeloProducto:producto_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloUnidad:unidad_id,nombre,abreviatura',
                                                                    'ModeloDetalleProducto.ModeloCategoria:categoria_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloMarca:marca_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloColor:color_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloTalla:talla_id,nombre',
                                                                    'ModeloDetalleProducto.PrimeraImagen')
                                                                ->where('pedido_id', $pedido->pedido_id)->paginate(10);

                        return view('componentes.carrito.productos_card', compact('pedido_detalle', 'filtro'))->render();

                        break;
                    case '2':

                        $pedido_detalle = ModeloDetallePedido::with('ModeloDetalleProducto:producto_detalle_id,producto_id,categoria_id,marca_id,unidad_id,color_id,talla_id',
                                                                'ModeloDetalleProducto.ModeloProducto:producto_id,nombre',
                                                                'ModeloDetalleProducto.ModeloUnidad:unidad_id,nombre,abreviatura',
                                                                'ModeloDetalleProducto.ModeloCategoria:categoria_id,nombre',
                                                                'ModeloDetalleProducto.ModeloMarca:marca_id,nombre',
                                                                'ModeloDetalleProducto.ModeloColor:color_id,nombre',
                                                                'ModeloDetalleProducto.ModeloTalla:talla_id,nombre',
                                                                'ModeloDetalleProducto.PrimeraImagen')
                                                            ->where('pedido_id', $pedido->pedido_id)->get();

                        return view('componentes.carrito.productos_card', compact('pedido_detalle', 'filtro'))->render();

                        break;
                    default:

                        $pedido_detalle = ModeloDetallePedido::with('ModeloDetalleProducto:producto_detalle_id,producto_id,categoria_id,marca_id,unidad_id,color_id,talla_id',
                                                                    'ModeloDetalleProducto.ModeloProducto:producto_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloUnidad:unidad_id,nombre,abreviatura',
                                                                    'ModeloDetalleProducto.ModeloCategoria:categoria_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloMarca:marca_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloColor:color_id,nombre',
                                                                    'ModeloDetalleProducto.ModeloTalla:talla_id,nombre',
                                                                    'ModeloDetalleProducto.PrimeraImagen')
                                                                ->where('pedido_id', $pedido->pedido_id)->paginate(10);

                        return view('componentes.carrito.productos_card', compact('pedido_detalle', 'filtro'))->render();

                        break;
                }
            }

        }


    }

    public function getListarProductosPedidoList()
    {

        $persona_id = Auth::user()->persona_id;
        $codigo     = session('codigo_pedido_activo');

        //Listamos el pedido, comprobamos que este pendiente [estado = 0] y que sea de la persona logueada
        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();

        if($pedido){

            $pedido_detalle = ModeloDetallePedido::with('ModeloDetalleProducto:producto_detalle_id,producto_id,categoria_id,marca_id,unidad_id,color_id,talla_id',
                                                        'ModeloDetalleProducto.ModeloProducto:producto_id,nombre',
                                                        'ModeloDetalleProducto.ModeloUnidad:unidad_id,nombre,abreviatura',
                                                        'ModeloDetalleProducto.ModeloCategoria:categoria_id,nombre',
                                                        'ModeloDetalleProducto.ModeloMarca:marca_id,nombre',
                                                        'ModeloDetalleProducto.ModeloColor:color_id,nombre',
                                                        'ModeloDetalleProducto.ModeloTalla:talla_id,nombre',
                                                        'ModeloDetalleProducto.PrimeraImagen')
                                                    ->where('pedido_id', $pedido->pedido_id)->get();

            return view('componentes.carrito.productos_list', compact('pedido_detalle'))->render();
        }


    }

    public function getListarProductosPedidoFinal()
    {

        $persona_id = Auth::user()->persona_id;
        $codigo     = session('codigo_pedido_activo');

        //Listamos el pedido, comprobamos que este pendiente [estado = 0] y que sea de la persona logueada
        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();

        if($pedido){

            $pedido_detalle = ModeloDetallePedido::with('ModeloDetalleProducto:producto_detalle_id,producto_id,categoria_id,marca_id,unidad_id,color_id,talla_id',
                                                        'ModeloDetalleProducto.ModeloProducto:producto_id,nombre',
                                                        'ModeloDetalleProducto.ModeloUnidad:unidad_id,nombre,abreviatura',
                                                        'ModeloDetalleProducto.ModeloCategoria:categoria_id,nombre',
                                                        'ModeloDetalleProducto.ModeloMarca:marca_id,nombre',
                                                        'ModeloDetalleProducto.ModeloColor:color_id,nombre',
                                                        'ModeloDetalleProducto.ModeloTalla:talla_id,nombre',
                                                        'ModeloDetalleProducto.PrimeraImagen')
                                                    ->where('pedido_id', $pedido->pedido_id)->get();

            return view('componentes.carrito.productos_list_final', compact('pedido_detalle'))->render();
        }


    }

    public function getCodigoVentaFin(){

        $codigo = session('codigo_pedido_activo');

        return view('venta.carritopaso6', compact('codigo'))->with('titulo', 'Confirmacion de Pedido');

    }


}
