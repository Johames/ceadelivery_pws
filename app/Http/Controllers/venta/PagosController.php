<?php

namespace App\Http\Controllers\venta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloPedido;
use App\Models\ModeloPersona;
use App\Models\ModeloTarjeta;
use Culqi\Culqi;
use Exception;
use Illuminate\Support\Facades\Auth;

class PagosController extends Controller
{

    protected $API_KEY;

    public function registrarPagoTarjeta(Request $request){

        $persona_id     = Auth::user()->persona_id;
        $codigo         = session('codigo_pedido_activo');

        $token          = $request->input('card');
        $tokenNo        = $request->input('cardno');
        $guard          = $request->input('guardar');

        $user_token = "";
        $cargo      = [];

        $this->API_KEY = getenv("API_KEY");
        $culqi = new Culqi(array("api_key" => $this->API_KEY ));

        $pedido = ModeloPedido::where('persona_id', $persona_id)        // El pedido es de la persona logueada?
                  ->where('codigo', $codigo)                            // Comprobar el codigo del pedido,
                  ->where('estado', '0')                                // El pedido esta pendiente? OJO: El cliente solo tiene un pedido pendiente por empresa,
                  ->first();

        $monto = $pedido->total_pagar + $pedido->precio_envio + $pedido->igv - $pedido->descuento;

        $monto = (float)number_format($monto, 2, '', '');

        if(!Auth::user()->ModeloPersona->user_token_card){

            $persona = ModeloPersona::where('persona_id', $persona_id)->first();

            // Crear un Cliente
            $client = $culqi->Customers->all(array("limit" => 1,"email" => Auth::user()->email));

            if($client->data){

                foreach($client->data as $data){

                    $persona->user_token_card = $data->id;
                    $persona->save();

                    $user_token = $data->id;

                }

            } else {

                $cliente = $culqi->Customers->create(
                    array(
                        "address" => $pedido->ModeloDireccion->direccion,
                        "address_city" => Auth::user()->ModeloPersona->ModeloUbigeo->nombre,
                        "country_code" => "PE",
                        "email" => Auth::user()->email,
                        "first_name" => Auth::user()->ModeloPersona->nombres,
                        "last_name" => Auth::user()->ModeloPersona->apellidos,
                        "metadata" => array("DNI"=>Auth::user()->ModeloPersona->ruc_dni),
                        "phone_number" => Auth::user()->ModeloPersona->telefono
                    )
                );

                $persona->user_token_card = $cliente->id;
                $persona->save();

                $user_token = $cliente->id;

            }

        } else {
            $user_token = Auth::user()->ModeloPersona->user_token_card;
        }



        if($guard == 'SI'){

            // Crear una Tarjeta
            try {
                $card = $culqi->Cards->create(
                    array(
                        "customer_id" => $user_token,
                        "token_id" => $token
                    )
                );
            } catch (Exception $e) {
                return json_encode($e->getMessage());
            }

            if($card->id){

                ModeloTarjeta::firstOrCreate(['persona_id' => $persona_id, 'card_id' => $card->id],
                    [
                        'persona_id'        => $persona_id,
                        'card_number'       => $card->source->card_number,
                        'card_brand'        => $card->source->iin->card_brand,
                        'card_type'         => $card->source->iin->card_type,
                        'card_id'           => $card->id
                    ]
                );

                if($pedido){

                    // Crear un Cargo
                    try {
                        $cargo = $culqi->Charges->create(
                            array(
                                "amount" => $monto,
                                "currency_code" => "PEN",
                                "email" => Auth::user()->email,
                                "description" => $pedido->ModeloEmpresa->ruc." - ".$pedido->ModeloEmpresa->razon_social,
                                "capture" => "false",
                                "metadata" => array(
                                    "order_id" => $pedido->codigo,
                                    "user_id" => Auth::user()->ModeloPersona->persona_id,
                                    "user_details" => Auth::user()->ModeloPersona->nombres." ".Auth::user()->ModeloPersona->apellidos
                                ),
                                "antifraud_details" => array(
                                    "address" => $pedido->ModeloDireccion->direccion,
                                    "address_city" => Auth::user()->ModeloPersona->ModeloUbigeo->nombre,
                                    "country_code" => "PE",
                                    "first_name" => Auth::user()->ModeloPersona->nombres,
                                    "last_name" => Auth::user()->ModeloPersona->apellidos,
                                    "phone_number" => Auth::user()->ModeloPersona->telefono
                                ),
                                "source_id" => $card->id
                            )
                        );
                    } catch (Exception $e) {
                        return json_encode($e->getMessage());
                    }

                    if($cargo->outcome){

                        $pedido->token_pago_card = $cargo->id;
                        $pedido->save();

                    }

                    return json_encode($cargo);

                }

            }

        } else {

            if($pedido){

                if($token){

                    // Crear un Cargo
                    try {
                        $cargo = $culqi->Charges->create(
                            array(
                                "amount" => $monto,
                                "currency_code" => "PEN",
                                "email" => Auth::user()->email,
                                "description" => $pedido->ModeloEmpresa->ruc." - ".$pedido->ModeloEmpresa->razon_social,
                                "capture" => "false",
                                "metadata" => array(
                                    "order_id" => $pedido->codigo,
                                    "user_id" => Auth::user()->ModeloPersona->persona_id,
                                    "user_details" => Auth::user()->ModeloPersona->nombres." ".Auth::user()->ModeloPersona->apellidos
                                ),
                                "antifraud_details" => array(
                                    "address" => $pedido->ModeloDireccion->direccion,
                                    "address_city" => Auth::user()->ModeloPersona->ModeloUbigeo->nombre,
                                    "country_code" => "PE",
                                    "first_name" => Auth::user()->ModeloPersona->nombres,
                                    "last_name" => Auth::user()->ModeloPersona->apellidos,
                                    "phone_number" => Auth::user()->ModeloPersona->telefono
                                ),
                                "source_id" => $token
                            )
                        );
                    } catch (Exception $e) {
                        return json_encode($e->getMessage());
                    }

                    if($cargo->outcome){

                        $pedido->token_pago_card = $cargo->id;
                        $pedido->save();

                    }

                    return json_encode($cargo);

                } else {

                    $tarjeta = ModeloTarjeta::where('card_id', $tokenNo)->where('estado', '0')->first();

                    if($tarjeta){

                        // Crear un Cargo
                        try {
                            $cargo = $culqi->Charges->create(
                                array(
                                    "amount" => $monto,
                                    "currency_code" => "PEN",
                                    "email" => Auth::user()->email,
                                    "description" => $pedido->ModeloEmpresa->ruc." - ".$pedido->ModeloEmpresa->razon_social,
                                    "capture" => "false",
                                    "metadata" => array(
                                        "order_id" => $pedido->codigo,
                                        "user_id" => Auth::user()->ModeloPersona->persona_id,
                                        "user_details" => Auth::user()->ModeloPersona->nombres." ".Auth::user()->ModeloPersona->apellidos
                                    ),
                                    "antifraud_details" => array(
                                        "address" => $pedido->ModeloDireccion->direccion,
                                        "address_city" => Auth::user()->ModeloPersona->ModeloUbigeo->nombre,
                                        "country_code" => "PE",
                                        "first_name" => Auth::user()->ModeloPersona->nombres,
                                        "last_name" => Auth::user()->ModeloPersona->apellidos,
                                        "phone_number" => Auth::user()->ModeloPersona->telefono
                                    ),
                                    "source_id" => $tokenNo
                                )
                            );
                        } catch (Exception $e) {
                            return json_encode($e->getMessage());
                        }

                        if($cargo->outcome){

                            $pedido->token_pago_card = $cargo->id;
                            $pedido->save();

                        }

                        return json_encode($cargo);

                    } else {

                        return json_encode(['message' => '{"user_message": "Los datos fueron manipulados, el evento sera registrado."}']);

                    }

                }

            }

        }

    }

}
