<?php

namespace App\Http\Controllers\venta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloPedido;
use App\Models\ModeloPromocion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PedidoController extends Controller
{

    public function getIndexPaso1($codigo)
    {
        // session(['codigo_pedido_activo' => $codigo]);

        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        if ($pedido) {

            return view('venta.carritopaso1')->with('titulo', 'Carrito de Compras')
                                             ->with('codigo', $codigo);

        }else{

            abort(404);

        }

    }

    public function getIndexPaso2($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        if ($pedido) {

            return view('venta.carritopaso2')->with('titulo', 'Carrito de Compras');

        }else{

            abort(404);

        }
    }

    public function getIndexPaso3($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        if ($pedido) {

            return view('venta.carritopaso3')->with('titulo', 'Carrito de Compras');

        }else{

            abort(404);

        }

    }

    public function getIndexPaso4($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        if ($pedido) {

            return view('venta.carritopaso4')->with('titulo', 'Carrito de Compras');

        }else{

            abort(404);

        }

    }

    public function getIndexPaso5($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        if ($pedido) {

            return view('venta.carritopaso5')->with('titulo', 'Carrito de Compras');

        }else{

            abort(404);

        }

    }

    public function getIndexPaso6($codigo)
    {
        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        if ($pedido) {

            return view('venta.carritopaso6')->with('titulo', 'Carrito de Compras');

        }else{

            abort(404);

        }

    }


    public function getIndexPaso($nro_paso)
    {

        $pedido = ModeloPedido::where('persona_id', Auth::user()->persona_id)
                            ->where('estado', '0')
                            ->where('codigo', session('codigo_pedido_activo'))
                            ->first();

        if ($pedido) {
            if($pedido->paso > 3 || $nro_paso > $pedido->paso){
                $nro_paso = $pedido->paso;
            }
        }


        switch ($nro_paso) {
            case '1':

                return view('venta.carritopaso1')->with('titulo', 'Carrito paso uno');
                break;

            case '2':

                return view('venta.carritopaso2')->with('titulo', 'Carrito paso dos');
                break;

            case '3':

                if ($pedido) {

                    if ($pedido->codigo_descuento) {

                        $promociones = ModeloPromocion::where('codigo', $pedido->codigo_descuento)->where('empresa_id', $pedido->empresa_id )->where('fecha_inicio', '>=', Carbon::now()->format('Y-m-d') )->where('fecha_fin', '<=', Carbon::now()->format('Y-m-d') )->where('estado', '0')->first();

                        // El codigo cumple con las condicones de fecha ?
                        if ($promociones) {

                            if ($promociones->tipo == 0) {
                                $pedido->descuento = $promociones->descuento;
                            }

                            if ($promociones->tipo == 1) {
                                $pedido->descuento = $pedido->total_pagar - ($pedido->total_pagar * ($promociones->descuento/100));
                            }

                            $pedido->save();

                        } else {

                            $pedido->descuento = 0;
                            $pedido->save();

                        }

                    }

                }

                return view('venta.carritopaso3')->with('titulo', 'Carrito paso tres');
                break;

            case '4':

                return view('venta.carritopaso4')->with('titulo', 'Carrito paso cuatro');
                break;

            case '5':

                return view('venta.carritopaso5')->with('titulo', 'Carrito paso cinco');
                break;

            default:
                abort(404);
                break;
        }
    }

    public function getRedirectPedido($codigo)
    {

        // Ponemos la variable en sesion
        session(['codigo_pedido_activo' => $codigo]);

        $persona_id = Auth::user()->persona_id;

        $pedido = ModeloPedido::where('persona_id', $persona_id)
                                ->where('estado', '0')
                                ->where('codigo', $codigo)
                                ->first();
        $paso = $pedido->paso;

        if ($pedido) {
            return redirect()->intended('mi/pedido/paso/' . $paso );
        }else{
            abort(404);
        }

    }

    public function ListaPedidos($idEmp)
    {
        $Pedidos = DB::table('cd_pedido')
            ->join('cd_empresa', 'cd_pedido.empresa_id', '=', 'cd_empresa.empresa_id')
            ->join('cd_persona', 'cd_pedido.persona_id', '=', 'cd_persona.persona_id')
            ->join('users', 'cd_pedido.users_id', '=', 'users.id')
            ->join('cd_metodo_pago_emp', 'cd_pedido.metodo_pago_emp_id', '=', 'cd_metodo_pago_emp.metodo_pago_emp_id')
            ->join("cd_metodo_pago", "cd_metodo_pago_emp.metodo_pago_id", "=", "cd_metodo_pago.metodo_pago_id")
            ->join('cd_metodo_envio_emp', 'cd_pedido.metodo_envio_emp_id', '=', 'cd_metodo_envio_emp.metodo_envio_emp_id')
            ->join("cd_metodo_envio", "cd_metodo_envio_emp.metodo_envio_id", "=", "cd_metodo_envio_emp.metodo_envio_id")
            ->join('cd_direcciones', 'cd_pedido.direcciones_id', '=', 'cd_direcciones.direcciones_id')
            ->select('cd_pedido.pedido_id AS id, (CASE WHEN CONCAT(cd_persona.nombres, cd_persona.apellidos) = "" THEN (CASE WHEN cd_persona.razon_social = "" THEN cd_persona.nombre_comercial ELSE cd_persona.razon_social END) ELSE CONCAT(cd_persona.nombres, " ", cd_persona.apellidos) END) AS cliente, cd_persona.avatar, cd_direcciones.direccion, cd_pedido.estado')
            ->where('cd_pedido.empresa_id', $idEmp)
            ->where('cd_pedido.estado', '0')
            ->get();

        $data = array();
        $cont = 1;

        foreach ($Pedidos as $direccion) {

            $data[] = array(
                "0" => $cont++,
                "1" => $direccion->id,
                "2" => $direccion->ciliente,
                "3" => $direccion->avatar,
                "4" => $direccion->direccion,
                "5" => $direccion->estado
            );
        }

        $results = array(
            "sEcho" => 1, //Información para el datatables
            "iTotalRecords" => count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords" => count($data), //enviamos el total registros a visualizar
            "aaData" => $data
        );

        return json_encode($results);
    }

    public function ListaPedidosFiltrado($idEmp, $idCliente, $idUser, $fechaPedido, $fechaEntrega, $estado)
    {
        //
    }

    public function PedidoId($idPedido)
    {
        $Pedidos = DB::table('cd_pedido')
            ->join('cd_empresa', 'cd_pedido.empresa_id', '=', 'cd_empresa.empresa_id')
            ->join('cd_persona', 'cd_pedido.persona_id', '=', 'cd_persona.persona_id')
            ->join('users', 'cd_pedido.users_id', '=', 'users.id')
            ->join('cd_metodo_pago_emp', 'cd_pedido.metodo_pago_emp_id', '=', 'cd_metodo_pago_emp.metodo_pago_emp_id')
            ->join("cd_metodo_pago", "cd_metodo_pago_emp.metodo_pago_id", "=", "cd_metodo_pago.metodo_pago_id")
            ->join('cd_metodo_envio_emp', 'cd_pedido.metodo_envio_emp_id', '=', 'cd_metodo_envio_emp.metodo_envio_emp_id')
            ->join("cd_metodo_envio", "cd_metodo_envio_emp.metodo_envio_id", "=", "cd_metodo_envio_emp.metodo_envio_id")
            ->join('cd_direcciones', 'cd_pedido.direcciones_id', '=', 'cd_direcciones.direcciones_id')
            ->select('cd_pedido.pedido_id AS id, (CASE WHEN CONCAT(cd_persona.nombres, cd_persona.apellidos) = "" THEN (CASE WHEN cd_persona.razon_social = "" THEN cd_persona.nombre_comercial ELSE cd_persona.razon_social END) ELSE CONCAT(cd_persona.nombres, " ", cd_persona.apellidos) END) AS cliente, cd_persona.avatar, cd_metodo_pago.nombre AS metodopago, cd_metodo_envio AS metodoenvio, cd_direcciones.direccion, cd_direcciones.referencia, cd_direcciones.lat, cd_direcciones.lgn, cd_pedido.fecha_pedido, cd_pedido.fecha_entrega, cd_pedido.extras, cd_pedido.comentarios, cd_pedido.total_pagar, cd_pedido.codigo_descuento, cd_pedido.descuento, cd_pedido.igv, cd_metodo_envio.precio AS costoenvio, cd_pedido.fecha_entregado, cd_pedido.hora_entregado, cd_pedido.puntos_ganados, cd_pedido.canjear_puntos, cd_pedido.estado')
            ->where('cd_empresa.empresa_id', $idPedido)
            ->where('cd_pedido.estado', '0')
            ->get();

        $data = array();
        $cont = 1;

        foreach ($Pedidos as $pedido) {

            $data[] = array(
                "0" => $cont++,
                "1" => $pedido->id,
                "2" => $pedido->cliente,
                "3" => $pedido->avatar,
                "4" => $pedido->metodopago,
                "5" => $pedido->metodoenvio,
                "6" => $pedido->direccion,
                "7" => $pedido->referencia,
                "8" => $pedido->lat,
                "9" => $pedido->lgn,
                "10" => $pedido->fecha_pedido,
                "11" => $pedido->fecha_entrega,
                "12" => $pedido->extras,
                "13" => $pedido->comentarios,
                "14" => $pedido->total_pagar,
                "15" => $pedido->codigo_descuento,
                "16" => $pedido->descuento,
                "17" => $pedido->igv,
                "18" => $pedido->costoenvio,
                "19" => $pedido->fecha_entregado,
                "20" => $pedido->hora_entregado,
                "21" => $pedido->puntos_ganados,
                "22" => $pedido->canjear_puntos,
                "23" => $pedido->estado
            );
        }

        $results = array(
            "sEcho" => 1, //Información para el datatables
            "iTotalRecords" => count($data), //enviamos el total registros al datatable
            "iTotalDisplayRecords" => count($data), //enviamos el total registros a visualizar
            "aaData" => $data
        );

        return json_encode($results);
    }

    public function AgregarPedido(Request $request)
    {
        $buscar = ModeloPedido::where('empresa_id', $request->input('empresa_id'))
            ->wehere('persona_id', $request->input('persona_id'))
            ->where('estado', '0')
            ->first();

        //
    }
}
