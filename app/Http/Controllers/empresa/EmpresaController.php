<?php

namespace App\Http\Controllers\empresa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FileController;
use App\Http\Requests\PerfilEmpresaRequest;
use App\Models\ModeloBlog;
use App\Models\ModeloCategoria;
use App\Models\ModeloColor;
use App\Models\ModeloDetalleMetodoPago;
use App\Models\ModeloDetalleProducto;
use App\Models\ModeloMarca;
use App\Models\ModeloDetallePedido;
use App\Models\ModeloDetallePublicidad;
use App\Models\ModeloEmpresa;
use App\Models\ModeloGrupo;
use App\Models\ModeloMetodoPago;
use App\Models\ModeloPedido;
use App\Models\ModeloPersona;
use App\Models\ModeloProducto;
use App\Models\ModeloPromocion;
use App\Models\ModeloPublicidad;
use App\Models\ModeloRedesPagina;
use App\Models\ModeloRubro;
use App\Models\ModeloTalla;
use App\Models\ModeloUnidad;
use App\User;
use Culqi\Culqi;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{

    // Ruta para Mantenimiento
    public function getIndexMantenimiento(){
        return view('en_mantenimiento');
    }

    // Ruta Inicial
    public function getIndex()
    {

        $rubros = ModeloRubro::where('estado', '=', '0')->whereHas('ModeloEmpresa', function ($query){
            return $query->has('ModeloDetalleProductos');
        })->get();

        $rubros_empresas = ModeloRubro::with('ModeloEmpresa', 'ModeloEmpresa.ModeloRedesSociales', 'ModeloEmpresa.ModeloValoracionEmpresas')
                                        ->whereHas('ModeloEmpresa', function ($query){
                                            return $query->has('ModeloDetalleProductos');
                                        })->get();

        // $rubros_empresas = ModeloRubro::with(['ModeloEmpresa' => function($query) use ($ubigeo_id){
        //                                 return $query->where('ubigeo_id', $ubigeo_id);
        //                             }, 'ModeloEmpresa.ModeloRedesSociales', 'ModeloEmpresa.ModeloValoracionEmpresas'])
        //                             ->whereHas('ModeloEmpresa', function ($query) {
        //                                 return $query->has('ModeloDetalleProductos');
        //                             })->get();


        $promociones = ModeloPromocion::whereDate('fecha_fin', '>=', now())->where('estado', 0)->get();

        $pagos = ModeloMetodoPago::where('estado', '0')->get();

        $publicacion = ModeloDetallePublicidad::with(['ModeloPublicidad' => function($query){
                                    return $query->where('estado', '0')->whereDate('fecha_fin', '>=', now());
                                }])->where('estado', '0')
                                ->orderBy(DB::raw('RAND()'))
                                ->get();

        if (Auth::user()) {

            $ubigeo_id = Auth::user()->ModeloPersona->ubigeo_id;

            if ($ubigeo_id) {

                $grupo_rubro = ModeloGrupo::with(['ModeloRubros' => function($query){
                                                    return $query->whereHas('ModeloEmpresa', function ($query) {
                                                        return $query->has('ModeloDetalleProductos');
                                                    });
                                                }, 'ModeloRubros.ModeloEmpresa' => function($query) use ($ubigeo_id){
                                                    return $query->where('ubigeo_id', $ubigeo_id);
                                                }, 'ModeloRubros.ModeloEmpresa.ModeloRedesSociales',
                                                'ModeloRubros.ModeloEmpresa.ModeloValoracionEmpresas'
                                            ])
                                            ->whereHas('ModeloRubros', function($query){
                                                return $query->whereHas('ModeloEmpresa', function ($query) {
                                                    return $query->has('ModeloDetalleProductos');
                                                });
                                            })->get();

                $persona       = ModeloPersona::with('ModeloUbigeo')->where('persona_id', Auth::user()->persona_id)->first();

                $ubigeo        = $persona->ModeloUbigeo->ubigeo;
                $ubigeo_id     = $persona->ModeloUbigeo->ubigeo_id;

                $ubigeo_departamento = intval(substr($ubigeo, 0, 2));
                $ubigeo_provincia    = intval(substr($ubigeo, 0, 4));

                $empZona = ModeloEmpresa::whereHas('ModeloUbigeos', function ($query) use ($ubigeo_id, $ubigeo_departamento, $ubigeo_provincia){
                                                return $query->where('cd_empresa_cd_ubigeo.ubigeo_id', $ubigeo_id)
                                                            ->orWhere('cd_empresa_cd_ubigeo.ubigeo_id', $ubigeo_departamento)
                                                            ->orWhere('cd_empresa_cd_ubigeo.ubigeo_id', $ubigeo_provincia);
                                            })->get();

            } else {

                $grupo_rubro = ModeloGrupo::with(['ModeloRubros' => function($query){
                                                    return $query->whereHas('ModeloEmpresa', function ($query) {
                                                        return $query->has('ModeloDetalleProductos');
                                                    });
                                                }, 'ModeloRubros.ModeloEmpresa.ModeloRedesSociales',
                                                'ModeloRubros.ModeloEmpresa.ModeloValoracionEmpresas'
                                            ])->whereHas('ModeloRubros', function($query){
                                                return $query->whereHas('ModeloEmpresa', function ($query) {
                                                    return $query->has('ModeloDetalleProductos');
                                                });
                                            })->get();

                $empZona = array();

            }

        } else {

            $grupo_rubro = ModeloGrupo::with(['ModeloRubros' => function($query){
                                                return $query->whereHas('ModeloEmpresa', function ($query) {
                                                    return $query->has('ModeloDetalleProductos');
                                                });
                                            }, 'ModeloRubros.ModeloEmpresa.ModeloRedesSociales',
                                            'ModeloRubros.ModeloEmpresa.ModeloValoracionEmpresas'
                                        ])->whereHas('ModeloRubros', function($query){
                                            return $query->whereHas('ModeloEmpresa', function ($query) {
                                                return $query->has('ModeloDetalleProductos');
                                            });
                                        })->get();

            $empZona = array();

        }

        return view('index', compact('rubros_empresas', 'rubros', 'empZona', 'promociones', 'pagos', 'publicacion', 'grupo_rubro')); //'blog','pubTop', 'pubBottom',
    }

    public function getIndexRubro($slug)
    {

        $rubros = ModeloRubro::where('estado', '=', '0')->where('slug', $slug)->first();

        if (Auth::user()) {

            $ubigeo_id = Auth::user()->ModeloPersona->ubigeo_id;

            if ($ubigeo_id) {

                $rubros_empresas = ModeloEmpresa::with('ModeloValoracionEmpresas')
                                ->where('rubro_id', $rubros->rubro_id)
                                ->where('ubigeo_id', $ubigeo_id)
                                ->orderBy(DB::raw('RAND()'))
                                ->get();

                $empDest = ModeloEmpresa::with('ModeloValoracionEmpresas')
                                ->where('rubro_id', $rubros->rubro_id)
                                ->where('ubigeo_id', $ubigeo_id)
                                ->where('destacado', '=', '0')
                                ->orderBy('posicion')
                                ->get();

                $rubro_id = $rubros->rubro_id;

                $persona       = ModeloPersona::with('ModeloUbigeo')->where('persona_id', Auth::user()->persona_id)->first();

                $ubigeo        = $persona->ModeloUbigeo->ubigeo;
                $ubigeo_id     = $persona->ModeloUbigeo->ubigeo_id;

                $ubigeo_departamento = intval(substr($ubigeo, 0, 2));
                $ubigeo_provincia    = intval(substr($ubigeo, 0, 4));

                $empZona = ModeloEmpresa::whereHas('ModeloUbigeos', function ($query) use ($ubigeo_id, $ubigeo_departamento, $ubigeo_provincia){
                                    return $query->where('cd_empresa_cd_ubigeo.ubigeo_id', $ubigeo_id)
                                                ->orWhere('cd_empresa_cd_ubigeo.ubigeo_id', $ubigeo_departamento)
                                                ->orWhere('cd_empresa_cd_ubigeo.ubigeo_id', $ubigeo_provincia);
                                })
                                ->whereHas('ModeloRubro', function ($query) use ($rubro_id){
                                    return $query->where('cd_rubro.rubro_id', $rubro_id);
                                })
                                ->get();

            } else {

                $rubros_empresas = ModeloEmpresa::with('ModeloValoracionEmpresas')
                                ->where('rubro_id', $rubros->rubro_id)
                                ->orderBy(DB::raw('RAND()'))
                                ->get();

                $empDest = ModeloEmpresa::with('ModeloValoracionEmpresas')
                            ->where('rubro_id', $rubros->rubro_id)
                            ->where('destacado', '=', '0')
                            ->orderBy('posicion')
                            ->get();

                $empZona = array();

            }

        } else {

            $rubros_empresas = ModeloEmpresa::with('ModeloValoracionEmpresas')
                                ->where('rubro_id', $rubros->rubro_id)
                                ->orderBy(DB::raw('RAND()'))
                                ->get();

            $empDest = ModeloEmpresa::with('ModeloValoracionEmpresas')
                        ->where('rubro_id', $rubros->rubro_id)
                        ->where('destacado', '=', '0')
                        ->orderBy('posicion')
                        ->get();

            $empZona = array();

        }

        return view('rubros_empresa',compact('rubros_empresas', 'empDest', 'empZona'));

    }

    public function getIndexProductosEmpresa($slug)
    {

        $empresa = ModeloEmpresa::with('ModeloUbigeo')->where('slug', $slug)->firstOrFail();

        $pagos = ModeloDetalleMetodoPago::with('ModeloMetodoPago')->where('empresa_id', $empresa->empresa_id)->where('estado', '0')->get();

        $categorias = ModeloCategoria::with(['ModeloDetalleProductos' => function ($query) use($empresa) {
                        return $query->where('empresa_id', $empresa->empresa_id)->where('estado', '0');
                    }])->whereHas('ModeloDetalleProductos', function ($query) use($empresa) {
                        return $query->where('cd_producto_detalle.empresa_id', '=', $empresa->empresa_id);
                    })->get();

        $descProds = ModeloDetalleProducto::with('ModeloProducto', 'ModeloCategoria', 'ModeloMarca', 'PrimeraImagen')
                    ->where('estado', '=', '0')
                    ->where('estado_desc', '=', '0')
                    ->where('empresa_id', '=', $empresa->empresa_id)
                    ->orderBy('created_at')
                    ->get();

        $productos_mas_vendidos = ModeloDetallePedido::selectRaw('count(*) AS num, producto_detalle_id, sum(cantidad) as cantidad_vent, sum(total) as total_vent')
                                ->with('ModeloDetalleProducto', 'ModeloDetalleProducto.ModeloProducto', 'ModeloDetalleProducto.ModeloCategoria', 'ModeloDetalleProducto.ModeloUnidad', 'ModeloDetalleProducto.PrimeraImagen')
                                ->whereHas('ModeloPedido', function ($query) use ($empresa){
                                    return $query->where('empresa_id', $empresa->empresa_id)->where('estado', 4);
                                })
                                ->groupBy('producto_detalle_id')
                                ->orderBy('cantidad_vent','desc')
                                ->limit(10)
                                ->get();

        $promociones = ModeloPromocion::whereDate('fecha_fin', '>=', now())->where('empresa_id', $empresa->empresa_id)->where('estado', 0)->get();

        return view('producto.productos_empresa', compact('empresa', 'categorias', 'productos_mas_vendidos', 'descProds', 'promociones', 'pagos'))->with('titulo', $slug);
    }

    public function getProductosCategoria($idEmp, $idCat)
    {

        if ($idCat != 0) {

            $productos = ModeloDetalleProducto::with('ModeloProducto', 'ModeloCategoria', 'ModeloMarca', 'PrimeraImagen')
                                        ->where('estado', '0')
                                        ->where('stock', '>', '0')
                                        ->where('empresa_id', $idEmp)
                                        ->where('categoria_id', $idCat)
                                        ->paginate(24);

        } else {

            $productos = ModeloDetalleProducto::with('ModeloProducto', 'ModeloCategoria', 'ModeloMarca', 'PrimeraImagen')
                                        ->where('estado', '0')
                                        ->where('stock', '>', '0')
                                        ->where('empresa_id', $idEmp)
                                        ->paginate(24);

        }

        return view('componentes.productos.detalle_categoria', compact('productos'));

    }

    public function getTiendaDetalle($slug)
    {

        $empresas =  ModeloEmpresa::with('ModeloContactos','ModeloHorarios', 'ModeloRedesSociales', 'ModeloUbigeo',
                                        'ModeloDetalleMetodoEnvios','ModeloDetalleMetodoEnvios.ModeloMetodoEnvio',
                                        'ModeloDetalleMetodoPagos', 'ModeloDetalleMetodoPagos.ModeloMetodoPago',
                                        'ModeloValoracionEmpresas','ModeloValoracionEmpresas.ModeloPersona','ModeloRubro')
                                    ->where('slug', $slug)->first();

        return view('secciones.detalle_tienda', compact('empresas'));

    }

    public function getTiendaResena($slug)
    {

        $empresa =  ModeloEmpresa::with('ModeloContactos','ModeloHorarios', 'ModeloRedesSociales',
                                        'ModeloDetalleMetodoEnvios','ModeloDetalleMetodoEnvios.ModeloMetodoEnvio',
                                        'ModeloDetalleMetodoPagos', 'ModeloDetalleMetodoPagos.ModeloMetodoPago',
                                        'ModeloValoracionEmpresas','ModeloValoracionEmpresas.ModeloPersona','ModeloRubro')
                                    ->where('slug', $slug)->first();

        return view('secciones.resena_tienda', compact('empresa'));

    }

    public function buscarProductosEmpresa($slug, $text){

        $empresa = ModeloEmpresa::where('slug', $slug)->first();

        $buscado = ModeloDetalleProducto::with(['ModeloCategoria', 'ModeloMarca', 'ModeloProductoImagenes', 'ModeloEmpresa', 'ModeloProducto'  => function ($query) use($text) {
                                            return $query->where('nombre', 'LIKE', '%' . $text . '%');
                                        }])->whereHas('ModeloProducto', function ($query) use($text) {
                                            return $query->where('nombre', 'LIKE', '%' . $text . '%');
                                        })
                                        ->where('estado', '=', '0')
                                        ->where('empresa_id', '=', $empresa->empresa_id)
                                        ->get();

        return view('componentes.productos.producto_empresa_buscador', compact('buscado', 'text'));

    }

    public function buscador(Request $request){

        $term = $request->get('term');

        $data = ModeloProducto::where('nombre', 'LIKE',  '%'.$term.'%')->get();

        $item = [];

        foreach($data as $row){
            $item[] = [
                'label' => $row->nombre
            ];
        }

        return $item;

    }

    public function getIndexBuscador($text = null)
    {

        if($text == 'Todos'){

            $text = 'Todos los productos';

            $productos = ModeloDetalleProducto::with(['ModeloProducto', 'ModeloCategoria', 'ModeloUnidad', 'PrimeraImagen', 'ModeloEmpresa', 'ModeloEmpresa.ModeloRubro'])
                                            ->Limit(100)
                                            ->orderBy(DB::raw('RAND()'))
                                            ->get();

            return view('buscador', compact('productos', 'text'))->with('titulo', 'Buscar Productos');

        } else if($text){

            $productos = ModeloDetalleProducto::with(['ModeloProducto' => function ($query) use($text){
                                                return $query->where('nombre', 'LIKE',  '%'.$text.'%');
                                            }, 'ModeloCategoria', 'ModeloUnidad', 'PrimeraImagen', 'ModeloEmpresa', 'ModeloEmpresa.ModeloRubro'])
                                            ->whereHas('ModeloProducto', function ($query) use($text){
                                                return $query->where('nombre', 'LIKE',  '%'.$text.'%');
                                            })->get();

            return view('buscador', compact('productos', 'text'))->with('titulo', 'Buscar Productos');

        } else {

            $text = 'Todos los productos';

            $productos = ModeloDetalleProducto::with(['ModeloProducto', 'ModeloCategoria', 'ModeloUnidad', 'PrimeraImagen', 'ModeloEmpresa', 'ModeloEmpresa.ModeloRubro'])
                                            ->Limit(100)
                                            ->orderBy(DB::raw('RAND()'))
                                            ->get();

            return view('buscador', compact('productos', 'text'))->with('titulo', 'Buscar Productos');

        }

    }

    public function getRedesPagina(){

        $redes = ModeloRedesPagina::where('estado', '0')->get();

        return view('componentes.redes.redes_button', compact('redes'));

    }




    // Rutas de las Vistas del Admin de Empresa *********************************************************************************
    public function getIndexPedidos()
    {
        return view('empresa.pedidos')->with('titulo', 'Lista de Pedidos');
    }

    public function getIndexCalificaciones()
    {
        return view('empresa.calificaciones')->with('titulo', 'Reseñas y Calificaciones a la Tienda');
    }

    public function getIndexPerfil()
    {
        return view('empresa.perfiltienda')->with('titulo', 'Perfil');
    }

    public function getIndexProductos()
    {
        return view('empresa.productos')->with('titulo', 'Lista de Productos');
    }

    public function getIndexUsuarios()
    {
        return view('empresa.usuarios')->with('titulo', 'Lista de Usuarios');
    }

    public function getIndexHorarios()
    {
        return view('empresa.horarios')->with('titulo', 'Horarios de Atención');
    }

    public function getIndexRedes()
    {
        return view('empresa.redessociales')->with('titulo', 'Redes Sociales');
    }

    public function getIndexCEAPuntos()
    {
        return view('empresa.ceapuntos')->with('titulo', 'Administración de los CEA Puntos');
    }

    public function getIndexPromociones()
    {
        return view('empresa.promociones')->with('titulo', 'Promociones, Descuentos y Banners');
    }

    public function getIndexServicios()
    {
        return view('empresa.servicios')->with('titulo', 'Servicios Brindados');
    }

    public function getIndexMetodoPago()
    {
        return view('empresa.metodopago')->with('titulo', 'Métodos de Pago');
    }
    public function getIndexMetodoEnvio()
    {
        return view('empresa.metodoenvio')->with('titulo', 'Métodos de Envío');
    }

    public function getIndexNotificaciones()
    {
        return view('empresa.notificaciones')->with('titulo', 'Notificaciones');
    }

    public function getIndexContactos()
    {
        return view('empresa.contacto')->with('titulo', 'Contactos');
    }

    // **************************************************************************************************************************

    protected $API_KEY;

    public function getTest(){

        $this->API_KEY = getenv("API_KEY");
        $culqi = new Culqi(array("api_key" => $this->API_KEY ));

        $customers = $culqi->Customers->all(array("limit" => 1,"email" => "johamessss.15@gmail.com"));

        // $customers = $culqi->Customers->delete('cus_test_0xAtFO3sGqlJRpJ8');

        // $customers = $culqi->Cards->all(array("limit" => 50));

        // $customers = $culqi->Cards->delete('crd_test_ohwLklSuawrrEj3w');

        // $customers = $culqi->Charges->get('chr_test_EO3rQJUhAUO5V9eJ');

        // $customers = $culqi->Charges->capture('chr_test_Jg9VzLwyxiEfg8dp');

        // foreach($customers->data as $data){
        //     return json_encode($data->id);
        // }

        return json_encode($customers->data);

    }

}
