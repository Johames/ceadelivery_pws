<?php

namespace App\Http\Controllers\empresa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloValoracionEmpresa;
use App\Models\ModeloEmpresa;
use App\Http\Requests\ValoracionEmpresaRequest;
use Illuminate\Support\Facades\Auth;

class ValoracionEmpresaController extends Controller
{

    public function ListaResenasCliente($idClient){
        $valorEmp = ModeloValoracionEmpresa::select('valor_prod_id as id')->where('estado', '0')->where('persona_id', $idClient)->get();

        return json_encode($valorEmp);
    }

    public function datosEmpresa($id){

        $datos = ModeloEmpresa::where('empresa_id', $id)->first();

        return json_encode($datos);

    }

    public function AgregarModificarResenaEmp(ValoracionEmpresaRequest $request){

        $persona_id         = Auth::user()->persona_id;

        $empresa_id         = $request->input('id_valoracion');
        $valoracion         = $request->input('valoracion');
        $resena             = $request->input('resena');

        $ver = ModeloValoracionEmpresa::where('empresa_id', $empresa_id)
                                        ->where('persona_id', $persona_id)
                                        ->first();

        if(empty($ver)){

            $valor = ModeloValoracionEmpresa::firstOrCreate(
                [
                    'persona_id'        => $persona_id,
                    'empresa_id'        => $empresa_id
                ],
                [
                    'estrellas'         => $valoracion,
                    'resena'            => $resena
                ]
            );

            return json_encode(['status' => true, 'message' => 'Valoración agregada']);

        } else {

            $valor = ModeloValoracionEmpresa::find($ver->valor_emp_id);

            $valor->estrellas       = $valoracion;
            $valor->resena          = $resena;
            $valor->save();

            return json_encode(['status' => true, 'message' => 'Valoración modificada']);

        }

    }


    public function ListaResenasEmpresa($idEmp){

        $valorEmp = ModeloValoracionEmpresa::select('valor_prod_id as id')->where('estado', '0')->where('producto_detalle_id', $idEmp)->get();

        return json_encode($valorEmp);

    }

    public function PromedioEstrellasEmpresa($idEmp){
        //
    }

    public function CantValorEstrellaEmpresa($idEmp){
        //
    }

}
