<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloValoracionDelivery;
use App\Models\ModeloValoracionEmpresa;
use App\Models\ModeloValoracionProducto;
use Illuminate\Support\Facades\Auth;

class CalificacionesController extends Controller
{

    public function getListarCalificaciones(Request $request){

        if (request()->has('filtro')) {

            switch ($request->filtro) {
                case 'Empresas':

                    $valor_prod = [];
                    $valor_deliv = [];
                    $valor_emp = ModeloValoracionEmpresa::with('ModeloEmpresa')
                                                            ->where('persona_id', Auth::user()->persona_id)
                                                            ->get();

                    return view('componentes.calificaciones.calificaciones_list', compact('valor_emp', 'valor_prod', 'valor_deliv'));

                    break;
                case 'Productos':

                    $valor_emp = [];
                    $valor_deliv = [];
                    $valor_prod = ModeloValoracionProducto::with('ModeloDetalleProducto', 'ModeloDetalleProducto.ModeloProducto', 'ModeloDetalleProducto.PrimeraImagen')
                                                            ->where('persona_id', Auth::user()->persona_id)
                                                            ->get();

                    return view('componentes.calificaciones.calificaciones_list', compact('valor_prod', 'valor_emp', 'valor_deliv'));

                    break;
                case 'Deliverista':

                        $valor_emp = [];
                        $valor_prod = [];
                        $valor_deliv = ModeloValoracionDelivery::with('ModeloDeliverista')
                                                                ->where('persona_id', Auth::user()->persona_id)
                                                                ->get();

                        return view('componentes.calificaciones.calificaciones_list', compact('valor_prod', 'valor_emp', 'valor_deliv'));

                        break;
                default:

                    $valor_prod = [];
                    $valor_emp = ModeloValoracionEmpresa::with('ModeloEmpresa')
                                                        ->where('persona_id', Auth::user()->persona_id)
                                                        ->get();

                    return view('componentes.calificaciones.calificaciones_list', compact('valor_emp', 'valor_prod', 'valor_deliv'));

                    break;

            }

        }

    }

}
