<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloDeseo;
use App\Models\ModeloDeseosProducto;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\DeseosRequest;

class DeseosController extends Controller
{

    public function ListaDeseos(){

        $deseos = ModeloDeseo::where('persona_id', Auth::user()->persona_id)
                            ->where('estado', '0')->get();

        return view('componentes.deseos.deseos_list', compact('deseos'));
    }

    public function ListaDeseosDetalle($id){

        $deseos = ModeloDeseo::with('ModeloDeseosProductos', 'ModeloDeseosProductos.ModeloDetalleProducto', 'ModeloDeseosProductos.ModeloDetalleProducto.ModeloEmpresa',
                                    'ModeloDeseosProductos.ModeloDetalleProducto.ModeloProducto', 'ModeloDeseosProductos.ModeloDetalleProducto.PrimeraImagen', 'ModeloDeseosProductos.ModeloDetalleProducto.ModeloUnidad')
                            ->where('persona_id', Auth::user()->persona_id)
                            ->where('deseos_id', $id)->get();

        return view('componentes.deseos.modal_deseos_list', compact('deseos'));

    }

    public function MostrarDeseos($id){
        $deseos = ModeloDeseo::where('deseos_id', $id)->where('persona_id', Auth::user()->persona_id)->get();

        return json_encode(['deseos' => $deseos]);
    }

    public function ListaDeseosSelect(){
        $lstsDeseos = ModeloDeseo::where('persona_id', Auth::user()->persona_id)->get();

        return json_encode(['deseos' => $lstsDeseos]);
    }

    public function postGuardarEditar(DeseosRequest $request)
    {
        $persona_id         = Auth::user()->persona_id;

        $deseos_id              = $request->input('deseos_id');
        $nombre                 = $request->input('nombre');
        $descripcion            = $request->input('descripcion');

        if(empty($deseos_id)){

            $deseos = ModeloDeseo::firstOrCreate(['nombre' => $nombre, 'persona_id' => $persona_id,],
                    [
                        'persona_id'            => $persona_id,
                        'nombre'                => $nombre,
                        'descripcion'           => $descripcion
                    ]
                );

            return json_encode(['status' => true, 'message' => 'Lista de deseos agregada']);

        } else {

            $deseos = ModeloDeseo::find($deseos_id);

            $deseos->persona_id         = $persona_id;
            $deseos->nombre             = $nombre;
            $deseos->descripcion        = $descripcion;
            $deseos->save();

            return json_encode(['status' => true, 'message' => 'Dirección Modificada']);

        }

    }

    public function postGuardarProdDeseo(Request $request)
    {
        $persona_id         = Auth::user()->persona_id;

        $idProd         = $request->input('idproducto');
        $idDeseo        = $request->input('iddeseo');

        $deseoProd = ModeloDeseosProducto::firstOrCreate(['producto_detalle_id' => $idProd, 'deseos_id' => $idDeseo],
            [
                'producto_detalle_id'         => $idProd,
                'deseos_id'                   => $idDeseo

            ]
        );

        return json_encode(['status' => true, 'message' => 'Se agrego a la lista de deseos']);

    }

    public function eliminarListaDeseo($id){

        $deseos = ModeloDeseo::where('deseos_id', $id)->first();

        if ($deseos) {
            if ($deseos->persona_id == Auth::user()->persona_id) {

                $deseosProd = ModeloDeseosProducto::where('deseos_id', $id)->delete();

                $deseos = ModeloDeseo::where('deseos_id', $id)->delete();

                return json_encode(['status' => true, 'message' => 'Se eliminó la lista de deseos']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

    }

    public function eliminarProdDeseo($id, $idDes){

        $deseos = ModeloDeseo::where('deseos_id', $idDes)->where('persona_id', Auth::user()->persona_id)->first();

        $deseosProd = ModeloDeseosProducto::where('prod_deseo_id', $id)->first();

        if ($deseosProd) {
            if ($deseosProd->deseos_id == $deseos->deseos_id) {

                $deseosProd = ModeloDeseosProducto::where('prod_deseo_id', $id)->delete();

                return json_encode(['status' => true, 'message' => 'Se eliminó el producto de la lista']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Ha intentado acceder a algo prohibido, su accion sera registrada']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Ha intentado acceder a algo prohibido, su accion sera registrada']);
        }

    }

}
