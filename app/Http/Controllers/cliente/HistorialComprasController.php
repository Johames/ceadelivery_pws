<?php

namespace App\Http\Controllers\cliente;

use App\Models\ModeloRespuestaReclamo;
use App\Http\Requests\ReclamoRequest;
use App\Http\Controllers\Controller;
use App\Models\ModeloReclamoPedido;
use App\Models\ModeloPedido;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HistorialComprasController extends Controller
{

    public function ListaHistorial(Request $request){

        $estado = 0;

        if (request()->has('filtro')) {

            switch ($request->filtro) {
                case 'pendientes':      $estado = 0; break;
                case 'EnProceso':       $estado = 1; break;
                case 'Confirmado':      $estado = 2; break;
                case 'EnCamino':        $estado = 3; break;
                case 'Entregado':       $estado = 4; break;
                case 'Rechazado':       $estado = 5; break;
                default: $estado = 0; break;
            }
        }

        $pedidos = ModeloPedido::with('ModeloEmpresa', 'ModeloDetalleMetodoEnvio')
                                ->where('estado', $estado)
                                ->where('persona_id', Auth::user()->persona_id)
                                ->orderBy('updated_at', 'desc')
                                ->paginate(10);

        // return json_encode(['pedidos' => $pedidos]);
        return view('componentes.pedidos.pedidos_list', compact('pedidos'));

    }

    public function DetalleHistorial($idHist){

        $detalle = ModeloPedido::with('ModeloDeliverista', 'ModeloDeliveristaExterno', 'ModeloEmpresa', 'ModeloEmpresa.ModeloContactos',
                                        'ModeloDetalleMetodoEnvio', 'ModeloDetalleMetodoEnvio.ModeloMetodoEnvio',
                                        'ModeloDetalleMetodoPago', 'ModeloDetalleMetodoPago.ModeloMetodoPago',
                                        'ModeloDetallePedidos', 'ModeloDetallePedidos.ModeloDetalleProducto',
                                        'ModeloDetallePedidos.ModeloDetalleProducto.ModeloProducto',
                                        'ModeloDetallePedidos.ModeloDetalleProducto.PrimeraImagen')
                                ->where('pedido_id', $idHist)->get();

        return view('secciones.detalle_historial', compact('detalle'));
        //return json_encode($detalle);

    }

    public function DiscussPedido($idHist){

        $discuss = ModeloPedido::with('ModeloEmpresa')->where('pedido_id', $idHist)->first();

        // return json_encode($discuss);
        return view('componentes.pedidos.modal_discusion', compact('discuss'));

    }

    public function DiscussPedidoRespuestas($idHist){

        $respuestas = ModeloReclamoPedido::with('ModeloRespuestaReclamos')->where('pedido_id', $idHist)->first();

        return view('componentes.pedidos.respuestas_discusion', compact('respuestas'));

    }

    public function guardarReclamo(ReclamoRequest $request){

        $id_pedido = $request->input('pedido_id');
        $id_persona = Auth::user()->persona_id;

        $pedido = ModeloPedido::with('ModeloReclamoPedidos')
                                ->where('pedido_id', $id_pedido)
                                ->where('persona_id', $id_persona)
                                ->first();

        if($pedido){

            if($pedido->estado < 4){

                if(count($pedido->ModeloReclamoPedidos) > 0){

                    $pregunta = ModeloRespuestaReclamo::create([

                        'reclamos_pedido_id'        => $pedido->ModeloReclamoPedidos->first()->reclamos_pedido_id,
                        'tipo'                      => '1',
                        'contenido'                 => $request->input('mensaje'),

                    ]);

                    return json_encode(['status' => true, 'message' => 'Se ha enviado su mensaje.']);

                } else {

                    $reclamo = ModeloReclamoPedido::create([

                        'pedido_id'                 => $pedido->pedido_id,
                        'persona_id'                => $id_persona,
                        'descripcion'               => 'Reclamo de Cliente',

                    ]);

                    $pregunta = ModeloRespuestaReclamo::create([

                        'reclamos_pedido_id'        => $reclamo->reclamos_pedido_id,
                        'tipo'                      => '1',
                        'contenido'                 => $request->input('mensaje'),

                    ]);

                    return json_encode(['status' => true, 'message' => 'Se ha enviado su mensaje.']);

                }

            } else {

                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);

            }

        } else {

            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);

        }

    }

}
