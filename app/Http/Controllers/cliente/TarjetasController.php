<?php

namespace App\Http\Controllers\cliente;

use App\Http\Controllers\Controller;
use App\Models\ModeloTarjeta;
use Culqi\Culqi;
use Illuminate\Support\Facades\Auth;

class TarjetasController extends Controller
{

    protected $API_KEY;

    public function listarTarjetas(){

        $tarjetas = ModeloTarjeta::where('persona_id', Auth::user()->persona_id)->where('estado', '0')->get();

        return view('componentes.tarjetas.tarjetas_list', compact('tarjetas'));

    }

    public function ListarTarjetasPago()
    {

        $tarjetas = ModeloTarjeta::where('persona_id', Auth::user()->persona_id)->where('estado', '0')->get();

        return view('componentes.pasos.modal_pago_tarjeta', compact('tarjetas'));

    }

    public function eliminarTarjetas($id){

        $this->API_KEY = getenv("API_KEY");
        $culqi = new Culqi(array("api_key" => $this->API_KEY ));

        $tarjetas = ModeloTarjeta::where('persona_id', Auth::user()->persona_id)->where('card_id', $id)->get();

        if($tarjetas){

            ModeloTarjeta::where('card_id', $id)->delete();

            $culqi->Cards->delete($id);

            return json_encode(['status' => true, 'message' => 'Se eliminó la tarjeta']);

        } else {
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

    }


}
