<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloNotificacion;
use App\Models\ModeloPedido;
use Illuminate\Support\Facades\Auth;

class NotificacionesController extends Controller
{
    public function ListaNotificacionesCliente(Request $request)
    {

        $visto = 0;

        if (request()->has('filtro')) {

            switch ($request->filtro) {
                case 'NoLeido':     $visto = 0; break;
                case 'Leido':       $visto = 1; break;
                default:            $visto = 0; break;
            }
        }

        $notificaciones = ModeloNotificacion::where('persona_id', Auth::user()->persona_id)
                                            ->where('tipo_accion', 'compra')
                                            ->where('visto', $visto)
                                            ->where('estado', '0')
                                            ->get();

        $descuentos     = ModeloNotificacion::where('tipo_accion', 'descuento')
                                            ->where('visto', $visto)
                                            ->where('estado', '0')
                                            ->get();

        $sistema        = ModeloNotificacion::where('tipo_accion', 'sistema')
                                            ->where('visto', $visto)
                                            ->where('estado', '0')
                                            ->get();

        $notificaciones = ModeloNotificacion::where('persona_id', Auth::user()->persona_id)
                                            ->where('tipo_accion', 'compra')
                                            ->where('visto', $visto)
                                            ->where('estado', '0')
                                            ->get();

        return view('componentes.notificaciones.notificaciones_list', compact('notificaciones'));

    }

    public function NotificacionesClienteNavbar()
    {

        $notificaciones = ModeloNotificacion::where('persona_id', Auth::user()->persona_id)->where('visto', 0)->get();

        return view('secciones.navbar.notificaciones_navbar',compact('notificaciones', 'notificaciones'));

    }

    public function mostrarNotificaciones($id)
    {

        $edit = ModeloNotificacion::find($id);
        $edit->visto        = '1';
        $edit->save();

        $notificacion = ModeloNotificacion::where('notificacion_id', $id)->first();

        $detalle = ModeloPedido::with('ModeloDeliverista', 'ModeloEmpresa', 'ModeloEmpresa.ModeloContactos',
                                        'ModeloDetalleMetodoEnvio', 'ModeloDetalleMetodoEnvio.ModeloMetodoEnvio',
                                        'ModeloDetalleMetodoPago', 'ModeloDetalleMetodoPago.ModeloMetodoPago',
                                        'ModeloDetallePedidos', 'ModeloDetallePedidos.ModeloDetalleProducto',
                                        'ModeloDetallePedidos.ModeloDetalleProducto.ModeloProducto',
                                        'ModeloDetallePedidos.ModeloDetalleProducto.PrimeraImagen')
                                ->where('pedido_id', $notificacion->id_accion)->get();

        // return json_encode(['notificacion' => $notificacion]);
        return view('componentes.notificaciones.detalle_pedido_notificacion', compact('notificacion', 'detalle'));

    }

}
