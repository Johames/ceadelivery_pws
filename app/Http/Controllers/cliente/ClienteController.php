<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloPersona;
use App\Http\Requests\PerfilClienteRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\FileController;
use App\Models\ModeloUbigeo;

class ClienteController extends Controller
{

    // Rutas de las Vistas del Admin de Cliente *********************************************************************************
    public function getIndexNotificaciones(){
        return view('cliente.notificaciones')->with('titulo', 'Lista de Notificaciones');
    }

    public function getIndexDeseos(){
        return view('cliente.deseos')->with('titulo', 'Listas de Deseos');
    }

    public function getIndexHistorial(){
        return view('cliente.historial')->with('titulo', 'Historial de Compras');
    }

    public function getIndexCalificaciones(){
        return view('cliente.calificaciones')->with('titulo', 'Calificaciones y Rese単as');
    }

    public function getIndexCeaPuntos(){
        return view('cliente.ceapuntos')->with('titulo', 'CEA Puntos');
    }

    public function getIndexPefil(){
        return view('cliente.perfilcliente')->with('titulo', 'Perfil del Cliente');
    }

    public function getIndexDirecciones(){
        return view('cliente.direcciones')->with('titulo', 'Administrar Direcciones');
    }

    public function getIndexTarjetas(){
        return view('cliente.tarjetas')->with('titulo', 'Administrar Medios de Pago');
    }


    // **************************************************************************************************************************
    public function getMostarPerfilCliente()
    {

        $persona_id = Auth::user()->persona_id;

        $cliente = ModeloPersona::where('persona_id', $persona_id)->first();

        return json_encode($cliente);

    }

    public function postEditarClientePerfil(PerfilClienteRequest $request)
    {
        $persona_id                         = Auth::user()->persona_id;
        $cliente                            = ModeloPersona::find($persona_id);

        $cliente->nombres                   = $request->input('nombres');
        $cliente->apellidos                 = $request->input('apellidos');
        $cliente->representante_legal       = $request->input('representante_legal');
        $cliente->telefono                  = $request->input('telefono_perfil');

        $cliente->save();

        return json_encode(['status' => true, 'message' => 'Se modificado su perfil.']);
    }

    public function postEditarClienteIcono(Request $request)
    {
        $persona_id = Auth::user()->persona_id;
        $cliente   = ModeloPersona::find($persona_id);

        $icono        = $request->file('avatar');
        $nombre_icono = FileController::saveImg($icono, $persona_id, 'cd_persona', 'persona_id', 'avatar');

        $cliente->avatar = $nombre_icono;
        $cliente->save();

        return json_encode(['status' => true, 'message' => 'Se ha modificado su imagen de perfil']);
    }

    public function SolicitarEliminarCliente($id){

    }

    public function getUbigeoForHumans($id)
    {
        $ubigeo = ModeloUbigeo::with('allParentUbigeos')->where('ubigeo_id', $id)->first();

        $nameForHumans = '';

        switch ($ubigeo->tipo) {
            case '1':
                $nameForHumans = 'Departamento de ' . $ubigeo->nombre ;
                break;
            case '2':
                $nameForHumans = 'Provincia de ' . $ubigeo->nombre . ', departamento de ' . $ubigeo->allParentUbigeos->nombre ;

                break;
            case '3':
                $nameForHumans = 'Distrito de ' . $ubigeo->nombre . ', provincia de ' . $ubigeo->allParentUbigeos->nombre . ', departamento de '. $ubigeo->allParentUbigeos->allParentUbigeos->nombre ;
                break;

            default:

                break;
        }

        return $nameForHumans;
    }

    public function mostrarModalUbigeo(){

        $ubigeo     = Auth::user()->ModeloPersona->ubigeo_id;

        $ciudades = ModeloUbigeo::where('ubigeo_id', $ubigeo)->first();

        if ($ciudades) {

            $nameForHumans = $this->getUbigeoForHumans($ciudades->ubigeo_id);

            return json_encode(['status' => true, 'ciudad' => $ciudades->ubigeo_id, 'nombre' => $ciudades->nombre, 'texto' => $nameForHumans]);

        } else {

            return json_encode(['status' => false, 'ciudad' => null, 'nombre' => null, 'texto' => null]);

        }

    }

    public function AgregarCiudad(Request $request)
    {

        $this->validate($request, [
            'ciudad' => 'required'
        ], [
          'ciudad.required' => 'Seleccione una ciudad'
        ]);

        $persona_id = Auth::user()->persona_id;
        $ciudad     = $request->input('ciudad');

        $persona = ModeloPersona::where('persona_id', $persona_id)->first();

        if ($persona) {

            $persona->ubigeo_id = $ciudad;
            $persona->save();

            return json_encode(['status' => true, 'message' => 'Se han guardado los cambios']);

        } else {

            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento será registrado.']);

        }

    }

    public function postAgregarTelefono(Request $request)
    {

        $this->validate($request, [
            'telefono' => 'required'
        ], [
          'telefono.required' => 'Ingrese un numero de Teléfono / Celular'
        ]);

        $persona_id     = Auth::user()->persona_id;
        $telefono       = $request->input('telefono');

        $persona = ModeloPersona::where('persona_id', $persona_id)->first();

        if ($persona) {

            $persona->telefono = $telefono;
            $persona->save();

            return json_encode(['status' => true, 'message' => 'Se han guardado los cambios']);

        } else {

            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento será registrado.']);

        }


    }

    public function getSelectUbigeo(Request $request)
    {
        $ubigeos = ModeloUbigeo::where('tipo', 1)->get();


        $page = $request->page;
        $resultCount = 10;
        $offset = ($page - 1) * $resultCount;

        $tipo = 3;
        $filter = $request->filter;
        $term = $request->term ?: '';

        switch ($filter) {
            case '1': $tipo = 1; break;
            case '2': $tipo = 2; break;
            case '3': $tipo = 3; break;
            default: $tipo = 3; break;
        }

        $ubigeos = ModeloUbigeo::where('tipo', $tipo)->where('nombre', 'LIKE', '%'.$term.'%')
        ->skip($offset)
        ->take($resultCount)
        ->get();

        $c_ubigeos = ModeloUbigeo::where('tipo', $tipo)->where('nombre', 'LIKE', '%'.$term.'%')->get();

        $data = Array();

        foreach ($ubigeos as $key => $ubigeo) {
            $data[] = array("id" => $ubigeo->ubigeo_id, "text" => $ubigeo->nombre . $this->getUbigeoForHumansToShort($ubigeo->ubigeo_id)  );
        }

        $endCount = $offset + $resultCount;
        $morePages = count($ubigeos) > $endCount;

        $results = array(
            "total" => count($c_ubigeos),
            "results" => $data,
            "pagination" => array(
                "more" => $morePages
            )
        );

        return json_encode($results);

    }

    public function getUbigeoForHumansToShort($id)
    {
        $ubigeo = ModeloUbigeo::with('allParentUbigeos')->where('ubigeo_id', $id)->first();

        $nameForHumans = '';

        switch ($ubigeo->tipo) {

            case '2':
                $nameForHumans = ', ' . $ubigeo->allParentUbigeos->nombre ;

                break;
            case '3':
                $nameForHumans = ', ' . $ubigeo->allParentUbigeos->nombre . ', '. $ubigeo->allParentUbigeos->allParentUbigeos->nombre ;
                break;

            default:

                break;
        }

        return $nameForHumans;
    }

}
