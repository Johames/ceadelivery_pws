<?php

namespace App\Http\Controllers\cliente;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloDireccion;
use App\Http\Requests\ClientDireccionRequest;
use Illuminate\Support\Facades\Auth;

class DireccionController extends Controller
{

    public function getListar(Request $request){

        $estado = 0;

        if (request()->has('filtro')) {

            switch ($request->filtro) {
                case 'Activas':         $estado = 0; break;
                case 'Inactivas':       $estado = 1; break;
                default:                $estado = 0; break;
            }
        }

        $direcciones = ModeloDireccion::where('persona_id', Auth::user()->persona_id )
                                        ->where('estado', $estado)
                                        ->get();

        // return json_encode(['direcciones' => $direcciones]);
        return view('componentes.direcciones.direcciones_list', compact('direcciones'));
    }

    public function getMostar($id)
    {
        $direcciones = ModeloDireccion::where('direciones_id', $id)->first();

        return json_encode($direcciones);
    }

    public function getPrincipal(){

        //hacer la funcion para poner un principal y el resto secundarios.

    }

    public function getListarSelect()
    {
        $persona_id = Auth::user()->persona_id;

        $direcciones = ModeloDireccion::where('persona_id', $persona_id)->where('estado', '0')->select('direciones_id as id', 'direccion as nombre')->get();

        return json_encode($direcciones);
    }

    public function postGuardarEditar(ClientDireccionRequest $request)
    {
        $persona_id             = Auth::user()->persona_id;

        $direciones_id          = $request->input('direciones_id');
        $direccion              = $request->input('direccion');
        $referencia             = $request->input('referencia');
        $coordenadas            = $request->input('coordenadas');

        if (empty($direciones_id)){

            $direcciones = ModeloDireccion::firstOrCreate(['direccion' => $direccion, 'persona_id' => $persona_id],
                    [
                        'persona_id'        => $persona_id,
                        'direccion'         => $direccion,
                        'referencia'        => $referencia,
                        'coordenadas'       => $coordenadas
                    ]
                );

            return json_encode(['status' => true, 'message' => 'Dirección agregada', 'id' => $direcciones->direciones_id ]);
        }
        else {

            $direcciones = ModeloDireccion::find($direciones_id);

            $direcciones->persona_id      = $persona_id;
            $direcciones->direccion       = $direccion;
            $direcciones->referencia      = $referencia;
            $direcciones->coordenadas     = $coordenadas;
            $direcciones->save();

            return json_encode(['status' => true, 'message' => 'Dirección Modificada']);
        }

    }

    public function getEliminar($id)
    {
        $direcciones = ModeloDireccion::where('direciones_id', $id)->first();

        if ($direcciones) {
            if ($direcciones->persona_id == Auth::user()->persona_id) {

                $direcciones = ModeloDireccion::where('direciones_id', $id)->delete();

                return json_encode(['status' => true, 'message' => 'Se eliminó la dirección']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

    }

    public function getDesactivar($id)
    {
        $direcciones = ModeloDireccion::where('direciones_id', $id)->first();

        if ($direcciones) {
            if ($direcciones->persona_id == Auth::user()->persona_id) {

                $direcciones->estado = 1;
                $direcciones->save();

                return json_encode(['status' => true, 'message' => 'Se ha desactivado la dirección']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

    }

    public function getActivar($id)
    {
        $direcciones = ModeloDireccion::where('direciones_id', $id)->first();

        if ($direcciones) {
            if ($direcciones->persona_id == Auth::user()->persona_id) {

                $direcciones->estado = 0;
                $direcciones->save();

                return json_encode(['status' => true, 'message' => 'Se ha activado la dirección']);
            }else{
                //hacer tabla de incidencias
                return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
            }
        }else{
            return json_encode(['status' => false, 'message' => 'Los datos fueron manipulados, el evento sera registrado.']);
        }

    }

}
