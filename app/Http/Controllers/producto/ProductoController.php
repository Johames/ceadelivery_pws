<?php

namespace App\Http\Controllers\producto;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FileController;
use App\Models\ModeloDetalleProducto;
use App\Models\ModeloProducto;
use App\Http\Request\DetalleProductoRequest;
use App\Http\Requests\GeneralRequest;
use App\Http\Requests\ProductoEmpresaRequest;
use App\Models\ModeloDeseosProducto;
use App\Models\ModeloProductoImagen;
use Illuminate\Support\Facades\Auth;

class ProductoController extends Controller
{

    public function getIndex()
    {
        return view('producto.productos_empresa')->with('titulo', 'Productos de la Empresa');
    }

    public function getIndexProduct()
    {
        return view('producto.productos')->with('titulo', 'Lista de Productos');
    }

    public function getListarProductosEmpresa(Request $request)
    {
        $empresa_id = Auth::user()->empresa_id;


        if (request()->has('filtro')) {
            # code...
            switch ($request->filtro) {
                case 'activo':
                    $productos  = ModeloDetalleProducto::with('ModeloProducto','ModeloCategoria',
                                                                'ModeloMarca','ModeloUnidad',
                                                                'ModeloTalla','ModeloColor','ModeloProductoImagenes')
                                                        ->where('empresa_id', $empresa_id)
                                                        ->where('estado', '0')
                                                        ->orderBy('updated_at', 'desc')
                                                        ->paginate(8);
                    break;
                case 'suspendidos':
                    $productos  = ModeloDetalleProducto::with('ModeloProducto','ModeloCategoria',
                                                                'ModeloMarca','ModeloUnidad',
                                                                'ModeloTalla','ModeloColor','ModeloProductoImagenes')
                                                        ->where('empresa_id', $empresa_id)
                                                        ->where('estado', '1')
                                                        ->orderBy('updated_at', 'desc')
                                                        ->paginate(8);
                break;
                case 'sin_stock':
                    $productos  = ModeloDetalleProducto::with('ModeloProducto','ModeloCategoria',
                                                                'ModeloMarca','ModeloUnidad',
                                                                'ModeloTalla','ModeloColor','ModeloProductoImagenes')
                                                        ->where('empresa_id', $empresa_id)
                                                        ->where('estado', '0')
                                                        ->where('stock', '0')
                                                        ->orderBy('updated_at', 'desc')
                                                        ->paginate(8);
                break;

                default:
                    # code...
                    break;
            }
        }else{
            $productos  = ModeloDetalleProducto::with('ModeloProducto','ModeloCategoria',
                                                  'ModeloMarca','ModeloUnidad',
                                                  'ModeloTalla','ModeloColor','ModeloProductoImagenes')
                                            ->where('empresa_id', $empresa_id)
                                            ->orderBy('updated_at', 'desc')
                                            ->paginate(8);

        }

        return view('producto.productos_paginacion', compact('productos'))->render();



    }

    public function getProductoDetalle($idProd){

        $productos  = ModeloDetalleProducto::with('ModeloEmpresa', 'ModeloProducto','ModeloCategoria',
                                                  'ModeloMarca','ModeloUnidad', 'ModeloTalla','ModeloColor',
                                                  'ModeloProductoImagenes', 'ModeloValoracionProductos',
                                                  'ModeloValoracionProductos.ModeloPersona', 'PrimeraImagen')
                                            ->where('producto_detalle_id', $idProd)->get();

        return view('secciones.detalle_producto', compact('productos'));
    }

    public function getListarSelect()
    {
        $productos = ModeloProducto::select('producto_id as id', 'nombre')->where('estado','0')->get();

        return json_encode($productos);
    }

}
