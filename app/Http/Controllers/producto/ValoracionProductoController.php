<?php

namespace App\Http\Controllers\producto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ModeloValoracionProducto;
use App\Models\ModeloDetalleProducto;
use App\Http\Requests\ValoracionProductoRequest;
use Illuminate\Support\Facades\Auth;

class ValoracionProductoController extends Controller
{

     public function ListaResenasProdClient($idClient){
         $valorProd = ModeloValoracionProducto::where('estado', '0')->where('persona_id', $idClient)->select('valor_prod_id as id')->get();

         return json_encode($valorProd);
     }


     public function datosProducto($id){

        $datos = ModeloDetalleProducto::with('ModeloProducto', 'PrimeraImagen')
                                        ->where('producto_detalle_id', $id)->get();

        return json_encode($datos);

     }


     public function AgregarModificarResenaProd(ValoracionProductoRequest $request){

        $persona_id         = Auth::user()->persona_id;

        $producto_detalle_id        = $request->input('id_valoracion');
        $valoracion                 = $request->input('valoracion');
        $resena                     = $request->input('resena');

        $ver = ModeloValoracionProducto::where('producto_detalle_id', $producto_detalle_id)
                                        ->where('persona_id', $persona_id)
                                        ->first();

        if(empty($ver)){

            $valor = ModeloValoracionProducto::firstOrCreate(
                [
                    'persona_id'                => $persona_id,
                    'producto_detalle_id'       => $producto_detalle_id
                ],
                [
                    'estrellas'         => $valoracion,
                    'resena'            => $resena
                ]
            );

            return json_encode(['status' => true, 'message' => 'Valoración agregada']);

        } else {

            $valor = ModeloValoracionProducto::find($ver->valor_prod_id);

            $valor->estrellas       = $valoracion;
            $valor->resena          = $resena;
            $valor->save();

            return json_encode(['status' => true, 'message' => 'Valoración modificada']);

        }

        // return $ver;

     }


     public function ListaResenasProd($idDetProd){

        $valorProd = ModeloValoracionproducto::select('valor_prod_id as id')->where('estado', '0')->where('producto_detalle_id', $idDetProd)->get();

        return json_encode($valorProd);

     }

     public function PromedioEstrellasProd($idDetProd){
         //
     }

     public function CantValorEstrellaProd($idDetProd){
         //
     }

}
