<?php

namespace App\Http\Controllers\cuenta;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ConfirmarCuentaMail;
use App\Mail\RecuperarCuentaMail;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CuentaController extends Controller
{
    //

    public function getViewRecuperarCuenta()
    {
        return view('auth.recuperar_cuenta');
    }

    // Route::post ('/recover') ~~ Generar codigo y mandar al gmail
    public function postRecuperarCuenta(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|email|exists:users,email'
        ],
        [
            'email.required' => 'Ingrese un correo electronico,',
            'email.exists' => 'Este correo no existe, es necesario que porporcione un correo váildo',
        ]);

        $email   = $request->input('email');

        $usuario = User::with('ModeloPersona')->where('email', $email)->first();

    
        if ($usuario) {
            
            $codigo = Str::upper(Str::random(6));
            $now    = Carbon::now();

            $data = [
                "nombres" => $usuario->ModeloPersona->nombres . ' ' . $usuario->ModeloPersona->apellidos,
                "codigo" => $codigo,
            ];

            $usuario->codigo_recuperacion = $codigo;
            $usuario->fecha_recuperacion  = $now;
            $usuario->save();
            

            Mail::to($email)->send(new RecuperarCuentaMail($data));

            return redirect('/recover/codigo?email=' . $email);

        }else{
            
            return view('auth.recuperar_cuenta')->withErrors(['Este correo no existe, es necesario que porporcione un correo váildo']);
        }
    }

    // Route::get ('/recover/codigo') ~~ Formulario ingresar codigo 
    public function getRecuperarCuentaCodigo(Request $request)
    {
        $usuario = User::where('email', $request->email)->first();
        $fecha_recuperacion = $usuario->fecha_recuperacion;

        if ($fecha_recuperacion) {

            $now = Carbon::now();
            $end = Carbon::parse($fecha_recuperacion);

            $minutos = $end->diffInMinutes($now, true);

            if ($minutos <= 30) {
                return view('auth.recuperar_cuenta_codigo')->with('email', $request->email);
            }else{
                return back()->withErrors(['Su código ha expirado, vuelva ha generar nueva petición']); 
            }

        }else{

            // return json_encode(['status' => false]); 
            return back()->with('message', 'Genere un pedido')->withErrors(['Necesita llenar este formulario']);
        }
        // 
    }

    // Route::post ('/recover/codigo/validate') ~~ Recibir codigo y validar cuenta
    public function postValidarCodigo(Request $request)
    {
        if ($request->email) {

            $code_1 = $request->code_1; $code_2 = $request->code_2; $code_3 = $request->code_3;
            $code_4 = $request->code_4; $code_5 = $request->code_5; $code_6 = $request->code_6;
    
            $code_complete = $code_1 . $code_2 . $code_3 . $code_4 . $code_5 . $code_6;
    
            $usuario = User::where('email', $request->email)->where('codigo_recuperacion', $code_complete)->first();
    
            if ($usuario) {
                
                return  redirect('/recover/reset?email=' . $usuario->email . '&codigo_recuperacion=' . $usuario->codigo_recuperacion);
    
            }else{
    
                return back()->withErrors(['El código es inválido, revise su correo por favor']);
            }
        }else{
            return redirect('/recover')->withErrors(['Datos incorrectos, intente generando uno nuevo']);
        }
    }

    public function getResetCuenta(Request $request)
    {
        //

        $usuario = User::where('email', $request->email)->first();
        $fecha_recuperacion = $usuario->fecha_recuperacion;

        if ($fecha_recuperacion) {

            $now = Carbon::now();
            $end = Carbon::parse($fecha_recuperacion);

            $minutos = $end->diffInMinutes($now, true);

            if ($minutos <= 30) {
                return view('auth.recuperar_cuenta_reset')->with('email', $request->email)->with('codigo_recuperacion', $request->codigo_recuperacion);
            }else{
                return back()->withErrors(['Su código ha expirado, vuelva ha generar nueva petición']); 
            }

        }else{

            // return json_encode(['status' => false]); 
            return back()->with('message', 'Genere un pedido')->withErrors(['Necesita llenar este formulario']);
        }

        
    }

    public function postResetCuentaSave(Request $request)
    {
        $validate = $request->validate([
            "email"    => "required",
            "password" => "required|min:8|max:16",
        ],
        [
            'email.required' => 'Ingrese un correo electronico,',
        ]);

        $usuario = User::where('email', $request->email)->where('codigo_recuperacion', $request->codigo_recuperacion)->first();

        if ($usuario) {
            $fecha_recuperacion = $usuario->fecha_recuperacion;

            $now = Carbon::now();
            $end = Carbon::parse($fecha_recuperacion);

            $minutos = $end->diffInMinutes($now, true);

            if ($minutos <= 40) {
                //

                $usuario->password = bcrypt($request->input('password'));
                $usuario->save();

                if (Auth::attempt(['email' => $usuario->email, 'password' => $request->input('password'), 'estado' => 0])) {

                    if (Auth::check()) {
                        return redirect('/cliente/perfil');
                    }
                } else {
                    return redirect('/recover')->withErrors(['Su código ha expirado, vuelva ha generar nueva petición']);
                }

            }else{
                return redirect('/recover')->withErrors(['Su código ha expirado, vuelva ha generar nueva petición']); 
            }
        }else{
            return redirect('/recover')->withErrors(['Su código ha expirado, vuelva ha generar nueva petición']); 
        }

        

    }

    /******************************************************** */
    /******************************************************** */
    /********** VERIFICAR CUENTA DEL NUEVO USUAIRO ********** */
    /******************************************************** */

    // 
    public function getVerificarCuentaIndex(Request $request)
    {

        $usuario = User::where('id', Auth::user()->id)->first();

        if ($usuario->verificado == 1) {
            return view('auth.verificar_cuenta');
        }else{
            return redirect('/');
        }

        // ;
        
    }

    public function postVerificarCuenta(Request $request)
    {
        
        $validate = $request->validate(
            [
                'email' => 'required|email',
                'codigo' => 'required',
            ]
        );
        

        $usuario = User::where('id', Auth::user()->id)->first();

        if ($usuario->codigo_confirmacion === $request->codigo) {

            $usuario->verificado = 0;
            $usuario->save();

            return json_encode(['status' => true, 'message' => 'Su cuenta ha sido activada']);
        }else{

            return json_encode(['status' => false, 'message' => 'El código no coincide, por favor inténtelo de nuevo']);
        }

    }

    public function getVerificarCuenta($codigo)
    {
      if ($codigo) {

        $usuario = User::where('id', Auth::user()->id)->first();

            if ($usuario->verificado == 0) {
                return redirect('/')->with('success', 'Su cuenta ya ha sido activada anteriormente');
            }else{
                if ($usuario->codigo_confirmacion === $codigo) {

                    $usuario->verificado = 0;
                    $usuario->codigo_confirmacion = null;
                    $usuario->save();
    
                    return redirect('/')->with('success', 'Su cuenta ha sido activada');
    
                }else{
                    return redirect('/verificar')->withErrors(['Su código es incorrecto ó ha expirado, vuelva ha enviar el correo de confirmación']);
                }
            }
        
      }
        
    }

    public function getReenviarVerificarCuenta()
    {
        $codigo = Str::upper(Str::random(6));

        $usuario = User::with('ModeloPersona')->where('id', Auth::user()->id)->first();

        if ($usuario) {
            $usuario->codigo_confirmacion =  $codigo;
            $usuario->save();
    
            $data = [
                "nombres" => $usuario->ModeloPersona->nombres . ' ' . $usuario->ModeloPersona->apellidos,
                "codigo" => $usuario->codigo_confirmacion,
            ];
    
            Mail::to($usuario->email)->queue(new ConfirmarCuentaMail($data));
    
            // return json_encode($usuario);
            return json_encode(['status' => true, 'message' => 'Se ha reenviado, revise su bandeja de entrada']);
        }else{
            return json_encode(['status' => false, 'message' => 'No tiene permisos, se notificara al administrador']);
        }
        
    }



}
