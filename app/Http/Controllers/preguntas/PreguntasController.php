<?php

namespace App\Http\Controllers\preguntas;

use App\Models\ModeloPreguntaFrecuente;
use App\Http\Controllers\Controller;

class PreguntasController extends Controller
{

    public function getIndexPreguntas()
    {
        $preguntas = ModeloPreguntaFrecuente::where('estado', '0')->get();

        $respuesta = [];


        return view('preguntas.preguntas', compact('preguntas', 'respuesta'))->with('titulo', 'Preguntas Frecuentes');
    }

    public function getPreguntas($slug)
    {
        $preguntas = ModeloPreguntaFrecuente::where('estado', '0')->get();

        $respuesta = ModeloPreguntaFrecuente::where('slug', $slug)->where('estado', '0')->first();


        return view('preguntas.preguntas', compact('preguntas', 'respuesta'))->with('titulo', 'Preguntas Frecuentes');
    }

    public function mostrarPreguntas(){

        $mostrar = ModeloPreguntaFrecuente::where('mostrar', '0')->where('estado', '0')->get();

        return json_encode($mostrar);

    }

}
