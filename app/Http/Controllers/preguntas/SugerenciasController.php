<?php

namespace App\Http\Controllers\preguntas;

use App\Http\Requests\SugerenciasRequest;
use App\Http\Controllers\Controller;
use App\Models\ModeloSugerencias;
use Illuminate\Http\Request;

class SugerenciasController extends Controller
{

    public function agregarSugerencia(SugerenciasRequest $request){

        $area                           = $request->input('area_coment');
        $tipo                           = $request->input('tipo_coment');
        $comentario                     = $request->input('comentario_coment');

        $sugerencia = ModeloSugerencias::create([

            'area'                      => $area,
            'tipo'                      => $tipo,
            'descripcion'               => $comentario

        ]);

        return json_encode(['status' => true, 'mensaje' => 'Se ha registrado su comentario.']);

    }

}
