<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SugerenciasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'area_coment'                           => 'required',
            'tipo_coment'                           => 'required',
            'comentario_coment'                     => 'required|max:1000',

        ];
    }

    public function messages()
    {
        return [

            "area_coment.required"                  => "Debe especificar el área de la página.",
            "tipo_coment.required"                  => "Debe seleccionar un tipo.",
            "comentario_coment.required"            => "Debe escribir un comentario.",
            "comentario_coment.max"                 => "El comentario debe tener menos de 1000 caracteres.",

        ];
    }
}
