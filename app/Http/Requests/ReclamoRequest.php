<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReclamoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mensaje'                       => 'required|min:10|max:1000'
        ];
    }

    public function messages()
    {
        return [
            'mensaje.required'              => 'Ingrese el mensaje que desea enviar a la tienda.',
            'mensaje.min'                   => 'El mensaje debe tener mas de 10 carácteres.',
            'mensaje.max'                   => 'El mensaje debe tener menos de 1000 carácteres.',
        ];
    }
}
