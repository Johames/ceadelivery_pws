<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistarClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            "email"                         => "required|email|unique:users,email",
            "password"                      => "required|confirmed|min:8|max:16",
            "password_confirmation"         => "required|min:8|max:16",

            "ruc_dni"                       => "required|min:8|max:11|unique:cd_persona,ruc_dni",
            "nombres"                       => "required|max:120",
            "apellidos"                     => "required|max:120",
        ];
    }

    public function messages()
    {
        return [

            "email.required"                    => "Ingrese un correo electronico.",
            "email.unique"                      => "El valor del campo correo ya está en uso.",
            "password.required"                 => "El campo contraseña es obligatorio.",
            "password.confirmed"                => "El campo confirmación de la contraseña no coincide.",
            "password.min"                      => "El campo contraseña debe contener al menos 8 caracteres.",
            "password.max"                      => "El campo contraseña debe contener menos de 16 caracteres.",
            "password_confirmation.required"    => "El campo confirmar contraseña es obligatorio.",
            "password_confirmation.min"         => "El campo confirmar contraseña debe contener al menos 8 caracteres.",
            "password_confirmation.max"         => "El campo confirmar contraseña debe contener menos de 16 caracteres.",

            "ruc_dni.min"                       => "El campo dni debe al menos 8 caracteres",
            "ruc_dni.max"                       => "El campo dni no debe tener mas 8 caracteres",
            "nombres.required"                  => "Debe ingresar su nombre",
            "apellidos.required"                => "Debe ingresar sus apellidos",
        ];
    }
}
