<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserGeneralRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nombres' => 'required',
            'email' => "min:7|max:45|required|unique:users,email,". $this->id .",id",
            'password' => 'required|min:8',
        ];
    }
}
