<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerfilClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'ruc_dni'                       => 'required|max:11',
            'nombres'                       => 'max:600',
            'apellidos'                     => 'max:600',
            'representante_legal'           => 'max:450',
            'telefono_perfil'               => 'max:15'

        ];
    }

    public function messages()
    {
        return [
            'nombres.required'              => 'Ingrese su Nombre',
            'apellidos.required'            => 'Ingrese sus Apellidos',
            'telefono_perfil.required'      => 'Ingrese su Número de Teléfono',
        ];
    }
}
