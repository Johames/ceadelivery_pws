<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientDireccionRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'direccion' => 'required|max:500',
            'referencia' => 'max:1000',
            'coordenadas' => 'required|min:10|max:100'
        ];
    }

    public function messages()
    {
        return [
            'direccion.required' => 'Debe ingresar una dirección'
        ];
    }
}
