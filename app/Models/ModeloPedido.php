<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloPedido extends Model
{
    protected $table          = 'cd_pedido';
    protected $primaryKey     = 'pedido_id';

    protected $fillable      = ['paso', 'codigo', 'empresa_id', 'persona_id', 'users_id', 'deliverista_id', 'metodo_pago_emp_id', 'metodo_envio_emp_id', 'direciones_id', 'fecha_pedido', 'fecha_entrega', 'extras', 'comentarios', 'total_pagar', 'codigo_descuento', 'descuento', 'igv', 'precio_envio', 'precio_comision', 'fecha_entregado', 'hora_entregado', 'puntos_ganados', 'canjear_puntos', 'token_pago_card', 'pagado', 'cliente_facturacion', 'tipo_documento_facturacion', 'nro_documento_facturacion', 'nro_telefono_facturacion', 'direccion_facturacion', 'voucher', 'motivo_rechazo', 'estado'];

    public function ModeloDetallePedidos(){
        return $this->hasMany('App\Models\ModeloDetallePedido', 'pedido_id', 'pedido_id');
    }

    public function ModeloEmpresa(){
        return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloDetalleMetodoPago(){
        return $this->belongsTo('App\Models\ModeloDetalleMetodoPago', 'metodo_pago_emp_id', 'metodo_pago_emp_id');
    }

    public function ModeloDetalleMetodoEnvio(){
        return $this->belongsTo('App\Models\ModeloDetalleMetodoEnvio', 'metodo_envio_emp_id', 'metodo_envio_emp_id');
    }

    public function ModeloDeliverista(){
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function ModeloDireccion(){
        return $this->belongsTo('App\Models\ModeloDireccion', 'direciones_id', 'direciones_id');
    }

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

    public function ModeloReclamoPedidos(){
        return $this->hasMany('App\Models\ModeloReclamoPedido', 'pedido_id', 'pedido_id');
    }

    public function ModeloDeliveristaExterno(){
        return $this->belongsTo('App\Models\ModeloDeliverista', 'deliverista_id', 'deliverista_id');
    }

}
