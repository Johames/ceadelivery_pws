<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloBlog extends Model
{

    protected $table          = 'cd_blog';
    protected $primaryKey     = 'blog_id';
    protected $fillable       = ['slug', 'titulo', 'categoria', 'banner', 'miniatura', 'contenido', 'autor', 'url', 'estado'];

    public function ModeloComentariosBlogs(){
        return $this->hasMany('App\Models\ModeloComentarioBlog', 'blog_id', 'blog_id');
    }

}
