<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloEstadoCuenta extends Model
{

    protected $table          = 'cd_estado_cuentas';
    protected $primaryKey     = 'estado_cuentas_id';
    protected $fillable       = ['deliverista_id', 'empresa_id', 'monto_tarjetas', 'monto_efectivo', 'comision_cobrar', 'monto_depositar', 'monto_cobrado', 'deuda_pendiente', 'estado'];

    public function ModeloDeliverista(){
        return $this->belongsTo('App\Models\ModeloDeliverista', 'deliverista_id', 'deliverista_id');
    }

    public function ModeloEmpresa(){
        return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }


}
