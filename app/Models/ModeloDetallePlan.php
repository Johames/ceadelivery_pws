<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDetallePlan extends Model
{

    protected $table          = 'cd_detalle_plan';
    protected $primaryKey     = 'cd_detalle_plan_id';
    protected $fillable       = ['cd_planes_id', 'nombre', 'descripcion', 'estado'];

}
