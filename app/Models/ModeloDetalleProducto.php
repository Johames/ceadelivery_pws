<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class ModeloDetalleProducto extends Model
{

    use Sluggable;

    protected $table          = 'cd_producto_detalle';
    protected $primaryKey     = 'producto_detalle_id';
    protected $fillable       = ['slug', 'producto_id', 'empresa_id', 'descripcion', 'marca_id', 'categoria_id', 'unidad_id', 'talla_id', 'color_id', 'precio', 'stock', 'estado_desc', 'tipo_desc', 'descuento', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloProducto(){
        return $this->belongsTo('App\Models\ModeloProducto', 'producto_id', 'producto_id');
   }

    public function ModeloMarca(){
         return $this->belongsTo('App\Models\ModeloMarca', 'marca_id', 'marca_id');
    }

    public function ModeloCategoria(){
         return $this->belongsTo('App\Models\ModeloCategoria', 'categoria_id', 'categoria_id');
    }

    public function ModeloUnidad(){
         return $this->belongsTo('App\Models\ModeloUnidad', 'unidad_id', 'unidad_id');
    }

    public function ModeloTalla(){
         return $this->belongsTo('App\Models\ModeloTalla', 'talla_id', 'talla_id');
    }

    public function ModeloColor(){
         return $this->belongsTo('App\Models\ModeloColor', 'color_id', 'color_id');
    }

    public function ModeloProductoImagenes(){
         return $this->hasMany('App\Models\ModeloProductoImagen', 'producto_detalle_id', 'producto_detalle_id');
    }

    public function ModeloValoracionProductos(){
         return $this->hasMany('App\Models\ModeloValoracionProducto', 'producto_detalle_id', 'producto_detalle_id');
    }

    public function ModeloDeseosProductos(){
         return $this->hasMany('App\Models\ModeloDeseosProducto', 'producto_detalle_id', 'producto_detalle_id');
    }

    public function ModeloDetallePedidos(){
         return $this->hasMany('App\Models\ModeloDetallePedido', 'producto_detalle_id', 'producto_detalle_id');
    }

    public function PrimeraImagen()
    {
        return $this->hasOne('App\Models\ModeloProductoImagen' ,'producto_detalle_id', 'producto_detalle_id')
        ->where('estado', 0)
        ->oldest();
    }

    public function sluggable(): array {
        return [
            'slug' => [
                'source' => ['ModeloProducto.nombre', 'ModeloCategoria.nombre'],
                'separator' => '_'
            ]
        ];
    }
}
