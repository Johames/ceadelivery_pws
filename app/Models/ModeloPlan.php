<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloPlan extends Model
{

    protected $table          = 'cd_planes';
    protected $primaryKey     = 'planes_id';
    protected $fillable       = ['nombre', 'descripcion', 'contenido', 'costo', 'estado'];

    public function ModeloEmpresas(){
        return $this->hasMany('App\Models\ModeloEmpresa', 'blog_id', 'blog_id');
    }

}
