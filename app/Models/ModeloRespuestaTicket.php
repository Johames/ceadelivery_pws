<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloRespuestaTicket extends Model
{

    protected $table          = 'cd_respuestas_ticket';
    protected $primaryKey     = 'respuestas_ticket_id';
    protected $fillable       = ['tickets_id', 'tipo', 'contenido'];

    public function ModeloTicket(){
        return $this->belongsTo('App\Models\ModeloTicket', 'tickets_id', 'tickets_id');
    }

}
