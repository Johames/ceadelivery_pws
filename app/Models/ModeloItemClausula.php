<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class ModeloItemClausula extends Model
{

    use Sluggable;

    protected $table            = 'cd_item_clausula';
    protected $primaryKey       = 'item_clausula_id';
    protected $fillable         = ['clausulas_id', 'slug', 'orden', 'titulo', 'subtitulo', 'contenido', 'img1', 'img2', 'img3', 'estado'];

    public function ModeloClausula(){
        return $this->belongsTo('App\Models\ModeloClausula', 'clausulas_id', 'clausulas_id');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['titulo'],
                'separator' => '_'
            ]
        ];
    }

}
