<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloSugerencias extends Model
{

    protected $table            = 'cd_sugerencias';
    protected $primaryKey       = 'sugerencias_id';
    protected $fillable         = ['area', 'tipo', 'descripcion', 'estado'];

}
