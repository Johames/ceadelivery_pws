<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDeliveryDocs extends Model
{
    protected $table          = 'cd_deseos';
    protected $primaryKey     = 'delivery_docs_id';
    protected $fillable       = ['deliverysta_id', 'dni', 'licencia', 'tarjeta_propiedad', 'soat', 'placa', 'estado'];

    public function ModeloDeliverista(){
        return $this->belongsTo('App\Models\ModeloDeliverista', 'deliverysta_id', 'deliverysta_id');
    }
}
