<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloValoracionEmpresa extends Model
{
    protected $table          = 'cd_valoracion_emp';
    protected $primaryKey     = 'valor_emp_id';
    protected $fillable       = ['empresa_id', 'persona_id', 'estrellas', 'resena', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloPersona(){
         return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }
}
