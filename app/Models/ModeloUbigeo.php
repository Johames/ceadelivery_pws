<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloUbigeo extends Model
{
    protected $table          = 'cd_ubigeo';
    protected $primaryKey     = 'ubigeo_id';
    protected $fillable       = ['nombre', 'tipo', 'ubigeo_id_sub'];

    public function ModeloEmpresa()
    {
        return $this->hasMany('App\Models\ModeloEmpresa', 'ubigeo_id', 'ubigeo_id');
    }

    public function ModeloEmpresas()
    {
        return $this->belongsToMany('App\Models\ModeloEmpresa', 'cd_empresa_cd_ubigeo', 'ubigeo_id', 'empresa_id');
    }

    public function ModeloPersona()
    {
        return $this->hasMany('App\Models\ModeloPersona', 'ubigeo_id', 'ubigeo_id');
    }

    public function childrenUbigeos()
    {
        return $this->hasMany('App\Models\ModeloUbigeo', 'ubigeo_id_sub', 'ubigeo_id');
    }

    public function parentUbigeos()
    {
        return $this->belongsTo('App\Models\ModeloUbigeo', 'ubigeo_id_sub', 'ubigeo_id');
    }

    public function allChildrenUbigeos()
    {
        return $this->childrenUbigeos()->with('allChildrenUbigeos');
    }

    public function allParentUbigeos()
    {
        return $this->parentUbigeos()->with('allParentUbigeos');
    }
}
