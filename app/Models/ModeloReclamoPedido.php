<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloReclamoPedido extends Model
{

    protected $table            = 'cd_reclamos_pedido';
    protected $primaryKey       = 'reclamos_pedido_id';
    protected $fillable         = ['pedido_id', 'persona_id', 'descripcion', 'calificacion', 'estado'];

    public function ModeloPedido(){
        return $this->belongsTo('App\Models\ModeloPedido', 'pedido_id', 'pedido_id');
    }

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

    public function ModeloRespuestaReclamos(){
        return $this->hasMany('App\Models\ModeloRespuestaReclamo', 'reclamos_pedido_id', 'reclamos_pedido_id');
    }

}
