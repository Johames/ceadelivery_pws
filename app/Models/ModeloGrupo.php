<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloGrupo extends Model
{

    protected $table            = 'cd_grupos';
    protected $primaryKey       = 'grupos_id';
    protected $fillable         = ['nombre', 'descripcion', 'estado'];

    public function ModeloRubros(){
         return $this->hasMany('App\Models\ModeloRubro', 'grupos_id', 'grupos_id');
    }

}
