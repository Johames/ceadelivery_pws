<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloPromocion extends Model
{
     protected $table          = 'cd_promociones';
     protected $primaryKey     = 'promociones_id';
     protected $fillable      = ['empresa_id', 'titulo', 'descripcion', 'extras', 'fecha_inicio', 'fecha_fin', 'codigo', 'descuento', 'texto', 'tipo', 'estado'];

     public function ModeloEmpresa(){
          return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
     }
}
