<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloCeaPuntosConfig extends Model
{
    protected $table          = 'cd_cea_puntos_config';
    protected $primaryKey     = 'cea_puntos_config_id';
    protected $fillable       = ['empresa_id', 'equiv_cant','equiv_monto','por_compra', 'por_resena_tienda', 'por_resena_producto', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }
}
