<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloReciente extends Model
{
     protected $table          = 'cd_reciente';
     protected $primaryKey     = 'reciente_id';
     protected $fillable       = ['persona_id', 'producto_detalle_id'];

     public function ModeloPersona(){
         return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
     }

     public function ModeloDetalleProducto(){
          return $this->belongsTo('App\Models\ModeloDetalleProducto', 'producto_detalle_id', 'producto_detalle_id');
     }
}
