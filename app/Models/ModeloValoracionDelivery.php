<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloValoracionDelivery extends Model
{

    protected $table          = 'cd_calificacion_delivery';
    protected $primaryKey     = 'calificacion_delivery_id';
    protected $fillable       = ['deliverista_id', 'persona_id', 'estrellas', 'resena', 'estado'];

    public function ModeloDeliverista(){
         return $this->belongsTo('App\Models\ModeloDeliverista', 'deliverista_id', 'deliverista_id');
    }

    public function ModeloPersona(){
         return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

}
