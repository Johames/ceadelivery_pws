<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloRubro extends Model
{
    protected $table          = 'cd_rubro';
    protected $primaryKey     = 'rubro_id';
    protected $fillable       = ['grupos_id', 'nombre', 'slug', 'img_rubro', 'estado'];

    public function ModeloEmpresa(){
         return $this->hasMany('App\Models\ModeloEmpresa', 'rubro_id', 'rubro_id');
    }

    public function ModeloGrupo(){
        return $this->belongsTo('App\Models\ModeloGrupo', 'grupos_id', 'grupos_id');
    }
}
