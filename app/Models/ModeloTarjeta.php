<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloTarjeta extends Model
{

    protected $table        = 'cd_tarjetas';
    protected $primaryKey   = 'tarjetas_id';
    protected $fillable     = ['persona_id', 'card_number', 'card_brand', 'card_type', 'card_id', 'estado'];

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

}
