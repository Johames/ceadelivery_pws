<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloFacturacion extends Model
{

    protected $table          = 'cd_facturacion';
    protected $primaryKey     = 'facturacion_id';
    protected $fillable       = ['persona_id', 'documento', 'cliente', 'direccion'];

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

}
