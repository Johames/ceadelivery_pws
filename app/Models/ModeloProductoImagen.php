<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloProductoImagen extends Model
{
    protected $table          = 'cd_producto_imagen';
    protected $primaryKey     = 'producto_imagen_id';
    protected $fillable       = ['producto_detalle_id', 'descripcion', 'ruta', 'estado'];

    public function ModeloDetalleProducto(){
         return $this->belongsTo('App\Models\ModeloDetalleProducto', 'producto_detalle_id', 'producto_detalle_id');
    }
}
