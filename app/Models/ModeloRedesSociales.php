<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloRedesSociales extends Model
{
    protected $table          = 'cd_redes_sociales';
    protected $primaryKey     = 'redes_sociales_id';
    protected $fillable       = ['empresa_id','nombre','url','icono','color', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id' ,'empresa_id');
    }
}
