<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloMetodoPago extends Model
{
    protected $table          = 'cd_metodo_pago';
    protected $primaryKey     = 'metodo_pago_id';
    protected $fillable       = ['nombre', 'estado'];

    public function ModeloDetalleMetodoPagos(){
         return $this->hasMany('App\Models\ModeloDetalleMetodoPago', 'metodo_pago_id', 'metodo_pago_id');
    }
}
