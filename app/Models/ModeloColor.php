<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloColor extends Model
{
    protected $table          = 'cd_color';
    protected $primaryKey     = 'color_id';
    protected $fillable       = ['nombre', 'estado'];

    public function ModeloDetalleProductos(){
         return $this->hasMany('App\Models\ModeloDetalleProducto', 'color_id', 'color_id');
    }
}
