<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// use Cviebrock\EloquentSluggable\Sluggable;

class ModeloCategoria extends Model
{
    // use Sluggable;

    protected $table         = 'cd_categoria';
    protected $primaryKey    = 'categoria_id';
    protected $fillable      = ['nombre','slug', 'estado'];

    public function ModeloDetalleProductos()
    {
        return $this->hasMany('App\Models\ModeloDetalleProducto', 'categoria_id', 'categoria_id');
    }

    // public function sluggable()
    // {
    //     return [
    //         'slug' => [
    //             'source' => 'nombre'
    //         ]
    //     ];
    // }
}
