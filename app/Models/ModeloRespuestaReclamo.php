<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloRespuestaReclamo extends Model
{

    protected $table            = 'cd_respuestas_reclamo';
    protected $primaryKey       = 'respuestas_reclamo_id';
    protected $fillable         = ['reclamos_pedido_id', 'tipo', 'contenido'];

    public function ModeloReclamoPedido(){
        return $this->belongsTo('App\Models\ModeloReclamoPedido', 'reclamos_pedido_id', 'reclamos_pedido_id');
    }

}
