<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloTerminoCondicion extends Model
{

    protected $table            = 'cd_terminos_condiciones';
    protected $primaryKey       = 'terminos_condiciones_id';
    protected $fillable         = ['plataforma', 'titulo', 'subtitulo', 'descripcion', 'estado'];

    public function ModeloClausulas(){
        return $this->hasMany('App\Models\ModeloClausula', 'terminos_condiciones_id', 'terminos_condiciones_id');
    }

}
