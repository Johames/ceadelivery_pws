<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloEmpresa extends Model
{

    protected $table          = 'cd_empresa';
    protected $primaryKey     = 'empresa_id';
    protected $fillable       = ['planes_id', 'rubro_id', 'slug', 'razon_social', 'nombre_comercial', 'representante_legal', 'ruc', 'descripcion', 'icono', 'color', 'direccion', 'coordenadas', 'referencia', 'cea_puntos', 'estado'];

    public function ModeloRubro(){
        return $this->belongsTo('App\Models\ModeloRubro', 'rubro_id', 'rubro_id');
    }

    public function ModeloDetalleMetodoPagos(){
        return $this->hasMany('App\Models\ModeloDetalleMetodoPago', 'empresa_id', 'empresa_id');
    }

    public function ModeloDetalleMetodoEnvios(){
        return $this->hasMany('App\Models\ModeloDetalleMetodoEnvio', 'empresa_id', 'empresa_id');
    }

    public function Users(){
        return $this->hasMany('App\User', 'empresa_id', 'empresa_id');
    }

    public function ModeloHorarios(){
        return $this->hasMany('App\Models\ModeloHorario', 'empresa_id', 'empresa_id');
    }

    public function ModeloPromocions(){
        return $this->hasMany('App\Models\ModeloPromocion', 'empresa_id', 'empresa_id');
    }

    public function ModeloRedesSociales(){
        return $this->hasMany('App\Models\ModeloRedesSociales', 'empresa_id', 'empresa_id');
    }

    public function ModeloContactos(){
        return $this->hasMany('App\Models\ModeloContacto', 'empresa_id', 'empresa_id');
    }

    public function ModeloModeloCeaPuntosConfigs(){
        return $this->hasMany('App\Models\ModeloCeaPuntosConfig', 'empresa_id', 'empresa_id');
    }

    public function ModeloValoracionEmpresas(){
        return $this->hasMany('App\Models\ModeloValoracionEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloPedidos(){
        return $this->hasMany('App\Models\ModeloPedido', 'empresa_id', 'empresa_id');
    }

    public function ModeloDetalleProductos(){
        return $this->hasMany('App\Models\ModeloDetalleProducto', 'empresa_id', 'empresa_id');
    }

    public function ModeloNotificacions(){
        return $this->hasMany('App\Models\ModeloNotificacion', 'empresa_id', 'empresa_id');
    }

    public function ModeloUbigeo()
    {
        return $this->belongsTo('App\Models\ModeloUbigeo', 'ubigeo_id', 'ubigeo_id');
    }

    public function ModeloUbigeos()
    {
        return $this->belongsToMany('App\Models\ModeloUbigeo', 'cd_empresa_cd_ubigeo', 'empresa_id', 'ubigeo_id');
    }

    public function ModeloCiudad()
    {
         return $this->belongsTo('App\Models\ModeloUbigeo', 'ubigeo_id', 'ubigeo_id');
    }

    public function ModeloPlan()
    {
        return $this->belongsTo('App\Models\ModeloPlan', 'planes_id', 'planes_id');
    }

    public function ModeloPublicidad()
    {
        return $this->hasMany('App\Models\ModeloPublicidad', 'empresa_id', 'empresa_id');
    }

    public function ModeloEstadoCuentas()
    {
        return $this->hasMany('App\Models\ModeloEstadoCuenta', 'empresa_id', 'empresa_id');
    }

}
