<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloPersona extends Model
{
    protected $table            = 'cd_persona';
    protected $primaryKey       = 'persona_id';
    protected $fillable         = ['ruc_dni', 'nombres', 'apellidos', 'razon_social', 'representante_legal', 'nombre_comercial', 'telefono', 'avatar', 'user_token_card', 'ubigeo_id', 'estado'];

    public function ModeloDeseos(){
        return $this->hasMany('App\Models\ModeloDeseo', 'persona_id', 'persona_id');
    }

    public function ModeloValoracionProductos(){
        return $this->hasMany('App\Models\ModeloValoracionProducto', 'persona_id', 'persona_id');
    }

    public function ModeloValoracionEmpresas(){
        return $this->hasMany('App\Models\ModeloValoracionEmpresa', 'persona_id', 'persona_id');
    }

    public function Users(){
        return $this->hasMany('App\User', 'persona_id', 'persona_id');
    }

    public function ModeloPedidos(){
        return $this->hasMany('App\Models\ModeloPedido', 'persona_id', 'persona_id');
    }

    public function ModeloNotificacions(){
        return $this->hasMany('App\Models\ModeloNotificacion', 'persona_id', 'persona_id');
    }

    public function ModeloRecientes(){
        return $this->hasMany('App\Models\ModeloReciente', 'persona_id', 'persona_id');
    }

    public function ModeloDireccions(){
        return $this->hasMany('App\Models\ModeloDireccion', 'persona_id', 'persona_id');
    }

    public function ModeloTarjetas(){
        return $this->hasMany('App\Models\ModeloTarjeta', 'persona_id', 'persona_id');
    }

    public function ModeloUbigeo()
    {
        return $this->belongsTo('App\Models\ModeloUbigeo', 'ubigeo_id', 'ubigeo_id');
    }

    public function ModeloReclamoPedidos(){
        return $this->hasMany('App\Models\ModeloReclamoPedido', 'persona_id', 'persona_id');
    }

}
