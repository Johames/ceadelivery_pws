<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloNotificacion extends Model
{
    protected $table          = 'cd_notificacion';
    protected $primaryKey     = 'notificacion_id';
    protected $fillable       = ['tipo_accion', 'persona_id', 'empresa_id', 'deliverista_id', 'titulo', 'descripcion', 'hora_entrega', 'id_accion', 'visto', 'estado'];

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

    public function ModeloEmpresa(){
        return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloDeliverista(){
        return $this->belongsTo('App\Models\ModeloDeliverista', 'deliverista_id', 'deliverista_id');
    }

}
