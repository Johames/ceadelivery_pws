<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDeliverista extends Model
{

    protected $table          = 'cd_deliverista';
    protected $primaryKey     = 'deliverista_id';
    protected $fillable       = ['dni', 'nombres', 'apellidos', 'nro_telefono', 'foto', 'ubigeo_id', 'estado'];

    public function Users() {
        return $this->hasMany('App\User', 'deliverista_id', 'deliverista_id');
    }

    public function ModeloPedidos() {
        return $this->hasMany('App\Models\ModeloPedido', 'deliverista_id', 'deliverista_id');
    }

    public function ModeloValoracionDelivery() {
        return $this->hasMany('App\Models\ModeloValoracionDelivery', 'deliverista_id', 'deliverista_id');
    }

    public function ModeloDeliveryDocs() {
        return $this->hasMany('App\Models\ModeloDeliveryDocs', 'deliverista_id', 'deliverista_id');
    }

    public function ModeloUbigeo() {
        return $this->belongsTo('App\Models\ModeloUbigeo', 'ubigeo_id', 'ubigeo_id');
    }

    public function ModeloEstadoCuentas(){
        return $this->hasMany('App\Models\ModeloEstadoCuenta', 'deliverista_id', 'deliverista_id');
    }

}
