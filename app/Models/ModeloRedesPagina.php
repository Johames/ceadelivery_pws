<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloRedesPagina extends Model
{

    protected $table          = 'cd_redes_pagina';
    protected $primaryKey     = 'redes_pagina_id';
    protected $fillable       = ['nombre', 'url', 'icono', 'color', 'estado'];

}
