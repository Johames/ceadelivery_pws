<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloContacto extends Model
{
    protected $table          = 'cd_contactos';
    protected $primaryKey     = 'contactos_id';
    protected $fillable       = ['empresa_id', 'tipo','nombre','area', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }
}
