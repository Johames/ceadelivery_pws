<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloPublicidad extends Model
{

    protected $table          = 'cd_publicidad';
    protected $primaryKey     = 'publicidad_id';
    protected $fillable       = ['empresa_id', 'fecha_inicio', 'fecha_fin', 'estado'];

    public function ModeloEmpresa(){
        return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloDetallePublicidades(){
        return $this->hasMany('App\Models\ModeloDetallePublicidad', 'publicidad_id', 'publicidad_id');
    }

}
