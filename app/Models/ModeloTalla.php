<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloTalla extends Model
{
    protected $table          = 'cd_talla';
    protected $primaryKey     = 'talla_id';
    protected $fillable       = ['nombre', 'estado'];

    public function ModeloDetalleProductos(){
         return $this->hasMany('App\Models\ModeloDetalleProducto', 'talla_id', 'talla_id');
    }
}
