<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloHorario extends Model
{
    protected $table          = 'cd_horario';
    protected $primaryKey     = 'horario_id';
    protected $fillable      = ['empresa_id', 'dia', 'turnos', 'hora_abre_am', 'hora_cierre_am', 'hora_abre_pm', 'hora_cierre_pm', 'descripcion', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }
}
