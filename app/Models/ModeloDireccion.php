<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDireccion extends Model
{
    protected $table          = 'cd_direciones';
    protected $primaryKey     = 'direciones_id';
    protected $fillable       = ['persona_id', 'direccion', 'coordenadas', 'referencia', 'principal', 'estado'];

    public function ModeloPersona(){
        return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }

    public function ModeloPedidos(){
        return $this->hasMany('App\Models\ModeloPedido', 'direciones_id', 'direciones_id');
    }
}
