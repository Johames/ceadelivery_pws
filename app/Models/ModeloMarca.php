<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloMarca extends Model
{
    protected $table          = 'cd_marca';
    protected $primaryKey     = 'marca_id';
    protected $fillable       = ['nombre', 'estado'];

    public function ModeloDetalleProductos(){
         return $this->hasMany('App\Models\ModeloDetalleProducto', 'marca_id', 'marca_id');
    }
}
