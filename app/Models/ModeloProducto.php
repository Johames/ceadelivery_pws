<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloProducto extends Model
{
    protected $table          = 'cd_producto';
    protected $primaryKey     = 'producto_id';
    protected $fillable       = ['nombre', 'estado'];

    public function ModeloDetalleProductos(){
         return $this->hasMany('App\Models\ModeloDetalleProducto', 'producto_id', 'producto_id');
    }

    public function scopeSearhProducto($query, $text)
    {
        return $query->where('nombre', 'LIKE',  '%'.$text.'%');
    }
}
