<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloComentarioBlog extends Model
{

    protected $table          = 'cd_comentario_blog';
    protected $primaryKey     = 'comentario_blog_id';
    protected $fillable       = ['blog_id', 'users_id', 'contenido', 'img', 'estado'];

    public function ModeloBlog(){
        return $this->belongsTo('App\Models\ModeloBlog', 'blog_id', 'blog_id');
    }

    public function User(){
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

}
