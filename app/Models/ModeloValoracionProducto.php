<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloValoracionProducto extends Model
{
    protected $table          = 'cd_valoracion_prod';
    protected $primaryKey     = 'valor_prod_id';
    protected $fillable       = ['producto_detalle_id', 'persona_id', 'estrellas', 'resena', 'estado'];

    public function ModeloDetalleProducto(){
         return $this->belongsTo('App\Models\ModeloDetalleProducto', 'producto_detalle_id', 'producto_detalle_id');
    }

    public function ModeloPersona(){
         return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }
}
