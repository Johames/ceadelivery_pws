<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDeseo extends Model
{
    protected $table          = 'cd_deseos';
    protected $primaryKey     = 'deseos_id';
    protected $fillable       = ['persona_id', 'nombre', 'descripcion', 'estado'];

    public function ModeloDeseosProductos(){
         return $this->hasMany('App\Models\ModeloDeseosProducto', 'deseos_id', 'deseos_id');
    }

    public function ModeloPersona(){
         return $this->belongsTo('App\Models\ModeloPersona', 'persona_id', 'persona_id');
    }
}
