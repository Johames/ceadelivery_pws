<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDetallePedido extends Model
{
    protected $table          = 'cd_detalle_pedido';
    protected $primaryKey     = 'detalle_id';
    protected $fillable       = ['pedido_id', 'producto_detalle_id', 'nombre_producto', 'cantidad', 'precio', 'precio_referencial', 'igv', 'total', 'estado'];

    public function ModeloPedido(){
         return $this->belongsTo('App\Models\ModeloPedido', 'pedido_id', 'pedido_id');
    }

    public function ModeloDetalleProducto(){
         return $this->belongsTo('App\Models\ModeloDetalleProducto', 'producto_detalle_id', 'producto_detalle_id');
    }
}
