<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDetalleMetodoEnvio extends Model
{
    protected $table          = 'cd_metodo_envio_emp';
    protected $primaryKey     = 'metodo_envio_emp_id';
    protected $fillable       = ['metodo_envio_id', 'empresa_id', 'descripcion', 'precio', 'llegada', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloMetodoEnvio(){
         return $this->belongsTo('App\Models\ModeloMetodoEnvio', 'metodo_envio_id', 'metodo_envio_id');
    }

    public function ModeloPedidos(){
         return $this->hasMany('App\Models\ModeloPedido', 'metodo_envio_emp_id', 'metodo_envio_emp_id');
    }
}
