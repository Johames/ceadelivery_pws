<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloTicket extends Model
{

    protected $table          = 'cd_tickets';
    protected $primaryKey     = 'tickets_id';
    protected $fillable       = ['users_id', 'asunto', 'pregunta', 'nro_ticket', 'estado'];

    public function User(){
        return $this->belongsTo('App\User', 'users_id', 'id');
    }

    public function ModeloRespuestasTickets(){
        return $this->hasMany('App\Models\ModeloRespuestaTicket', 'tickets_id', 'tickets_id');
    }

}
