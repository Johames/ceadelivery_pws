<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDetallePublicidad extends Model
{

    protected $table          = 'cd_detalle_publicidad';
    protected $primaryKey     = 'detalle_publicidad_id';
    protected $fillable       = ['publicidad_id', 'alt', 'banner', 'ubicacion', 'estado'];

    public function ModeloPublicidad(){
        return $this->belongsTo('App\Models\ModeloPublicidad', 'publicidad_id', 'publicidad_id');
    }

}
