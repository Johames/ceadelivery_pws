<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class ModeloClausula extends Model
{
    use Sluggable;

    protected $table            = 'cd_clausulas';
    protected $primaryKey       = 'clausulas_id';
    protected $fillable         = ['terminos_condiciones_id', 'slug', 'orden', 'titulo', 'subtitulo', 'contenido', 'estado'];

    public function ModeloTerminoCondicion(){
        return $this->belongsTo('App\Models\ModeloTerminoCondicion', 'terminos_condiciones_id', 'terminos_condiciones_id');
    }

    public function ModeloItemClausulas(){
        return $this->hasMany('App\Models\ModeloItemClausula', 'clausulas_id', 'clausulas_id');
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['titulo'],
                'separator' => '_'
            ]
        ];
    }

}
