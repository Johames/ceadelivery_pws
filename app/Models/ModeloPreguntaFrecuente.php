<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class ModeloPreguntaFrecuente extends Model
{

    use Sluggable;

    protected $table            = 'cd_preguntas_frecuentes';
    protected $primaryKey       = 'preguntas_frecuentes_id';
    protected $fillable         = ['slug', 'pregunta', 'respuesta', 'mostrar', 'grupo', 'estado'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['pregunta'],
                'separator' => '_'
            ]
        ];
    }

}
