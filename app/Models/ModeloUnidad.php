<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloUnidad extends Model
{
    protected $table          = 'cd_unidad';
    protected $primaryKey     = 'unidad_id';
    protected $fillable       = ['nombre', 'abreviatura', 'estado'];

    public function ModeloDetalleProductos(){
         return $this->hasMany('App\Models\ModeloDetalleProducto', 'unidad_id', 'unidad_id');
    }
}
