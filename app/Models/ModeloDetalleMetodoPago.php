<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDetalleMetodoPago extends Model
{
    protected $table          = 'cd_metodo_pago_emp';
    protected $primaryKey     = 'metodo_pago_emp_id';
    protected $fillable       = ['empresa_id', 'metodo_pago_id', 'descripcion', 'estado'];

    public function ModeloEmpresa(){
         return $this->belongsTo('App\Models\ModeloEmpresa', 'empresa_id', 'empresa_id');
    }

    public function ModeloMetodoPago(){
         return $this->belongsTo('App\Models\ModeloMetodoPago', 'metodo_pago_id', 'metodo_pago_id');
    }

    public function ModeloPedidos(){
         return $this->hasMany('App\Models\ModeloPedido', 'metodo_pago_emp_id', 'metodo_pago_emp_id');
    }
}
