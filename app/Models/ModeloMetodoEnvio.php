<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloMetodoEnvio extends Model
{
    protected $table          = 'cd_metodo_envio';
    protected $primaryKey     = 'metodo_envio_id';
    protected $fillable       = ['nombre', 'estado'];

    public function ModeloDetalleMetodoEnvios(){
         return $this->hasMany('App\Models\ModeloDetalleMetodoEnvio', 'metodo_envio_id', 'metodo_envio_id');
    }
}
