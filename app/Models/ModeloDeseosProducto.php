<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModeloDeseosProducto extends Model
{
    protected $table          = 'cd_prod_deseo';
    protected $primaryKey     = 'prod_deseo_id';
    protected $fillable       = ['producto_detalle_id', 'deseos_id', 'estado'];

    public function ModeloDetalleProducto(){
         return $this->belongsTo('App\Models\ModeloDetalleProducto', 'producto_detalle_id', 'producto_detalle_id');
    }

    public function ModeloDeseo(){
         return $this->belongsTo('App\Models\ModeloDeseo', 'deseos_id', 'deseos_id');
    }
}
